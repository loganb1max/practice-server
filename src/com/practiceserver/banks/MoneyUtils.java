package com.practiceserver.banks;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.practiceserver.mobs.ItemMechanics;
import com.practiceserver.player.PracticeServerPlayer;
import com.practiceserver.utils.Utils;

public class MoneyUtils {
	
	public ItemStack createBankNote(int amount) {
		return ItemMechanics.signNewCustomItem(Material.PAPER, (short) 0, Utils.colorCodes("&aBank Note"), Utils.colorCodes("&f&lValue: &f" + amount + " Gems,&7Exchange at any bank for GEM(s)"));
	}
	
	public void withdrawBankNote(Player player, int amount) {
		PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(player);
		PlayerBank bank = pl.getPlayerBank();
		if (amount > bank.getGems()) {
			pl.msg("&7You cannot withdraw more GEMS than you have stored.");
			return;
		} else if (amount <= 0) {
			pl.msg("&cYou must enter a POSIVIVE amount.");
			return;
		} else {
			if (player.getInventory().firstEmpty() == -1) {
				pl.msg("&cNo space available in inventory. Type 'cancel' or clear some room.");
				return;
			}
			bank.withdraw(amount);
			player.getInventory().setItem(player.getInventory().firstEmpty(), createBankNote(amount));
			
			
		}
	}

}

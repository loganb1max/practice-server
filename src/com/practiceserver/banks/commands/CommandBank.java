package com.practiceserver.banks.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.practiceserver.banks.BankHandler;
import com.practiceserver.enums.Alignment;
import com.practiceserver.player.PracticeServerPlayer;

public class CommandBank implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
			if (p.isOp()) {//TODO have it check if the player is sub
				if (pl.isCombat()) {
					pl.msg("&cYou cannot open you're bank while in combat.");
					return true;
				}
				else if (pl.getAlignment() == Alignment.CHAOTIC) {
					pl.msg("&cYou cannot open you're bank while chaotic");
					return true;
				}
				else if (pl.getAlignment() == Alignment.NEUTRAL) {
					pl.msg("&cYou cannot open you're bank while neutral");
					return true;
				} else {
					if ((BankHandler.summon_bank.containsKey(p.getName()) || (BankHandler.banking_location.containsKey(p.getName())))) {
						return true;
					} else {
						BankHandler.summon_bank.put(p.getName(), 6);
						BankHandler.banking_location.put(p.getName(), p.getLocation());
						return true;
					}
				}
				
			}
		}
		return true;
	}
}
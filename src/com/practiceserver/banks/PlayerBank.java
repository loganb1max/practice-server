package com.practiceserver.banks;

import org.bukkit.entity.Player;

import com.practiceserver.database.DataHandler;
import com.practiceserver.player.PracticeServerPlayer;

public class PlayerBank {
	
	private Player player;
	private int gems;
	private PracticeServerPlayer practiceServerPlayer;
	private String inv_string;
	private String inv_2;
	
	public PlayerBank(Player player) {
		this.player = player;
		this.practiceServerPlayer = PracticeServerPlayer.getPracticeServerPlayer(player);
		this.gems = practiceServerPlayer.getGems();
		this.inv_string = DataHandler.getInstance().getBankInventory(player);
		this.inv_2 = DataHandler.getInstance().getBankInventory1(player);
	}
	
	public static PlayerBank getPlayerBank(Player player) {
		return new PlayerBank(player);
	}

	public Player getPlayer() {
		return player;
	}
	
	public int getGems() {
		return gems;
	}
	
	public PracticeServerPlayer getPracticeServerPlayer() {
		return practiceServerPlayer;
	}
	
	public void setGems(int gems) {
		DataHandler.getInstance().setGems(player, gems);
	}
	
	public void pay(int amount) {
		setGems((getGems() + amount));
	}
	
	public void withdraw(int amount) {
		if (amount >= getGems()) {
			setGems(0);
		} else {
			setGems((getGems() - amount));
		}
	}

	public String getInventory() {
		return inv_string;
	}

	public String getInventory2() {
		return inv_2;
	}
}

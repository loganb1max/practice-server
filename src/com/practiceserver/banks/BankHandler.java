package com.practiceserver.banks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerAnimationEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.practiceserver.Main;
import com.practiceserver.database.DataHandler;
import com.practiceserver.events.PlayerChatEvent;
import com.practiceserver.mobs.ItemMechanics;
import com.practiceserver.player.InventoryHandler;
import com.practiceserver.player.PracticeServerPlayer;

public class BankHandler implements Listener {
	
	public static List<Player> withdrawingBankNote = new ArrayList<Player>();
	public static List<Player> withdrawingRawGems = new ArrayList<Player>();
	public static List<Player> upgradingBank = new ArrayList<Player>();
	public static HashMap<Player, String> bank_upgrade_codes = new HashMap<Player, String>();
	private static final String ALPHA_NUM = "123456789";
	
	public void saveBank(Player player, Inventory inv) {
		if (inv.getTitle().startsWith("Bank Chest")) {
			if (inv.getTitle().equalsIgnoreCase("Bank Chest (2/2)")) {
				DataHandler.getInstance().setBankInventory2(player, InventoryHandler.convertInventoryToString(player.getName(), inv, false));
				return;
			}
			DataHandler.getInstance().setBankInventory(player, InventoryHandler.convertInventoryToString(player.getName(), inv, false));
			return;
		}
	}
	
	public static HashMap<String, Location> banking_location = new HashMap<String, Location>();
    public static ConcurrentHashMap<String, Integer> summon_bank = new ConcurrentHashMap<String, Integer>();
    
    @EventHandler
    public void onPlayerMoveEvent(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
        if (banking_location.containsKey(p.getName())) {
            Location loc = banking_location.get(p.getName());
            if (!p.getWorld().getName().equalsIgnoreCase(loc.getWorld().getName()) || e.getTo().distanceSquared(loc) >= 2) {
            	banking_location.remove(p.getName());
                summon_bank.remove(p.getName());
                pl.msg("&cBank Opening - " + ChatColor.BOLD + "CANCELLED");
                return;
            }
        }
    }
	
    @EventHandler
    public void onDamage(EntityDamageEvent e) {
    	if (!( e.getEntity() instanceof Player)) return;
    	 Player p = (Player) e.getEntity();
         PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
         if (summon_bank.containsKey(p.getName())) {
         	banking_location.remove(p.getName());
         	summon_bank.remove(p.getName());
         	 pl.msg("&cBank Opening - " + ChatColor.BOLD + "CANCELLED");
             return;
         }
    
    }
    
    @EventHandler
    public void onPlayerAnimation(PlayerAnimationEvent e) {
        Player p = e.getPlayer();
        PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
        if (summon_bank.containsKey(p.getName())) {
        	banking_location.remove(p.getName());
        	summon_bank.remove(p.getName());
        	 pl.msg("&cBank Opening - " + ChatColor.BOLD + "CANCELLED");
            e.setCancelled(true);
            return;
        }
    }
    
    
	public static void bankTask() {
		Main.plugin.getServer().getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
			public void run() {
				   for (Entry<String, Integer> data : summon_bank.entrySet()) {
	                    final String p_name = data.getKey();
	                    int seconds = data.getValue();

	                    if (Bukkit.getPlayer(p_name) == null) {
	                        summon_bank.remove(p_name);
	                        banking_location.remove(p_name);
	                        continue;
	                    }

	                    final Player pl = Bukkit.getPlayer(p_name);
	                    final PracticeServerPlayer p = PracticeServerPlayer.getPracticeServerPlayer(pl);
	                    seconds--;

	                    if (seconds <= 0) {
	                    	if ((withdrawingBankNote.contains(p)) || (withdrawingRawGems.contains(p)) || (upgradingBank.contains(p))) {
	            				p.msg("&cYou cannot perform that action while you have a pending withdrawl request. Type 'cancel' and try again.");
	            				return;
	            			}
	            			p.playSound(Sound.BLOCK_CHEST_OPEN, 1.0F, 1.0F);
	            			pl.openInventory(new BankHandler().getBankInventory(pl, 1));
	            			 summon_bank.remove(p_name);
		                       banking_location.remove(p_name);
	                       return;
	                    }
	                    p.msg("&cOpening Bank ... " + seconds + "&ls");



	                    summon_bank.put(p_name, seconds);
	                }
	            }
	        }, 5 * 20L, 20L);
	}
	
	@EventHandler
	public void onBankOpen(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
		if ((e.getAction() == Action.RIGHT_CLICK_BLOCK) && (e.getClickedBlock().getType() == Material.ENDER_CHEST)) {
			e.setCancelled(true);
			if ((withdrawingBankNote.contains(p)) || (withdrawingRawGems.contains(p)) || (upgradingBank.contains(p))) {
				pl.msg("&cYou cannot perform that action while you have a pending withdrawl request. Type 'cancel' and try again.");
				return;
			}
			pl.playSound(Sound.BLOCK_CHEST_OPEN, 1.0F, 1.0F);
			p.openInventory(getBankInventory(p, 1));
			return;
		}
	}
	
	public ItemStack getGem() {
		ItemStack i = new ItemStack(Material.EMERALD);
		List<String> new_lore = new ArrayList<String>(Arrays.asList(ChatColor.GRAY.toString() + "The currency of Andalucia"));

		ItemMeta im = i.getItemMeta();
		im.setLore(new_lore);

		im.setDisplayName(ChatColor.WHITE.toString() + "Gem");
		i.setItemMeta(im);
		return i;
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		withdrawingBankNote.remove(p);
		withdrawingRawGems.remove(p);
		upgradingBank.remove(p);
		bank_upgrade_codes.remove(p);
		summon_bank.remove(p.getName());
		banking_location.remove(p.getName());
	}
	
	@EventHandler
	public void onInvClose(InventoryCloseEvent e) {
		saveBank((Player) e.getPlayer(), e.getInventory());
	}
	
	public boolean isGemNote(ItemStack is) {
		return ((is.getItemMeta().getDisplayName().contains("Bank Note")) && (is.getType().equals(Material.PAPER)));
	}
	
	@EventHandler
	public void onBankClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
		if (e.getSlotType().equals(SlotType.OUTSIDE)) {
			return;
		}
		if (e.getClickedInventory().getTitle().contains("Bank Chest")) {
			if ((e.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY) && (e.getInventory().contains(getGem()))) {
				for (int i = 0; i < e.getInventory().getSize(); i++) {
					if ((e.getInventory().getItem(i) != null) && (isGem(e.getInventory().getItem(i)))) {
						e.setCancelled(true);
						int amt = e.getInventory().getItem(i).getAmount();
						pl.getPlayerBank().pay(amt);
						e.getInventory().setItem(getGemSlot(DataHandler.getInstance().getBankUpgradeLevel(p), Integer.parseInt(e.getInventory().getTitle().substring(e.getInventory().getTitle().lastIndexOf("(") + 1, e.getInventory().getTitle().lastIndexOf("/")))), getGemItem(p));
						e.getInventory().removeItem(e.getInventory().getItem(i));
						pl.playSound(Sound.ENTITY_EXPERIENCE_ORB_TOUCH, 1.0F, 1.0F);
						pl.msg("&a&l+&a" + amt + "&lG&a, &lNew Balance: &a" + (pl.getGems()) + " GEM(s)");
					}
				}
			}
			if ((e.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY) && ((isGemNote(e.getCursor()) || (isGemNote(e.getCurrentItem()))))) {
				e.setCancelled(true);
				int amt = 0;
				if (isGemNote(e.getCurrentItem())) {
					amt = getBankNoteValue(e.getCurrentItem());
					e.setCurrentItem(new ItemStack(Material.AIR));
				}
				if (isGemNote(e.getCursor())) {
					amt = getBankNoteValue(e.getCursor());
					e.setCursor(new ItemStack(Material.AIR));
				}
				if (amt > 0) {
					pl.playSound(Sound.ENTITY_EXPERIENCE_ORB_TOUCH, 1.0F, 1.0F);
					pl.msg("&a&l+&a" + amt + "&lG&a, &lNew Balance: &a" + (pl.getGems() + amt) + " GEM(s)");
					pl.getPlayerBank().pay(amt);
					return;
				}
			}
			if ((isNextArrow(e.getCurrentItem()) && (e.getCursor().getType() == Material.AIR))) {
				saveBank(p, e.getClickedInventory());
				e.setCancelled(true);
				String title = e.getClickedInventory().getTitle();
				int current_page = Integer.parseInt(title.substring(title.lastIndexOf("(") + 1, title.lastIndexOf("/")));
				p.closeInventory();
				pl.playSound(Sound.ENTITY_BAT_TAKEOFF, 1F, 1.2F);
				p.openInventory(getBankInventory(p, (current_page + 1)));
				return;
			}
			if ((isPreviousArrow(e.getCurrentItem()) && (e.getCursor().getType() == Material.AIR))) {
				saveBank(p, e.getClickedInventory());
				e.setCancelled(true);
				String title = e.getClickedInventory().getTitle();
				int current_page = Integer.parseInt(title.substring(title.lastIndexOf("(") + 1, title.lastIndexOf("/")));
				p.closeInventory();
				pl.playSound(Sound.ENTITY_BAT_TAKEOFF, 1F, 1.2F);
				p.openInventory(getBankInventory(p, (current_page - 1)));
				return;
			}
			if ((e.getClick() == ClickType.MIDDLE) && (e.getCursor().getType() == Material.AIR) && (isWithdrawGem(p, e.getCurrentItem()))) {
				if ((DataHandler.getInstance().getBankUpgradeLevel(p) + 1) >= 18) {
					pl.msg("&cYour bank is already at it's maximum size. (162 slots)");
				}
				if (upgradingBank.contains(p)) return;
				generateUpgradeAuthenticationCode(p, String.valueOf((DataHandler.getInstance().getBankUpgradeLevel(p) + 1)));
				upgradingBank.add(p);
				e.setCancelled(true);
				p.updateInventory();
				p.closeInventory();
				pl.playSound(Sound.BLOCK_CHEST_CLOSE, 1.0F, 1.0F);
				pl.msg("");
				pl.msg("&8           *** &a&lBank Upgrade Confirmation &8 ***");
				pl.msg("&8           CURRENT Slots: &a" + (DataHandler.getInstance().getBankUpgradeLevel(p) * 9) + "&8          NEW Slots: &a" + ((DataHandler.getInstance().getBankUpgradeLevel(p) + 1) * 9));
				pl.msg("&8                  Upgrade Cost: &a" + getCost(p) + " Gem(s)");
				pl.msg("");
				pl.msg("&aEnter the code '&l" + getUpgradeAuthenticationCode(p) + "&a' to confirm your upgrade.");
				pl.msg("");
				pl.msg("&c&lWARNING: &cBank upgrades are &l&cNOT &creversible or refundable. Type 'cancel' to void this upgrade request.");
				pl.msg("");
				return;
			}
			else if ((e.isRightClick()) && (e.getCursor().getType() == Material.AIR) && (isWithdrawGem(p, e.getCurrentItem()))) {
				e.setCancelled(true);
				withdrawingBankNote.add(p);
				p.updateInventory();
				p.closeInventory();
				pl.playSound(Sound.BLOCK_CHEST_CLOSE, 1.0F, 1.0F);
				pl.msg("&a&lCurrent Balance: &a" + pl.getGems() + " GEM(s)");
				pl.msg("&7Banker: &fHow much would you like to CONVERT today, " + p.getName() + "?");
				pl.msg("&7Please enter the amount you'd like To CONVERT into a gem note. Alternatively, type  &c'cancel' &7to void this operation.");
				return;
			}
			else if ((e.isLeftClick()) && (e.getCursor().getType() == Material.AIR) && (isWithdrawGem(p, e.getCurrentItem()))) {
				e.setCancelled(true);
				withdrawingRawGems.add(p);
				p.updateInventory();
				p.closeInventory();
				pl.playSound(Sound.BLOCK_CHEST_CLOSE, 1.0F, 1.0F);
				pl.msg("&a&lCurrent Balance: &a" + pl.getGems() + " GEM(s)");
				pl.msg("&7Banker: &fHow much would you like to WITHDRAW today, " + p.getName() + "?");
				pl.msg("&7Please enter the amount you'd like To WITHDRAW. Alternatively, type &c'cancel' &7to void this operation.");
				return;
			}
			else if (e.getCurrentItem().getType() == Material.AIR) {
				return;
			} else {
				if ((e.getCurrentItem().equals(ItemMechanics.glass)) || (e.getCursor().equals(ItemMechanics.glass))) {
					e.setCancelled(true);
					return;
				}
				if ((isUntradeable(e.getCurrentItem()) || (isUntradeable(e.getCursor())))) {
					e.setCancelled(true);
					pl.msg("&cYou &ncannot&c bank this item, as it is part of your spawn kit.");
					return;
				}
			}
		}
	}
	
	public boolean isGem(ItemStack is) {
		return ((is.getItemMeta().getDisplayName().contains("GEM")) && (is.getType() == Material.EMERALD));
	}

	public boolean giveGems(Player p, int Gems_worth) {
		if(Gems_worth > 64) {
			int space_needed = Math.round(Gems_worth / 64) + 1;
			int count = 0;
			ItemStack[] contents = p.getInventory().getContents();
			for(int z = 0; z < contents.length; z++) {
				if(contents[z] == null || contents[z].getType() == Material.AIR) {
					count++;
				}
			}
			int empty_slots = count;

			if(space_needed > empty_slots) {
				p.sendMessage(ChatColor.RED + "You do not have enough space in your inventory to withdraw " + Gems_worth + " GEM(s).");
				p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "REQ: " + space_needed + " slots");
				return false;
			}
		}

		while(Gems_worth > 0) {
			while(Gems_worth > 64) {
				if(p.getInventory().firstEmpty() == -1) {
					p.getWorld().dropItemNaturally(p.getLocation(), makeGems(Gems_worth));
					p.sendMessage(ChatColor.RED + "" + ChatColor.UNDERLINE + "WARNING: " + ChatColor.RED + "Not all GEMS fit in your inventory, the remainder have been dropped on the ground nearby.");
					Gems_worth = 0;
				}
				p.getInventory().setItem(p.getInventory().firstEmpty(), makeGems(64));
				Gems_worth -= 64;
			}

			if(p.getInventory().firstEmpty() == -1) {
				p.getWorld().dropItemNaturally(p.getLocation(), makeGems(Gems_worth));
				p.sendMessage(ChatColor.RED + "" + ChatColor.UNDERLINE + "WARNING: " + ChatColor.RED + "Not all GEMS fit in your inventory, the remainder have been dropped on the ground nearby.");
				Gems_worth = 0;
			} else if(p.getInventory().firstEmpty() != -1) {
				p.getInventory().setItem(p.getInventory().firstEmpty(), makeGems(Gems_worth));
				// p.getInventory().addItem(new ItemStack(Material.EMERALD, Gems_worth));
				Gems_worth = 0;
			}
		}
		if(Gems_worth > 0) {
			p.getWorld().dropItemNaturally(p.getLocation(), makeGems(Gems_worth));
			p.sendMessage(ChatColor.RED + "" + ChatColor.UNDERLINE + "WARNING: " + ChatColor.RED + "Not all GEMS fit in your inventory, the remainder have been dropped on the ground nearby.");
			Gems_worth = 0;
		}
		p.updateInventory();
		return true;
	}
	
	//TODO add custom chat event
	@EventHandler
	public void onChat(PlayerChatEvent e) {
		Player p = e.getPlayer();
		PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
		if (withdrawingRawGems.contains(p)) {
			e.setCancelled(true);
			int amt = 0;
			if (e.getMessage().equalsIgnoreCase("cancel")) {
				pl.msg("&cWithdrawl operation - &lCANCELLED");
				withdrawingRawGems.remove(p);
				return;
			}
			try {
				amt = Integer.parseInt(e.getMessage().trim());
			} catch (NumberFormatException ee) {
				pl.msg("&cPlease enter a NUMBER, the amount you'd like To WITHDRAW from your bank account. Or type 'cancel' to void the withdrawl.");
				return;
			}
			if (amt > pl.getGems()) {
				pl.msg("&7Banker: &fI'm sorry sir, but you only have " + pl.getGems() + " GEM(s) stored in our bank.");
				pl.msg("&7You cannot withdraw more GEMS than you have stored.");
				withdrawingRawGems.remove(p);
				return;
			}
			if (amt <= 0) {
				pl.msg("&cYou must enter a POSITIVE amount.");
				return;
			}
			if (p.getInventory().firstEmpty() == -1) {
				pl.msg("&cNo space available in inventory. Type 'cancel' or clear some room.");
				return;
			} else {
				p.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "New Balance: " + ChatColor.GREEN + (pl.getGems() - amt) + " GEM(s)");
				p.sendMessage(ChatColor.GRAY + "You have withdrawn " + amt + " GEM(s) from your bank account.");
				p.sendMessage(ChatColor.GRAY + "Banker: " + ChatColor.WHITE + "Here are your Gems, thank you for your business!");
				p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_TOUCH, 1F, 1F);
				pl.getPlayerBank().withdraw(amt);
				giveGems(p, amt);
				withdrawingRawGems.remove(p);
				return;
			}
		}
		if (withdrawingBankNote.contains(p)) {
			e.setCancelled(true);
			int amt = 0;
			if (e.getMessage().equalsIgnoreCase("cancel")) {
				pl.msg("&cWithdrawl operation - &lCANCELLED");
				withdrawingBankNote.remove(p);
				return;
			}
			try {
				amt = Integer.parseInt(e.getMessage().trim());
			} catch (NumberFormatException ee) {
				pl.msg("&cPlease enter a NUMBER, the amount you'd like To WITHDRAW from your bank account. Or type 'cancel' to void the withdrawl.");
				return;
			}
			if (amt > pl.getGems()) {
				pl.msg("&7Banker: &fI'm sorry sir, but you only have " + pl.getGems() + " GEM(s) stored in our bank.");
				pl.msg("&7You cannot withdraw more GEMS than you have stored.");
				withdrawingBankNote.remove(p);
				return;
			}
			if (amt <= 0) {
				pl.msg("&cYou must enter a POSITIVE amount.");
				return;
			}
			if (p.getInventory().firstEmpty() == -1) {
				pl.msg("&cNo space available in inventory. Type 'cancel' or clear some room.");
				return;
			} else {
				pl.msg("&a&lNew Balance: &a" + (pl.getGems() - amt) + " GEM(s)");
				pl.msg("&7Banker: &fHere are your Gems, thank you for your business!");
				pl.getPlayerBank().withdraw(amt);
				p.getInventory().setItem(p.getInventory().firstEmpty(), new MoneyUtils().createBankNote(amt));
				pl.playSound(Sound.ENTITY_EXPERIENCE_ORB_TOUCH, 1.0F, 1.0F);
				withdrawingBankNote.remove(p);
				return;
			}
		}
		if (upgradingBank.contains(p)) {
			e.setCancelled(true);
			if (bank_upgrade_codes.containsKey(p)) {
				e.setCancelled(true);
				if (e.getMessage().contains(getUpgradeAuthenticationCode(p))) {
					if (!doTheyHaveEnoughMoney(p, getCost(p))) {
						pl.msg("&cYou do not have enough gems to purchase this upgrade. Upgrade cancelled.");
						pl.msg("&c&lCOST: &c" + getCost(p) + "&lG");
						upgradingBank.remove(p);
						bank_upgrade_codes.remove(p);
						return;
					}
					subtractMoney(p, getCost(p));
					upgradeBank(p);
					pl.msg("");
					pl.msg("&a&l*** BANK UPGRADE TO LEVEL " + DataHandler.getInstance().getBankUpgradeLevel(p) + " COMPLETE ***");
					pl.msg("&7You now have " + getSlotsBasedOnUpgradeLevel(DataHandler.getInstance().getBankUpgradeLevel(p), -1) + " bank slots available.");
					pl.playSound(Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 1.0F);
					bank_upgrade_codes.remove(p);
					upgradingBank.remove(p);
					return;
				} else {
					pl.msg("&cInvalid authentication code entered. Bank upgrade cancelled.");
					bank_upgrade_codes.remove(p);
					upgradingBank.remove(p);
					return;
				}
			} 
		}
	}
	
	public void upgradeBank(Player p) {
		Inventory bank = getBankInventory(p, 1);
		if (DataHandler.getInstance().getBankUpgradeLevel(p) == 7) {
			DataHandler.getInstance().setBankPages(p, 2);
		}
		DataHandler.getInstance().setUpgradeLevel(p, (DataHandler.getInstance().getBankUpgradeLevel(p) + 1));
		bank.remove(Material.EMERALD);
		DataHandler.getInstance().setBankInventory(p, InventoryHandler.convertInventoryToString(p.getName(), bank, false));
		return;
		
	}

	public static boolean doTheyHaveEnoughMoney(Player p, int amount) {
		Inventory i = p.getInventory();
		int paid_off = 0;

		HashMap<Integer, ? extends ItemStack> invItems = i.all(Material.EMERALD);
		for (Entry<Integer, ? extends ItemStack> entry : invItems.entrySet()) {
			ItemStack item = entry.getValue();
			int stackAmount = item.getAmount();

			if ((paid_off + stackAmount) <= amount) {
				paid_off += stackAmount;
			}

			else {
				int to_take = amount - paid_off;
				paid_off += to_take;
			}

			if (paid_off >= amount) {
				return true; // WE CAN AFFORD IT! WOO!
			}

		}

		HashMap<Integer, ? extends ItemStack> gem_pouches = i.all(Material.INK_SACK);
		for (Entry<Integer, ? extends ItemStack> entry : gem_pouches.entrySet()) {
			ItemStack item = entry.getValue();



			int worth = 0;//GemSacks.getGemPouchWorth(item);

			if ((paid_off + worth) <= amount) {
				paid_off += worth;
			}

			else {
				int to_take = amount - paid_off;
				paid_off += to_take;
			}

			if (paid_off >= amount) {
				return true; // WE CAN AFFORD IT! WOO!
			}

		}

		// If code reaches this point, it's still not all paid off.

		HashMap<Integer, ? extends ItemStack> bank_notes = i.all(Material.PAPER);
		for (Entry<Integer, ? extends ItemStack> entry : bank_notes.entrySet()) {
			ItemStack item = entry.getValue();
			int bank_note_val = getBankNoteValue(item);

			if ((paid_off + bank_note_val) <= amount) {
				paid_off += bank_note_val;
			}

			else {
				int to_take = amount - paid_off;
				paid_off += to_take;
			}

			if (paid_off >= amount) {
				return true; // They can pay it! WOO!
			}

		}

		return false;
	}
	
	public static void subtractMoney(Player p, int amount) {
		Inventory i = p.getInventory();
		int paid_off = 0;

		if (amount <= 0) {
			return; // It's free.
		}

		HashMap<Integer, ? extends ItemStack> invItems = i.all(Material.EMERALD);
		for (Entry<Integer, ? extends ItemStack> entry : invItems.entrySet()) {
			int index = entry.getKey();
			ItemStack item = entry.getValue();
			int stackAmount = item.getAmount();

			if ((paid_off + stackAmount) <= amount) {
				p.getInventory().setItem(index, new ItemStack(Material.AIR));
				paid_off += stackAmount;
			}

			else {
				int to_take = amount - paid_off;
				p.getInventory().setItem(index, makeGems(stackAmount - to_take));
				paid_off += to_take;
			}

			if (paid_off >= amount) {
				p.updateInventory();
				break;
			}
		}

		HashMap<Integer, ? extends ItemStack> gem_pouches = i.all(Material.INK_SACK);
		for (Entry<Integer, ? extends ItemStack> entry : gem_pouches.entrySet()) {
			ItemStack item = entry.getValue();


			int worth = 0;

			if ((paid_off + worth) <= amount) {
				paid_off += worth;
				//GemSacks.setPouchWorth(item, 0);
			}

			else {
				int to_take = amount - paid_off;
				paid_off += to_take;
				//GemSacks.setPouchWorth(item, worth - to_take);
			}

			if (paid_off >= amount) {
				p.updateInventory();
				break;
			}

		}

		// They still aren't paid off!

		HashMap<Integer, ? extends ItemStack> bank_notes = i.all(Material.PAPER);
		for (Entry<Integer, ? extends ItemStack> entry : bank_notes.entrySet()) {
			ItemStack item = entry.getValue();
			int bank_note_val = getBankNoteValue(item);
			int index = entry.getKey();

			if ((paid_off + bank_note_val) <= amount) {
				p.getInventory().setItem(index, new ItemStack(Material.AIR));
				paid_off += bank_note_val;
			}

			else {
				int to_take = amount - paid_off;
				paid_off += to_take;
				updateMoney(p, index, (bank_note_val - to_take));
			}

			if (paid_off >= amount) {
				p.updateInventory();
				break;
			}

		}
	}
	
	public static void updateMoney(Player p, int slot, int new_amount) {
		p.getInventory().setItem(slot, new MoneyUtils().createBankNote(new_amount));
		// addMoneyCert(p, new_amount, false); Depreciated.
	}

	public static int getBankNoteValue(ItemStack i) {
		if(i != null && i.hasItemMeta() && i.getItemMeta().hasLore()) {
			List<String> lore = i.getItemMeta().getLore();
			for(String s : lore) {
				s = ChatColor.stripColor(s);
				if(s.startsWith("Value:")) {
					int value = Integer.parseInt(s.substring(s.indexOf(" ") + 1, s.lastIndexOf(" ")));
					return value;
				}
			}
		}

		return 0;
	}
	
	public boolean isUntradeable(ItemStack is) {
		return (is.getItemMeta().getLore().contains("Untradeable"));
	}
	 
	public int getCost(Player p) {
		int lvl = (DataHandler.getInstance().getBankUpgradeLevel(p) + 1);
		if (lvl == 1) return 500;
		if (lvl == 2) return 1000;
		if (lvl == 3) return 2500;
		if (lvl == 4) return 3200;
		if (lvl == 5) return 5000;
		if (lvl == 6) return 8000;
		if (lvl == 7) return 10000;
		if (lvl == 8) return 10000;
		if (lvl == 9) return 10000;
		if (lvl == 10) return 10000;
		if (lvl == 11) return 10000;
		if (lvl == 12) return 10000;
		if (lvl == 13) return 10000;
		if (lvl == 14) return 10000;
		if (lvl == 15) return -1;
		return (int) ((650 * (DataHandler.getInstance().getBankUpgradeLevel(p) + 1)) * (1.40));
	}

	public String getUpgradeAuthenticationCode(Player p) {
		if(bank_upgrade_codes.containsKey(p)) {
			return bank_upgrade_codes.get(p);
		} else {
			return null;
		}
	}
	
	public static ItemStack makeGems(int amount) {
		ItemStack i = new ItemStack(Material.EMERALD, amount);
		List<String> new_lore = new ArrayList<String>(Arrays.asList(ChatColor.GRAY.toString() + "The currency of Andalucia"));

		ItemMeta im = i.getItemMeta();
		im.setLore(new_lore);

		im.setDisplayName(ChatColor.WHITE.toString() + "Gem");
		i.setItemMeta(im);
		i.setAmount(amount);

		return i;
	}
	
	public void generateUpgradeAuthenticationCode(Player p, String tier) {
		StringBuffer sb = new StringBuffer(4);
		for(int i = 0; i < 4; i++) {
			int ndx = (int) (Math.random() * ALPHA_NUM.length());
			sb.append(ALPHA_NUM.charAt(ndx));
		}

		bank_upgrade_codes.put(p, tier + sb.toString());
	}

	public boolean isWithdrawGem(Player player, ItemStack is) {
		return (is.isSimilar(getGemItem(player)));
	}

	public Inventory getBankInventory1(Player player, int page) {
		int pages = DataHandler.getInstance().getBankPages(player);
		int upgrade = DataHandler.getInstance().getBankUpgradeLevel(player);
		Inventory inv = null;
		if (page == 1) {
			inv = Bukkit.createInventory(null, getSlotsBasedOnUpgradeLevel(DataHandler.getInstance().getBankUpgradeLevel(player), page));
			
		}
		return inv;
	}
	
	public Inventory getBankInventory(Player player, int page) {
		int pages = DataHandler.getInstance().getBankPages(player);
		int upgrade = DataHandler.getInstance().getBankUpgradeLevel(player);
		Inventory inv = Bukkit.createInventory(null, getSlotsBasedOnUpgradeLevel(DataHandler.getInstance().getBankUpgradeLevel(player), page));
		if (page == 1) {
			if (InventoryHandler.convertStringToInventory(null, PlayerBank.getPlayerBank(player).getInventory(), "Bank Chest (1/" + pages + ")", getSlotsBasedOnUpgradeLevel(DataHandler.getInstance().getBankUpgradeLevel(player), page)) != null) {
				inv = InventoryHandler.convertStringToInventory(null, PlayerBank.getPlayerBank(player).getInventory(), "Bank Chest (1/" + pages + ")", getSlotsBasedOnUpgradeLevel(DataHandler.getInstance().getBankUpgradeLevel(player), page));
			}
			inv.setItem(getGemSlot(DataHandler.getInstance().getBankUpgradeLevel(player), page), getGemItem(player));
			if (upgrade == 7) {
				inv.setItem(54, ItemMechanics.glass);
				inv.setItem(55, ItemMechanics.glass);
				inv.setItem(56, ItemMechanics.glass);
				inv.setItem(57, ItemMechanics.glass);
				//inv.setItem(58, ItemMechanics.glass);
				inv.setItem(59, ItemMechanics.glass);
				inv.setItem(60, ItemMechanics.glass);
				inv.setItem(61, ItemMechanics.glass);
				inv.setItem(62, ItemMechanics.glass);
			}
			if (upgrade == 8) {
				inv.setItem(54, ItemMechanics.glass);
				inv.setItem(55, ItemMechanics.glass);
				inv.setItem(56, ItemMechanics.glass);
				inv.setItem(57, ItemMechanics.glass);
				//inv.setItem(58, ItemMechanics.glass);
				inv.setItem(59, ItemMechanics.glass);
				inv.setItem(60, ItemMechanics.glass);
				inv.setItem(61, ItemMechanics.glass);
				inv.setItem(62, generateArrowButton("next", 1, pages));
			}
			if (upgrade == 9) {
				inv.setItem(54, ItemMechanics.glass);
				inv.setItem(55, ItemMechanics.glass);
				inv.setItem(56, ItemMechanics.glass);
				inv.setItem(57, ItemMechanics.glass);
				//inv.setItem(58, ItemMechanics.glass);
				inv.setItem(59, ItemMechanics.glass);
				inv.setItem(60, ItemMechanics.glass);
				inv.setItem(61, ItemMechanics.glass);
				inv.setItem(62, generateArrowButton("next", 1, pages));
			}
			if (upgrade == 10) {
				inv.setItem(54, ItemMechanics.glass);
				inv.setItem(55, ItemMechanics.glass);
				inv.setItem(56, ItemMechanics.glass);
				inv.setItem(57, ItemMechanics.glass);
				//inv.setItem(58, ItemMechanics.glass);
				inv.setItem(59, ItemMechanics.glass);
				inv.setItem(60, ItemMechanics.glass);
				inv.setItem(61, ItemMechanics.glass);
				inv.setItem(62, generateArrowButton("next", 1, pages));
			}
			if (upgrade == 11) {
				inv.setItem(54, ItemMechanics.glass);
				inv.setItem(55, ItemMechanics.glass);
				inv.setItem(56, ItemMechanics.glass);
				inv.setItem(57, ItemMechanics.glass);
				//inv.setItem(58, ItemMechanics.glass);
				inv.setItem(59, ItemMechanics.glass);
				inv.setItem(60, ItemMechanics.glass);
				inv.setItem(61, ItemMechanics.glass);
				inv.setItem(62, generateArrowButton("next", 1, pages));
			}
			if (upgrade == 12) {
				inv.setItem(54, ItemMechanics.glass);
				inv.setItem(55, ItemMechanics.glass);
				inv.setItem(56, ItemMechanics.glass);
				inv.setItem(57, ItemMechanics.glass);
				//inv.setItem(58, ItemMechanics.glass);
				inv.setItem(59, ItemMechanics.glass);
				inv.setItem(60, ItemMechanics.glass);
				inv.setItem(61, ItemMechanics.glass);
				inv.setItem(62, generateArrowButton("next", 1, pages));
			}
			if (upgrade == 13) {
				inv.setItem(54, ItemMechanics.glass);
				inv.setItem(55, ItemMechanics.glass);
				inv.setItem(56, ItemMechanics.glass);
				inv.setItem(57, ItemMechanics.glass);
				//inv.setItem(58, ItemMechanics.glass);
				inv.setItem(59, ItemMechanics.glass);
				inv.setItem(60, ItemMechanics.glass);
				inv.setItem(61, ItemMechanics.glass);
				inv.setItem(62, generateArrowButton("next", 1, pages));
			}
			if (upgrade == 14) {
				inv.setItem(54, ItemMechanics.glass);
				inv.setItem(55, ItemMechanics.glass);
				inv.setItem(56, ItemMechanics.glass);
				inv.setItem(57, ItemMechanics.glass);
				//inv.setItem(58, ItemMechanics.glass);
				inv.setItem(59, ItemMechanics.glass);
				inv.setItem(60, ItemMechanics.glass);
				inv.setItem(61, ItemMechanics.glass);
				inv.setItem(62, generateArrowButton("next", 1, pages));
			}
			return inv;
		}
		if (page == 2) {
			if (InventoryHandler.convertStringToInventory(null, PlayerBank.getPlayerBank(player).getInventory2(), "Bank Chest (2/" + pages + ")", getSlotsBasedOnUpgradeLevel(DataHandler.getInstance().getBankUpgradeLevel(player), page)) != null) {
				inv = InventoryHandler.convertStringToInventory(null, PlayerBank.getPlayerBank(player).getInventory2(), "Bank Chest (2/" + pages + ")", getSlotsBasedOnUpgradeLevel(DataHandler.getInstance().getBankUpgradeLevel(player), page));
			}
			if (upgrade == 8) {
				inv.setItem(0, generateArrowButton("previous", 2, pages));
			}
			if (upgrade == 9) {
				inv.setItem(9, generateArrowButton("previous", 2, pages));
			}
			if (upgrade == 10) {
				inv.setItem(18, generateArrowButton("previous", 2, pages));
			}
			if (upgrade == 11) {
				inv.setItem(27, generateArrowButton("previous", 2, pages));
			}
			if (upgrade == 12) {
				inv.setItem(36, generateArrowButton("previous", 2, pages));
			}
			if (upgrade == 13) {
				inv.setItem(45, generateArrowButton("previous", 2, pages));
			}
			if (upgrade == 14) {
				inv.setItem(54, generateArrowButton("previous", 2, pages));
				inv.setItem(55, ItemMechanics.glass);
				inv.setItem(56, ItemMechanics.glass);
				inv.setItem(57, ItemMechanics.glass);
				inv.setItem(58, ItemMechanics.glass);
				inv.setItem(59, ItemMechanics.glass);
				inv.setItem(60, ItemMechanics.glass);
				inv.setItem(61, ItemMechanics.glass);
				inv.setItem(62, ItemMechanics.glass);
			}
			return inv;
		}
		return inv;
	}
	
	public static boolean isNextArrow(ItemStack is) {
		if(is != null && is.getType() == Material.ARROW && is.hasItemMeta() && is.getItemMeta().hasDisplayName() && is.getDurability() == (short) 0 && is.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW.toString() + "Next Page " + ChatColor.BOLD.toString() + "->")) { return true; }
		return false;
	}

	public static boolean isPreviousArrow(ItemStack is) {
		if(is != null && is.getType() == Material.ARROW && is.hasItemMeta() && is.getItemMeta().hasDisplayName() && is.getDurability() == (short) 0 && is.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW.toString() + ChatColor.BOLD + "<-" + ChatColor.YELLOW.toString() + " Previous Page ")) { return true; }
		return false;
	}
	
	public static ItemStack generateArrowButton(String type, int current_page, int max_pages) {
		if(type.equalsIgnoreCase("next")) {
			return ItemMechanics.signCustomItem(Material.ARROW, (short) 0, ChatColor.YELLOW.toString() + "Next Page " + ChatColor.BOLD.toString() + "->", ChatColor.GRAY.toString() + "Page " + current_page + "/" + max_pages);
		} else if(type.equalsIgnoreCase("previous")) { return ItemMechanics.signCustomItem(Material.ARROW, (short) 0, ChatColor.YELLOW.toString() + ChatColor.BOLD + "<-" + ChatColor.YELLOW.toString() + " Previous Page ", ChatColor.GRAY.toString() + "Page " + current_page + "/" + max_pages); }
		return null;
	}

	public int getGemSlot(int l, int page) {
		if (page == 1) {
			if (l == 1) return 8;
			if (l == 2) return 17;
			if (l == 3) return 26;
			if (l == 4) return 35;
			if (l == 5) return 44;
			if (l == 6) return 53;
			if (l == 7) return 58;
			if (l == 8) return 58;
			if (l == 9) return 58;
			if (l == 10) return 58;
			if (l == 11) return 58;
			if (l == 12) return 58;
			if (l == 13) return 58;
			if (l == 14) return 58;
		}
		return 0;
	}

	public ItemStack getGemItem(Player player) {
		return ItemMechanics.signCustomItem(Material.EMERALD, (short) 0, ChatColor.GREEN.toString() + PlayerBank.getPlayerBank(player).getGems() + ChatColor.GREEN.toString() + "" + ChatColor.BOLD.toString() + " GEM(s)", ChatColor.GRAY.toString() + "" + ChatColor.GREEN.toString() + "Left Click" + ChatColor.GRAY.toString() + " to withdraw " + ChatColor.GREEN + ChatColor.BOLD + "RAW GEMS" + "," + ChatColor.GRAY.toString() + "" + ChatColor.GREEN.toString() + "Right Click" + ChatColor.GRAY.toString() + " to create " + ChatColor.GREEN + ChatColor.BOLD + "A GEM NOTE" + "," + ChatColor.GREEN.toString() + "Middle Click" + ChatColor.GRAY.toString() + " to upgrade your bank size.");
	}

	public int getSlotsBasedOnUpgradeLevel(int level, int pages) {
		if (pages == 1) {
			if (level == 1) return 9;
			if (level == 2) return 18;
			if (level == 3) return 27;
			if (level == 4) return 36;
			if (level == 5) return 45;
			if (level == 6) return 54;
			if (level == 7) return 63;
			if (level == 8) return 63;
			if (level == 9) return 63;
			if (level == 10) return 63;
			if (level == 11) return 63;
			if (level == 12) return 63;
			if (level == 13) return 63;
			if (level == 14) return 63;
		}
		if (pages == 2) {
			if (level == 8) return 9;
			if (level == 9) return 18;
			if (level == 10) return 27;
			if (level == 11) return 36;
			if (level == 12) return 45;
			if (level == 13) return 54;
			if (level == 14) return 63;
		}
		if (pages == -1) {
			return (level * 9);
		}
		return 9;
		
	}

	public static void close() {
		for (Player player : Bukkit.getServer().getOnlinePlayers()) {
			withdrawingBankNote.remove(player);
			withdrawingRawGems.remove(player);
			upgradingBank.remove(player);
		}
		
	}

}

package com.practiceserver.shards;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.practiceserver.Main;
import com.practiceserver.database.Config;
import com.practiceserver.database.DataHandler;
import com.practiceserver.player.InventoryHandler;
import com.practiceserver.player.PlayerUUID;
import com.practiceserver.player.PracticeServerPlayer;

public class UploadPlayerData extends Thread {
	
	String p_name = "";
	String old_server = "";
	int g_attempts = 0;
	static int g_up_attempts = 0;
	
	public UploadPlayerData(String safe_pname, String old_server) {
		p_name = safe_pname;
		this.old_server = old_server;
	}
	
	public void run() {
		try {
			if(ShardHandler.server_swap.containsKey(p_name) && Main.plugin.getServer().getPlayer(p_name) != null) {
				Server s = new Server(old_server);
				Player pl = Main.plugin.getServer().getPlayer(p_name);
				InventoryHandler inv = new InventoryHandler();
				File data = new File(Config.getWorld() + "/playerdata/" + PlayerUUID.getPlayerUUID(pl) + ".dat");
				data.delete();
				inv.saveInventory(pl);
				DataHandler.getInstance().saveData(pl);
				//if (s.getOnlinePlayers() == 1) {
					//DataHandler.getInstance().setServerOnlinePlayersCount(old_server, (0));
					
				//} else {
				//	DataHandler.getInstance().setServerOnlinePlayersCount(old_server, (s.getOnlinePlayers() - 1));
				//}
				//DataHandler.getInstance().saveData(pl);
				String server_prefix = ShardHandler.server_swap.get(p_name);
				Bukkit.getMessenger().registerOutgoingPluginChannel(Main.plugin, "BungeeCord");
				ByteArrayOutputStream b = new ByteArrayOutputStream();
				DataOutputStream out = new DataOutputStream(b);
				try {
					out.writeUTF("Connect");
					out.writeUTF(server_prefix);
					pl.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
					//Server server = new Server(server_prefix);
					//DataHandler.getInstance().setServerOnlinePlayersCount(server_prefix, (server.getOnlinePlayers() + 1));
					//Bukkit.getPluginManager().callEvent(new PlayerQuitEvent(pl, null));
					//Bukkit.getPluginManager().callEvent(new PlayerJoinEvent(pl, null));
					//DataHandler.getInstance().loadData(pl);
				} catch(IOException eee) {//
					
				} finally {
					ShardHandler.server_swap.remove(p_name);
					ShardHandler.server_swap_location.remove(p_name);
					ShardHandler.server_swap_pending.remove(p_name);
					PracticeServerPlayer.getPracticeServerPlayer(pl).setShard(server_prefix);
					pl.getInventory().clear();
					pl.getEquipment().clear();
					inv.loadInventory(pl);
					DataHandler.getInstance().loadData(pl);
				
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
package com.practiceserver.shards;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.craftbukkit.v1_10_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import com.practiceserver.Main;
import com.practiceserver.database.Config;
import com.practiceserver.database.DataHandler;
import com.practiceserver.mobs.ItemMechanics;
import com.practiceserver.player.PracticeServerPlayer;
import com.practiceserver.utils.RegionUtils;
import com.practiceserver.utils.Utils;

public class ShardHandler implements Listener {
	
	public static long last_shard_update = 0;
	public static volatile List<String> offline_servers = new ArrayList<String>();
	
	public static volatile ConcurrentHashMap<Integer, Long> last_ping = new ConcurrentHashMap<Integer, Long>();
	public static HashMap<String, String> server_swap = new HashMap<String, String>();
	public static HashMap<String, Location> server_swap_location = new HashMap<String, Location>();
	public static HashMap<String, String> server_swap_pending = new HashMap<String, String>();
	public static List<String> shards = new ArrayList<String>();

	public static Inventory ShardMenu = null;
	public static String MOTD = "";
	public static int player_count = 0;
	
	public static void setup() {
		MOTD = Utils.getShard1();
		Server server = new Server(MOTD);
		server.setOnline(true);
		setupShards();
		Main.plugin.getServer().getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
			public void run() {
				player_count = server.getOnlinePlayers();
			}
		}, 10 * 20L, 5 * 20L);
		
		Main.plugin.getServer().getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
			public void run() {
				for (Entry<Integer, Long> data : last_ping.entrySet()) {
					long time = data.getValue();
					int server_num = data.getKey();

					if ((System.currentTimeMillis() - time) > (15 * 1000)) {
						String server_prefix = "US-" + server_num;
						if (!(offline_servers.contains(server_prefix))) {
							offline_servers.add(server_prefix);
						}//
						//server_population.put(server_num, new ArrayList<Integer>(Arrays.asList(0, 0)));
						last_ping.remove(server_num);
					}
				}
			}
		}, 10 * 20L, 1 * 20L);

	}

	
	
	public static void setupShards() {
		for (String s : Config.us_public_servers) {
			shards.add(s);
		}
		for (String s : Config.us_private_servers) {
			shards.add(s);
		}
		for (String s : Config.us_beta_servers) {
			shards.add(s);
		}
		
	}



	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerInteract(PlayerInteractEvent e) {
		Player pl = e.getPlayer();
		if (server_swap.containsKey(pl.getName())) {
			e.setCancelled(true);
			e.setUseInteractedBlock(org.bukkit.event.Event.Result.DENY);
			e.setUseItemInHand(org.bukkit.event.Event.Result.DENY);
		}
	}
	

	@EventHandler
	public void onInventory(InventoryCloseEvent e) {
		final Player pl = (Player) e.getPlayer();
		if (server_swap_pending.containsKey(pl.getName())) {
			// Remove in 2 ticks.
			Main.plugin.getServer().getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {
				public void run() {
					server_swap_pending.remove(pl.getName());
				}
			}, 2L);
		}
	}

	
	public static Inventory getShardInventory(Player pl) {
		if ((System.currentTimeMillis() - last_shard_update) > 10 * 1000) {
			ShardMenu = generateShardMenu(pl);
			last_shard_update = System.currentTimeMillis();
		}
		return ShardMenu;
	}

	public static ItemStack generateShardItem(String server_prefix, Player pl) {
		// TODO: Show where friends/guildies are
	int server_num = Utils.getShardNum();
	ItemStack icon = null;
	ChatColor cc = null;
	boolean vip_server = false;
	boolean rp_server = false;
	boolean beta_server = false;
	Server server = new Server(server_prefix);
	server_num = Integer.parseInt(Bukkit.getMotd().split("-")[1].split(" ")[0]);
	if (server_prefix.contains("US")) {
		server_num = server.getServerNumber(); 
	}
	if (server_prefix.contains("EU")) {
		server_num = server_num + 1000;
	}
	if (server_prefix.contains("BR")) {
		server_num = server_num + 2000;
	}

	if (server_num == 5) {
		vip_server = true;
	}

	if (server_num == 11) {
		rp_server = true;
	}
	if (server_num >= 100 && server_num <= 110) {
		beta_server = true;
	}
	boolean server_open = false;
	int online_players = 0;
	int max_players = 0;
	 if (server.isOnline()) {
		 online_players = server.getOnlinePlayers();
		 max_players = server.getMaxPlayers();
		 icon = new ItemStack(Material.WOOL, 1, (short) 5);
		cc = ChatColor.GREEN;
		server_open = true;
	 }
	 if (server_open) {
			icon = new ItemStack(Material.WOOL, 1, (short) 0);
			cc = ChatColor.WHITE;
	 } else {
		 icon = new ItemStack(Material.WOOL, 1, (short) 14);
		   cc = ChatColor.RED;
	 }
	 if (PracticeServerPlayer.getPracticeServerPlayer(pl).getShard().equalsIgnoreCase(server_prefix)) {
		 icon = new ItemStack(Material.WOOL, 1, (short) 5);
			cc = ChatColor.GREEN;
	 }
	 



	ItemMeta im = icon.getItemMeta();

	online_players = (int) Math.round(online_players);

	if (online_players > max_players) {
		// So if the spoofed amount is > the maximum, we're going to take away 5-15 of the online count so more players can join.
		online_players = max_players - (new Random().nextInt(15 - 5) + 5);
	}

	if (online_players > 0 || max_players > 0) {
		im.setDisplayName(cc.toString() + server_prefix + ChatColor.GRAY + " " + online_players + "/" + max_players + "");
	} else {
		im.setDisplayName(cc.toString() + server_prefix);
	}
	List<String> lore = new ArrayList<String>();

	if (vip_server && cc != ChatColor.RED) {
		int this_server_num = Integer.parseInt(Bukkit.getMotd().split("-")[1].split(" ")[0]);
		if (this_server_num >= 100 && this_server_num <= 110)
			icon.setDurability((short) 4);
		lore.add(ChatColor.GREEN + "Subscriber Server");
	}

	if (rp_server && cc != ChatColor.RED) {
		lore.add(ChatColor.AQUA + "Roleplay Server");
	}

	if (server_prefix.contains("BR")) {
		lore.add(ChatColor.DARK_AQUA + "Language: Portuguese");
	}

	if (beta_server && cc != ChatColor.RED) {
		lore.add(ChatColor.YELLOW + "Beta Server");
	}

	if (cc == ChatColor.GREEN) {
		lore.add(ChatColor.GRAY.toString() + ChatColor.ITALIC.toString() + "You are currently in this shard.");
	} else if (cc == ChatColor.WHITE) {
		lore.add(ChatColor.GRAY.toString() + ChatColor.ITALIC.toString() + "Click to join this shard.");
	} else if (cc == ChatColor.RED) {
		lore.add(ChatColor.RED.toString() + ChatColor.ITALIC + "Shard Offline");
	}

	im.setLore(lore);
	icon.setItemMeta(im);
	return icon;
	}

	public static Inventory generateShardMenu(Player pl) {
		ItemStack divider = ItemMechanics.signCustomItem(Material.PISTON_MOVING_PIECE, (short) 0, " ", "");
		//ItemStack lobby = ItemMechanics.signCustomItem(Material.SKULL_ITEM, (short) 3, ChatColor.WHITE + "DungeonRealms Lobby", ChatColor.GRAY.toString()
				//+ ChatColor.ITALIC.toString() + "Go back to the dungeonrealms lobby.");
		Inventory shard_menu = Bukkit.createInventory(null, 18, "Shard Selection");
		int index = 0;
		for (String s : Config.us_public_servers) {
			shard_menu.setItem(index, generateShardItem(s, pl));
			index++;
		}

		// index = 9; // Move to next row for BR servers.
		// br servers are no longer used
/*		for (String s : Config.br_servers) {
			shard_menu.setItem(index, generateShardItem(s));
			index++;
		}*/

		// index = 18;

		for (String s : Config.us_private_servers) {
			shard_menu.setItem(index, generateShardItem(s, pl));
			index++;
		}

		//shard_menu.setItem(8, lobby);
		index = 9;

		for (String s : Config.us_beta_servers) {
			shard_menu.setItem(index, generateShardItem(s, pl));
			index++;
		}

		int x = 0;
		for (ItemStack is : shard_menu.getContents()) {
			if (is == null || is.getType() == Material.AIR) {
				shard_menu.setItem(x, CraftItemStack.asCraftCopy(divider));
			}
			x++;
		}

		return shard_menu;
	}

	public boolean vipServer(ItemStack is) {
		if (is != null && is.hasItemMeta() && is.getItemMeta().hasLore()) {
			List<String> lore = is.getItemMeta().getLore();
			for (String s : lore) {
				s = ChatColor.stripColor(s);
				if (s.equalsIgnoreCase("Subscriber Server")) {
					return true;
				}
			}
		}
		return false;
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		if (e.getInventory().getName().equalsIgnoreCase("Shard Selection")) {
			e.setCancelled(true);
			final Player pl = (Player) e.getWhoClicked();
			if (e.getRawSlot() >= 27) {
				return;
			}
			if (e.getCurrentItem() == null) {
				return;
			}

			ItemStack cur_item = e.getCurrentItem();

			if (cur_item.getType() == Material.SKULL_ITEM
					|| (cur_item.getType() == Material.WOOL && cur_item.hasItemMeta() && cur_item.getItemMeta().hasDisplayName() && cur_item.getItemMeta()
					.hasLore())) {
				// Hop servers?
						short durability = cur_item.getDurability();
				if (durability == 5) {
					// Current server, do nothing.
					pl.sendMessage(ChatColor.YELLOW + "You are already on the " + ChatColor.BOLD + MOTD.substring(0, MOTD.indexOf(" ")) + ChatColor.YELLOW
							+ " shard.");
					Main.plugin.getServer().getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {
						public void run() {
							pl.closeInventory();
						}
					}, 2L);

				}
				if (durability == 14) {
					// Current server, do nothing.
					pl.sendMessage(ChatColor.RED + "This shard is currently " + ChatColor.UNDERLINE + "unavailable.");
					Main.plugin.getServer().getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {
						public void run() {
							pl.closeInventory();
						}
					}, 2L);
				}

				if (durability == 4) {
					// Sub Server From Beta Server
					pl.sendMessage(ChatColor.RED + "You " + ChatColor.UNDERLINE + "must" + ChatColor.RED
							+ " join this shard from a public shard, not a beta shard!");
					Main.plugin.getServer().getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {
						public void run() {
							pl.closeInventory();
						}
					}, 2L);
				}

				if (durability == 0 || cur_item.getType() == Material.SKULL_ITEM) {
					// Ok, need to move them to new server.

					boolean deny = false;

					if (!deny && PracticeServerPlayer.getPracticeServerPlayer(pl).isCombat()) {
						double seconds_left = PracticeServerPlayer.getPracticeServerPlayer(pl).getCombatTime();
						//long dif = ((HealthMechanics.HealthRegenCombatDelay * 1000) + HealthMechanics.in_combat.get(pl.getName())) - System.currentTimeMillis();
						//seconds_left = (dif / 1000.0D) + 0.5D;
						//seconds_left = Math.round(seconds_left);

						pl.sendMessage(ChatColor.RED + "You " + ChatColor.UNDERLINE + "cannot" + ChatColor.RED + " change shards while in combat.");
						pl.sendMessage(ChatColor.GRAY + "Try again in approx. " + seconds_left + ChatColor.BOLD + "s");
						deny = true;
					}
					
					if (!deny && !pl.isOp() && !RegionUtils.isDamageDisabled(pl.getLocation())) {
					
						pl.sendMessage(ChatColor.RED + "You " + ChatColor.UNDERLINE + "cannot" + ChatColor.RED
								+ " change shards while in a wilderness / chaotic zone for another " + ChatColor.UNDERLINE + 0 + ChatColor.BOLD
								+ "s");
						pl.sendMessage(ChatColor.GRAY + "" + ChatColor.ITALIC + "This delay is to prevent resource, monster, and treasure farming abuse.");
						deny = true;
					}

					if (deny) {
						Main.plugin.getServer().getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {
							public void run() {
								pl.closeInventory();
							}
						}, 2L);
						return;
					}

				

					if (vipServer(cur_item)) {
						String rank = "default";
						boolean op = false;
						for (OfflinePlayer p : Bukkit.getOperators()) {
							if (p.getName().equalsIgnoreCase(pl.getName())) {
								op = true;
								break;
							}
						}
						if (rank.equalsIgnoreCase("default") && !(pl.isOp()) && !op) {
							// Don't let them in.
							pl.sendMessage(ChatColor.RED + "You are " + ChatColor.UNDERLINE + "not" + ChatColor.RED
									+ " authorized to connect to subscriber only servers.");
							pl.sendMessage(ChatColor.GRAY + "Subscribe at http://me.matt11matthew.dungeonrealms.net/shop to gain instant access!");
							Main.plugin.getServer().getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {
								public void run() {
									pl.closeInventory();
								}
							}, 2L);
							return;
						}
					}

					String i_name = ChatColor.stripColor(cur_item.getItemMeta().getDisplayName());
					final String server_prefix = !i_name.contains(Config.getWorld()) ? i_name.substring(0, i_name.indexOf(" ")) : "lobby1";

					if (cur_item.getType() != Material.SKULL_ITEM) {
						int online = Integer.parseInt(i_name.substring(i_name.lastIndexOf(" ") + 1, i_name.lastIndexOf("/")));
						int max_online = Integer.parseInt(i_name.substring(i_name.lastIndexOf("/") + 1, i_name.length()));

						// TODO: The stats on /shard are not in realtime, do we allow overflow or should we use ServerSwitchEvent to catch it?
						if (online >= max_online && online != 0) {
							// int group = forum_usergroup.get(pl.getName());
							String rank = "default";
							if (!pl.isOp() && rank.equalsIgnoreCase("default")) {
								pl.sendMessage(ChatColor.RED + "This shard is currently " + ChatColor.UNDERLINE + "FULL.");
								Main.plugin.getServer().getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {
									public void run() {
										pl.closeInventory();
									}
								}, 2L);
								return;
							}
						}
					}
					

					server_swap.put(pl.getName(), server_prefix);
					server_swap_location.put(pl.getName(), pl.getLocation());
					server_swap_pending.remove(pl.getName()); // Remove AFTER server_swap has been populated.

					/*if (!(pl.getWorld().getName().equalsIgnoreCase(main_world_name))) {
						Location safe = null;
						if (RealmMechanics.saved_locations.containsKey(pl.getName())) {
							safe = RealmMechanics.saved_locations.get(pl.getName());
							if (RealmMechanics.inv_portal_map.containsKey(pl.getName())) {
								Location l = RealmMechanics.inv_portal_map.get(pl.getName());
								RealmMechanics.inv_portal_map.remove(pl.getName());
								RealmMechanics.portal_map.remove(l);
								l.getBlock().setType(Material.AIR);
								l.subtract(0, 1, 0).getBlock().setType(Material.AIR);
							}
						} else if (InstanceMechanics.saved_location_instance.containsKey(pl.getName())) {
							safe = InstanceMechanics.saved_location_instance.get(pl.getName());
						} else {
							safe = SpawnMechanics.getRandomSpawnPoint(pl.getName());
						}

						pl.teleport(safe);
					}*/

					// pl.saveData();
					/*player_inventory.put(pl.getName(), pl.getInventory());
					player_location.put(pl.getName(), pl.getLocation());
					Damageable damp = (Damageable) pl;
					player_hp.put(pl.getName(), (double) damp.getHealth());
					player_level.put(pl.getName(), LevelMechanics.getPlayerLevel(pl.getName()));
					player_food_level.put(pl.getName(), pl.getFoodLevel());
					player_armor_contents.put(pl.getName(), pl.getInventory().getArmorContents());
					*/
					// Update local data.
					String old = PracticeServerPlayer.getPracticeServerPlayer(pl).getShard();
					pl.sendMessage("");
					pl.sendMessage(ChatColor.YELLOW + "                       Loading Shard - " + ChatColor.BOLD + server_prefix + ChatColor.YELLOW + " ... ");
					pl.sendMessage(ChatColor.GRAY.toString() + ChatColor.ITALIC.toString()
							+ "Your current game session has been paused while your data is transferred.");
					pl.sendMessage("");



					for (Player p_online : Bukkit.getServer().getOnlinePlayers()) {
						p_online.hidePlayer(pl);
					}

					// We should give them god mode so they can't die which would cause dupe issues.
					// pl.setPlayerListName(""); // So server treats them like NPC, no trading, etc.
					//RealmMechanics.player_god_mode.put(pl.getName(), System.currentTimeMillis());

					Main.plugin.getServer().getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {
						public void run() {
							pl.closeInventory();
						}
					}, 2L);

					String p_name = pl.getName();
					final String f_server_prefix = server_swap.get(p_name);
					new BukkitRunnable() {
						
						public void run() {
							Thread t = new UploadPlayerData(pl.getName(), old);
							t.start();
						}
					}.runTaskLaterAsynchronously(Main.plugin, 4L);
					Main.plugin.getServer().getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {
						public void run() {
							// 30 second timeout, if they're still online then d/c them.
							if (Main.plugin.getServer().getPlayer(pl.getName()) != null && server_swap.containsKey(pl.getName())) {
								//Player upl = Main.plugin.getServer().getPlayer(pl.getName());WAS UN COMENTED
								//upl.kickPlayer("Connection Timeout");WAS UN COMENTED
							}
						}
					}, 10 * 20L);
				}
			}
		}
	}
	
	@EventHandler
	public void dynamicMotd(ServerListPingEvent e) {
		String pref = Utils.getShard1();
		Server server = new Server(pref);
		if (server.isOnline()) {
			String motd = server.getMotd();
			motd = motd.replaceAll("%online%", server.getOnlinePlayers() + "");
			motd = motd.replaceAll("%maxonline%", server.getMaxPlayers() + "");
			motd = motd.replaceAll("%shard%", server.getName());
			e.setMotd(Utils.colorCodes(motd));
		}
	
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		String pref = Utils.getShard1();
		Server server = new Server(pref);
		PracticeServerPlayer.getPracticeServerPlayer(p).setShard(pref);
		DataHandler.getInstance().setServerOnlinePlayersCount(pref, (server.getOnlinePlayers() + 1));
		
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		String pref = Utils.getShard1();
		Server server = new Server(pref);
		PracticeServerPlayer.getPracticeServerPlayer(p).setShard(pref);
		if (server.getOnlinePlayers() == 1) {
			DataHandler.getInstance().setServerOnlinePlayersCount(pref, (0));
			
		} else {
			DataHandler.getInstance().setServerOnlinePlayersCount(pref, (server.getOnlinePlayers() - 1));
		}
	}
	
	public void uploadFromUS0To(Server server) {
		Server us0 = new Server("US-0");
		File worldFile = new File(us0.getWorldName() + "");
		
	}



	public static void close() {
		String pref = Utils.getShard1();
		Server server = new Server(pref);
		server.setOnline(false);
		DataHandler.getInstance().setServerOnlinePlayersCount(pref, 0);
		for (Player p : Bukkit.getServer().getOnlinePlayers()) {
			p.kickPlayer(Utils.colorCodes("&a&lThe server is rebooting!"));
		}
		
	}



	public static List<String> getShards() {
		return shards;
	}

}

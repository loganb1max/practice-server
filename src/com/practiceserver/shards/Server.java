package com.practiceserver.shards;

import com.practiceserver.database.DataHandler;

public class Server {
	
	private String name;
	private String motd;
	private String worldname;
	private int onlineplayers;
	private int maxplayers;
	private boolean online;
	private String ip;
	
	public Server(String name) {
		this.name = name;
		this.motd = DataHandler.getInstance().getServerMOTD(name);
		this.ip = DataHandler.getInstance().getServerIP(name);
		this.worldname = DataHandler.getInstance().getServerWorldName(name);
		this.onlineplayers = DataHandler.getInstance().getServerOnlinePlayers(name);
		this.maxplayers = DataHandler.getInstance().getServerMaxPlayers(name);
		this.online = DataHandler.getInstance().isOnline(name);
	}
	
	public String getName() {
		return name;
	}
	
	public void setOnline(boolean online) {
		DataHandler.getInstance().setServerOnline(name, online);
		this.online = online;
	}
	
	public String getMotd() {
		return motd;
	}
	
	public String getWorldName() {
		return worldname;
	}
	
	public int getOnlinePlayers() {
		return onlineplayers;
	}
	
	public int getMaxPlayers() {
		return maxplayers;
	}
	
	public boolean isOnline() {
		return online;
	}
	
	public String getIP() {
		return ip;
	}

	public int getServerNumber() {
		int num = 0;
		try {
			num = Integer.parseInt(name.split("-")[1].trim());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return num;
	}
}

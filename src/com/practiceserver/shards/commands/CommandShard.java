package com.practiceserver.shards.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.practiceserver.Main;
import com.practiceserver.shards.ShardHandler;

public class CommandShard implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		final Player p = (Player) sender;
	
		if(ShardHandler.server_swap.containsKey(p.getName()) || ShardHandler.server_swap_pending.containsKey(p.getName())) {
			p.sendMessage(ChatColor.RED + "You " + ChatColor.UNDERLINE + "cannot" + ChatColor.RED + " change shards while you have another shard request pending.");
			return true;
		}
		
		ShardHandler.server_swap_pending.put(p.getName(), ""); // Prevent packet abuse, put a map here immediatly.
		Main.plugin.getServer().getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {
			public void run() {
				// Delay the openning of the inventory to prevent packet abuse.
				p.openInventory(ShardHandler.getShardInventory(p));
			}
		}, 2L);
		return true;
	}


}

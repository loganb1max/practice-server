package com.practiceserver.utils;

import org.bukkit.Location;

import com.practiceserver.enums.Zone;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;

public class RegionUtils {

    public static WorldGuardPlugin getWg() {
        return (WorldGuardPlugin) org.bukkit.Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");
    }

    public static ApplicableRegionSet getRegionSet(Location loc) {
        return getWg().getRegionManager(loc.getWorld()).getApplicableRegions(loc);
    }

    public static boolean allowsPvP(Location loc) {
        return getRegionSet(loc).allows(DefaultFlag.PVP);
    }

    public static boolean allowsMobDamage(Location loc) {
        return getRegionSet(loc).allows(DefaultFlag.MOB_DAMAGE);
    }

    public static boolean isSafeZone(Location loc) {
        return ((!allowsPvP(loc)) && (!allowsMobDamage(loc)));
    }

    public static boolean isChaoticZone(Location loc) {
        return ((allowsPvP(loc)) && (allowsMobDamage(loc)));
    }

    public static boolean isWildernessZone(Location loc) {
        return ((!allowsPvP(loc)) && (allowsMobDamage(loc)));
    }

    public static Zone getZone(Location loc) {
        if (isSafeZone(loc)) return Zone.SAFE;
        if (isWildernessZone(loc)) return Zone.WILDERNESS;
        if (isChaoticZone(loc)) return Zone.CHAOTIC;
        return Zone.CHAOTIC;
    }

	public static boolean isDamageDisabled(Location location) {
		return (isSafeZone(location));
	}
}
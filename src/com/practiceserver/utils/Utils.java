package com.practiceserver.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;

public class Utils {

	public static List<LivingEntity> chargedMobs = new ArrayList();
	
	public static String colorCodes(String msg) {
		return ChatColor.translateAlternateColorCodes('&', msg);
	}
	
    public static String getShard() {
    	return Bukkit.getServer().getMotd().split("-")[1].split(" ")[0];
    }
    
    public static String getShard1() {
        String motd = Bukkit.getServer().getMotd();
        String server_name = Bukkit.getServer().getMotd().split("-")[1].split(" ")[0];
        if(motd.startsWith("US-")) {
            return "US-" + server_name;
        }else if (motd.startsWith("EU-")) {
            return "EU-" + server_name;
        }else if (motd.startsWith("BR-")) {
            return "BR-" + server_name;

        }
        return motd;
    }
    
    public static int getShardNum() {
    	String server_name = Bukkit.getServer().getMotd().split("-")[1].split(" ")[0];
    	int num = 0;
    	try {
    		num = Integer.parseInt(server_name.trim());
    	} catch (NumberFormatException e) {
    		e.printStackTrace();//
    	}
    	return num;
    }
    
    public static int getShardNum(Player p) {
    	String server_name = p.getServer().getMotd().split("-")[1].split(" ")[0];
    	int num = 0;
    	try {
    		num = Integer.parseInt(server_name.trim());
    	} catch (NumberFormatException e) {
    		e.printStackTrace();
    	}
    	return num;
    }
    
    public static long getTime(){
    	return Calendar.getInstance().getTimeInMillis();
    }

	public static Location getLocation(String loc, Player player) {
		String[] l = loc.split(",");
		double x = Double.parseDouble(l[0].trim());
		double y = Double.parseDouble(l[1].trim());
		double z = Double.parseDouble(l[2].trim());
		float yaw = Float.parseFloat(l[3].trim());
		float pitch = Float.parseFloat(l[4].trim());
		Location location = new Location(player.getWorld(), x, y, z, yaw, pitch);
		return location;
	}

	public static int ir(int low, int high) {
		Random r = new Random();
		if(low == high) {
			return 1;
		}
		int num = r.nextInt(high - low + 1) + low;
	    if(num == 0) {
	    	return 1;
	    }
	    return num;
	}
	
	public static int rand(int low, int high) {
		Random r = new Random();
		return r.nextInt(high - low + 1) + low;
	}

	public static String getNameHealth(double health, double maxhealth, String mob_name, LivingEntity instance) {
	    String inpat = mob_name;
	    boolean isBold = false;
	    if (inpat.contains(ChatColor.BOLD + "")) {
	      isBold = true;
	    }
	    int max = 45;//was 45
	    int min = 30;//was 30
	    int toPutIn = (int)(maxhealth / 30.0D);
	    if (toPutIn < min) {
	      toPutIn = min;
	    }
	    if (toPutIn > max) {
	      toPutIn = max;
	    }
	    mob_name = "";
	    for (int i = 0; i < toPutIn; i++) {
	      mob_name = mob_name + "|";
	    }
	    mob_name = mob_name.replaceAll("&0", ChatColor.BLACK.toString());
	    mob_name = mob_name.replaceAll("&1", ChatColor.DARK_BLUE.toString());
	    mob_name = mob_name.replaceAll("&2", ChatColor.DARK_GREEN.toString());
	    mob_name = mob_name.replaceAll("&3", ChatColor.DARK_AQUA.toString());
	    mob_name = mob_name.replaceAll("&4", ChatColor.DARK_RED.toString());
	    mob_name = mob_name.replaceAll("&5", ChatColor.DARK_PURPLE.toString());
	    mob_name = mob_name.replaceAll("&6", ChatColor.GOLD.toString());
	    mob_name = mob_name.replaceAll("&7", ChatColor.GRAY.toString());
	    mob_name = mob_name.replaceAll("&8", ChatColor.DARK_GRAY.toString());
	    mob_name = mob_name.replaceAll("&9", ChatColor.BLUE.toString());
	    mob_name = mob_name.replaceAll("&a", ChatColor.GREEN.toString());
	    mob_name = mob_name.replaceAll("&b", ChatColor.AQUA.toString());
	    mob_name = mob_name.replaceAll("&c", ChatColor.RED.toString());
	    mob_name = mob_name.replaceAll("&d", ChatColor.LIGHT_PURPLE.toString());
	    mob_name = mob_name.replaceAll("&e", ChatColor.YELLOW.toString());
	    mob_name = mob_name.replaceAll("&f", ChatColor.WHITE.toString());
	    mob_name = mob_name.replaceAll("&u", ChatColor.UNDERLINE.toString());
	    mob_name = mob_name.replaceAll("&l", ChatColor.BOLD.toString());
	    mob_name = mob_name.replaceAll("&i", ChatColor.ITALIC.toString());
	    mob_name = mob_name.replaceAll("&m", ChatColor.MAGIC.toString());
	    double percent = health / maxhealth;
	    double length = mob_name.length();
	    double scolor = length * percent;
	    int color = (int)Math.ceil(scolor);
	    String charBlank = colorCodes("&0");
	    if (isBold) {
	      charBlank = charBlank + colorCodes("&l");
	    }
	    String n = mob_name;
	    String adding = colorCodes("&a");
	    if (color < length / 3.0D) {
	      String g = n.substring(color);
	      String char1 = colorCodes("&c");
	      String char2 = colorCodes("&d");
	      adding = char1;
	      if (isBold) {
	        char1 = char1 + colorCodes("&l");
	        char2 = char2 + colorCodes("&l");
	      }
	      String h = n.substring(0, color);
	      if (chargedMobs.contains(instance)) {
	        n = char2 + h + charBlank + g;
	      } else {
	        n = char1 + h + charBlank + g;
	      }
	    } else if (color < length / 1.5D) {
	      String g = n.substring(color);
	      String char1 = colorCodes("&e");
	      String char2 = colorCodes("&d");
	      adding = char1;
	      if (isBold) {
	        char1 = char1 + colorCodes("&l");
	        char2 = char2 + colorCodes("&l");
	      }
	      String h = n.substring(0, color);
	      if (chargedMobs.contains(instance)) {
	        n = char2 + h + charBlank + g;
	      } else {
	        n = char1 + h + charBlank + g;
	      }
	    } else {
	      String g = n.substring(color);
	      String h = n.substring(0, color);
	      String char1 = colorCodes("&a");
	      String char2 = colorCodes("&d");
	      adding = char1;
	      if (isBold) {
	        char1 = char1 + colorCodes("&l");
	        char2 = char2 + colorCodes("&l");
	      }
	      if (chargedMobs.contains(instance)) {
	        n = char2 + h + charBlank + g;
	      } else {
	        n = char1 + h + charBlank + g;
	      }
	    }
	    if (chargedMobs.contains(instance)) {
	      adding = colorCodes("&d");
	    }
	    if (isBold) {
	      adding = adding + colorCodes("&l");
	    }
	    adding = adding + "▉";
	    n = adding + n;
	    n = n + adding;
	    n = n.trim();
	    return n;
	}
	
	/**
	 * 
	 * @param l The mob getting it's name set.
	 */
	public static void updateHealthbar(LivingEntity l) {
	    l.setCustomName(getNameHealth(l.getHealth(), l.getMaxHealth(), (String)((MetadataValue)l.getMetadata("DisplayName").get(0)).value(), l));
	  }
}

	
	


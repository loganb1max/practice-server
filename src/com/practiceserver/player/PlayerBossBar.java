package com.practiceserver.player;

import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

import com.practiceserver.utils.Utils;

public class PlayerBossBar {
	
	private String title;
	private BarFlag barFlag;
	private Player player;
	private BarColor barColor;
	private BarStyle barStyle;
	private double health;
	private BossBar bossBar;
	
	public PlayerBossBar(Player player, String title, BarFlag flag, BarStyle barStyle, BarColor barColor) {
		this.player = player;
		setBarColor(barColor);
		setBarFlag(flag);
		setBarStyle(barStyle);
		setTitle(title);
		this.bossBar = Bukkit.createBossBar(getTitle(), getBarColor(), getBarStyle(), getBarFlag());
		getBossBar().addPlayer(player);
	}
	
	public String getTitle() {
		return title;
	}
	
	public BossBar getBossBar() {
		return bossBar;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public Player getPlayer() {
		return player;
	}

	public BarFlag getBarFlag() {
		return barFlag;
	}

	public void setBarFlag(BarFlag barFlag) {
		this.barFlag = barFlag;
	}

	public BarColor getBarColor() {
		return barColor;
	}

	public void setBarColor(BarColor barColor) {
		this.barColor = barColor;
	}

	public BarStyle getBarStyle() {
		return barStyle;
	}

	public void setBarStyle(BarStyle barStyle) {
		this.barStyle = barStyle;
	}
	
	public void setHealthPercent(double health) {
		this.health = health;
	}
	
	public double getHealthPercent() {
		return health;
	}
	
	public void send() {
		if (getBossBar().getPlayers().contains(player)) {
			getBossBar().setVisible(true);
		} else {
			getBossBar().addPlayer(player);
			getBossBar().setVisible(true);
		}
	}
	
	public void remove() {
		if (getBossBar().getPlayers().contains(player)) {
			getBossBar().setVisible(false);
			getBossBar().removeAll();
		}
	}
	
	public void update() {
		if (getBossBar().getPlayers().contains(player)) {
			getBossBar().setTitle(Utils.colorCodes("&d&lHP &d" + (int) player.getHealth() + "&d&l / &d" + (int) player.getMaxHealth()));
			getBossBar().setProgress((float) player.getHealth() / (float) player.getMaxHealth());
		}
	}
	

}

package com.practiceserver.player;

import org.bukkit.entity.Player;

import com.practiceserver.database.DataHandler;

public class PlayerUUID {
	
	private Player player;
	private String uuid;
	
	public PlayerUUID(Player player) {
		this.player = player;
		this.uuid = DataHandler.getInstance().getUUID(player);
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public static PlayerUUID getPlayerUUID(Player player) {
		return new PlayerUUID(player);
	}
	
	public String getUUID() {
		return uuid;
	}

}

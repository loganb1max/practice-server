package com.practiceserver.player.commands.toggles;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.practiceserver.player.PlayerToggles;

public class CommandTogglePvP implements CommandExecutor {
	
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		final Player p = (Player) sender;
		
		
		if(!(args.length == 0)) {
			p.sendMessage(ChatColor.RED + "Invalid Command.");
			p.sendMessage(ChatColor.GRAY + "Usage: /togglepvp");
			p.sendMessage(ChatColor.GRAY + "Description: Enables / Disables killing blows on Lawful players.");
			return true;
		}
		PlayerToggles pl = PlayerToggles.getPlayerToggles(p);
		if (pl.isTogglePvP()) {
			pl.setTogglePvP(false);
			p.sendMessage(ChatColor.GREEN + "Outgoing PVP Damage - " + ChatColor.BOLD + "ENABLED");
			return true;
		}
		
		if (!pl.isTogglePvP()) {
			pl.setTogglePvP(true);
			p.sendMessage(ChatColor.RED + "Outgoing PVP Damage - " + ChatColor.BOLD + "DISABLED");
			return true;
		}
		return true;
	}
	
}
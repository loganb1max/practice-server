package com.practiceserver.player.commands.toggles;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.practiceserver.player.PlayerToggles;

public class CommandDebug implements CommandExecutor {
	
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		final Player p = (Player) sender;
		
		if(cmd.getName().equalsIgnoreCase("crypt")) {
			if(p != null) {
				if(!(p.isOp())) { return true; }
			}
			
			return true;
		}
		
		if(!(args.length == 0)) {
			p.sendMessage(ChatColor.RED + "Invalid Command.");
			p.sendMessage(ChatColor.GRAY + "Usage: /debug");
			p.sendMessage(ChatColor.GRAY + "Description: Enables / Disables displaying debug messages.");
			return true;
		}
		
		PlayerToggles pl = PlayerToggles.getPlayerToggles(p);
		if (pl.isToggleDebug()) {
			pl.setToggleDebug(false);
			pl.getPracticeServerPlayer().msg("&cDebug Messages - &lDISABLED");
			return true;
		}
		
		if (!pl.isToggleDebug()) {
			pl.setToggleDebug(true);
			pl.getPracticeServerPlayer().msg("&aDebug Messages - &lENABLED");
			return true;
		}
		return true;
	}
	
}

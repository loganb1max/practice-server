package com.practiceserver.player.commands.toggles;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.practiceserver.player.PlayerToggles;

public class CommandToggleFilter implements CommandExecutor {
	
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		final Player p = (Player) sender;
		
		
		if(!(args.length == 0)) {
			p.sendMessage(ChatColor.RED + "Invalid Command.");
			p.sendMessage(ChatColor.GRAY + "Usage: /togglefilter");
			p.sendMessage(ChatColor.GRAY + "Description: Enables / Disables the adult language chat filter.");
			return true;
		}
		
		PlayerToggles pl = PlayerToggles.getPlayerToggles(p);
		if (pl.isToggleFilter()) {
			pl.setToggleFilter(false);
			p.sendMessage(ChatColor.GREEN + "Adult Chat Filter - " + ChatColor.BOLD + "ENABLED");
			return true;
		}
		
		if (!pl.isToggleFilter()) {
			pl.setToggleFilter(true);
			p.sendMessage(ChatColor.RED + "Adult Chat Filter - " + ChatColor.BOLD + "DISABLED");
			return true;
		}
		return true;
	}
	
}
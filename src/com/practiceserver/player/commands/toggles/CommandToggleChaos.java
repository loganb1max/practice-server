package com.practiceserver.player.commands.toggles;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.practiceserver.player.PlayerToggles;

public class CommandToggleChaos implements CommandExecutor {
	
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		final Player p = (Player) sender;
	
		
		if(!(args.length == 0)) {
			p.sendMessage(ChatColor.RED + "Invalid Command.");
			p.sendMessage(ChatColor.GRAY + "Usage: /togglechaos");
			p.sendMessage(ChatColor.GRAY + "Description: Enables / Disables recieving chaotic alignment.");
			return true;
		}
		
		PlayerToggles pl = PlayerToggles.getPlayerToggles(p);
		if (pl.isToggleChaos()) {
			pl.setToggleChaos(false);
			pl.getPracticeServerPlayer().msg("&cAnti-Chaotic - &lDISABLED");
			return true;
		}
		if (pl.isToggleChaos()) {
			pl.setToggleChaos(true);
			pl.getPracticeServerPlayer().msg("&aAnti-Chaotic - &lENABLED");
			return true;
		}
		
		return true;
	}
	
}
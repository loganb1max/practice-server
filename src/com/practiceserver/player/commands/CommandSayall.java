package com.practiceserver.player.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import com.practiceserver.network.NetworkHandler;
import com.practiceserver.utils.Utils;

public class CommandSayall implements CommandExecutor {
	

	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			String msg = "";
			for (String s : args) msg += s + " ";
			final String lserver = sender.getServer().getMotd().split("-")[1].split(" ")[0];
			final String f_msg = msg;
			String server_name = Utils.getShardNum((Player) sender) + "";
        	int num = 0;
        	try {
        		num = Integer.parseInt(server_name.trim());
        	} catch (NumberFormatException e) {
        		e.printStackTrace();
        	}
            for (Player pl : Bukkit.getOnlinePlayers()) {
            	String personal_msg = msg;
	            if (personal_msg.endsWith(" ")) {
	                personal_msg = personal_msg.substring(0, personal_msg.length() - 1);
	            }
	            personal_msg = "&b" + personal_msg;
				pl.sendMessage(Utils.colorCodes("&b&l>> &b" + personal_msg + "&b-" + sender.getName()));
            }
            NetworkHandler.sendPacketCrossServer("[sayall]" + sender.getName() + "@" + lserver + ":" + f_msg, num, true);
            return true;
		}
		if (sender instanceof ConsoleCommandSender) {
			String msg = "";
			for (String s : args) msg += s + " ";
			//msg = ChatColor.WHITE + msg;
			final String lserver = Bukkit.getServer().getMotd().split("-")[1].split(" ")[0];
			final String f_msg = msg;
			String server_name = Bukkit.getServer().getMotd().split("-")[1].split(" ")[0];
        	int num = 0;
        	try {
        		num = Integer.parseInt(server_name.trim());
        	} catch (NumberFormatException e) {
        		e.printStackTrace();
        	}
            for (Player pl : Bukkit.getOnlinePlayers()) {
            	String personal_msg = msg;
	            if (personal_msg.endsWith(" ")) {
	                personal_msg = personal_msg.substring(0, personal_msg.length() - 1);
	            }
	            personal_msg = "&b" + personal_msg;
				pl.sendMessage(Utils.colorCodes("&b&l>> &b" + personal_msg + "&b-" + sender.getName()));
            }
            NetworkHandler.sendPacketCrossServer("[sayall]" + "Console" + "@" + lserver + ":" + f_msg, num, true);
           
            return true;
		}
		return true;
	}
}

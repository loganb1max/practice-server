package com.practiceserver.player.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.practiceserver.chat.ChatUtils;
import com.practiceserver.network.NetworkHandler;
import com.practiceserver.player.PracticeServerPlayer;
import com.practiceserver.utils.Utils;

public class CommandSC implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			if(!sender.isOp()) return true;
			String msg = "";
			for (String s : args) msg += s + " ";
			final String lserver = sender.getServer().getMotd().split("-")[1].split(" ")[0];
			final String f_msg = msg;
			String server_name = Utils.getShardNum((Player) sender) + "";
        	int num = 0;
        	try {
        		num = Integer.parseInt(server_name.trim());
        	} catch (NumberFormatException e) {
        		e.printStackTrace();
        	}
            for (Player pl : Bukkit.getOnlinePlayers()) {
				if (!pl.isOp()) return true;
				String personal_msg = msg;
	            if (personal_msg.endsWith(" ")) {
	                personal_msg = personal_msg.substring(0, personal_msg.length() - 1);
	            }
	    		msg = ChatUtils.fixCaps(msg);
	    		msg = ChatUtils.filter(msg);
	    		msg = ChatColor.stripColor(msg);
	    		String name = "";
	    		if (sender.isOp()) {
	    			name = "&b&lGM &7" + sender.getName();
	    		} else {
	    			name = "&7" + sender.getName();
	    		}
	            PracticeServerPlayer.getPracticeServerPlayer(pl).msg("&6<&lSC&6> " + name + ": &f" + msg);
            }
    		msg = ChatUtils.fixCaps(msg);
    		msg = ChatUtils.filter(msg);
    		msg = ChatColor.stripColor(msg);
    		String name = "";
    		if (sender.isOp()) {
    			name = "&b&lGM &7" + sender.getName();
    		} else {
    			name = "&7" + sender.getName();
    		}
            NetworkHandler.sendPacketCrossServer("[sc]" + name + "@" + lserver + ":" + msg, num, true);
            return true;
		}
		return true;
	}
}
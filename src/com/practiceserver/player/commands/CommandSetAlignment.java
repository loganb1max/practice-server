package com.practiceserver.player.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import com.practiceserver.enums.Alignment;
import com.practiceserver.player.PracticeServerPlayer;

public class CommandSetAlignment implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			PracticeServerPlayer player = PracticeServerPlayer.getPracticeServerPlayer((Player) sender);
			if (args.length != 2) {
				player.msg("&c&lInvalid Syntax! &c/setalignment <NAME> <ALIGNMENT>");
				return true;
			} else {
				String p_name = args[0];
				if ((p_name != null) && (Bukkit.getPlayer(p_name).isOnline())) {
					PracticeServerPlayer p = PracticeServerPlayer.getPracticeServerPlayer(Bukkit.getPlayer(p_name));
					p.setAlignmentTime(Alignment.valueOf(args[1].toUpperCase()).getDefaultTime());
					p.setAlignment(Alignment.valueOf(args[1].toUpperCase()));
					player.msg("&bYou have set " + p_name + "'s &lALIGNMENT&b");
					player.msg("&7Alignment: " + Alignment.valueOf(args[1].toUpperCase()).toString());
					return true;
				} else {
					player.msg("&c&lInvalid Syntax! &c/setalignment <NAME> <ALIGNMENT>");
					return true;
				}
			}
		}
		if (sender instanceof ConsoleCommandSender) {
			if (args.length != 2) {
				sender.sendMessage("Invalid Syntax! /setalignment <NAME> <ALIGNMENT>");
				return true;
			} else {
				String p_name = args[0];
				if ((p_name != null) && (Bukkit.getPlayer(p_name).isOnline())) {
					PracticeServerPlayer p = PracticeServerPlayer.getPracticeServerPlayer(Bukkit.getPlayer(p_name));
					p.setAlignmentTime(Alignment.valueOf(args[1].toUpperCase()).getDefaultTime());
					p.setAlignment(Alignment.valueOf(args[1].toUpperCase()));
					sender.sendMessage("You've set " + p_name + "'s alignment to " + Alignment.valueOf(args[1].toUpperCase()).toString());
					return true;
				} else {
					sender.sendMessage("/setalignment <NAME> <ALIGNMENT>");
					return true;
				}
			}
		}
		return true;
		
	}//setalignment <NAME> <ALIGNMENT>

}

package com.practiceserver.player.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.practiceserver.mounts.enums.MountType;
import com.practiceserver.player.PracticeServerPlayer;

public class CommandTest implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			PracticeServerPlayer p = PracticeServerPlayer.getPracticeServerPlayer((Player) sender);
			p.getPlayer().getInventory().addItem(MountType.T2_MOUNT.getItem());
			return true;
		}
		return true;
	}
}

package com.practiceserver.player.commands;

import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.practiceserver.enums.Zone;
import com.practiceserver.player.PracticeServerPlayer;
import com.practiceserver.utils.RegionUtils;

public class CommandZone implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			PracticeServerPlayer p = PracticeServerPlayer.getPracticeServerPlayer((Player) sender);
			Zone zone = RegionUtils.getZone(p.getPlayer().getLocation());
			if (zone == Zone.CHAOTIC) {
				p.msg("&c                &l*** CHAOTIC ZONE (PVP-ON) ***");
				p.playSound(Sound.ENTITY_WITHER_SHOOT, 0.25F, 0.30F);
				return true;
			}
			if (zone == Zone.SAFE) {
				p.msg("&a                &l*** SAFE ZONE (DMG-OFF) ***");
				p.playSound(Sound.ENTITY_WITHER_SHOOT, 0.25F, 0.30F);
				return true;
			}
			if (zone == Zone.WILDERNESS) {
				p.msg("&e                &l*** WILDERNESS (MOBS-ON, PVP-OFF) ***");
				p.playSound(Sound.ENTITY_WITHER_SHOOT, 0.25F, 0.30F);
				return true;
			}
		}
		return true;
	}
}
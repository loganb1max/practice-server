package com.practiceserver.player;

import java.io.File;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.lang.StringEscapeUtils;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import com.google.common.base.Joiner;
import com.practiceserver.database.Config;
import com.practiceserver.database.ConnectionPool;
import com.practiceserver.database.DataHandler;
import com.practiceserver.mobs.ItemMechanics;

public class InventoryHandler implements Listener {

	public static String rootDir = ""; 
	
	
	@EventHandler
	public void onInventoryLoad(PlayerJoinEvent e) {
		e.getPlayer().getInventory().clear();
		e.getPlayer().getEquipment().clear();
		loadInventory(e.getPlayer());
		DataHandler.getInstance().loadData(e.getPlayer());
	}

	@EventHandler
	public void onInventorySave(PlayerQuitEvent e) {
		File data = new File(Config.getWorld() + "/playerdata/" + PlayerUUID.getPlayerUUID(e.getPlayer()) + ".dat");
		data.delete();
		saveInventory(e.getPlayer());
		DataHandler.getInstance().saveData(e.getPlayer());
		
	}
	
	
	public void loadInventory(Player p) {
		String UUID = PlayerUUID.getPlayerUUID(p).getUUID();
		boolean login = true;
		PreparedStatement pst = null;
		try {
			pst = ConnectionPool.getConnection().prepareStatement("SELECT name, inventory FROM inventorys WHERE uuid = '" + UUID + "'");
			
			pst.execute();
			ResultSet rs = pst.getResultSet();
			
			if (!(rs.next())) {
				Bukkit.getLogger().log(Level.INFO, "[HIVE (Slave Edition)] No PLAYER DATA found for " + p.getName() + ", return null.");
			}
			
			String inv = rs.getString("inventory");
			
			convertStringToInventory(p, inv, null, 0);
			
		}catch (Exception ex) {
			ex.printStackTrace();

		} finally {
			try {
				if (pst != null) {
					try {
						pst.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			} finally {
			}
		}
	}
	
	public void saveInventory(Player p) {
		
		String inventory = convertInventoryToString(p.getName(), p.getInventory(), true);
		String UUID = PlayerUUID.getPlayerUUID(p).getUUID();
		PreparedStatement pst = null;
		try {
			pst = ConnectionPool.getConnection().prepareStatement("INSERT INTO inventorys (uuid, name, inventory) " + "VALUES('" + UUID + "', '" + p.getName() + "', '" + StringEscapeUtils.escapeSql(inventory) + "') " +
					"ON DUPLICATE KEY UPDATE name='" + p.getName() + "', inventory='" + StringEscapeUtils.escapeSql(inventory) + "'");
			pst.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();

		} finally {
			try {
				if (pst != null) {
					try {
						pst.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			} finally {
			}
		}
		
	}


	public static String convertInventoryToString(String p_name, Inventory inv, boolean player)
	{
		// @item@Slot:ItemID-Amount.Durability#Item_Name#$Item_Lore$[lam1]lam_color[lam2]
		// @item@1:267-1.54#Magic Sword#$DMG: 5 - 7, CRIT: 5%$@item@
		
		String return_string = "";
		int slot = -1;
		for (ItemStack is : inv.getContents())
		{
			slot++;
			if (is == null || is.getType() == Material.AIR)
			{
				continue;
			}
			
			String i_name = "";
			if (is.hasItemMeta() && is.getItemMeta().hasDisplayName())
			{
				i_name = is.getItemMeta().getDisplayName();
			}
			else
			{
				// Default name.
				i_name = "null";
			}
			
			String i_lore = "";
			if (is.hasItemMeta() && is.getItemMeta().hasLore())
			{
				for (String s : is.getItemMeta().getLore())
				{
					i_lore = i_lore + "," + s;
				}
			}
			else
			{
				// No lore.
				i_lore = "null";
			}
			
			while (i_lore.contains(",,"))
			{
				i_lore = i_lore.replace(",,", ",");
			}
			
			return_string = return_string + ("@item@" + slot + ":" + is.getTypeId() + "-" + is.getAmount() + "." + is.getDurability() + "#" + i_name + "#" + "$" + i_lore + "$");
			if (is.hasItemMeta() && is.getItemMeta() instanceof LeatherArmorMeta)
			{
				return_string = return_string + "[lam1]" + ((LeatherArmorMeta) is.getItemMeta()).getColor().asBGR() + "[lam2]";
			}
		}
		
		List<ItemStack> armor_contents = new ArrayList<ItemStack>();
		if (player)
		{
			if (Bukkit.getPlayer(p_name) != null)
			{
				Player owner = Bukkit.getPlayer(p_name);
				for (ItemStack is : owner.getInventory().getArmorContents())
				{
					armor_contents.add(is);
				}
			}
			
			if (armor_contents.size() > 0)
			{
				for (ItemStack is : armor_contents)
				{
					slot++;
					if (is == null)
					{
						continue;
					}
					
					String i_name = "";
					if (is.hasItemMeta() && is.getItemMeta().hasDisplayName())
					{
						i_name = is.getItemMeta().getDisplayName();
					}
					else
					{
						// Default name.
						i_name = "null";
					}
					
					String i_lore = "";
					if (is.hasItemMeta() && is.getItemMeta().hasLore())
					{
						for (String s : is.getItemMeta().getLore())
						{
							i_lore = i_lore + "," + s;
						}
					}
					else
					{
						// No lore.
						i_lore = "null";
					}
					
					return_string = return_string + ("@item@" + slot + ":" + is.getTypeId() + "-" + is.getAmount() + "." + is.getDurability() + "#" + i_name + "#" + "$" + i_lore + "$");
					if (is.hasItemMeta() && is.getItemMeta() instanceof LeatherArmorMeta)
					{
						return_string = return_string + "[lam1]" + ((LeatherArmorMeta) is.getItemMeta()).getColor().asBGR() + "[lam2]";
					}
				}
			}
		}
		
		return return_string;
	}
	
	
	public static Inventory convertStringToInventory(Player pl, String inventory_string, String inventory_name, int slots)
	{
		Inventory inv = null;
		// int slot_cache = -1;
		int expected_item_size = inventory_string.split("@item@").length - 1;
		
		if (pl == null && inventory_name != null)
		{
			// Using inventory.
			inv = Bukkit.createInventory(null, slots, inventory_name);
		}
		for (String s : inventory_string.split("@item@"))
		{
			// slot_cache++;
			
			if (s.length() <= 1 || s.equalsIgnoreCase("null"))
			{
				continue;
			}
			
			int slot = Integer.parseInt(s.substring(0, s.indexOf(":")));
			
			if (inventory_name != null && inventory_name.startsWith("Bank Chest"))
			{
				if (slot > expected_item_size && (slot > (slots - 1)))
				{ // slots - 1, 0 index = start
					slot = inv.firstEmpty();
				}
			}
			
			if (s.length() <= 1)
			{
				continue;
			}
			
			int item_id = Integer.parseInt(s.substring(s.indexOf(":") + 1, s.indexOf("-")));
			int amount = Integer.parseInt(s.substring(s.indexOf("-") + 1, s.indexOf(".")));
			short durability = Short.parseShort(s.substring(s.indexOf(".") + 1, s.indexOf("#")));
			
			String i_name = s.substring(s.indexOf("#") + 1, s.lastIndexOf("#"));
			String i_lore = s.substring(s.indexOf("$") + 1, s.lastIndexOf("$"));
			
			Color leather_armor_color = null;
			if (s.contains("[lam1]"))
			{
				leather_armor_color = Color.fromBGR(Integer.parseInt(s.substring(s.indexOf("[lam1]") + 6, s.lastIndexOf("[lam2]"))));
			}
			
			ItemStack is = new ItemStack(Material.getMaterial(item_id), amount, durability);
			
			List<String> splitlore = new ArrayList<String>(new LinkedHashSet<String>(Arrays.asList(i_lore.split(","))));
			i_lore = Joiner.on(',').join(splitlore);
			
			if (is.getType() == Material.POTION && is.getDurability() > 0)
			{
				// Renames potion to Instant Heal.
				
				is = ItemMechanics.signNewCustomItem(Material.getMaterial(item_id), durability, i_name, i_lore);
				if (pl != null)
				{
					pl.getInventory().setItem(slot, is);
				}
				else if (inv != null)
				{
					inv.setItem(slot, is);
				}
				continue;
			}
			
			if (is.getType() == Material.WRITTEN_BOOK)
			{
				continue; // TODO: Code book loading.
			}
			
			ItemMeta im = is.getItemMeta();
			
			if (!(i_name.equalsIgnoreCase("null")))
			{
				// Custom name!
				im.setDisplayName(i_name);
			}
			
			if (!(i_lore.equalsIgnoreCase("null")))
			{
				// Lore!
				List<String> all_lore = new ArrayList<String>();
				
				for (String lore : i_lore.split(","))
				{
					if (lore.length() > 1)
					{
						all_lore.add(lore);
					}
				}
				im.setLore(all_lore);
			}
			
			if (!(leather_armor_color == null))
			{
				((LeatherArmorMeta) im).setColor(leather_armor_color);
			}
			
			if (!(i_name.equalsIgnoreCase("null")) || !(i_lore.equalsIgnoreCase("null")))
			{
				is.setItemMeta(im);
			}
			
			if (pl != null)
			{
				pl.getInventory().setItem(slot, is);
			}
			else if (inv != null)
			{
				inv.setItem(slot, is);
			}
		}
		
		if (inv != null) {
			return inv;
		}
		else {
			return null;
		}
	}
	
	public static void setSystemPath()
	{
		CodeSource codeSource = InventoryHandler.class.getProtectionDomain().getCodeSource();
		File jarFile = null;
		try
		{
			jarFile = new File(codeSource.getLocation().toURI().getPath());
		}
		catch (URISyntaxException e1)
		{
		}
		rootDir = jarFile.getParentFile().getPath();
		int rep = rootDir.contains("/plugins") ? rootDir.indexOf("/plugins") : rootDir.indexOf("\\plugins");
		rootDir = rootDir.substring(0, rep);
	}
	
}

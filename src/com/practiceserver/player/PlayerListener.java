package com.practiceserver.player;

import java.util.HashMap;

import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.practiceserver.database.DataHandler;
import com.practiceserver.enums.Alignment;
import com.practiceserver.enums.Zone;
import com.practiceserver.events.ZoneEnterEvent;
import com.practiceserver.utils.Utils;

public class PlayerListener implements Listener {
	
	public static HashMap<Player, PlayerBossBar> bossbars = new HashMap<Player, PlayerBossBar>();
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		if(!p.hasPlayedBefore()) {
			DataHandler.getInstance().createPlayer(p);
			DataHandler.getInstance().addToSaveData(p);
			DataHandler.getInstance().createMountPlayer(p);
			DataHandler.getInstance().createBank(p);
			p.setMaxHealth(50.0D);
			p.setHealth(50.0D);
		} else {
			p.setMaxHealth(p.getMaxHealth());
			p.setHealth(p.getHealth());
			updateBossBar(p);
		}
		p.setHealthScale(20.0D);
		p.setHealthScaled(true);
		p.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(0);
		
	}
	
	public static void updateBossBar(Player player) {
		if (bossbars.containsKey(player)) {
			bossbars.get(player).update();
			bossbars.get(player).getBossBar().setVisible(true);
		} else {
			PlayerBossBar bar = new PlayerBossBar(player, Utils.colorCodes("&d&lHP &d" + (int) player.getHealth() + "&d&l / &d" + (int) player.getMaxHealth()), BarFlag.CREATE_FOG, BarStyle.SOLID, BarColor.PINK);
			bossbars.put(player, bar);
			bossbars.get(player).update();
			bossbars.get(player).getBossBar().setVisible(true);
		}
		
	}
	 
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Player player = e.getPlayer();
		if (bossbars.containsKey(player)) {
			bossbars.get(player).remove();
			bossbars.remove(player);
			
			
		}
	}
	
	
	//@EventHandler
	public void bypassArmor(EntityDamageEvent e) {
		e.setDamage(e.getDamage());
	}
	
	@EventHandler
	public void onZoneEnter(ZoneEnterEvent e) {
		PracticeServerPlayer p = e.getPracticeServerPlayer();
		if (e.getNewZone() == Zone.CHAOTIC) {
			p.msg("&c                &l*** CHAOTIC ZONE (PVP-ON) ***");
            p.playSound(Sound.ENTITY_WITHER_SHOOT, 0.25F, 0.30F);
            return;
		}
		if (e.getNewZone() == Zone.SAFE) {
			if (p.getAlignment() == Alignment.CHAOTIC) {
				e.setCancelled(true);
				p.msg("&cYou &ncannot&c enter &lNON-PVP &czones with a chaotic alignment.");
				return;
			} else if (p.isCombat()) {
				e.setCancelled(true);
				p.msg("&cYou &ncannot&c leave a chaotic zone while in combat.");
				p.msg("&7Out of combat in: &l" + p.getCombatTime() + "s");
				return;
				
			} else {
				p.msg("&a                &l*** SAFE ZONE (DMG-OFF) ***");
				p.playSound(Sound.ENTITY_WITHER_SHOOT, 0.25F, 0.30F);
				return;
			}
		}
		if (e.getNewZone() == Zone.WILDERNESS) {
			if (p.getAlignment() == Alignment.CHAOTIC) {
				e.setCancelled(true);
				p.msg("&cYou &ncannot&c enter &lNON-PVP &czones with a chaotic alignment.");
				return;
			} else if (p.isCombat()) {
				e.setCancelled(true);
				p.msg("&cYou &ncannot&c leave a chaotic zone while in combat.");
				p.msg("&7Out of combat in: &l" + p.getCombatTime() + "s");
				return;
			} else {
				p.msg("&e                &l*** WILDERNESS (MOBS-ON, PVP-OFF) ***");
				p.playSound(Sound.ENTITY_WITHER_SHOOT, 0.25F, 0.30F);
				return;
			}
		}
	}
}

package com.practiceserver.player;

import org.bukkit.entity.Player;

public class PlayerToggles {
	
	private Player player;
	private boolean togglepvp;
	private boolean togglechaos;
	private boolean togglefilter;
	private boolean toggledebug;
	
	public PlayerToggles(Player player) {
		this.player = player;
		this.togglepvp = false;
		this.toggledebug = true;
		this.togglechaos = true;
		this.togglefilter = false;
	}
	
	public Player getPlayer() {
		return player;
	}

	public static PlayerToggles getPlayerToggles(Player p) {
		return new PlayerToggles(p);
	}

	public PracticeServerPlayer getPracticeServerPlayer() {
		return PracticeServerPlayer.getPracticeServerPlayer(player);
		
	}

	public void setToggleDebug(boolean b) {
		this.toggledebug = b;
		
	}

	public boolean isToggleDebug() {
		return toggledebug;
	}

	public void setToggleChaos(boolean b) {
		this.togglechaos = b;
		
	}

	public boolean isToggleChaos() {
		return togglechaos;
	}

	public boolean isTogglePvP() {
		return togglepvp;
	}

	public void setTogglePvP(boolean b) {
		this.togglepvp = b;
		
	}

	public boolean isToggleFilter() {
		return togglefilter;
	}

	public void setToggleFilter(boolean b) {
		this.togglefilter = b;
		
	}
	

}

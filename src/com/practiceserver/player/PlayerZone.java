package com.practiceserver.player;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.practiceserver.enums.Zone;
import com.practiceserver.utils.RegionUtils;

public class PlayerZone {

    private Player player;
    private Zone zone;
    private Location location;

    public PlayerZone(Player player) {
        this.player = player;
        this.location = player.getLocation();
        this.zone = RegionUtils.getZone(location);
    }

    public static PlayerZone getPlayerZone(Player p) {
        return new PlayerZone(p);
    }

    public Player getPlayer() {
        return player;
    }

    public Zone getZone() {
        return zone;
    }

    public Location getLocation() {
        return location;
    }
}

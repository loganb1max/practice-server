package com.practiceserver.player;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.entity.Player;

import com.practiceserver.banks.PlayerBank;
import com.practiceserver.database.DataHandler;
import com.practiceserver.enums.Alignment;
import com.practiceserver.enums.Zone;
import com.practiceserver.utils.Utils;

public class PracticeServerPlayer {

	private Player player;
	private String uuid;
	private Alignment alignment;
	private int alignmentTime;
	private Zone zone;
	private String shard;
	private PlayerBossBar bar;
	private int gems;
	
	public PracticeServerPlayer(Player player) {
		this.player = player;
		this.uuid = getPlayerUUID().getUUID();
		this.alignment = PlayerAlignment.getPlayerAlignment(player).getAlignment();
		this.alignmentTime = PlayerAlignment.getPlayerAlignment(player).getAlignmentTime();
		this.zone = PlayerZone.getPlayerZone(player).getZone();
		this.shard = DataHandler.getInstance().getShard(player);
		this.gems = DataHandler.getInstance().getGems(player);
		this.bar = new PlayerBossBar(player, Utils.colorCodes("&d&lHP &d" + (int) player.getHealth() + "&d&l / &d" + (int) player.getMaxHealth()), BarFlag.CREATE_FOG, BarStyle.SOLID, BarColor.PINK);
		
	}
	
	public void setShard(String shard) {
		this.shard = shard;
		DataHandler.getInstance().setShard(player, shard);
	}
	
	public PlayerBank getPlayerBank() {
		return PlayerBank.getPlayerBank(player);
	}
	
	public PlayerBossBar getBossBar() {
		return bar;
	}
	
	public int getGems() {
		return gems;
	}
	
	public String getShard() {
		return shard;
	}
	
	public static PracticeServerPlayer getPracticeServerPlayer(Player player) {
		return new PracticeServerPlayer(player);
	}
	
	public Player getPlayer() {//
		return player;
	}
	
	public PlayerUUID getPlayerUUID() {
		return PlayerUUID.getPlayerUUID(player);
	}
	
	public String getUUID() {
		return uuid;
	}
	
	public Alignment getAlignment() {
		return alignment;
	}
	
	public int getAlignmentTime() {
		return alignmentTime;
	}
	
	public Zone getZone() {
		return zone;
	}
	
	public void setAlignment(Alignment alignment) {
		PlayerAlignment.getPlayerAlignment(player).setAlignment(alignment);
	}
	
	public void setAlignmentTime(int time) {
		PlayerAlignment.getPlayerAlignment(player).setAlignmentTime(time);
	}
	
	public void setCombat(boolean bool) {
		DataHandler.getInstance().setCombat(player, bool);
	}
	
	public void setCombatTime(int time) {
		DataHandler.getInstance().setCombatTime(player, time);
	}
	
	public void msg(String msg) {
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
	}

	public void playSound(Sound sound, float f, float g) {
		player.playSound(player.getLocation(), sound, f, g);
	}

	public boolean isCombat() {
		return DataHandler.getInstance().isCombat(player);
	}

	public int getCombatTime() {
		return DataHandler.getInstance().getCombatTime(player);
	}

	public boolean isTogglePvP() {
		return PlayerToggles.getPlayerToggles(player).isTogglePvP();
	}

	public boolean isToggleDebug() {
		return PlayerToggles.getPlayerToggles(player).isToggleDebug();
	}

	public boolean isToggleChaos() {
		return PlayerToggles.getPlayerToggles(player).isToggleChaos();
	}
}

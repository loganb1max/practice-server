package com.practiceserver.player;

import org.bukkit.entity.Player;

import com.practiceserver.database.DataHandler;
import com.practiceserver.enums.Alignment;

public class PlayerAlignment {
	
	private Player player;
	private Alignment alignment;
	private int alignmentTime;
	
	public PlayerAlignment(Player player) {
		this.player = player;
		this.alignment = DataHandler.getInstance().getAlignment(player);
		this.alignmentTime = DataHandler.getInstance().getAlignmentTime(player);
		
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public static PlayerAlignment getPlayerAlignment(Player player) {
		return new PlayerAlignment(player);
	}
	
	public Alignment getAlignment() {
		return alignment;
	}
	
	public int getAlignmentTime() {
		return alignmentTime;
	}
	
	public void setAlignment(Alignment alignment) {
		DataHandler.getInstance().setAlignment(player, alignment);
		if (alignment == Alignment.CHAOTIC) {
			if (getAlignment() == Alignment.CHAOTIC) {
				player.sendMessage("�cLAWFUL player slain, �l+300s �cadded to Chaotic timer");
				return;
			} else {
				player.sendMessage("          �c* YOU ARE NOW �lCHAOTIC �cALIGNMENT *");
				player.sendMessage("�7If you are killed while chaotic, you will lose enerything");
				player.sendMessage("�7in your inventory. Chaotic alignment will expire 5 minutes after");
				player.sendMessage("�7your last player kill.");
				player.sendMessage("          �c* YOU ARE NOW �lCHAOTIC �cALIGNMENT *");
				player.sendMessage("�cLAWFUL player slain, �l+300s �cadded to Chaotic timer");
				return;
			}
		}
		if (alignment == Alignment.NEUTRAL) {
			if (getAlignment() == Alignment.NEUTRAL) {
				return;
			} else {
				player.sendMessage("          �e* YOU ARE NOW �e�lNEUTRAL �eALIGNMENT *");
				player.sendMessage("�7While neutral, players who kill you will not become chaotic. You");
				player.sendMessage("�7have a 50% chance of dropping your weapon, and a 25%");
				player.sendMessage("�7chance of dropping each piece of equiped armor on death.");
				player.sendMessage("�7Neutral alignment will expire 1 minute after last hit on player.");
				player.sendMessage("          �e* YOU ARE NOW �e�lNEUTRAL �eALIGNMENT *");
				return;
			}
		}
		if (alignment == Alignment.LAWFUL) {
			player.sendMessage("          �a* YOU ARE NOW �lLAWFUL �aALIGNMENT *");
			player.sendMessage("�7While lawful, you will not lose any equiped armor on death.");
			player.sendMessage("�7Any players who kill you while you're lawfully aligned will");
			player.sendMessage("�7become chaotic.");
			player.sendMessage("          �a* YOU ARE NOW �lLAWFUL �aALIGNMENT *");
			return;
		}
	}
	
	public void setAlignmentTime(int time) {
		DataHandler.getInstance().setAlignmentTime(player, time);
	}
	
	public void tick() {
		PracticeServerPlayer p = PracticeServerPlayer.getPracticeServerPlayer(player);
		if (p.isCombat()) {
			if (p.getCombatTime() <= 0) {
				p.setCombat(false);
				p.setCombatTime(0);
			} else {
				p.setCombatTime((p.getCombatTime() - 1));
			}
		}
		if (p.getAlignment() == Alignment.CHAOTIC) {
			if (p.getAlignmentTime() <= 0) {
				p.setAlignment(Alignment.NEUTRAL);
				p.setAlignmentTime(Alignment.NEUTRAL.getDefaultTime());
			} else {
				p.setAlignmentTime((p.getAlignmentTime() - 1));
			}
		}
		if (p.getAlignment() == Alignment.NEUTRAL) {
			if (p.getAlignmentTime() <= 0) {
				p.setAlignment(Alignment.LAWFUL);
				p.setAlignmentTime(Alignment.LAWFUL.getDefaultTime());
			} else {
				p.setAlignmentTime((p.getAlignmentTime() - 1));
			}
		}
	}


}

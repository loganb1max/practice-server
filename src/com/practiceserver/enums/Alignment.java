package com.practiceserver.enums;

import org.bukkit.ChatColor;

public enum Alignment {
	
	LAWFUL(0, ChatColor.GREEN),
	NEUTRAL(60, ChatColor.YELLOW),
	CHAOTIC(300, ChatColor.RED);
	
	
	private int time;
	private ChatColor color;
	
	Alignment(int time, ChatColor color) {
		this.time = time;
		this.color = color;
	}
	
	public int getDefaultTime() {
		return this.time;
	}//
	
	public ChatColor getChatColor() {
		return this.color;
	}
	
	public ChatColor getTabNameColor() {
		if (this == Alignment.LAWFUL) return ChatColor.WHITE;
		return getChatColor(); 
	}

}

package com.practiceserver.enums;

public enum Zone {
	
	SAFE(false, false),
	WILDERNESS(false, true),
	CHAOTIC(true, true);
	
	private boolean pvp;
	private boolean mobdamage;
	
	Zone(boolean pvp, boolean mobdamage) {
		this.pvp = pvp;
		this.mobdamage = mobdamage;
	}
	
	public boolean isPvPAllowed() {
		return this.pvp;
	}
	
	public boolean isMobDamageAllowed() {
		return this.mobdamage;
	}
	
	

}
//
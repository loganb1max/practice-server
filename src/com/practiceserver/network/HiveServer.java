package com.practiceserver.network;

import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class HiveServer {
	
	public static void send8008Packet(String input, String server_ip, boolean all) {
		
		Socket kkSocket = null;
		PrintWriter out = null;
		
		List<String> servers = new ArrayList<String>();
		if(server_ip != null) {
			servers.add(server_ip);
		}
		if(all) {
			for(String s : NetworkHandler.server_list.values()) {
				servers.add(s);
			}
		}
		
		for(String s : servers) {
			// s == IP of server.a
			try {
				try {
					kkSocket = new Socket();
                    kkSocket.connect(new InetSocketAddress(s.contains(":") ? s.split(":")[0] : s, 8008), 2500);
					out = new PrintWriter(kkSocket.getOutputStream(), true);
				} catch(SocketTimeoutException e) {
					e.printStackTrace();
					continue;
				}
				
				out.println(input);
				out.close();
				kkSocket.close();
			} catch(Exception e) {
				e.printStackTrace();
				continue;
			}
		}
	}

}

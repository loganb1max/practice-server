package com.practiceserver.network;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;

import org.bukkit.Bukkit;

import com.practiceserver.database.DataHandler;

public class ListenThread extends Thread {
	InetAddress lAddress;
	
	@SuppressWarnings("resource")
	public void run() {
		ServerSocket ss;
		int port = NetworkHandler.server_list.get(DataHandler.getServerNum()).contains(":") ? Integer.parseInt(NetworkHandler.server_list
                .get(DataHandler.getServerNum()).split(":")[1])
                : com.practiceserver.database.Config.getTransferPort();
		try {
			
			lAddress = InetAddress.getByName(Bukkit.getIp()); //Bukkit.getIp()
            ss = new ServerSocket(port, 25605, lAddress);
           // CommunityMechanics.log.info("[CommunityMechanics] LISTENING on port " + port + " @ " + Bukkit.getIp() + " ...");
            Bukkit.getLogger().log(Level.INFO, "[CommunityMechanics] LISTENING on port " + port + " @ " + Bukkit.getIp() + " ...");
			
			while(true) {
				final Socket clientSocket = ss.accept();
				String ip = clientSocket.getInetAddress().getHostAddress();
				
				/*if(!(NetworkHandler.ip_whitelist.contains(ip))) {
					//CommunityMechanics.log.info("[CommunityMechanics] Illegal connection on port " + port + " by " + ip + ": " + port);
					Bukkit.getLogger().log(Level.INFO, "[CommunityMechanics] Illegal connection on port " + port + " by " + ip + ": " + port);
					clientSocket.close();
					continue;
				}*/
				
				Thread process = new Thread(new ConnectProtocol(clientSocket, ip));
				process.start();
			}
		} catch(IOException e) {
			e.printStackTrace();
			//CommunityMechanics.log.info("Could not listen on port: " + port);
			Bukkit.getLogger().log(Level.INFO, "Could not listen on port: " + port);
			return;
		}
	}
	
}

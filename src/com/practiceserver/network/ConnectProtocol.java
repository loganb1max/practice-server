package com.practiceserver.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.practiceserver.player.PracticeServerPlayer;
import com.practiceserver.utils.Utils;



@SuppressWarnings("deprecation")
public class ConnectProtocol implements Runnable {
	
	private Socket clientSocket;
	private String sendingIP;

	public ConnectProtocol(Socket s, String ip) {
		this.clientSocket = s;
		this.sendingIP = ip;
	}

	public static void sendResultCrossServer(String server_ip, String message, int server_num) {
		Socket kkSocket = null;
		PrintWriter out = null;

		try {
			try {
				kkSocket = NetworkHandler.getSocket(server_num);
				out = new PrintWriter(kkSocket.getOutputStream(), true);
			} catch (Exception err) {
                String ipAndPort = server_ip;
                String ipNoPort = ipAndPort.contains(":") ? server_ip.split(":")[0] : ipAndPort;
                int port = ipAndPort.contains(":") && StringUtils.isNumeric(ipAndPort.split(":")[1]) ? Integer.parseInt(ipAndPort
                        .split(":")[1]) : com.practiceserver.database.Config.getTransferPort();
				kkSocket = new Socket();
				kkSocket.connect(new InetSocketAddress(ipNoPort, port), 150);
				out = new PrintWriter(kkSocket.getOutputStream(), true);
			}

			out.println(message);
			out.close();
		} catch (IOException e) {

		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	
	public void run() {

		try {
			// PrintWriter out = new PrintWriter(clientSocket.getOutputStream(),
			// true);
			BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			String inputLine;
			List<String> receivedData = new ArrayList<String>();

			while ((inputLine = in.readLine()) != null) {
			    receivedData.add(inputLine);

				if (inputLine.startsWith("@date_update@")) {
					long time = Long.parseLong(inputLine.substring(inputLine.lastIndexOf("@") + 1, inputLine.length()));
					Runtime.getRuntime().exec("date -s @" + time);
				}

			
				if (inputLine.startsWith("[sayall]")) {
					String player_string = inputLine.substring(inputLine.indexOf("]") + 1, inputLine.indexOf("@"));
					String from_server = inputLine.substring(inputLine.indexOf("@") + 1, inputLine.indexOf(":"));
					String msg = inputLine.substring(inputLine.indexOf(":") + 1, inputLine.length());
					for (Player pl : Bukkit.getOnlinePlayers()) {
						String personal_msg = msg;
			            if (personal_msg.endsWith(" ")) {
			                personal_msg = personal_msg.substring(0, personal_msg.length() - 1);
			            }
			            personal_msg = "&b" + personal_msg;
						pl.sendMessage(Utils.colorCodes("&b&l>> &b" + personal_msg + "&b-" + player_string));
						return;
					}
				}
				if (inputLine.startsWith("[msg]")) {
					 //"[sayall]matt11matthew@1:test"
					//"[msg]to:matt11matthew,from:matt11matthew,msg:Hello@1:test"
					String from_player_string = inputLine.substring(inputLine.indexOf("from:") + 1, inputLine.indexOf(","));
					String to_player_string = inputLine.substring(inputLine.indexOf("]") + 1, inputLine.indexOf(","));			
					String from_server = inputLine.substring(inputLine.indexOf("@") + 1, inputLine.indexOf(":"));
					String msg = inputLine.substring(inputLine.indexOf("msg:") + 1, inputLine.indexOf("@"));
					if (Bukkit.getPlayer(to_player_string) != null) {
						Player p_to = Bukkit.getPlayer(to_player_string);
						if (Bukkit.getPlayer(from_player_string) != null) {
							Player p_from = Bukkit.getPlayer(from_player_string);
							if (Utils.getShardNum(p_from) == Utils.getShardNum(p_to)) { 
								PracticeServerPlayer.getPracticeServerPlayer(p_from).msg("&8&lTO &7" + p_to.getName() + ":&f " + msg);
								PracticeServerPlayer.getPracticeServerPlayer(p_to).msg("&8&lFROM &7" + p_from.getName() + ":&f " + msg);
								p_to.playSound(p_to.getLocation(), Sound.ENTITY_CHICKEN_EGG, 2F, 1.2F);
							} else {
								PracticeServerPlayer.getPracticeServerPlayer(p_from).msg("&8&lTO (" + PracticeServerPlayer.getPracticeServerPlayer(p_to).getShard() + ") &7" + p_to.getName() + ":&f " + msg);
								PracticeServerPlayer.getPracticeServerPlayer(p_to).msg("&8&lFROM (" + PracticeServerPlayer.getPracticeServerPlayer(p_from).getShard() + ") &7" + p_from.getName() + ":&f " + msg);
								p_to.playSound(p_to.getLocation(), Sound.ENTITY_CHICKEN_EGG, 2F, 1.2F);
							}
						} 
					} else {
						Player p_from = Bukkit.getPlayer(from_player_string);
						PracticeServerPlayer.getPracticeServerPlayer(p_from).msg("&c&l" + to_player_string + "&c is OFFLINE");
						in.close();
					}
				}
				if (inputLine.startsWith("[sc]")) {
					String player_string = inputLine.substring(inputLine.indexOf("]") + 1, inputLine.indexOf("@"));
					String from_server = inputLine.substring(inputLine.indexOf("@") + 1, inputLine.indexOf(":"));
					String msg = inputLine.substring(inputLine.indexOf(":") + 1, inputLine.length());
					for (Player pl : Bukkit.getOnlinePlayers()) {
						if (!pl.isOp()) return;
						String personal_msg = msg;
			            if (personal_msg.endsWith(" ")) {
			                personal_msg = personal_msg.substring(0, personal_msg.length() - 1);
			            }
			            if (Utils.getShardNum(pl) == Integer.parseInt(from_server)) {
			            	PracticeServerPlayer.getPracticeServerPlayer(pl).msg("&6<&lSC&6> &7" + player_string + ": &f" + msg);
			            	return;
			            } else {
			            	PracticeServerPlayer.getPracticeServerPlayer(pl).msg("&6<&lSC&6> &8&l(US-" + from_server + ") &7" + player_string + ": &f" + msg);
			            	return;
			            }
					}
				}
			}
			int line = 0;
			JsonBuilder data = new JsonBuilder("receiving_server", Utils.getShard()).setData("sending_server", sendingIP);

			for (String dataLine : receivedData) {
			    line++;
			    data.setData("line_" + String.valueOf(line), dataLine);
			}

			//new LogModel(LogType.PACKET, "CONSOLE", data.getJson());
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}
	
	public static void broadcastMessage(String msg, String from_server, String sender_string) {
		for (Player pl : Bukkit.getOnlinePlayers()) {
			String personal_msg = msg;
            if (personal_msg.endsWith(" ")) {
                personal_msg = personal_msg.substring(0, personal_msg.length() - 1);
            }
            personal_msg = "&b" + personal_msg;
			pl.sendMessage(Utils.colorCodes("&b&l>> &b" + personal_msg + "&b-" + sender_string));
			return;
		}
	}
	
	public static void broadcastMessageSC(String msg, String from_server, String sender_string) {
		for (Player pl : Bukkit.getOnlinePlayers()) {
			if (!pl.isOp()) return;
			String personal_msg = msg;
            if (personal_msg.endsWith(" ")) {
                personal_msg = personal_msg.substring(0, personal_msg.length() - 1);
            }
            if (Utils.getShardNum(pl) == Integer.parseInt(from_server)) {
            	PracticeServerPlayer.getPracticeServerPlayer(pl).msg("&6<&lSC&6> &7" + sender_string + ": &f" + msg);
            	return;
            } else {
            	PracticeServerPlayer.getPracticeServerPlayer(pl).msg("&6<&lSC&6> &8&l(US-" + from_server + ") &7" + sender_string + ": &f" + msg);
            	return;
            }
		}
	}
}

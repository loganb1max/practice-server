package com.practiceserver.network;

import java.util.List;
import java.util.Map.Entry;

public class CrossServerPacketThread extends Thread {
	
	public void run() {
		while(true) {
			try {
				Thread.sleep(250);
			} catch(Exception err) {}
			for(Entry<String, List<Object>> edata : NetworkHandler.social_query_list.entrySet()) {
				List<Object> data = edata.getValue();
				// NetworkHandler.log.info("d2 - " + data.toString());
				String query = (String) data.get(0);
				String p_name = (String) data.get(1); // Will just be null if all_servers=true
				boolean all_servers = (boolean) data.get(2);
				int server_num = -1;
				if(all_servers == false) {
					server_num = NetworkHandler.getPlayerServer(p_name, true);
				}
				NetworkHandler.sendPacketCrossServer(query, server_num, all_servers);
				NetworkHandler.social_query_list.remove(edata.getKey());
			}
			
		}
	}
}

package com.practiceserver.network;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;

import com.practiceserver.database.Config;
import com.practiceserver.database.DataHandler;

public class NetworkHandler {
	
	
	public static volatile CopyOnWriteArrayList<String> socket_list = new CopyOnWriteArrayList<String>();
	public static HashMap<Integer, String> server_list = new HashMap<Integer, String>();
	// Server #, Server IP

	public static HashMap<Integer, Socket> sock_list = new HashMap<Integer, Socket>();
	public static volatile ConcurrentHashMap<String, List<Object>> social_query_list = new ConcurrentHashMap<String, List<Object>>();
	public static List<String> ip_whitelist = new ArrayList<String>();

	public static List<String> toggle_map = new ArrayList<String>();
	// A map used to cycle through when generating toggle menu of /toggle.

	
	Thread ConnectProtocol;
	static // Controls socket handling.

	Thread CrossServerPacketThread;
	
	public static Thread message_listener;
	
	
	public static void setup() {
		server_list.put(1, "127.0.0.1:25601");
		server_list.put(2, "127.0.0.1:25602");
		for (String s : server_list.values()) {
			ip_whitelist.add(s);
		}
		CrossServerPacketThread = new CrossServerPacketThread();
		CrossServerPacketThread.start();
		message_listener = new ListenThread();
		message_listener.start();
		
	}
	
	public static int getPlayerServer(String p, boolean refresh) {
		return DataHandler.getInstance().getServer(Bukkit.getPlayer(p));
		
	}
	
	public static Socket getSocket(int server_num) {
		Socket return_socket = sock_list.get(server_num);
		int delay = 200;
		if (return_socket == null || return_socket.isClosed()) {
			try {
				/*
				* if(server_num > 1000 && server_num < 3000){ delay = 3000; }
				*/
			    String ipAndPort = server_list.get(server_num);
                String ipNoPort = ipAndPort.contains(":") ? server_list.get(server_num).split(":")[0]
                        : ipAndPort;
                int port = ipAndPort.contains(":") && StringUtils.isNumeric(ipAndPort.split(":")[1]) ? Integer.parseInt(ipAndPort
                        .split(":")[1]) : Config.getTransferPort();
				Socket s = new Socket();
				// s.bind(new InetSocketAddress(Hive.local_IP,
				// Hive.transfer_port+1));
				s.connect(new InetSocketAddress(ipNoPort, port), delay);
				return s;
			} catch (IOException e) {
				// e.printStackTrace(); Worthless spam for dead servers.
			}
		}
		return return_socket;
	}

	public void refreshSockets() {
		// DEPRECIATED.
	}

	// @server_num@p_name:server_num
	public static void sendPacketCrossServer(String packet_data, int server_num, boolean all_servers) {
		Socket kkSocket = null;
		PrintWriter out = null;

		if (all_servers) {
			for (int sn : NetworkHandler.server_list.keySet()) {
			    String ipAndPort = NetworkHandler.server_list.get(sn);
                String ipNoPort = ipAndPort.contains(":") ? NetworkHandler.server_list.get(sn).split(":")[0]
                        : ipAndPort;
                int port = ipAndPort.contains(":") && StringUtils.isNumeric(ipAndPort.split(":")[1]) ? Integer.parseInt(ipAndPort
                        .split(":")[1]) : Config.getTransferPort();
                if (sn == DataHandler.getServerNum()) {
					continue; // Don't send to same server.
				}
				try {
					kkSocket = new Socket();
					// kkSocket.bind(new InetSocketAddress(Hive.local_IP,
					// Hive.transfer_port+1));
					kkSocket.connect(new InetSocketAddress(ipNoPort, port), 100);
					out = new PrintWriter(kkSocket.getOutputStream(), true);

					out.println(packet_data);

				} catch (IOException e) {
					if (out != null) {
						out.close();
					}
					continue;
				}

				if (out != null) {
					out.close();
				}
			}
		} else if (!all_servers) {
			try {
			    String ipAndPort = NetworkHandler.server_list.get(server_num);
                String ipNoPort = ipAndPort.contains(":") ? NetworkHandler.server_list.get(server_num).split(":")[0]
                        : ipAndPort;
                int port = ipAndPort.contains(":") && StringUtils.isNumeric(ipAndPort.split(":")[1]) ? Integer.parseInt(ipAndPort
                        .split(":")[1]) : Config.getTransferPort();
				kkSocket = new Socket();
				// kkSocket.bind(new InetSocketAddress(Hive.local_IP,
				// Hive.transfer_port+1));
				kkSocket.connect(new InetSocketAddress(ipNoPort, port), 100);
				out = new PrintWriter(kkSocket.getOutputStream(), true);

				out.println(packet_data);

			} catch (IOException e) {

			} finally {
				if (out != null) {
					out.close();
				}
			}

			if (out != null) {
				out.close();
			}
		}
	}

	public static void sendPacketCrossServer(String packet_data, String ip) {
		Socket kkSocket = null;
		PrintWriter out = null;

		try {
			kkSocket = new Socket();
			// kkSocket.bind(new InetSocketAddress(Hive.local_IP,
			// Hive.transfer_port+1));
			String ipAndPort = ip;
            String ipNoPort = ipAndPort.contains(":") ? ip.split(":")[0]
                    : ipAndPort;
            int port = ipAndPort.contains(":") && StringUtils.isNumeric(ipAndPort.split(":")[1]) ? Integer.parseInt(ipAndPort
                    .split(":")[1]) : Config.getTransferPort();
            kkSocket.connect(new InetSocketAddress(ipNoPort, port), 100);
			out = new PrintWriter(kkSocket.getOutputStream(), true);

			out.println(packet_data);
			kkSocket.close();
		} catch (IOException e) {

		} finally {
			if (out != null) {
				out.close();
			}
		}

		if (out != null) {
			out.close();
		}
	}
	

}

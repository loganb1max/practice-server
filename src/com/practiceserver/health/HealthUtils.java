package com.practiceserver.health;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

/*
Copyright � matt11matthew 2016
*/

public class HealthUtils {
	
	private static HealthUtils instance = new HealthUtils();

	public static HealthUtils getInstance() {
		return instance;
	}
	
	public int getMinValueFromLore(ItemStack item, String value)
	  {
	    int returnVal = 1;
	    ItemMeta meta = item.getItemMeta();
	    try
	    {
	      List<?> lore = meta.getLore();
	      if ((lore != null) && (((String)lore.get(0)).contains(value)))
	      {
	        String vals = ((String)lore.get(0)).split(": ")[1];
	        vals = ChatColor.stripColor(vals);
	        vals = vals.split(" - ")[0];
	        returnVal = Integer.parseInt(vals.trim());
	      }//
	    }
	    catch (Exception localException) {}
	    return returnVal;
	  }
	  
	public int getMaxValueFromLore(ItemStack item, String value)
	  {
	    int returnVal = 1;
	    ItemMeta meta = item.getItemMeta();
	    try
	    {
	      List<?> lore = meta.getLore();
	      if ((lore != null) && (((String)lore.get(0)).contains(value)))
	      {
	        String vals = ((String)lore.get(0)).split(": ")[1];
	        vals = ChatColor.stripColor(vals);
	        vals = vals.split(" - ")[1];
	        returnVal = Integer.parseInt(vals.trim());
	      }
	    }
	    catch (Exception localException) {}
	    return returnVal;
	  }

	public int getLifestealFromLore(ItemStack item, String value)
	  {
	    int returnVal = 0;
	    ItemMeta meta = item.getItemMeta();
	    try
	    {
	      List<?> lore = meta.getLore();
	      if (lore != null) {
	        for (int i = 0; i < lore.size(); i++) {
	          if (((String)lore.get(i)).contains(value))
	          {
	            String vals = ((String)lore.get(i)).split(": ")[1];
	            vals = ChatColor.stripColor(vals);
	            vals = vals.replace("%", "").trim().toString();
	            returnVal = Integer.parseInt(vals.trim());
	          }
	        }
	      }
	    }
	    catch (Exception localException) {}
	    return returnVal;
	  }
	  
	public static int getVitFromLore(ItemStack item, String value)

	  {
	    int returnVal = 0;
	    ItemMeta meta = item.getItemMeta();
	    try
	    {
	      List<?> lore = meta.getLore();
	      if ((lore != null) && (((String)lore.get(2)).contains(value)))
	      {
	        String vals = ((String)lore.get(2)).split(": +")[1];
	        vals = ChatColor.stripColor(vals);
	        returnVal = Integer.parseInt(vals.trim());
	      }
	    }
	    catch (Exception localException) {}
	  return returnVal;
	  }
	
	public int getElemFromLore(ItemStack item, String value)
	  {
	    int returnVal = 0;
	    ItemMeta meta = item.getItemMeta();
	    try
	    {
	      List<?> lore = meta.getLore();
	      if (lore != null) {
	        for (int i = 0; i < lore.size(); i++) {
	          if (((String)lore.get(i)).contains(value))
	          {
	            String vals = ((String)lore.get(i)).split(": +")[1];
	            vals = ChatColor.stripColor(vals);
	            returnVal = Integer.parseInt(vals.trim());
	          }
	        }
	      }
	    }
	    catch (Exception localException) {}
	    return returnVal;
	  }
	
	public int getValueFromLore(ItemStack item, String value, String value2, String x){
	    int returnVal = 0;
	    ItemMeta meta = item.getItemMeta();
	    try
	    {
	      List<?> lore = meta.getLore();
	      if (lore != null) {
	        for (int i = 0; i < lore.size(); i++) {
	          if (((String)lore.get(i)).contains(value))
	          {
	            String vals = ((String)lore.get(i)).split(": "+x+"")[1];
	            vals = vals.replaceAll(value2, "");
	            vals = ChatColor.stripColor(vals);
	            returnVal = Integer.parseInt(vals.trim());
	          }
	        }
	      }
	    }
	    catch (Exception localException) {}
	    return returnVal;
	  }
	
	public double getHPFromItem(ItemStack item) {
		if(item.getItemMeta().hasLore())return getValueFromLore(item, "HP", "", "+");
		return 0;
	}
	
	public double getVitFromItem(ItemStack item) {
		if(item.getItemMeta().hasLore()) {
			return getValueFromLore(item, "VIT", "", "+");
		}
		return 0;
	}
	
	public double getStrFromItem(ItemStack item) {
		if(item.getItemMeta().hasLore())return getValueFromLore(item, "STR", "", "+");
		return 0;
	}
	
	public double getIntFromItem(ItemStack item) {
		if(item.getItemMeta().hasLore())return getValueFromLore(item, "INT", "", "+");
		return 0;
	}
	
	public double getDexFromItem(ItemStack item) {
		if(item.getItemMeta().hasLore())return getValueFromLore(item, "DEX", "", "+");
		return 0;
	}
	
	public double getBlockFromItem(ItemStack item) {
		if(item.getItemMeta().hasLore())return getValueFromLore(item, "BLOCK", "%", "");
		return 0;
	}
	
	public double getDodgeFromItem(ItemStack item) {
		if(item.getItemMeta().hasLore())return getValueFromLore(item, "DODGE", "%", "");
		return 0;
	}
	
	public double getReflectFromItem(ItemStack item) {
		if(item.getItemMeta().hasLore())return getValueFromLore(item, "REFLECTION", "%", "");
		return 0;
	}
	
	public double getItemFindFromItem(ItemStack item) {
		if(item.getItemMeta().hasLore())return getValueFromLore(item, "ITEM FIND", "%", "");
		return 0;
	}
	
	public double getGemFindFromItem(ItemStack item) {
		if(item.getItemMeta().hasLore())return getValueFromLore(item, "GEM FIND", "%", "");
		return 0;
	}
	
	public double getHPsFromItem(ItemStack item) {
		if(item.getItemMeta().hasLore())return getValueFromLore(item, "HP REGEN", " HP/s", "");
		return 0;
	}
	
	public int getPriceFromItem(ItemStack item) {
		if(item.getItemMeta().hasLore())return getValueFromLore(item, "Price", "g", "");
		return 0;
	}
	
	public double getVIT(Player p) {
		double vit = 0.0D;
		PlayerInventory i = p.getInventory();
		if ((i.getHelmet() != null)) {
			vit += HealthUtils.getInstance().getVitFromItem(i.getHelmet());
		}
		if ((i.getChestplate() != null)) {
			vit += HealthUtils.getInstance().getVitFromItem(i.getChestplate());
		}
		if ((i.getLeggings() != null)) {
			vit += HealthUtils.getInstance().getVitFromItem(i.getLeggings());
		}
		if ((i.getBoots() != null)) {
			vit += HealthUtils.getInstance().getVitFromItem(i.getBoots());
		}
		if((i.getItem(0) != null)) {
			vit += HealthUtils.getInstance().getVitFromItem(i.getItem(0));
		}
		if(vit > 0.0D) {
			return vit;
		} else {
			return 0.0D;
		}
	}
	
	public double getDodge1(Player p) {
		double vit = 0.0D;
		PlayerInventory i = p.getInventory();
		if ((i.getHelmet() != null)) {
			vit += HealthUtils.getInstance().getDodgeFromItem(i.getHelmet());
		}
		if ((i.getChestplate() != null)) {
			vit += HealthUtils.getInstance().getDodgeFromItem(i.getChestplate());
		}
		if ((i.getLeggings() != null)) {
			vit += HealthUtils.getInstance().getDodgeFromItem(i.getLeggings());
		}
		if ((i.getBoots() != null)) {
			vit += HealthUtils.getInstance().getDodgeFromItem(i.getBoots());
		}
		if(vit > 0.0D) {
			return vit;
		} else {
			return 0.0D;
		}
	}
	
	public double getBlock1(Player p) {
		double vit = 0.0D;
		PlayerInventory i = p.getInventory();
		if ((i.getHelmet() != null)) {
			vit += HealthUtils.getInstance().getBlockFromItem(i.getHelmet());
		}
		if ((i.getChestplate() != null)) {
			vit += HealthUtils.getInstance().getBlockFromItem(i.getChestplate());
		}
		if ((i.getLeggings() != null)) {
			vit += HealthUtils.getInstance().getBlockFromItem(i.getLeggings());
		}
		if ((i.getBoots() != null)) {
			vit += HealthUtils.getInstance().getBlockFromItem(i.getBoots());
		}
		if(vit > 0.0D) {
			return vit;
		} else {
			return 0.0D;
		}
	}
	
	public double getItemFind(Player p) {
		double vit = 0.0D;
		PlayerInventory i = p.getInventory();
		if ((i.getHelmet() != null)) {
			vit += HealthUtils.getInstance().getItemFindFromItem(i.getHelmet());
		}
		if ((i.getChestplate() != null)) {
			vit += HealthUtils.getInstance().getItemFindFromItem(i.getChestplate());
		}
		if ((i.getLeggings() != null)) {
			vit += HealthUtils.getInstance().getItemFindFromItem(i.getLeggings());
		}
		if ((i.getBoots() != null)) {
			vit += HealthUtils.getInstance().getItemFindFromItem(i.getBoots());
		}
		if(vit > 0.0D) {
			return vit;
		} else {
			return 0.0D;
		}
	}
	

	
	public double getSTR(Player p) {
		double vit = 0.0D;
		PlayerInventory i = p.getInventory();
		if ((i.getHelmet() != null)) {
			vit += HealthUtils.getInstance().getStrFromItem(i.getHelmet());
		}
		if ((i.getChestplate() != null)) {
			vit += HealthUtils.getInstance().getStrFromItem(i.getChestplate());
		}
		if ((i.getLeggings() != null)) {
			vit += HealthUtils.getInstance().getStrFromItem(i.getLeggings());
		}
		if ((i.getBoots() != null)) {
			vit += HealthUtils.getInstance().getStrFromItem(i.getBoots());
		}
		if((i.getItem(0) != null)) {
			vit += HealthUtils.getInstance().getStrFromItem(i.getItem(0));
		}
		if(vit > 0.0D) {
			return vit;
		} else {
			return 0.0D;
		}
	}
	
	public double getDEX(Player p) {
		double vit = 0.0D;
		PlayerInventory i = p.getInventory();
		if ((i.getHelmet() != null)) {
			vit += HealthUtils.getInstance().getDexFromItem(i.getHelmet());
		}
		if ((i.getChestplate() != null)) {
			vit += HealthUtils.getInstance().getDexFromItem(i.getChestplate());
		}
		if ((i.getLeggings() != null)) {
			vit += HealthUtils.getInstance().getDexFromItem(i.getLeggings());
		}
		if ((i.getBoots() != null)) {
			vit += HealthUtils.getInstance().getDexFromItem(i.getBoots());
		}
		if((i.getItem(0) != null)) {
			vit += HealthUtils.getInstance().getDexFromItem(i.getItem(0));
		}
		if(vit > 0.0D) {
			return vit;
		} else {
			return 0.0D;
		}
	}
	
	public double getDPS(Player p) {
		double vit = 0.0D;
		PlayerInventory i = p.getInventory();
		if ((i.getHelmet() != null)) {
			vit += HealthUtils.getInstance().getDPSFromItem(i.getHelmet());
		}
		if ((i.getChestplate() != null)) {
			vit += HealthUtils.getInstance().getDPSFromItem(i.getChestplate());
		}
		if ((i.getLeggings() != null)) {
			vit += HealthUtils.getInstance().getDPSFromItem(i.getLeggings());
		}
		if ((i.getBoots() != null)) {
			vit += HealthUtils.getInstance().getDPSFromItem(i.getBoots());
		}
		if(vit > 0.0D) {
			return vit;
		} else {
			return 0.0D;
		}
	}
	
	public double getArmor(Player p) {
		double vit = 0.0D;
		PlayerInventory i = p.getInventory();
		if ((i.getHelmet() != null)) {
			vit += HealthUtils.getInstance().getArmorFromItem(i.getHelmet());
		}
		if ((i.getChestplate() != null)) {
			vit += HealthUtils.getInstance().getArmorFromItem(i.getChestplate());
		}
		if ((i.getLeggings() != null)) {
			vit += HealthUtils.getInstance().getArmorFromItem(i.getLeggings());
		}
		if ((i.getBoots() != null)) {
			vit += HealthUtils.getInstance().getArmorFromItem(i.getBoots());
		}
		if(vit > 0.0D) {
			return vit;
		} else {
			return 0.0D;
		}
	}
	
	public double getDPSFromItem(ItemStack i) {
		return getMinValueFromLore(i, "DPS");
	}
	
	public double getArmorFromItem(ItemStack i) {
		return getMinValueFromLore(i, "ARMOR");
	}
	

	public double getINT(Player p) {
		double vit = 0.0D;
		PlayerInventory i = p.getInventory();
		if ((i.getHelmet() != null)) {
			vit += HealthUtils.getInstance().getIntFromItem(i.getHelmet());
		}
		if ((i.getChestplate() != null)) {
			vit += HealthUtils.getInstance().getIntFromItem(i.getChestplate());
		}
		if ((i.getLeggings() != null)) {
			vit += HealthUtils.getInstance().getIntFromItem(i.getLeggings());
		}
		if ((i.getBoots() != null)) {
			vit += HealthUtils.getInstance().getIntFromItem(i.getBoots());
		}
		if((i.getItem(0) != null)) {
			vit += HealthUtils.getInstance().getIntFromItem(i.getItem(0));
		}
		if(vit > 0.0D) {
			return vit;
		} else {
			return 0.0D;
		}
	}
	
	public double getHPsFromVit(Player p) {
		double hps = HealthMechanics.getInstance().getHps(p);
		return hps;
		/*
		double vit = HealthUtils.getInstance().getVIT(p);
		if((vit > 0.0D) && (hps > 0.0D)) {
			double new_hps = (vit * 0.3D) + (hps);
			return new_hps;
		} else {
			return hps;
		}
		*/
	}
	
	public double getBlockFromSTR(Player p) {
		double block = (getSTR(p) * 0.017D);
		return block;
	}

	public double getDodgeFromDEX(Player p) {
		double dodge = (getDEX(p) * 0.03D);
		return dodge;
	}
	
	public double getArmorFromDEX(Player p) {
		double armor = (getDEX(p) * 0.03D);
		return armor;
	}
	
	public double getFinalArmor(Player p) {
		return (getArmorFromDEX(p) + getArmor(p));
	}
	
	public double getFinalDPS(Player p) {
		return getDPS(p);
	}
	
	public double getDodge(Player p) {
		return (getDodgeFromDEX(p) + getDodge1(p));
	}
	
	public double getBlock(Player p) {
		return (getBlockFromSTR(p) + getBlock1(p));
	}
	
	public double getBlodge(Player p) {
		return (getDodgeFromDEX(p) + getBlockFromSTR(p));
	}

	public double getGemFind(Player p) {
		double vit = 0.0D;
		PlayerInventory i = p.getInventory();
		if ((i.getHelmet() != null)) {
			vit += HealthUtils.getInstance().getGemFindFromItem(i.getHelmet());
		}
		if ((i.getChestplate() != null)) {
			vit += HealthUtils.getInstance().getGemFindFromItem(i.getChestplate());
		}
		if ((i.getLeggings() != null)) {
			vit += HealthUtils.getInstance().getGemFindFromItem(i.getLeggings());
		}
		if ((i.getBoots() != null)) {
			vit += HealthUtils.getInstance().getGemFindFromItem(i.getBoots());
		}
		if(vit > 0.0D) {
			return vit;
		} else {
			return 0.0D;
		}
	}
	
 
}


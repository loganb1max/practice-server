package com.practiceserver.health;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import com.practiceserver.Main;
import com.practiceserver.damage.Damage;
import com.practiceserver.mobs.ItemMechanics;
import com.practiceserver.mobs.Mob;


/*
Copyright � matt11matthew 2016
*/

public class HealthMechanics implements Listener {
	
	private static HealthMechanics instance = new HealthMechanics();
	
	public static HealthMechanics getInstance() {
		return instance;
	} 
	
	
	@EventHandler
	public void onRegen(EntityRegainHealthEvent e) {
		e.setCancelled(true);
	}
	
	public void regen(Player p) {
		double hps = HealthUtils.getInstance().getHPsFromVit(p);
		if(Damage.tagged.contains(p))return;
		if(p.getHealth() + hps > p.getMaxHealth()) {
			p.setHealth(p.getMaxHealth());
		} else {
			p.setHealth(p.getHealth() + hps);
		}
	}//
	
	public double getHps(Player p) {
		double hps = 5.0D;
		PlayerInventory i = p.getInventory();
		if ((i.getHelmet() != null)) {
			hps += HealthUtils.getInstance().getHPsFromItem(i.getHelmet());
		}
		if ((i.getChestplate() != null)) {
			hps += HealthUtils.getInstance().getHPsFromItem(i.getChestplate());
		}
		if ((i.getLeggings() != null)) {
			hps += HealthUtils.getInstance().getHPsFromItem(i.getLeggings());
		}
		if ((i.getBoots() != null)) {
			hps += HealthUtils.getInstance().getHPsFromItem(i.getBoots());
		}
		return hps;
		
	}
	
	public static int getPotionTier(org.bukkit.inventory.ItemStack i) { return Mob.getItemTier(i); }
	
	public double getHpsFromLore(ItemStack item, String value) {
		return HealthUtils.getInstance().getHPsFromItem(item);

	}
	
	public void hpCheck(Player p) {
		PlayerInventory i = p.getInventory();
        double a = 50.0D;
        double vital = 0.0D;
        if ((i.getHelmet() != null) && (i.getHelmet().getItemMeta().hasLore())) {
            double health = getValueFromLore(i.getHelmet(), "HP");
            int vit = (int) HealthUtils.getInstance().getVitFromItem(i.getHelmet());
            a += health;
            vital += vit;
        }
        if ((i.getChestplate() != null) && (i.getChestplate().getItemMeta().hasLore())) {
            double health = getValueFromLore(i.getChestplate(), "HP");
            int vit = (int) HealthUtils.getInstance().getVitFromItem(i.getChestplate());
            a += health;
            vital += vit;
        }
        if ((i.getLeggings() != null) && (i.getLeggings().getItemMeta().hasLore())) {
            double health = getValueFromLore(i.getLeggings(), "HP");
            int vit = (int) HealthUtils.getInstance().getVitFromItem(i.getLeggings());
            a += health;
            vital += vit;
        }
        if ((i.getBoots() != null) && (i.getBoots().getItemMeta().hasLore())) {
            double health = getValueFromLore(i.getBoots(), "HP");
            int vit = (int) HealthUtils.getInstance().getVitFromItem(i.getBoots());
            a += health;
            vital += vit;
        }
        if (vital > 0.0D) {
            double divide = vital / 2000.0D;
            double pre = a * divide;
            int cleaned = (int) (a + pre);
            if (p.getHealth() > cleaned) {
                p.setHealth(cleaned);
            }
            p.setMaxHealth(cleaned);
        } else {
            p.setMaxHealth(a);
        }
        p.setHealthScale(20.0D);
        p.setHealthScaled(true);
		
		
	}
	
	@EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        final Player p = (Player) e.getWhoClicked();
        new BukkitRunnable() {
            public void run() {
                hpCheck(p);
            }
        }.runTaskLater(Main.plugin, 1L);
	}
	
	public int getVitFromLore(ItemStack item, String value) {
       return (int) HealthUtils.getInstance().getVitFromItem(item);    
	}
	
	public static int getValueFromLore(ItemStack item, String value) {
        int returnVal = 0;
        ItemMeta meta = item.getItemMeta();
        try {
            List<?> lore = meta.getLore();
            if ((lore != null) &&
                    (((String) lore.get(1)).contains(value))) {
                String vals = ((String) lore.get(1)).split(": +")[1];
                vals = ChatColor.stripColor(vals);
                returnVal = Integer.parseInt(vals.trim());
            }
        } catch (Exception localException) {
        }
        return returnVal;
	}
	
	public static int getHealthVal(ItemStack armor) {
		String armor_data = ItemMechanics.getArmorData(armor);
		if(armor_data.equalsIgnoreCase("no")) { return 0; }

		int health_val = Integer.parseInt(armor_data.substring(armor_data.indexOf(":") + 1, armor_data.indexOf("@")));

		return health_val;
	}
	
	
	
	
	
}
			
		

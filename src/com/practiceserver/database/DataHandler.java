package com.practiceserver.database;

import java.sql.ResultSet;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.mysql.jdbc.PreparedStatement;
import com.practiceserver.enums.Alignment;
import com.practiceserver.player.PlayerUUID;
import com.practiceserver.utils.Utils;

public class DataHandler {

	public static DataHandler getInstance() {
		return new DataHandler();
	}
	
	public void addPlayerUUIDCatching(Player player) {
		PreparedStatement pst = null;
		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("INSERT INTO `uuidcatching` (`Player`,`UUID`) VALUES ('" + player.getName() + "', '" + player.getUniqueId().toString() + "');");
			pst.executeUpdate();	
		} catch(Exception e) {
			
		}
	}
	
	public void createPlayer(Player player) {
		addPlayerUUIDCatching(player);
		PreparedStatement pst = null;
		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("INSERT INTO `playerdata` (`UUID`,`Player`,`Gems`,`Rank`,`Alignment`,`AlignmentTime`,`Combat`,`CombatTime`, `Shard`)"
					+ " VALUES ('" + getUUID(player) + "', '" + player.getName() + "', '0', 'default', 'LAWFUL', '0', 'false', '0', 'US-1');");
			pst.executeUpdate();	
	
		} catch(Exception e) {
			e.printStackTrace();
		}
		//
	}
	
	public void createMountPlayer(Player player) {
		PreparedStatement pst = null;
		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("INSERT INTO `mounts` (`UUID`,`Mount`) VALUES ('" + PlayerUUID.getPlayerUUID(player).getUUID() + "', '" + 0 + "');");
			pst.executeUpdate();	
		} catch(Exception e) {
			
		}
	}
	
	public void createBank(Player player) {
		PreparedStatement pst = null;
		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("INSERT INTO `banks` (`UUID`,`Player`,`UpgradeLevel`,`Pages`,`Inventory`,`Page2`)"
					+ " VALUES ('" + PlayerUUID.getPlayerUUID(player).getUUID() + "', '" + player.getName() + "', '1', '1', '', '');");
			pst.executeUpdate();	
		} catch(Exception e) {
			
		}
	}
	
	
	
	public int getMountTier(Player player) {
		PreparedStatement pst = null;

		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("SELECT * FROM `mounts` WHERE `UUID`='" + PlayerUUID.getPlayerUUID(player).getUUID() + "';");

			pst.execute();
			ResultSet rs = pst.getResultSet();
			if (!(rs.next())) {
				return 0;
			}
			return rs.getInt("Mount");
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public void createServer(String name, String motd, int maxplayers, String ip, String worldname) {
		PreparedStatement pst = null;
		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("INSERT INTO `servers` (`Name`,`Max Players`,`Online`,`IP`,`Online Players`,`WorldName`,`MOTD`)"
					+ " VALUES ('" + name + "', '" + maxplayers + "', 'false', '" + ip + "', '0', '" + worldname + "', '" + motd + "');");
			pst.executeUpdate();	
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getServerMOTD(String name) {
		PreparedStatement pst = null;

		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("SELECT * FROM `servers` WHERE `Name`='" + name + "';");

			pst.execute();
			ResultSet rs = pst.getResultSet();
			if (!(rs.next())) {
				return null;
			}
			return rs.getString("MOTD");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean isOnline(String name) {
		PreparedStatement pst = null;

		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("SELECT * FROM `servers` WHERE `Name`='" + name + "';");

			pst.execute();
			ResultSet rs = pst.getResultSet();
			if (!(rs.next())) {
				return false;
			}
			return Boolean.parseBoolean(rs.getString("Online"));
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public String getServerWorldName(String name) {
		PreparedStatement pst = null;

		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("SELECT * FROM `servers` WHERE `Name`='" + name + "';");

			pst.execute();
			ResultSet rs = pst.getResultSet();
			if (!(rs.next())) {
				return null;
			}
			return rs.getString("WorldName");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String getServerIP(String name) {
		PreparedStatement pst = null;

		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("SELECT * FROM `servers` WHERE `Name`='" + name + "';");

			pst.execute();
			ResultSet rs = pst.getResultSet();
			if (!(rs.next())) {
				return null;
			}
			return rs.getString("IP");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public int getServerMaxPlayers(String name) {
		PreparedStatement pst = null;

		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("SELECT * FROM `servers` WHERE `Name`='" + name + "';");

			pst.execute();
			ResultSet rs = pst.getResultSet();
			if (!(rs.next())) {
				return 0;
			}
			return rs.getInt("Max Players");
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public int getGems(Player player) {
		PreparedStatement pst = null;

		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("SELECT * FROM `playerdata` WHERE `UUID`='" + PlayerUUID.getPlayerUUID(player).getUUID() + "';");

			pst.execute();
			ResultSet rs = pst.getResultSet();
			if (!(rs.next())) {
				return 0;
			}
			return rs.getInt("Gems");
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public int getServerOnlinePlayers(String name) {
		PreparedStatement pst = null;

		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("SELECT * FROM `servers` WHERE `Name`='" + name + "';");

			pst.execute();
			ResultSet rs = pst.getResultSet();
			if (!(rs.next())) {
				return 0;
			}
			return rs.getInt("Online Players");
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public void addToSaveData(Player player) {
		PreparedStatement pst = null;
		try {
			Location l = player.getLocation();
			double x = l.getX();
			double y = l.getY();
			double z = l.getZ();
			float pitch = l.getPitch();
			float yaw = l.getYaw();
			String loc = x + "," + y + "," + z + "," + yaw + "," + pitch;
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("INSERT INTO `savedata` (`UUID`,`Name`,`FoodLevel`,`MaxHP`,`HP`,`Location`)"
					+ " VALUES ('" + PlayerUUID.getPlayerUUID(player).getUUID() + "', '" + player.getName() + "', '" + player.getFoodLevel() + "', '" + (int)player.getMaxHealth() + "', '" + (int)player.getHealth() + "', '" + loc + "');");
			pst.executeUpdate();	
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void saveData(Player player) {
		PreparedStatement pst = null;
		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("UPDATE `savedata` SET `FoodLevel`='" + player.getFoodLevel() + "' WHERE `UUID`='" + PlayerUUID.getPlayerUUID(player).getUUID() + "';");
			pst.execute();
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("UPDATE `savedata` SET `MaxHP`='" + (int)player.getMaxHealth() + "' WHERE `UUID`='" + PlayerUUID.getPlayerUUID(player).getUUID() + "';");
			pst.execute();
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("UPDATE `savedata` SET `HP`='" + (int)player.getHealth() + "' WHERE `UUID`='" + PlayerUUID.getPlayerUUID(player).getUUID() + "';");
			pst.execute();
			Location l = player.getLocation();
			double x = l.getX();
			double y = l.getY();
			double z = l.getZ();
			float pitch = l.getPitch();
			float yaw = l.getYaw();
			String loc = x + "," + y + "," + z + "," + yaw + "," + pitch;
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("UPDATE `savedata` SET `Location`='" + loc + "' WHERE `UUID`='" + PlayerUUID.getPlayerUUID(player).getUUID() + "';");
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void loadData(Player player) {
		PreparedStatement pst = null;

		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("SELECT * FROM `savedata` WHERE `UUID`='" + getUUID(player) + "';");

			pst.execute();
			ResultSet rs = pst.getResultSet();
			if (!(rs.next())) {
				return;
			}
			int foodLevel = rs.getInt("FoodLevel");
			double maxHP = rs.getInt("MaxHP");
			double hp = rs.getInt("HP");
			String loc = rs.getString("Location");
			Location location = Utils.getLocation(loc, player);
			player.setFoodLevel(foodLevel);
			player.setMaxHealth(maxHP);
			player.setHealth(hp);
			player.setHealthScale(20);
			player.setHealthScaled(true);
			player.teleport(location);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}
	
	public int getAlignmentTime(Player player) {
		PreparedStatement pst = null;

		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("SELECT * FROM `playerdata` WHERE `UUID`='" + getUUID(player) + "';");

			pst.execute();
			ResultSet rs = pst.getResultSet();
			if (!(rs.next())) {
				return 0;
			}
			return rs.getInt("AlignmentTime");
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public int getCombatTime(Player player) {
		PreparedStatement pst = null;

		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("SELECT * FROM `playerdata` WHERE `UUID`='" + getUUID(player) + "';");

			pst.execute();
			ResultSet rs = pst.getResultSet();
			if (!(rs.next())) {
				return 0;
			}
			return rs.getInt("CombatTime");
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public int getBankPages(Player player) {
		PreparedStatement pst = null;

		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("SELECT * FROM `banks` WHERE `UUID`='" + getUUID(player) + "';");

			pst.execute();
			ResultSet rs = pst.getResultSet();
			if (!(rs.next())) {
				return 0;
			}
			return rs.getInt("Pages");
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public int getBankUpgradeLevel(Player player) {
		PreparedStatement pst = null;

		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("SELECT * FROM `banks` WHERE `UUID`='" + getUUID(player) + "';");

			pst.execute();
			ResultSet rs = pst.getResultSet();
			if (!(rs.next())) {
				return 0;
			}
			return rs.getInt("UpgradeLevel");
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public String getBankInventory(Player player) {
		PreparedStatement pst = null;

		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("SELECT * FROM `banks` WHERE `UUID`='" + getUUID(player) + "';");

			pst.execute();
			ResultSet rs = pst.getResultSet();
			if (!(rs.next())) {
				return null;
			}
			return rs.getString("Inventory");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String getBankInventory1(Player player) {
		PreparedStatement pst = null;

		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("SELECT * FROM `banks` WHERE `UUID`='" + getUUID(player) + "';");

			pst.execute();
			ResultSet rs = pst.getResultSet();
			if (!(rs.next())) {
				return null;
			}
			return rs.getString("Page2");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean isLawful(Player player) {
		return (getAlignment(player) == Alignment.LAWFUL);
	}
	
	public boolean isChaotic(Player player) {
		return (getAlignment(player) == Alignment.CHAOTIC);
	}
	
	public boolean isNeutral(Player player) {
		return (getAlignment(player) == Alignment.NEUTRAL);
	}
	
	public void setServerOnline(String name, boolean bool) {
		PreparedStatement pst = null;
		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("UPDATE `servers` SET `Online`='" + bool + "' WHERE `Name`='" + name + "';");
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setBankInventory(Player player, String inv) {
		PreparedStatement pst = null;
		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("UPDATE `banks` SET `Inventory`='" + inv + "' WHERE `UUID`='" + PlayerUUID.getPlayerUUID(player).getUUID() + "';");
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setBankInventory2(Player player, String inv) {
		PreparedStatement pst = null;
		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("UPDATE `banks` SET `Page2`='" + inv + "' WHERE `UUID`='" + PlayerUUID.getPlayerUUID(player).getUUID() + "';");
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setUpgradeLevel(Player player, int level) {
		PreparedStatement pst = null;
		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("UPDATE `banks` SET `UpgradeLevel`='" + level + "' WHERE `UUID`='" + PlayerUUID.getPlayerUUID(player).getUUID() + "';");
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setBankPages(Player player, int pages) {
		PreparedStatement pst = null;
		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("UPDATE `banks` SET `Pages`='" + pages + "' WHERE `UUID`='" + PlayerUUID.getPlayerUUID(player).getUUID() + "';");
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setGems(Player player, int gems) {
		PreparedStatement pst = null;
		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("UPDATE `playerdata` SET `Gems`='" + gems + "' WHERE `UUID`='" + PlayerUUID.getPlayerUUID(player).getUUID() + "';");
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setServerOnlinePlayersCount(String name, int count) {
		PreparedStatement pst = null;
		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("UPDATE `servers` SET `Online Players`='" + count + "' WHERE `Name`='" + name + "';");
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setAlignmentTime(Player player, int time) {
		PreparedStatement pst = null;
		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("UPDATE `playerdata` SET `AlignmentTime`='" + time + "' WHERE `UUID`='" + getUUID(player) + "';");
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setMountTier(Player player, int tier) {
		PreparedStatement pst = null;
		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("UPDATE `mounts` SET `Mount`='" + tier + "' WHERE `UUID`='" + getUUID(player) + "';");
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setCombatTime(Player player, int time) {
		PreparedStatement pst = null;
		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("UPDATE `playerdata` SET `CombatTime`='" + time + "' WHERE `UUID`='" + getUUID(player) + "';");
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setAlignment(Player player, Alignment alignment) {
		PreparedStatement pst = null;
		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("UPDATE `playerdata` SET `Alignment`='" + alignment.toString() + "' WHERE `UUID`='" + getUUID(player) + "';");
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setCombat(Player player, boolean bool) {
		PreparedStatement pst = null;
		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("UPDATE `playerdata` SET `Combat`='" + bool + "' WHERE `UUID`='" + getUUID(player) + "';");
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setShard(Player player, String shard) {
		PreparedStatement pst = null;
		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("UPDATE `playerdata` SET `Shard`='" + shard + "' WHERE `UUID`='" + getUUID(player) + "';");
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean isCombat(Player player) {
		PreparedStatement pst = null;

		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("SELECT * FROM `playerdata` WHERE `UUID`='" + getUUID(player) + "';");

			pst.execute();
			ResultSet rs = pst.getResultSet();
			if (!(rs.next())) {
				return false;
			}
			return Boolean.parseBoolean(rs.getString("Combat"));
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public Alignment getAlignment(Player player) {
		PreparedStatement pst = null;

		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("SELECT * FROM `playerdata` WHERE `UUID`='" + getUUID(player) + "';");

			pst.execute();
			ResultSet rs = pst.getResultSet();
			if (!(rs.next())) {
				return null;
			}
			return Alignment.valueOf(rs.getString("Alignment").trim());
		} catch (Exception e) {
			e.printStackTrace();
			return Alignment.LAWFUL;
		}
		
	}
	
	public String getShard(Player player) {
		PreparedStatement pst = null;

		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("SELECT * FROM `playerdata` WHERE `UUID`='" + getUUID(player) + "';");

			pst.execute();
			ResultSet rs = pst.getResultSet();
			if (!(rs.next())) {
				return null;
			}
			return rs.getString("Shard").trim();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String getUUID(Player player) {
		PreparedStatement pst = null;

		try {
			pst = (PreparedStatement) ConnectionPool.getConnection().prepareStatement("SELECT * FROM `uuidcatching` WHERE `Player`='" + player.getName() + "';");

			pst.execute();
			ResultSet rs = pst.getResultSet();
			if (!(rs.next())) {
				return null;
			}
			return rs.getString("UUID").trim();
		} catch (Exception e) {
			e.printStackTrace();
			return player.getUniqueId().toString();
		}
	}

	public int getServer(Player p) {
		return Utils.getShardNum(p);
	}

	public static int getServerNum() {
		String server_name = Bukkit.getServer().getMotd().split("-")[1].split(" ")[0];
    	int num = 0;
    	try {
    		num = Integer.parseInt(server_name.trim());
    	} catch (NumberFormatException e) {
    		e.printStackTrace();
    	}
    	return num;
	}
}
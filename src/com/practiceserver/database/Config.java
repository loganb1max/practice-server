package com.practiceserver.database;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Config {
	
	private static boolean localhost = false;
	
	public static List<String> us_public_servers = new ArrayList<String>(Arrays.asList("US-1", "US-2"));//, "US-3", "US-4"));
	public static List<String> us_beta_servers = new ArrayList<String>(Arrays.asList("US-100"));
    public static List<String> us_private_servers = new ArrayList<String>(Arrays.asList("US-5"));
    public static List<String> us_dev_servers = new ArrayList<String>(Arrays.asList("US-0"));
	
	private static String sqlIP = "127.0.0.1"; 
	private static String sqlPort = "3306";
	private static String sqlUser = "matt11matthew";//"prserver"; 
	private static String sqlPass = "pass";//"JuVBeQRU7GxVtrSD"; 
	private static String dbName = "prserver"; 
	private static int transfer_port = 3306;
	private static String world = "world";
	//
	public static void setup() {
		if (localhost) {
			sqlIP = "127.0.0.1"; 
			sqlPort = "3306";
			sqlUser = "matt11matthew";
			sqlPass = "pass";
			dbName = "prserver";
			world = "world";
		} else {
			sqlIP = "127.0.0.1"; 
			sqlPort = "3306";
			sqlUser = "prserver";
			sqlPass = "JuVBeQRU7GxVtrSD";
			dbName = "prserver";
			world = "DungeonRealms";
		}
	}
	
	public static String getIP() {return sqlIP;};
	public static String getPort() {return sqlPort;};
	public static String getUser() {return sqlUser;};
	public static String getPass() {return sqlPass;};
	public static String getDBName() {return dbName;};
	public static String getURL() {return "jdbc:mysql://" + getIP() + ":" + getPort() + "/" + getDBName();}

	public static int getTransferPort() {
		return transfer_port;
	}

	public static String getWorld() {
		return world;
	}

}

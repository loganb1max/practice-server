package com.practiceserver.database;

import java.sql.DriverManager;
import java.util.logging.Level;

import com.mysql.jdbc.Connection;
import com.practiceserver.Main;

public class ConnectionPool {
	
	private static Connection con;
	
	public static boolean refresh = false;
	
	public static Connection getConnection() {
		try {
			if (refresh || con == null || con.isClosed()) {
				refresh = false;
				if (con != null) con.close();
				con = (Connection) DriverManager.getConnection(Config.getURL(), Config.getUser(), Config.getPass());
			}
		} catch (Exception e) {
			Main.plugin.getLogger().log(Level.SEVERE, "Could Not Connect To Database.");
			Main.plugin.getLogger().log(Level.SEVERE, e.getMessage());
		}
		return con;
	}
	
}
//
package com.practiceserver.mob;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class FileManager {
	
private static FileManager instance = new FileManager();
	
	public static FileManager getInstance() {
		return instance;
	}
	
	/**
	 * 
	 * @param file The yml file.
	 * @param path The file path.
	 * @return The FileConfiguration of the yml getting returned.
	 */
	public FileConfiguration loadFile(String file, String path) {
		File ff = new File(path);
		if(!ff.exists()) {
			ff.mkdirs();
		}
		ff = new File(path, file);
		if(!ff.exists()) {
			try {
				ff.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		FileConfiguration c = YamlConfiguration.loadConfiguration(ff);
		return c;
	}
	
	

}
//
package com.practiceserver.mob.commands;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.practiceserver.mob1.Mob;
import com.practiceserver.mobs.ItemMechanics;
import com.practiceserver.utils.Utils;


public class CommandMon implements CommandExecutor {
	

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player) sender;
		if(!p.isOp())return true;
		if(args.length > 2) {
			p.getInventory().addItem(new ItemStack(Material.MOB_SPAWNER, 5));
			p.sendMessage(Utils.colorCodes("&ePlace it like it's hot!"));
			return true;
		} else if(args.length == 2) {
			if(args[0].equalsIgnoreCase("add")) {
				p.getInventory().addItem(ItemMechanics.getItemFromFile(args[1]));
				return true;
			}
		} else if(args.length == 0) {
			//p.getInventory().addItem(ItemMechanics.generateBook(p));
			//p.getInventory().addItem(GemSacks.t1_gem_pouch);
			//p.getInventory().addItem(GemSacks.t2_gem_pouch);
			//p.getInventory().addItem(GemSacks.t3_gem_pouch);
			/*
				p.getInventory().addItem(EnchantHandler.t1_armor_scroll);
				p.getInventory().addItem(EnchantHandler.t2_armor_scroll);
				p.getInventory().addItem(EnchantHandler.t3_armor_scroll);
				p.getInventory().addItem(EnchantHandler.t4_armor_scroll);
				p.getInventory().addItem(EnchantHandler.t5_armor_scroll);
				p.getInventory().addItem(EnchantHandler.t1_wep_scroll);
				p.getInventory().addItem(EnchantHandler.t2_wep_scroll);
				p.getInventory().addItem(EnchantHandler.t3_wep_scroll);
				p.getInventory().addItem(EnchantHandler.t4_wep_scroll);
				p.getInventory().addItem(EnchantHandler.t5_wep_scroll);
				p.getInventory().addItem(EnchantHand//ler.t1_white_scroll);
				p.getInventory().addItem(EnchantHandler.t2_white_scroll);
				p.getInventory().addItem(EnchantHandler.t3_white_scroll);
				p.getInventory().addItem(EnchantHandler.t4_white_scroll);
				p.getInventory().addItem(EnchantHandler.t5_white_scroll);
				p.getInventory().addItem(EnchantHandler.orb_of_alteration);
				*/
			
				return true;
		
		} else if(args.length == 1) {
			//Spawners.getInstance().spawnMob(args[0].trim(), p.getEyeLocation());
			//Drops.spawnTierMob(args[0], p.getLocation(), EntityType.SKELETON, 5, p.getLocation(), false, "", false);
			try {
				//Drops.spawnTierMob(p.getLocation(), EntityType.SKELETON, tier, -1, p.getLocation(), false, "", "", true);
				Mob.getInstance().spawnMob(args[0].trim(), p.getEyeLocation());
			} catch (Exception e) {
				p.sendMessage(Utils.colorCodes("&cThe mob '" + args[0] + "' doesn't exist!"));
				return true;
			}
			
				//p.sendMessage(Utils.colorCodes("&cThe mob '" + args[0] + "' doesn't exist!"));
				return true;
			
		}
		return true;
		
	}

}

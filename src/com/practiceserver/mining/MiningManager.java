package com.practiceserver.mining;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import com.practiceserver.Main;

public class MiningManager implements Listener{

	
	
	
	
	
	
	
	
	@EventHandler
	public void onOreMine(BlockBreakEvent e) {
		e.setExpToDrop(0);
		if (e.getBlock().getType().equals(Material.COAL_ORE)) {
			Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
				@Override
				public void run() {
					e.getBlock().setType(Material.COAL_ORE);
				}
			}, 600);
		}
		else if (e.getBlock().getType().equals(Material.EMERALD_ORE)) {
			Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
				@Override
				public void run() {
					e.getBlock().setType(Material.EMERALD_ORE);
				}
			}, 800);
		}
		else if (e.getBlock().getType().equals(Material.IRON_ORE)) {
			Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
				@Override
				public void run() {
					e.getBlock().setType(Material.IRON_ORE);
				}
			}, 1000);
		}
		else if (e.getBlock().getType().equals(Material.DIAMOND_ORE)) {
			Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
				@Override
				public void run() {
					e.getBlock().setType(Material.DIAMOND_ORE);
				}
			}, 1200);
		}
		else if (e.getBlock().getType().equals(Material.GOLD_ORE)) {
			Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
				@Override
				public void run() {
					e.getBlock().setType(Material.GOLD_ORE);
				}
			}, 1400);
		}
	}
	
}

package com.practiceserver.itemgenerator.engine;

public enum ModifierType {
	
	RANGE, STATIC, TRIPLE;
	
}

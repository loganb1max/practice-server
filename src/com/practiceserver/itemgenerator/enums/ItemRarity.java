package com.practiceserver.itemgenerator.enums;

import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

/*
Copyright � matt11matthew 2016
*/

public enum ItemRarity
{
  COMMON,  UNCOMMON,  RARE,  UNIQUE;
  
  private ItemRarity() {}
  
  public static ItemRarity getRarityFromItem(ItemStack is)
  {
    if ((!is.hasItemMeta()) || (!is.getItemMeta().hasLore())) {
      return null;
    }
    for (String line : is.getItemMeta().getLore()) {
      for (ItemRarity rarity : values()) {
        if (line.toLowerCase().contains(rarity.toString().toLowerCase())) {
          return valueOf(ChatColor.stripColor(line).toUpperCase());
        }
      }
    }
    return null;
  }
  
  public static ItemRarity getRandomRarity() {
	  Random random = new Random();
	  int rarity = random.nextInt(50) + 1;
	  if (rarity <= 43) {
		  return COMMON;
	  } else if ((rarity == 44) || (rarity == 45) || (rarity == 46) ||  (rarity == 47)) {
		  return UNCOMMON;
	  } else if ((rarity == 48) || (rarity == 49)) {
		  return RARE;
	  } else if (rarity == 50) {
		  return UNIQUE;
	  }
	return COMMON;
  }
}

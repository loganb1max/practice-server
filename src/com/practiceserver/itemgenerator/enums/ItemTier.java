package com.practiceserver.itemgenerator.enums;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/*
Copyright � matt11matthew 2016
*/

public enum ItemTier {

	T1,  T2,  T3,  T4,  T5;
	  
	  private ItemTier() {}
	  
	  public ChatColor getTierColor()
	  {
	    switch (this)
	    {
	    case T1: 
	      return ChatColor.WHITE;
	    case T2: 
	      return ChatColor.GREEN;
	    case T3: 
	      return ChatColor.AQUA;
	    case T4: 
	      return ChatColor.LIGHT_PURPLE;
	    case T5: 
	      return ChatColor.YELLOW;
	    }
	    return null;
	  }
	  
	  public static ItemTier getTierFromInt(int tier)
	  {
	    switch (tier)
	    {
	    case 1: 
	      return T1;
	    case 2: 
	      return T2;
	    case 3: 
	      return T3;
	    case 4: 
	      return T4;
	    case 5: 
	      return T5;
	    }
	    return null;
	  }
	  
	  public static ItemTier getTierFromMaterial(Material m)
	  {
	    String name = m.toString().toLowerCase();
	    if ((name.startsWith("leather")) || (name.startsWith("wood"))) {
	      return T1;
	    }
	    if ((name.startsWith("chainmail")) || (name.startsWith("stone"))) {
	      return T2;
	    }
	    if (name.startsWith("iron")) {
	      return T3;
	    }
	    if (name.startsWith("diamond")) {
	      return T4;
	    }
	    if (name.startsWith("gold")) {
	      return T5;
	    }
	    if (name.equalsIgnoreCase("Bow")) {}
	    return null;
	  }
	  
	  public static ItemTier getTierFromItem(ItemStack is)
	  {
	    try
	    {
	      String name = is.getItemMeta().getDisplayName();
	      if (name.contains(ChatColor.GREEN.toString())) {
	        return T2;
	      }
	      if (name.contains(ChatColor.AQUA.toString())) {
	        return T3;
	      }
	      if (name.contains(ChatColor.LIGHT_PURPLE.toString())) {
	        return T4;
	      }
	      if (name.contains(ChatColor.YELLOW.toString())) {
	        return T5;
	      }
	      if (name.contains(ChatColor.WHITE.toString())) {
	        return T1;
	      }
	      return T1;
	    }
	    catch (NullPointerException npe) {}
	    return null;
	  }
	  
	  public static int getTierNumFromItem(ItemStack is)
	  {
	    try
	    {
	      String name = is.getItemMeta().getDisplayName();
	      if (name.contains(ChatColor.GREEN.toString())) {
	        return 2;
	      }
	      if (name.contains(ChatColor.AQUA.toString())) {
	        return 3;
	      }
	      if (name.contains(ChatColor.LIGHT_PURPLE.toString())) {
	        return 4;
	      }
	      if (name.contains(ChatColor.YELLOW.toString())) {
	        return 5;
	      }
	      if (name.contains(ChatColor.WHITE.toString())) {
	        return 1;
	      }
	      return 1;
	    }
	    catch (NullPointerException npe) {}
	    return 0;
	  }

}


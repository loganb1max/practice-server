
package com.practiceserver.spawners;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import com.practiceserver.database.ConnectionPool;


public class Spawner {

	private Location loc;
	private List<String> types = new ArrayList<String>();
	private int spawnDelay;
	private int spawnAmount;
	private boolean active;
	private String type;
	
	
	public Spawner(Location location, String types, int delay, int amt ) {
		loc = location;
		if (types.contains(",")){
			for (String s : types.split(",")){
				s = s.replaceAll(" ", "");
				s = s.replaceAll(",", "");
				s = s.trim();
				this.types.add(s);
			}
		}
		else{
			this.types.add(types);
		}
		spawnDelay = delay;
		spawnAmount = amt;
		String type1 = types;
		this.type = type1.trim();
	}
	
	
	public String getType() {
		return type;
	}
	
	public Location getLoc() {
		return loc;
	}
	
	public void setLoc(Location location) {
		loc = location;
	}
	
	public List<String> getTypes() {
		return types;
	}
	
	public int getSpawnDelay() {
		return spawnDelay;
	}
	
	public void setSpawnDelay(int delay) {
		spawnDelay = delay;
	}
	
	public int getSpawnAmount() {
		return spawnAmount;
	}
	
	public void setSpawnAmount(int amt) {
		spawnAmount = amt;
	}
	
	public boolean isActive() {
		return active;
	}
	
	public void setActive(boolean act) {
		active = act;
	}
	
	private String locToString() {
		return this.getLoc().getBlockX() + "." + this.getLoc().getBlockY() + "." + this.getLoc().getBlockZ();
	}
	
	private String typesToString() {
		String output = "";
		output += types.get(0);
		for (int i = 1; i < types.size(); i++){
			output += ", " + types.get(i);
		}
		return output;
	}
	
	public void save() {
		String loc = locToString();
		String typesin = this.typesToString();
		int delay = this.getSpawnDelay();
		int amt = this.getSpawnAmount();

		PreparedStatement pst = null;
		try{
			pst = ConnectionPool.getConnection().prepareStatement("INSERT INTO spawners "
					+ "(loc, types, delay, amount) "
					+ "VALUES('" + loc + "', '" + typesin + "', '" + delay + "', '" + amt + "') "
					+ "ON DUPLICATE KEY UPDATE loc='" + loc + "', types='" + typesin + "', delay='" + delay + "', amount='" + amt + "'");
			pst.executeUpdate();	
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
		}
		Bukkit.getLogger().log(Level.INFO, "Spawner @ " + loc + " Saved!");
	}
	
	public void delete() {	
		PreparedStatement pst = null;
		try{
			pst = ConnectionPool.getConnection().prepareStatement("DELETE FROM `prserver`.`spawners` WHERE `spawners`.`loc` = \'" + locToString() + "\'");
			pst.executeUpdate();	
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
		}
		Bukkit.getLogger().log(Level.INFO, "Spawner @ " + locToString() + " Removed!");
	}
	
	
}

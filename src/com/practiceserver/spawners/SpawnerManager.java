package com.practiceserver.spawners;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.practiceserver.Main;
import com.practiceserver.database.Config;
import com.practiceserver.database.ConnectionPool;
import com.practiceserver.events.PlayerChatEvent;

public class SpawnerManager implements Listener{

	public static HashMap<Location, Spawner> spawner_list = new HashMap<Location, Spawner>();
	
	private HashMap<Player, Integer> spawner_create_step = new HashMap<Player, Integer>();
	private HashMap<Player, Location> spawner_create_loc = new HashMap<Player, Location>();
	private HashMap<Player, String> spawner_create_typelist = new HashMap<Player, String>();
	private HashMap<Player, Integer> spawner_create_delay = new HashMap<Player, Integer>();
	private HashMap<Player, Integer> spawner_create_amount = new HashMap<Player, Integer>();
	
	@EventHandler
	public void onSpawnerPlace(BlockPlaceEvent e) {
		if (e.getBlock().getType().equals(Material.MOB_SPAWNER)) {
			e.setCancelled(true);
			if (e.getPlayer().isOp()) {
				if (!spawner_create_step.containsKey(e.getPlayer())) {
					e.getPlayer().sendMessage(ChatColor.GREEN.toString() + ChatColor.BOLD.toString() + "Spawner Creation Started!");
					e.getPlayer().sendMessage(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD.toString() + "Please Enter the Types of Entitys!");
					e.getPlayer().sendMessage(ChatColor.GRAY.toString() + "Entity Format: \"type:tier:elite:named-elite:name");
					e.getPlayer().sendMessage(ChatColor.GRAY.toString() + "(seperated by a comma and a space EX: \"skeleton:5:false:false:Infernal_Skeleton, zombie:3:true:true:Impa\"");
					e.getPlayer().sendMessage(ChatColor.GRAY.toString() + "To Cancel Type \"cancel\"");
					spawner_create_step.put(e.getPlayer(), 1);
					spawner_create_loc.put(e.getPlayer(), e.getBlock().getLocation());
				}
			} else {
				e.getPlayer().getInventory().remove(e.getItemInHand());
				e.getPlayer().sendMessage(ChatColor.RED.toString() + "You Are Not Permited to Have That Item!");
			}
		}
	}
	
	@EventHandler
	public void onSpawnerBreak(BlockBreakEvent e) {
		if (e.getBlock().getType().equals(Material.MOB_SPAWNER)) {
			if (spawner_list.containsKey(e.getBlock().getLocation())) {
				e.getPlayer().sendMessage(ChatColor.RED.toString() + ChatColor.BOLD.toString() + "Spawner Removed!");
				spawner_list.get(e.getBlock().getLocation()).delete();
				spawner_list.remove(e.getBlock().getLocation());
			}
		}
	}
	
	@EventHandler
	public void onSpawnerInfo(PlayerInteractEvent e) {
		if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			if (e.getClickedBlock().getType().equals(Material.MOB_SPAWNER)) {
				if (spawner_list.containsKey(e.getClickedBlock().getLocation())) {
					e.getPlayer().sendMessage(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD.toString() + ChatColor.UNDERLINE.toString() + "Spawner Info");
					e.getPlayer().sendMessage(ChatColor.DARK_GRAY.toString() + "Type: " + ChatColor.GRAY.toString() + spawner_list.get(e.getClickedBlock().getLocation()).getType());
					e.getPlayer().sendMessage(ChatColor.DARK_GRAY.toString() + "Delay: " + ChatColor.GRAY.toString() + spawner_list.get(e.getClickedBlock().getLocation()).getSpawnDelay());
					e.getPlayer().sendMessage(ChatColor.DARK_GRAY.toString() + "Amount: " + ChatColor.GRAY.toString() + spawner_list.get(e.getClickedBlock().getLocation()).getSpawnAmount());
				}
			}
		}
	}
	
	@EventHandler
	public void onChatEvent(PlayerChatEvent e) {
		if (spawner_create_step.containsKey(e.getPlayer())) {
			e.setCancelled(true);
			if (spawner_create_step.get(e.getPlayer()) == 1) {
				String msg = e.getMessage();
				if (msg.equalsIgnoreCase("cancel")) {
					spawner_create_step.remove(e.getPlayer());
					spawner_create_loc.remove(e.getPlayer());
					e.getPlayer().sendMessage(ChatColor.RED.toString() + ChatColor.BOLD.toString() + "Spawner Creation CANCELLED!");
					return;
				}
				spawner_create_typelist.put(e.getPlayer(), msg);
				e.getPlayer().sendMessage(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD.toString() +  "Please Enter the Spawn Delay!");
				e.getPlayer().sendMessage(ChatColor.GRAY.toString() + "Do NOT use Decimals! EX: (30) IS IN SECONDS!");
				e.getPlayer().sendMessage(ChatColor.GRAY.toString() + "To Cancel Type \"cancel\"");
				spawner_create_step.put(e.getPlayer(), 2);
			}
			else if (spawner_create_step.get(e.getPlayer()) == 2) {
				String msg = e.getMessage();
				if (msg.equalsIgnoreCase("cancel")) {
					spawner_create_step.remove(e.getPlayer());
					spawner_create_loc.remove(e.getPlayer());
					spawner_create_typelist.remove(e.getPlayer());
					e.getPlayer().sendMessage(ChatColor.RED.toString() + ChatColor.BOLD.toString() + "Spawner Creation CANCELLED!");
					return;
				}
				spawner_create_delay.put(e.getPlayer(), Integer.parseInt(msg));
				e.getPlayer().sendMessage(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD.toString() +  "Please Enter the Spawn Amount");
				e.getPlayer().sendMessage(ChatColor.GRAY.toString() + "Do NOT use Decimals! EX: (4) This tells max entitys for area!");
				e.getPlayer().sendMessage(ChatColor.GRAY.toString() + "To Cancel Type \"cancel\"");
				spawner_create_step.put(e.getPlayer(), 3);
			}
			else if (spawner_create_step.get(e.getPlayer()) == 3) {
				String msg = e.getMessage();
				if (msg.equalsIgnoreCase("cancel")) {
					spawner_create_step.remove(e.getPlayer());
					spawner_create_loc.remove(e.getPlayer());
					spawner_create_typelist.remove(e.getPlayer());
					spawner_create_delay.remove(e.getPlayer());
					e.getPlayer().sendMessage(ChatColor.RED.toString() + ChatColor.BOLD.toString() + "Spawner Creation CANCELLED!");
					return;
				}
				spawner_create_amount.put(e.getPlayer(), Integer.parseInt(msg));
				e.getPlayer().sendMessage(ChatColor.GREEN.toString() + ChatColor.BOLD.toString() + "Spawner Created!");
				spawner_list.put(spawner_create_loc.get(e.getPlayer()), new Spawner(spawner_create_loc.get(e.getPlayer()), spawner_create_typelist.get(e.getPlayer()), spawner_create_delay.get(e.getPlayer()), spawner_create_amount.get(e.getPlayer())));
				spawner_list.get(spawner_create_loc.get(e.getPlayer())).save();
				spawner_create_step.remove(e.getPlayer());
				spawner_create_loc.remove(e.getPlayer());
				spawner_create_typelist.remove(e.getPlayer());
				spawner_create_delay.remove(e.getPlayer());
				spawner_create_amount.remove(e.getPlayer());
				
			}
		}
	}
	
	public void loadAllSpawners() {
		PreparedStatement pst = null;

		try {
			pst = ConnectionPool
					.getConnection()
					.prepareStatement(
							"SELECT loc, types, delay, amount FROM spawners order by loc");

			pst.execute();
			ResultSet rs = pst.getResultSet();

			while (rs.next()){			
				String s = rs.getString("loc").replace(".", ",");
				String[] sSplit = s.split(",");
				Location l = Bukkit.getServer().getWorld(Config.getWorld()).getBlockAt(Integer.parseInt(sSplit[0]), Integer.parseInt(sSplit[1]), Integer.parseInt(sSplit[2])).getLocation();
				String types = rs.getString("types");
				int delay = rs.getInt("delay");
				int amt = rs.getInt("amount");
				spawner_list.put(l, new Spawner(l, types, delay, amt));
			}

		}catch (Exception ex) {
			ex.printStackTrace();

		} finally {
			try {
				if (pst != null) {
					try {
						pst.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			} finally {

			}
		}
		Bukkit.getLogger().log(Level.INFO, "Spawners Loaded!");
	}
	
	
	private static Set<Entity> getEntitiesInChunks(Location l, int chunkRadius) {
	    Block b = l.getBlock();
	    Set<Entity> entities = new HashSet<Entity>();
	    for (int x = -16 * chunkRadius; x <= 16 * chunkRadius; x += 16) {
	        for (int z = -16 * chunkRadius; z <= 16 * chunkRadius; z += 16) {
	            for (Entity e : b.getRelative(x, 0, z).getChunk().getEntities()) {
	                entities.add(e);
	            }
	        }
	    }
	    return entities;
	}
	
	public void setup() {	
		Main.plugin.getServer().getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {	
			public void run() {					
				for (Spawner s : spawner_list.values()) {
					s.setActive(false);
					for (Player p : Bukkit.getServer().getOnlinePlayers()) {
						if (getEntitiesInChunks(s.getLoc(), 1).contains(p)) {
							s.setActive(true);
						}
						if (s.isActive()) {
							Main.plugin.getServer().getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
								public void run() {					
									int spawnedEntitys = 0;
									for (Entity e : getEntitiesInChunks(s.getLoc(), 1)) {
										if (!(e instanceof Player)) {
											spawnedEntitys++;
										}
									}
									if (!s.isActive()) {
										s.setActive(false);
										return;
									}
									while (spawnedEntitys < s.getSpawnAmount()) {
										String rawmob = s.getType();//s.getTypes().get(rand.nextInt(s.getTypes().size()));
										//rawmob = rawmob.replaceAll(",", "").trim();
										//String type = rawmob.split(":")[0].trim();
										//String tier = rawmob.split(":")[1].trim();
										//String elite = rawmob.split(":")[2].trim();
										//String namedelite = rawmob.split(":")[3].trim();
										String name = rawmob;
										com.practiceserver.mob1.Mob.getInstance().spawnMob(name, s.getLoc());
										//MobHandler.spawnMob(s.getLoc(), 1, "skeleton", true, "", false);
									   // MobHandler.spawnMob(s.getLoc(), Integer.parseInt(tier), type, Boolean.parseBoolean(elite), name.replaceAll("null", ""), Boolean.parseBoolean(namedelite));
										s.setActive(false);
										return;
									}
									
								}
							}, s.getSpawnDelay(), (s.getSpawnDelay()*20));
						}
					}
				}
			}
		}, 1L, 1L);
	}
}

package com.practiceserver.spawners.commands;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.practiceserver.spawners.SpawnerManager;

public class CommandHideMS implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (p.isOp()) {
				for (Location l : SpawnerManager.spawner_list.keySet()) {
					if (l.distance(p.getLocation()) <= 50) {
						l.getBlock().setType(Material.AIR);
					}
				}
			}
		}
		return true;
	}
	
}

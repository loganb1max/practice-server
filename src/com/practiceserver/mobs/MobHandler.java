package com.practiceserver.mobs;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Ghast;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import com.practiceserver.Main;
import com.practiceserver.utils.RegionUtils;
import com.practiceserver.utils.Utils;

public class MobHandler implements Listener {
	
	public static List<LivingEntity> chargedMobs = new ArrayList();
	
	  @EventHandler(priority = EventPriority.HIGHEST)
	    public void onPlayerTargeted(EntityTargetLivingEntityEvent e) {
	        if (e.isCancelled())
	            return;
	        if (e.getTarget() instanceof Player) {
	            Mob.mob_target.put(e.getEntity(), ((Player) e.getTarget()).getName());
	        }
	    }
	
	@EventHandler
	public void ondeath(EntityDeathEvent e) {
		 final Entity ent = e.getEntity();
	        final String p_name = Mob.mob_target.get(ent);
	        boolean never_unload_home = false;

	        if (ent instanceof Player) {
	            Player pl = (Player) ent;
	            if (pl.hasMetadata("NPC")) {
	                e.getDrops().clear();
	                return;
	            }//
	            return; // Not a custom monster, no one cares.
	        }

		 if (p_name != null && Bukkit.getPlayer(p_name) != null  && !RegionUtils.isDamageDisabled(ent.getLocation())
	                && Mob.mob_health.containsKey(ent) && Mob.mob_loot.containsKey(ent) && Mob.mob_target.containsKey(ent)
	                 && Bukkit.getPlayer(Mob.mob_target.get(ent)) != null
	                && ent.getWorld().getName().equalsIgnoreCase(Bukkit.getWorlds().get(0).getName())) {
	            Player pl = Bukkit.getPlayer(p_name);
	            ItemStack in_hand = pl.getItemInHand();
	            int tier = Mob.getItemTier(in_hand);
	            int mob_tier = getMobTier(ent);
	            if (mob_tier >= 4) {
	            }

	            if ((tier - mob_tier) <= 2) {
	                final List<ItemStack> ent_gear = Mob.mob_loot.get(ent);

	                // Thread t = new Thread(new Runnable() {
	                // public void run() {
	                Player p = Bukkit.getPlayer(p_name);
	                if (p.getWorld().getName().equalsIgnoreCase(ent.getWorld().getName())) { // && p.getLocation().distanceSquared(ent.getLocation()) <= 484

	                    double do_i_drop = new Random().nextInt(100); // 10% Chance.
	                    double do_i_drop_scroll = new Random().nextInt(100);
	                    // double do_i_drop_gems = new Random().nextInt(100);
	                    double drop_chance = 0;
	                    double gem_drop_chance = new Random().nextInt(100);
	                    double scroll_drop_chance = 0;
	                    // log.info(String.valueOf(gem_drop_chance));
	                    double gem_drop_amount = 0;
	                    double drop_multiplier = 1;
	                    boolean is_elite = false;
	                    // Elite = 1.5x money chance / item chance.
	                    ItemStack weapon = ent_gear.get(0);
	                    if (weapon.getEnchantments().containsKey(Enchantment.KNOCKBACK)) {
	                        is_elite = true;
	                    }

	                    if (is_elite == true) {
	                        drop_multiplier = 70;//mumoxx was here
	                    }

	                    double item_drop_multiplier = 1; //+ (ItemMechanics.ifind_data.get(p_name) / 100);
	                    double gold_drop_multiplier = 1;// + (ItemMechanics.gfind_data.get(p_name) / 100);

	                 
	                    if (weapon.getType() == Material.WOOD_AXE || weapon.getType() == Material.WOOD_SWORD || weapon.getType() == Material.WOOD_SPADE
	                            || weapon.getType() == Material.WOOD_HOE || Mob.getItemTier(weapon) == 1) { // T1
	                        scroll_drop_chance = 2;
	                        drop_chance = 15;
	                        if (gem_drop_chance <= ((50 * gold_drop_multiplier) * drop_multiplier)) {
	                            gem_drop_amount = (new Random().nextInt(12 - 2) + 2) * gold_drop_multiplier;
	                        }
	                        if (scroll_drop_chance >= do_i_drop_scroll) {
	                            // Drop a scroll. Which one?
	                            int scroll_type = new Random().nextInt(2); // 0, 1
	                            if (scroll_type == 0) {
	                             //   ent.getWorld().dropItemNaturally(ent.getLocation(),
	                                  //      CraftItemStack.asCraftCopy(TeleportationMechanics.makeUnstackable(TeleportationMechanics.Cyrennica_scroll)));
	                            }
	                            if (scroll_type == 1) {
	                               // ent.getWorld().dropItemNaturally(ent.getLocation(),
	                                //        CraftItemStack.asCraftCopy(TeleportationMechanics.makeUnstackable(TeleportationMechanics.Harrison_scroll)));
	                            }
	                        }
	                    }

	                    if (weapon.getType() == Material.STONE_AXE || weapon.getType() == Material.STONE_SWORD || weapon.getType() == Material.STONE_SPADE
	                            || weapon.getType() == Material.STONE_HOE || Mob.getItemTier(weapon) == 2) { // T2
	                        scroll_drop_chance = 4;
	                        drop_chance = 15;//was 5
	                        if (gem_drop_chance <= ((40 * gold_drop_multiplier) * drop_multiplier)) {
	                            gem_drop_amount = (new Random().nextInt(30 - 10) + 10) * gold_drop_multiplier;
	                        }
	                        if (scroll_drop_chance >= do_i_drop_scroll) {
	                            // Drop a scroll. Which one?
	                            int scroll_type = new Random().nextInt(5); // 0, 1
	                            if (scroll_type == 0) {
	                              //  ent.getWorld().dropItemNaturally(ent.getLocation(),
	                                     //   CraftItemStack.asCraftCopy(TeleportationMechanics.makeUnstackable(TeleportationMechanics.Cyrennica_scroll)));
	                            }
	                            if (scroll_type == 1) {
	                               // ent.getWorld().dropItemNaturally(ent.getLocation(),
	                                  //      CraftItemStack.asCraftCopy(TeleportationMechanics.makeUnstackable(TeleportationMechanics.Harrison_scroll)));
	                            }
	                            if (scroll_type == 2) {
	                               // ent.getWorld().dropItemNaturally(ent.getLocation(),
	                                  //      CraftItemStack.asCraftCopy(TeleportationMechanics.makeUnstackable(TeleportationMechanics.Dark_Oak_Tavern_scroll)));
	                            }
	                            if (scroll_type == 3) {
	                               // ent.getWorld().dropItemNaturally(ent.getLocation(),
	                                  //      CraftItemStack.asCraftCopy(TeleportationMechanics.makeUnstackable(TeleportationMechanics.Jagged_Rocks_Tavern)));
	                            }
	                            if (scroll_type == 4) {
	                             //   ent.getWorld().dropItemNaturally(ent.getLocation(),
	                                   //     CraftItemStack.asCraftCopy(TeleportationMechanics.makeUnstackable(TeleportationMechanics.Tripoli_scroll)));
	                            }
	                        }
	                    }

	                    if (weapon.getType() == Material.IRON_AXE || weapon.getType() == Material.IRON_SWORD || weapon.getType() == Material.IRON_SPADE
	                            || weapon.getType() == Material.IRON_HOE || Mob.getItemTier(weapon) == 3) { // T3
	                        scroll_drop_chance = 2;
	                        drop_chance = 15; // mumoxx was here it was 3 // was 4
	                        if (gem_drop_chance <= ((30 * gold_drop_multiplier) * drop_multiplier)) {
	                            gem_drop_amount = (new Random().nextInt(50 - 20) + 20) * gold_drop_multiplier;
	                        }
	                        if (scroll_drop_chance >= do_i_drop_scroll) {
	                            // Drop a scroll. Which one?
	                            int scroll_type = new Random().nextInt(5); // 0, 1, 2, 3
	                            if (scroll_type == 0) {
	                               // ent.getWorld().dropItemNaturally(ent.getLocation(), CraftItemStack.asCraftCopy(TeleportationMechanics.makeUnstackable(TeleportationMechanics.Cyrennica_scroll)));
	                            }
	                            if (scroll_type == 1) {
	                              //  ent.getWorld().dropItemNaturally(ent.getLocation(),
	                                    //    CraftItemStack.asCraftCopy(TeleportationMechanics.makeUnstackable(TeleportationMechanics.Dark_Oak_Tavern_scroll)));
	                            }
	                            if (scroll_type == 2) {
	                              //  ent.getWorld().dropItemNaturally(ent.getLocation(),
	                              //          CraftItemStack.asCraftCopy(TeleportationMechanics.makeUnstackable(TeleportationMechanics.Jagged_Rocks_Tavern)));
	                            }
	                            if (scroll_type == 3) {
	                              //  ent.getWorld().dropItemNaturally(ent.getLocation(),
	                               //         CraftItemStack.asCraftCopy(TeleportationMechanics.makeUnstackable(TeleportationMechanics.Swamp_safezone_scroll)));
	                            }
	                            if (scroll_type == 4) {
	                              //  ent.getWorld().dropItemNaturally(ent.getLocation(),
	                               //         CraftItemStack.asCraftCopy(TeleportationMechanics.makeUnstackable(TeleportationMechanics.Crestguard_keep_scroll)));
	                            }
	                        }
	                    }

	                    if (weapon.getType() == Material.DIAMOND_AXE || weapon.getType() == Material.DIAMOND_SWORD || weapon.getType() == Material.DIAMOND_HOE
	                            || weapon.getType() == Material.DIAMOND_SPADE || Mob.getItemTier(weapon) == 4) { // T4
	                        scroll_drop_chance = 1;
	                        drop_chance = 15;//was 1
	                        if (gem_drop_chance <= ((20 * gold_drop_multiplier) * drop_multiplier)) {
	                            gem_drop_amount = (new Random().nextInt(200 - 75) + 75) * gold_drop_multiplier;
	                        }
	                        if (scroll_drop_chance >= do_i_drop_scroll) {
	                            // Drop a scroll. Which one?
	                            int scroll_type = new Random().nextInt(2); // 0, 1
	                            if (scroll_type == 0) {
	                              //  ent.getWorld().dropItemNaturally(
	                                   //     ent.getLocation(),
	                                   //     CraftItemStack.asCraftCopy(TeleportationMechanics
	                                     //           .makeUnstackable(TeleportationMechanics.Deadpeaks_Mountain_Camp_scroll)));
	                            }
	                            if (scroll_type == 1) {
	                             //   ent.getWorld().dropItemNaturally(ent.getLocation(),
	                                //        CraftItemStack.asCraftCopy(TeleportationMechanics.makeUnstackable(TeleportationMechanics.Swamp_safezone_scroll)));
	                            }
	                        }
	                    }
	                    if (weapon.getType() == Material.GOLD_AXE || weapon.getType() == Material.GOLD_SWORD || weapon.getType() == Material.GOLD_HOE
	                            || weapon.getType() == Material.GOLD_SPADE || Mob.getItemTier(weapon) == 5) { // T5
	                        scroll_drop_chance = 1;
	                        drop_chance = 15;//was 1
	                        if (gem_drop_chance <= ((35 * gold_drop_multiplier) * drop_multiplier)) {
	                            gem_drop_amount = (new Random().nextInt(400 - 150) + 150) * gold_drop_multiplier;
	                        }
	                        if (scroll_drop_chance >= do_i_drop_scroll) {
	                            // Drop a scroll. Which one?
	                            int scroll_type = new Random().nextInt(2); // 0, 1
	                            if (scroll_type == 0) {
	                                //ent.getWorld().dropItemNaturally(
	                                    //    ent.getLocation(),
	                                     //   CraftItemStack.asCraftCopy(TeleportationMechanics
	                                       //         .makeUnstackable(TeleportationMechanics.Deadpeaks_Mountain_Camp_scroll)));
	                            }
	                            if (scroll_type == 1) {
	                              //  ent.getWorld().dropItemNaturally(ent.getLocation(),
	                                  //      CraftItemStack.asCraftCopy(TeleportationMechanics.makeUnstackable(TeleportationMechanics.Swamp_safezone_scroll)));
	                            }
	                        }
	                    }

	                    String mob_type = ChatColor.stripColor(Mob.getMobType(ent)).toLowerCase();
	                    if (mob_type.contains("imp") || mob_type.contains("daemon")) {
	                        scroll_drop_chance = 2;
	                        if (scroll_drop_chance >= do_i_drop_scroll) {
	                            // Drop a scroll. Which one?
	                            int scroll_type = new Random().nextInt(1); // 0
	                            if (scroll_type == 0) {
	                             //   ent.getWorld().dropItemNaturally(ent.getLocation(),
	                                   //     CraftItemStack.asCraftCopy(TeleportationMechanics.makeUnstackable(TeleportationMechanics.Crestguard_keep_scroll)));
	                            }
	                        }
	                    }

	                    /*
	                     * String mob_type = getMobType(ent); if(mob_type.equalsIgnoreCase("naga")){ // GLOOMY scroll_drop_chance = 3; if(scroll_drop_chance >=
	                     * do_i_drop_scroll){ // Drop a scroll. Which one? int scroll_type = new Random().nextInt(1); // 0 if(scroll_type == 0){
	                     * ent.getWorld().dropItemNaturally(ent.getLocation(),
	                     * CraftItemStack.asCraftCopy(TeleportationMechanics.makeUnstackable(TeleportationMechanics.Swamp_safezone_scroll))); } } }
	                     * if(mob_type.equalsIgnoreCase("tripoli soldier") || mob_type.equalsIgnoreCase("lizardman")){ // TRIPOLI scroll_drop_chance = 2;
	                     * if(scroll_drop_chance >= do_i_drop_scroll){ // Drop a scroll. Which one? int scroll_type = new Random().nextInt(1); // 0 if(scroll_type
	                     * == 0){ ent.getWorld().dropItemNaturally(ent.getLocation(),
	                     * CraftItemStack.asCraftCopy(TeleportationMechanics.makeUnstackable(TeleportationMechanics.Tripoli_scroll))); } } }
	                     */

	                    drop_chance = (drop_chance * item_drop_multiplier);
	                    drop_chance = (drop_chance * drop_multiplier);

	                    if (is_elite) {
	                        if (mob_tier == 1) {
	                            drop_chance = 80;//was 80
	                        }
	                        if (mob_tier == 2) {
	                            drop_chance = 80;//was 50
	                        }
	                        if (mob_tier == 3) {
	                            drop_chance = 80;//was 30
	                        }
	                        if (mob_tier == 4) {
	                            drop_chance = 80;//was 10
	                        }
	                        if (mob_tier == 5) {
	                            drop_chance = 95;//was 1
	                        }
	                    }
	                    
	                 
	                    
	                            
	                
	                    int do_i_drop_arrows = new Random().nextInt(100);
	                    int do_i_drop_quiver = new Random().nextInt(100);
	                    if (weapon.getType() == Material.BOW && do_i_drop_arrows <= 85) { // Drop some arrows!
	                        int amount_to_drop = new Random().nextInt(30) + 10;

	                        if (mob_tier == 1) {
	                            ItemStack arrow_loot = ItemMechanics.t1_arrow;
	                            arrow_loot.setAmount(amount_to_drop);
	                            ent.getWorld().dropItemNaturally(ent.getLocation(), arrow_loot);
	                        }

	                        if (mob_tier == 2) {
	                            ItemStack arrow_loot = ItemMechanics.t2_arrow;
	                            arrow_loot.setAmount(amount_to_drop);
	                            ent.getWorld().dropItemNaturally(ent.getLocation(), arrow_loot);
	                        }
	                        if (mob_tier == 3) {
	                            int drop_t4 = new Random().nextInt(100);
	                            ItemStack arrow_loot = null;
	                            if (drop_t4 <= 5) {
	                                arrow_loot = ItemMechanics.t4_arrow;
	                            } else {
	                                arrow_loot = ItemMechanics.t3_arrow;
	                            }
	                            arrow_loot.setAmount(amount_to_drop);
	                            ent.getWorld().dropItemNaturally(ent.getLocation(), arrow_loot);
	                            if (do_i_drop_quiver <= 10) {
	                               // ent.getWorld().dropItemNaturally(ent.getLocation(), ItemMechanics.t1_quiver);
	                            }
	                        }
	                        if (mob_tier == 4) {
	                            int drop_t5 = new Random().nextInt(100);
	                            ItemStack arrow_loot = null;
	                            if (drop_t5 <= 15) {
	                                arrow_loot = ItemMechanics.t5_arrow;
	                            } else {
	                                arrow_loot = ItemMechanics.t4_arrow;
	                            }
	                            arrow_loot.setAmount(amount_to_drop);
	                            ent.getWorld().dropItemNaturally(ent.getLocation(), arrow_loot);
	                            if (do_i_drop_quiver <= 10) {
	                             //   ent.getWorld().dropItemNaturally(ent.getLocation(), ItemMechanics.t1_quiver);
	                            }
	                        }
	                        if (mob_tier == 5) {
	                            ItemStack arrow_loot = ItemMechanics.t5_arrow;
	                            arrow_loot.setAmount(amount_to_drop);
	                            ent.getWorld().dropItemNaturally(ent.getLocation(), arrow_loot);
	                            if (do_i_drop_quiver <= 10) {
	                             //   ent.getWorld().dropItemNaturally(ent.getLocation(), ItemMechanics.t1_quiver);
	                            }
	                        }
	                    }

	                    // EASTER EGG DROP CODE
	                    /*
	                     * int do_i_drop_easter = new Random().nextInt(2000); if (mob_tier <= 3) { if (do_i_drop_easter == 1) { // 0.05% chance Location loc =
	                     * ent.getLocation(); loc.getWorld().dropItemNaturally(loc, CraftItemStack.asCraftCopy(ItemMechanics.easter_egg)); } } if (mob_tier > 3) {
	                     * // 4, 5 if (do_i_drop_easter <= 10) { // 0.5% chance Location loc = ent.getLocation(); loc.getWorld().dropItemNaturally(loc,
	                     * CraftItemStack.asCraftCopy(ItemMechanics.easter_egg)); } }
	                     */
	                    // HALLOWEEN DROP CODE
	                    /*
	                     * int do_i_drop_candy = new Random().nextInt(1000); int do_i_drop_mask = new Random().nextInt(10000); ItemStack candy =
	                     * CraftItemStack.asCraftCopy(Halloween.halloween_candy); ItemStack mask = CraftItemStack.asCraftCopy(Halloween.halloween_mask);
	                     * 
	                     * if(mob_tier == 1 && do_i_drop_candy == 0){ candy.setAmount(new Random().nextInt(4) + 2);
	                     * ent.getLocation().getWorld().dropItemNaturally(ent.getLocation(), candy);}
	                     * 
	                     * if(mob_tier == 2 && do_i_drop_candy <= 5){ candy.setAmount(new Random().nextInt(4) + 2);
	                     * ent.getLocation().getWorld().dropItemNaturally(ent.getLocation(), candy);}
	                     * 
	                     * if(mob_tier == 3 && do_i_drop_candy <= 10){ candy.setAmount(new Random().nextInt(4) + 2);
	                     * ent.getLocation().getWorld().dropItemNaturally(ent.getLocation(), candy);}
	                     * 
	                     * if(mob_tier == 4 && do_i_drop_candy <= 20){ candy.setAmount(new Random().nextInt(4) + 2);
	                     * ent.getLocation().getWorld().dropItemNaturally(ent.getLocation(), candy);}
	                     * 
	                     * if(mob_tier == 5 && do_i_drop_candy <= 30){ candy.setAmount(new Random().nextInt(4) + 2);
	                     * ent.getLocation().getWorld().dropItemNaturally(ent.getLocation(), candy);}
	                     * 
	                     * 
	                     * if(mob_tier == 1 && do_i_drop_mask == 0) ent.getLocation().getWorld().dropItemNaturally(ent.getLocation(), mask);
	                     * 
	                     * if(mob_tier == 2 && do_i_drop_mask <= 2) ent.getLocation().getWorld().dropItemNaturally(ent.getLocation(), mask);
	                     * 
	                     * if(mob_tier == 3 && do_i_drop_mask <= 5) ent.getLocation().getWorld().dropItemNaturally(ent.getLocation(), mask);
	                     * 
	                     * if(mob_tier == 4 && do_i_drop_mask <= 7) ent.getLocation().getWorld().dropItemNaturally(ent.getLocation(), mask);
	                     * 
	                     * if(mob_tier == 5 && do_i_drop_mask <= 12) ent.getLocation().getWorld().dropItemNaturally(ent.getLocation(), mask);
	                     */

	                    // Orb of Peace DROP CODE
	                    /*
	                     * int do_i_drop_oop = new Random().nextInt(100); if(mob_tier == 2){ if(do_i_drop_oop <= 1){ Location loc = ent.getLocation();
	                     * loc.getWorld().dropItemNaturally(loc, CraftItemStack.asCraftCopy(ItemMechanics.orb_of_peace)); } } if(mob_tier == 3){ if(do_i_drop_oop <=
	                     * 1){ Location loc = ent.getLocation(); loc.getWorld().dropItemNaturally(loc, CraftItemStack.asCraftCopy(ItemMechanics.orb_of_peace)); } }
	                     * if(mob_tier == 4){ if(do_i_drop_oop <= 1){ Location loc = ent.getLocation(); loc.getWorld().dropItemNaturally(loc,
	                     * CraftItemStack.asCraftCopy(ItemMechanics.orb_of_peace)); } } if(mob_tier == 5){ if(do_i_drop_oop <= 2){ Location loc = ent.getLocation();
	                     * loc.getWorld().dropItemNaturally(loc, CraftItemStack.asCraftCopy(ItemMechanics.orb_of_peace)); } }
	                     */

	                    if (mob_tier == 5 && !is_elite) { //
	                        int chance = new Random().nextInt(1000);
	                        if (chance <= 2) { // 0.2% chance of it equalling that.       if (chance == 500 || chance == 501)
	                            drop_chance = 100;
	                        } else {
	                            drop_chance = 0;
	                        }
	                    }
	                    if (mob_tier == 4 && !is_elite) {
	                        int chance = new Random().nextInt(1000);
	                        if (chance <= 10) { // 1% chance of it equalling that. (chance <= 5) mumoxx was here
	                            drop_chance = 100;
	                        } else {
	                            drop_chance = 0;
	                        }
	                    }

	                   // if (loot_buff == true) {
	                   //     drop_chance *= 1.20;//was 1.20
	                   // }

	                    if (drop_chance >= do_i_drop) {
	                        int drop_type = new Random().nextInt(2); // 0-1 = weapon, 1 = armor
	                        ItemStack i_gear = null;

	                        if (drop_type == 0) {
	                            i_gear = weapon;
	                        }

	                        if (drop_type == 1) {
	                            if ((ent_gear.size() - 1) > 0) {
	                                int gear_index = new Random().nextInt(ent_gear.size() - 1) + 1; // Never 0.
	                                if (gear_index > (ent_gear.size() - 1)) {
	                                    gear_index = (ent_gear.size() - 1);
	                                }

	                                i_gear = ent_gear.get(gear_index);

	                                if (i_gear.getTypeId() == 397 || i_gear.getTypeId() == 144) { // Set the drop to chest, since they'll have chest if they have
	                                                                                              // helmet / mask.
	                                    i_gear = ent_gear.get(2);
	                                }
	                            }

	                            else if ((ent_gear.size() - 1) <= 0) {
	                                i_gear = ent_gear.get(0);
	                            }

	                        }

	                        int pre_random_dur = i_gear.getType().getMaxDurability();
	                        if (pre_random_dur <= 0) {
	                            pre_random_dur = 1;
	                        }

	                        int random_dur = new Random().nextInt(pre_random_dur) + (i_gear.getType().getMaxDurability() / 10);
	                        if (random_dur > i_gear.getType().getMaxDurability()) {
	                            random_dur = i_gear.getType().getMaxDurability();
	                        }
	                        i_gear.setDurability((short) random_dur);

	                        ItemMeta im = i_gear.getItemMeta();

	                        try {
	                            if (im.hasEnchants()) {
	                                for (Entry<Enchantment, Integer> data : im.getEnchants().entrySet()) {
	                                    i_gear.removeEnchantment(data.getKey());
	                                }
	                            }
	                            i_gear.removeEnchantment(Enchantment.LOOT_BONUS_MOBS);
	                            i_gear.removeEnchantment(Enchantment.KNOCKBACK);
	                            //i_gear.removeEnchantment(EnchantMechanics.getCustomEnchant());
	                        } catch (NullPointerException npe) {
	                            npe.printStackTrace();
	                        }

	                        i_gear.setItemMeta(im);
	                        Player target = Bukkit.getPlayer(Mob.mob_target.get(ent));
	                        if (5 + 1 >= getMobTier(ent)) {
	                            if (i_gear.getType() != Material.AIR) {
	                                ent.getWorld().dropItemNaturally(ent.getLocation(), i_gear);
	                            }
	                        }
	                    }

	                    if (gem_drop_amount > 0) {
	                        // net.minecraft.server.ItemStack gems = (((CraftItemStack)
	                        // ).getHandle());
	                        while (gem_drop_amount > 64) {
	                            ItemStack i = ItemMechanics.makeGems((int) 64);
	                            Location loc = ent.getLocation();
	                            loc.getWorld().dropItemNaturally(loc, i);
	                            gem_drop_amount = gem_drop_amount - 64;
	                        }

	                        ItemStack i = ItemMechanics.makeGems((int) gem_drop_amount); // Drop the remainder.
	                        Location loc = ent.getLocation();
	                        loc.getWorld().dropItemNaturally(loc, i);
	                    }
	                }
	            }
		 }
	}
	            
		 
	
	
	public static void spawnMob(Location loc, int tier, String type, boolean elite, String customName, boolean namedElite) {
		Mob mob = new Mob("T1", type, elite, namedElite, customName, tier, "100-1500");
		mob.spawn(loc);
	}
	
	
	  public static void subtractMHealth(Entity e, int amount) {
	        if (!(Mob.mob_health.containsKey(e))) {
	            //log.info("[MonsterMechanics] Skipping subtractMHealth() for entity " + e.toString() + " due to no mob_health.");
	            return; // No data available, GG.
	        }
	        int old_hp = Mob.mob_health.get(e);
	        int new_hp = old_hp - amount;
	        // int max_health = max_mob_health.get(e);

	        boolean is_elite = false;
	        int tier = getMobTier(e);
	        // List<ItemStack> ent_gear = mob_loot.get(ent);
	        LivingEntity l = (LivingEntity) e;
	        ItemStack weapon = l.getEquipment().getItemInHand();
	        if (weapon.getEnchantments().containsKey(Enchantment.KNOCKBACK)) {
	            // log.info("ELITE!");
	            is_elite = true;
	        }

	        if (new_hp > 0) {
	        	Mob.mob_health.put(e, new_hp);
	            if (Mob.max_mob_health.containsKey(e)) {
	                LivingEntity le = (LivingEntity) e;
	               // le.setCustomName(generateOverheadBar(e, new_hp, Mob.max_mob_health.get(e), tier, is_elite));
	                updateHealthbar(le);
	                le.setCustomNameVisible(true);
	            }
	        } else if (new_hp <= 0) {
	        	Mob.mob_health.put(e, 0);
	            final LivingEntity le = (LivingEntity) e;
	            // System.out.print("SET MOB HEALTH TO 0");
	            if (Mob.max_mob_health.containsKey(e)) {
	            	updateHealthbar(le);
	                //le.setCustomName(generateOverheadBar(e, 0, Mob.max_mob_health.get(e), tier, is_elite));
	            }
	            Damageable poo = (Damageable) le;
	            le.damage(poo.getHealth());
	            if (le.getVehicle() != null) {
	                Entity mount = le.getVehicle();
	                le.eject();
	                mount.remove();
	            }
	            new BukkitRunnable() {
	                public void run() {
	                    // This addresses mobs being 1 hp.
	                    if (le != null && !le.isDead()) {
	                        // This needs to be called so the mobs will be respawned and stuff.
	                        List<ItemStack> drops = new ArrayList<ItemStack>();
	                        if (Mob.mob_loot.containsKey(le)) {
	                            drops = Mob.mob_loot.get(le);
	                        }
	                        EntityDeathEvent e = new EntityDeathEvent(le, drops);
	                        Bukkit.getPluginManager().callEvent(e);
	                        le.setHealth(0);
	                    }
	                }
	            }.runTaskLater(Main.plugin, 1L);
	        }
	  }
	  
		public static void updateHealthbar(LivingEntity l) {
		    l.setCustomName(getNameHealth(Mob.mob_health.get(l), Mob.max_mob_health.get(l), getMobName(l), l));
		    l.setCustomNameVisible(true);
		  }


	  public static int getMobTier(Entity ent) {
	        if (ent instanceof Ghast) {
	            return 4;
	        }
	        if (ent.getPassenger() != null) {
	            Entity r_ent = ent.getPassenger();
	            ent = r_ent;
	        }
	        if (Mob.mob_tier.containsKey(ent)) {
	            return Mob.mob_tier.get(ent);
	        }
	        LivingEntity le = (LivingEntity) ent;
	        ItemStack i = le.getEquipment().getItemInHand();
	        if (i == null) {
	            return -1; // No tier.
	        }
	        int wep_tier = Mob.getItemTier(i);
	        // log.info("[MonsterMechanics] No mob tier stored for entity " + ent.toString() + ", saving new value to memory. (" + wep_tier + ")");
	        Mob.mob_tier.put(ent, wep_tier);
	        return wep_tier;
	    }


		public static int getMHealth(Entity e) {
	        /*
	         * if(!(mob_loot.containsKey(e))){ return 1; // Not custom, or something is wrong. }
	         */
	        if (!(Mob.mob_health.containsKey(e))) {
	            // They're dead.
	            // log.info("No health data for " + e.toString());
	            return 0;
	            // mob_health.put(e, calculateMobHP(e));
	        }
	        return Mob.mob_health.get(e);
	    }

	    public static int getBarLength(int tier) {
	        if (tier == 1) {
	            return 25;
	        }
	        if (tier == 2) {
	            return 30;
	        }
	        if (tier == 3) {
	            return 35;
	        }
	        if (tier == 4) {
	            return 40;
	        }
	        if (tier == 5) {
	            return 50;
	        }
	        return 25;
	    }

	    public static String generateOverheadBar1(Entity ent, double cur_hp, double max_hp, int tier, boolean elite) {
	        int max_bar = getBarLength(tier);

	        ChatColor cc = null;

	        DecimalFormat df = new DecimalFormat("##.#");
	        double percent_hp = (double) (Math.round(100.0D * Double.parseDouble((df.format((cur_hp / max_hp)))))); // EX: 0.5054134131

	        if (percent_hp <= 0 && cur_hp > 0) {
	            percent_hp = 1;
	        }

	      

	        double percent_interval = (100.0D / max_bar);
	        int bar_count = 0;

	        cc = ChatColor.GREEN;
	        if (percent_hp <= 45) {
	            cc = ChatColor.YELLOW;
	        }
	        if (percent_hp <= 20) {
	            cc = ChatColor.RED;
	        }
	      
	//was � //was ▉
	        String return_string = cc + ChatColor.BOLD.toString() + "" + ChatColor.RESET.toString() + cc.toString() + "";
	        if (elite) {
	            return_string += ChatColor.BOLD.toString();
	        }

	        while (percent_hp > 0 && bar_count < max_bar) {
	            percent_hp -= percent_interval;
	            bar_count++;
	            return_string += "|";
	        }

	        return_string += ChatColor.BLACK.toString();

	        if (elite) {
	            return_string += ChatColor.BOLD.toString();
	        }

	        while (bar_count < max_bar) {
	            return_string += "|";
	            bar_count++;
	        }
	        //was � //was ▉
	        return_string = return_string + cc + ChatColor.BOLD.toString() + "";

	        // TODO: Generate activity bar status

	        /*if (special_attack.containsKey(ent) && cur_hp > 0) {

	            return_string += ChatColor.LIGHT_PURPLE.toString();

	            int special_bar_length = 5;
	            int charge_state = special_attack.get(ent);
	            int charge_count = charge_state;

	            while (charge_state > 0) {
	                return_string += "|";
	                charge_state--;
	            }

	            return_string += ChatColor.BLACK.toString();

	            while ((special_bar_length - charge_count) > 0) {
	                return_string += "|";
	                charge_count++;
	            }

	            return_string += cc.toString() + ChatColor.BOLD + "â•‘";
	        } /*else{ return_string += ChatColor.BLACK.toString() + "|||||" + cc.toString() +
	          ChatColor.BOLD.toString() + "â•‘"; }
	         */
	        return_string = Utils.colorCodes(cc + "&l▉" + return_string + cc +"&l▉");
	            return return_string;
	            // 20 Bars, that's 5% HP per bar
	        
	    }

		public static String getMobName(LivingEntity mob) {
			if (Mob.mob_name.containsKey(mob)) {
				return Utils.colorCodes(Mob.mob_name.get(mob));
			}
			return null;
		}
		
		public static String getNameHealth(double health, double maxhealth, String mob_name, LivingEntity instance) {
		    String inpat = mob_name;
		    boolean isBold = false;
		    if (inpat.contains(ChatColor.BOLD + "")) {
		      isBold = true;
		    }
		    int max = 45;//was 45
		    int min = 30;//was 30
		    int toPutIn = (int)(maxhealth / 30.0D);
		    if (toPutIn < min) {
		      toPutIn = min;
		    }
		    if (toPutIn > max) {
		      toPutIn = max;
		    }
		    mob_name = "";
		    for (int i = 0; i < toPutIn; i++) {
		      mob_name = mob_name + "|";
		    }
		    mob_name = mob_name.replaceAll("&0", ChatColor.BLACK.toString());
		    mob_name = mob_name.replaceAll("&1", ChatColor.DARK_BLUE.toString());
		    mob_name = mob_name.replaceAll("&2", ChatColor.DARK_GREEN.toString());
		    mob_name = mob_name.replaceAll("&3", ChatColor.DARK_AQUA.toString());
		    mob_name = mob_name.replaceAll("&4", ChatColor.DARK_RED.toString());
		    mob_name = mob_name.replaceAll("&5", ChatColor.DARK_PURPLE.toString());
		    mob_name = mob_name.replaceAll("&6", ChatColor.GOLD.toString());
		    mob_name = mob_name.replaceAll("&7", ChatColor.GRAY.toString());
		    mob_name = mob_name.replaceAll("&8", ChatColor.DARK_GRAY.toString());
		    mob_name = mob_name.replaceAll("&9", ChatColor.BLUE.toString());
		    mob_name = mob_name.replaceAll("&a", ChatColor.GREEN.toString());
		    mob_name = mob_name.replaceAll("&b", ChatColor.AQUA.toString());
		    mob_name = mob_name.replaceAll("&c", ChatColor.RED.toString());
		    mob_name = mob_name.replaceAll("&d", ChatColor.LIGHT_PURPLE.toString());
		    mob_name = mob_name.replaceAll("&e", ChatColor.YELLOW.toString());
		    mob_name = mob_name.replaceAll("&f", ChatColor.WHITE.toString());
		    mob_name = mob_name.replaceAll("&u", ChatColor.UNDERLINE.toString());
		    mob_name = mob_name.replaceAll("&l", ChatColor.BOLD.toString());
		    mob_name = mob_name.replaceAll("&i", ChatColor.ITALIC.toString());
		    mob_name = mob_name.replaceAll("&m", ChatColor.MAGIC.toString());
		    double percent = health / maxhealth;
		    double length = mob_name.length();
		    double scolor = length * percent;
		    int color = (int)Math.ceil(scolor);
		    String charBlank = colorCodes("&0");
		    if (isBold) {
		      charBlank = charBlank + colorCodes("&l");
		    }
		    String n = mob_name;
		    String adding = colorCodes("&a");
		    if (color < length / 3.0D) {
		      String g = n.substring(color);
		      String char1 = colorCodes("&c");
		      String char2 = colorCodes("&d");
		      adding = char1;
		      if (isBold) {
		        char1 = char1 + colorCodes("&l");
		        char2 = char2 + colorCodes("&l");
		      }
		      String h = n.substring(0, color);
		      if (chargedMobs.contains(instance)) {
		        n = char2 + h + charBlank + g;
		      } else {
		        n = char1 + h + charBlank + g;
		      }
		    } else if (color < length / 1.5D) {
		      String g = n.substring(color);
		      String char1 = colorCodes("&e");
		      String char2 = colorCodes("&d");
		      adding = char1;
		      if (isBold) {
		        char1 = char1 + colorCodes("&l");
		        char2 = char2 + colorCodes("&l");
		      }
		      String h = n.substring(0, color);
		      if (chargedMobs.contains(instance)) {
		        n = char2 + h + charBlank + g;
		      } else {
		        n = char1 + h + charBlank + g;
		      }
		    } else {
		      String g = n.substring(color);
		      String h = n.substring(0, color);
		      String char1 = colorCodes("&a");
		      String char2 = colorCodes("&d");
		      adding = char1;
		      if (isBold) {
		        char1 = char1 + colorCodes("&l");
		        char2 = char2 + colorCodes("&l");
		      }
		      if (chargedMobs.contains(instance)) {
		        n = char2 + h + charBlank + g;
		      } else {
		        n = char1 + h + charBlank + g;
		      }
		    }
		    if (chargedMobs.contains(instance)) {
		      adding = colorCodes("&d");
		    }
		    if (isBold) {
		      adding = adding + colorCodes("&l");
		    }
		    adding = adding + "▉";
		    n = adding + n;
		    n = n + adding;
		    n = n.trim();
		    return n;
		}

		public static String colorCodes(String string) {
			return Utils.colorCodes(string);
		}

}

package com.practiceserver.mobs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftSkeleton;
import org.bukkit.craftbukkit.v1_10_R1.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Ghast;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Skeleton;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import com.practiceserver.Main;
import com.practiceserver.itemgenerator.ItemGenerator;
import com.practiceserver.itemgenerator.enums.ItemRarity;
import com.practiceserver.itemgenerator.enums.ItemTier;
import com.practiceserver.itemgenerator.enums.ItemType;

import net.minecraft.server.v1_10_R1.EntityLiving;
import net.minecraft.server.v1_10_R1.EnumItemSlot;
import net.minecraft.server.v1_10_R1.EnumSkeletonType;
import net.minecraft.server.v1_10_R1.NBTTagCompound;



public class Mob {
	
	
    public static ConcurrentHashMap<Entity, String> mob_target = new ConcurrentHashMap<Entity, String>();
	 public static ConcurrentHashMap<Entity, Integer> mob_health = new ConcurrentHashMap<Entity, Integer>();
	    static HashMap<Entity, Integer> max_mob_health = new HashMap<Entity, Integer>();
	    public static HashMap<Entity, List<Integer>> mob_damage = new HashMap<Entity, List<Integer>>();
	    public static HashMap<Entity, Integer> mob_armor = new HashMap<Entity, Integer>();
	    static HashMap<Entity, Integer> mob_tier = new HashMap<Entity, Integer>();
	    public static HashMap<Entity, List<ItemStack>> mob_loot = new HashMap<Entity, List<ItemStack>>();
	    public static HashMap<Entity, Integer> mob_level = new HashMap<Entity, Integer>();
	    public static HashMap<Entity, String> mob_name = new HashMap<Entity, String>();
	//TODO recode armor setter
	
	private String type;
	private boolean elite;
	private boolean namedElite;
	private String mobName;
	private int tier;
	private String rawhp;
	private double minhp;
	private double maxhp;
	private String id;
	private Entity entity;
	
	//
	public Mob(String id, String type, boolean elite, boolean namedElite, String mobName, int tier, String rawhp) {
		this.type = type;
		this.rawhp = rawhp;
		this.elite = elite;
		this.namedElite = namedElite;
		this.mobName = mobName;
		this.tier = tier;
		this.id = id;
		double min = Double.parseDouble(rawhp.split("-")[0]);
		double max = Double.parseDouble(rawhp.split("-")[1]);
		this.setMaxHP(max);
		this.setMinHP(min);
		
	}
	
	public String getID() {
		return id;
	}
	
	public static ItemStack getHead(String player_name) {

        ItemStack c_mask = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        net.minecraft.server.v1_10_R1.ItemStack mItem = CraftItemStack.asNMSCopy(c_mask);
        NBTTagCompound tag = mItem.getTag();
        tag = new NBTTagCompound();
        tag.setString("SkullOwner", player_name);
        return CraftItemStack.asBukkitCopy(mItem);
    }

	public Entity getEntity() {
		return entity;
	}
	
	public void spawn(Location loc) {
		com.practiceserver.mob1.Mob.getInstance().spawnMob(getID(), loc);
	}
	
	 public static int calculateMobHP(Entity e) {
	        int total_hp = 10;

	        if (mob_loot.containsKey(e)) {
	            for (ItemStack i : mob_loot.get(e)) {
	                if (i == null || i.getType() == Material.AIR) {
	                    continue;
	                }
	                total_hp += ItemMechanics.getHealthVal(i);
	            }
	        }

	        return total_hp;
	 }
	
    public static ItemStack generateMobGear(ItemType type, ItemTier tier) {
        int armor_range_check = new Random().nextInt(100);
        ItemRarity rarity = null;
        
        if (armor_range_check <= 80) {
            rarity = ItemRarity.COMMON;
        }
        else if (armor_range_check < 95) {
            rarity = ItemRarity.UNCOMMON;
        }
        else if (armor_range_check < 98) {
            rarity = ItemRarity.RARE;
        }
        else if (armor_range_check == 99) {
            rarity = ItemRarity.UNIQUE;
        }
        
        if (rarity == null) {
            rarity = ItemRarity.COMMON;
        }
        
        return (new ItemGenerator()).setType(type).setTier(tier).setRarity(rarity).generateItem().getItem();
    }

    public static ItemStack spawnRandomWeapon(ItemTier tier, boolean no_bow, boolean no_weapon) {
        if (no_weapon == true) {
            int wep_type = new Random().nextInt(2);
            if (wep_type == 0) {
                return generateMobGear(ItemType.SWORD, tier);
            }
            if (wep_type == 1) {
                return generateMobGear(ItemType.AXE, tier);
            }
        } else if (no_bow == true && no_weapon == false) {
            int wep_type = new Random().nextInt(4);
            if (wep_type == 0) {
                return generateMobGear(ItemType.SWORD, tier);
            }
            else if (wep_type == 1) {
                return generateMobGear(ItemType.AXE, tier);
            }
            else if (wep_type == 2) {
                return generateMobGear(ItemType.POLEARM, tier);
            }
            else if (wep_type == 3) {
                return generateMobGear(ItemType.STAFF, tier);
            }
        } else if (no_bow == false && no_weapon == false) {
            int wep_type = new Random().nextInt(5);
            if (wep_type == 0) {
                return generateMobGear(ItemType.SWORD, tier);
            }
            else if (wep_type == 1) {
                return generateMobGear(ItemType.AXE, tier);
            }
            else if (wep_type == 2) {
                return generateMobGear(ItemType.BOW, tier);
            }
            else if (wep_type == 3) {
                return generateMobGear(ItemType.POLEARM, tier);
            }
            else if (wep_type == 4) {
                return generateMobGear(ItemType.STAFF, tier);
            }
        }
        return generateMobGear(ItemType.SWORD, tier);
    }
	
	public void setupGear(int tier, String type) {
		ItemTier itemTier = ItemTier.getTierFromInt(tier);
		String meta_data = type;
		EntityType et = getEntityType();
		CopyOnWriteArrayList<ItemStack> gear_list = new CopyOnWriteArrayList<ItemStack>(Arrays.asList(new ItemStack(Material.AIR), new ItemStack(Material.AIR),
                new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR)));

        int gear_check = new Random().nextInt(3) + 1; // 1, 2, 3, 4
        net.minecraft.server.v1_10_R1.ItemStack weapon = null;
        ItemStack is_weapon = null;

        if (et == EntityType.WOLF || et == EntityType.IRON_GOLEM || et == EntityType.ENDERMAN || et == EntityType.BLAZE || et == EntityType.SILVERFISH
                || et == EntityType.WITCH || et == EntityType.MAGMA_CUBE || et == EntityType.SPIDER || et == EntityType.CAVE_SPIDER) {
            is_weapon = spawnRandomWeapon(itemTier, false, true);
        } else if (et == EntityType.PIG_ZOMBIE) {
            is_weapon = spawnRandomWeapon(itemTier, true, false);
        } else {
            is_weapon = spawnRandomWeapon(itemTier, false, false);
        }
        if (elite == true) {
            is_weapon.addUnsafeEnchantment(Enchantment.KNOCKBACK, 2);
        }
        weapon = CraftItemStack.asNMSCopy(is_weapon);

        // ent.setEquipment(0, weapon);
        gear_list.set(0, is_weapon);

        if (elite == true) {
            gear_check = 4;
        }

        if (tier == 3) {
            int type1 = new Random().nextInt(2); // 0, 1
            if (type1 == 0) {
                gear_check = 3;
            }
            if (type1 == 1) {
                gear_check = 4;
            }
        }
        if (tier >= 4) {
            gear_check = 4;
        }

        ItemStack boots = null, legs = null, chest = null, helmet = null;
        Random armor_type = new Random();
        int larmor_type = 0;

        while (gear_check > 0) {
            larmor_type = armor_type.nextInt(4) + 1; // 1, 2, 3, 4
            if (larmor_type == 1 && boots == null) {
                boots = generateMobGear(ItemType.BOOTS, itemTier);
                if (elite == true) {
                	ItemMechanics.addGlow(boots);
                    //boots.addUnsafeEnchantment(EnchantMechanics.getCustomEnchant(), 1);
                }
                gear_list.set(1, boots);
                gear_check = gear_check - 1;
            }
            if (larmor_type == 2 && legs == null) {
                legs = generateMobGear(ItemType.LEGGINGS, itemTier);
                if (elite == true) {
                	ItemMechanics.addGlow(legs);
                    //legs.addUnsafeEnchantment(EnchantMechanics.getCustomEnchant(), 1);
                }
                gear_list.set(2, legs);
                gear_check = gear_check - 1;
            }
            if (larmor_type == 3 && chest == null) {
                chest = generateMobGear(ItemType.CHESTPLATE, itemTier);
                if (elite == true) {
                	ItemMechanics.addGlow(chest);
                    //chest.addUnsafeEnchantment(EnchantMechanics.getCustomEnchant(), 1);
                }
                gear_list.set(3, chest);
                gear_check = gear_check - 1;
            }
            if (larmor_type == 4 && helmet == null) {
                helmet = generateMobGear(ItemType.HELMET, itemTier);
                if (elite == true) {
                	ItemMechanics.addGlow(helmet);
                    //helmet.addUnsafeEnchantment(EnchantMechanics.getCustomEnchant(), 1);
                }
                gear_list.set(4, helmet);
                gear_check = gear_check - 1;
            }

            continue;
        }

        if (getNamedElite()) {
            // We need to check if they have any armor or weapon, if so we'll need to give/equip it to them.
            List<ItemStack> drops = getDrops();
            for (ItemStack is : drops) {
                if (is == null) {
                    continue;
                }
                String mat_name = is.getType().name().toLowerCase();
                if (mat_name.contains("helmet") && !ItemMechanics.getArmorData(is).equalsIgnoreCase("no")) {
                    helmet = is;
                    if (elite == true) {
                    	ItemMechanics.addGlow(helmet);
                       // helmet.addUnsafeEnchantment(EnchantMechanics.getCustomEnchant(), 1);
                    }
                    gear_list.set(4, helmet);
                }
                if (mat_name.contains("chestplate") && !ItemMechanics.getArmorData(is).equalsIgnoreCase("no")) {
                    chest = is;
                    if (elite == true) {
                    	ItemMechanics.addGlow(chest);
                        //chest.addUnsafeEnchantment(EnchantMechanics.getCustomEnchant(), 1);
                    }
                    gear_list.set(4, chest);
                }
                if (mat_name.contains("leggings") && !ItemMechanics.getArmorData(is).equalsIgnoreCase("no")) {
                    legs = is;
                    if (elite == true) {
                    	ItemMechanics.addGlow(legs);
                        //legs.addUnsafeEnchantment(EnchantMechanics.getCustomEnchant(), 1);
                    }
                    gear_list.set(4, legs);
                }
                if (mat_name.contains("boots") && !ItemMechanics.getArmorData(is).equalsIgnoreCase("no")) {
                    boots = is;
                    if (elite == true) {
                    	ItemMechanics.addGlow(boots);
                      //  boots.addUnsafeEnchantment(EnchantMechanics.getCustomEnchant(), 1);
                    }
                    gear_list.set(4, boots);
                }

                if (!ItemMechanics.getDamageData(is).equalsIgnoreCase("no")) {
                    is_weapon = is;
                    if (elite == true) {
                    	ItemMechanics.addGlow(is_weapon);
                        //is_weapon.addUnsafeEnchantment(EnchantMechanics.getCustomEnchant(), 1);
                    }
                    gear_list.set(0, is_weapon);
                }

            }
        }

        if (!meta_data.equalsIgnoreCase("")) {
            if (meta_data.equalsIgnoreCase("acolyte")) {

                if (helmet != null) {
                    gear_list.remove(4);
                }
            }
            if (meta_data.equalsIgnoreCase("tripoli1")) {

                if (helmet != null) {
                    gear_list.remove(4);
                }
            }
            if (meta_data.equalsIgnoreCase("naga")) {

                if (helmet != null) {
                    gear_list.remove(4);
                }
            }
            if (meta_data.equalsIgnoreCase("lizardman")) {

                if (helmet != null) {
                    gear_list.remove(4);
                }
            }
            if (meta_data.equalsIgnoreCase("troll1")) {

                if (helmet != null) {
                    gear_list.remove(4);
                }
            }
            if (meta_data.equalsIgnoreCase("goblin")) {

                if (helmet != null) {
                    gear_list.remove(4);
                }
            }
            if (meta_data.equalsIgnoreCase("bandit") || meta_data.equalsIgnoreCase("monk")) {

                if (chest == null) { // They don't normally have the torso.
                    chest = generateMobGear(ItemType.CHESTPLATE, itemTier);
                    if (elite == true) {
                    	ItemMechanics.addGlow(chest);
                       // chest.addUnsafeEnchantment(EnchantMechanics.getCustomEnchant(), 1);
                    }
                    gear_list.set(3, chest);
                }

                if (gear_check >= 4) {
                    gear_list.remove(4);
                }
            }
        }

        double total_hp = 1;

        for (ItemStack i : gear_list) {
            if (i == null || i.getType() == Material.AIR || i == is_weapon) {
                continue;
            }
            total_hp += ItemMechanics.getHealthVal(i);
        }

        int total_armor = 0;

        for (ItemStack i : gear_list) {
            if (i == null || i.getType() == Material.AIR || i == is_weapon) {
                continue;
            }
            if (ItemMechanics.getArmorVal(i, false) == null) {
                continue;
            }
            List<Integer> armor_vals = new ArrayList<Integer>(ItemMechanics.getArmorVal(i, true));
            int median = armor_vals.get(0) + (armor_vals.get(1) - armor_vals.get(0));
            total_armor += median;
        }

        double total_armor_dmg = 0;

        for (ItemStack i : gear_list) {
            if (i == null || i.getType() == Material.AIR || i == is_weapon) {
                continue;
            }
            if (ItemMechanics.getDmgVal(i, false) == null) {
                continue;
            }
            List<Integer> armor_vals = new ArrayList<Integer>(ItemMechanics.getDmgVal(i, true));
            int median = armor_vals.get(0) + (armor_vals.get(1) - armor_vals.get(0));
            total_armor_dmg += median;
        }

        total_armor_dmg = total_armor_dmg / 100.0D;

        //List<Integer> tdmg_range = ItemMechanics.getDmgRangeOfWeapon(is_weapon);
        /*
         * NBTTagList description = CraftItemStack.asNMSCopy(is_weapon).getTag().getCompound("display").getList("Lore", 9); String raw_dmg_data =
         * description.get(0).toString(); raw_dmg_data = raw_dmg_data.replaceAll(ChatColor.RED.toString() + "DMG: ", ""); raw_dmg_data =
         * raw_dmg_data.replaceAll(" ", "");
         * 
         * String[] dmg_data = raw_dmg_data.split("-");
         */
        List<Integer> tdmg_range = ItemMechanics.getDmgRangeOfWeapon(is_weapon);

        double min_dmg = tdmg_range.get(0);
        double max_dmg = tdmg_range.get(1);

        if (tier == 1) {
            if (elite == false) {
                min_dmg = (min_dmg * 0.8);
                max_dmg = (max_dmg * 0.8);
                total_hp = (total_hp * 0.40);
            }
            if (elite == true) {
                min_dmg = (min_dmg * 2.50);
                max_dmg = (max_dmg * 2.50);
                total_hp = (total_hp * 1.80);
            }
        }
        if (tier == 2) {
            if (elite == false) {
                min_dmg = (min_dmg * 0.9);
                max_dmg = (max_dmg * 0.9);
                total_hp = (total_hp * 0.9);
            }
            if (elite == true) {
                min_dmg = (min_dmg * 2.50);
                max_dmg = (max_dmg * 2.50);
                total_hp = (total_hp * 2.50);
            }

        }
        if (tier == 3) {
            if (elite == false) {
                min_dmg = (min_dmg * 1.20);
                max_dmg = (max_dmg * 1.20);
                total_hp = (total_hp * 1.20);
            }
            if (elite == true) {
                min_dmg = (min_dmg * 3.00);
                max_dmg = (max_dmg * 3.00);
                total_hp = (total_hp * 3.00);
            }
        }
        if (tier == 4) {
            if (elite == false) {
                min_dmg = (min_dmg * 1.4);
                max_dmg = (max_dmg * 1.4);
                total_hp = (total_hp * 1.4);
            }
            if (elite == true) {
                min_dmg = (min_dmg * 5.00);
                max_dmg = (max_dmg * 5.00);
                total_hp = (total_hp * 5.00);
            }
        }
        if (tier == 5) {
            if (elite == false) {
                min_dmg = (min_dmg * 2.00);
                max_dmg = (max_dmg * 2.00);
                total_hp = (total_hp * 2.00);
            }
            if (elite == true) {
                min_dmg = (min_dmg * 7.0);
                max_dmg = (max_dmg * 7.0);
                total_hp = (total_hp * 7.0);
            }
        }

        if (min_dmg < 1) {
            min_dmg = 1;
        }

        if (max_dmg < 1) {
            max_dmg = 1;
        }

        int mob_l = 1;
        if (tier == 1) {
            mob_l = 1;
        } else if (tier == 2) {
            mob_l = 20;
        } else if (tier == 3) {
            mob_l = 40;
        } else if (tier == 4) {
            mob_l = 60;
        } else if (tier == 5) {
            mob_l = 80;
        }
        List<Integer> dmg_range = new ArrayList<Integer>();
        min_dmg += ((double) min_dmg * (double) total_armor_dmg);
        max_dmg += ((double) max_dmg * (double) total_armor_dmg);
        min_dmg += (int) ((mob_l * .01) * mob_l);
        max_dmg += (int) ((mob_l * .01) * mob_l);
        dmg_range.add((int) Math.round(min_dmg));
        dmg_range.add((int) Math.round(max_dmg));
        
        total_hp += (int) ((mob_l * .1) * mob_l) + new Random().nextInt(mob_l) + mob_l;
        mob_tier.put(entity, tier);
        max_mob_health.put(entity, (int) total_hp);
        mob_health.put(entity, (int) total_hp);
        mob_damage.put(entity, dmg_range);
        mob_armor.put(entity, total_armor);
        for (ItemStack i : gear_list) {
            if (i == null || i.getType() == Material.AIR || i.getType() == Material.SKULL_ITEM) {
                gear_list.remove(i);
            }
        }
        LivingEntity lol = (LivingEntity) entity;

        mob_loot.put(entity, gear_list);
        EntityLiving ent = ((CraftLivingEntity) entity).getHandle();
        if (((meta_data.equalsIgnoreCase("wither") || (elite == true && (meta_data.equalsIgnoreCase("bandit") || meta_data.equalsIgnoreCase("monk")))) && entity instanceof CraftSkeleton)) {
            // Make it a wither skeleton.
        	 ((CraftSkeleton) entity).getHandle().setSkeletonType(EnumSkeletonType.WITHER);
        }

        if (meta_data.equalsIgnoreCase("imp")) {
            PigZombie pz = (PigZombie) entity;
            pz.setBaby(true);
        }
        ent.setEquipment(EnumItemSlot.MAINHAND, weapon);
        lol.getEquipment().setItemInHand(CraftItemStack.asBukkitCopy(weapon));
        if (boots != null) {
        	 lol.getEquipment().setBoots(CraftItemStack.asCraftCopy(gear_list.get(1)));
        
        }
        if (legs != null) {
        	lol.getEquipment().setLeggings(CraftItemStack.asCraftCopy(gear_list.get(2)));
        	//ent.setEquipment(EnumItemSlot.LEGS, CraftItemStack.asNMSCopy(gear_list.get(2)));
        }
        if (chest != null) {
        	lol.getEquipment().setChestplate(CraftItemStack.asCraftCopy(gear_list.get(3)));
        	//ent.setEquipment(EnumItemSlot.CHEST, CraftItemStack.asNMSCopy(gear_list.get(3)));
        }
        if (helmet != null && meta_data.equalsIgnoreCase("")) {
        	lol.getEquipment().setHelmet(CraftItemStack.asCraftCopy(gear_list.get(4)));
        	//ent.setEquipment(EnumItemSlot.HEAD, CraftItemStack.asNMSCopy(gear_list.get(4)));
        }
		
	}

	private List<ItemStack> getDrops() {
		// TODO Auto-generated method stub
		return null;
	}


	public String getName(int tier, String type) {
		Entity e = getEntity();
		LivingEntity le = (LivingEntity) e;
		String mob_name = "";
		ItemStack weapon = le.getEquipment().getItemInHand();
		mob_name = type;
		EntityType et = getEntityType();
		if (et == EntityType.IRON_GOLEM) {
            mob_name = "Golem";
        }
        if (et == EntityType.SKELETON && type.equalsIgnoreCase("Chaosskeleton")) {
            mob_name = "Chaos Skeleton";
        }
        if (mob_name == "") { // No custom meta_data
            String system_name = (et.getName().substring(0, 1).toUpperCase() + et.getName().substring(1, et.getName().length()).toLowerCase()).replaceAll("_",
                    " ");
            mob_name = system_name;
        }

        // Now that the main name is set, we need to set the prefix/suffix based on their tier.

        // Ok, now we need to do checks for elemental damage
        String elemental_type = "";
        String elemental_prefix = "";
        int elemental_chance = new Random().nextInt(100);

        if (mob_name.equalsIgnoreCase("bandit") || mob_name.equalsIgnoreCase("monk")) { // Poison
            int do_i_elemental = 15;
            if (do_i_elemental >= elemental_chance) {
                // Give them elemental damage!
                elemental_type = "Poison";
                elemental_prefix = ChatColor.DARK_GREEN.toString(); // + Chatcolor.BOLD.toString() + "P";
               // attachPotionEffect(le, 0x669900);
            }
        }
        if (et == EntityType.IRON_GOLEM) { // Ice
            int do_i_elemental = 30;
            if (do_i_elemental >= elemental_chance) {
                // Give them elemental damage!
                elemental_type = "Ice";
                elemental_prefix = ChatColor.BLUE.toString(); // + Chatcolor.BOLD.toString() + "I";
           //     attachPotionEffect(le, 0x33FFFF);
            }
        }
        if (et == EntityType.MAGMA_CUBE) {
            int do_i_elemental = 15;
            if (do_i_elemental >= elemental_chance) {
                // Give them elemental damage!
                elemental_type = "Fire";
                elemental_prefix = ChatColor.RED.toString(); // + Chatcolor.BOLD.toString() + "I";
               // attachPotionEffect(le, 0xCC0033);
            }
        }
        if (et == EntityType.BLAZE) {
            int do_i_elemental = 15;
            if (do_i_elemental >= elemental_chance) {
                // Give them elemental damage!
                elemental_type = "Fire";
                elemental_prefix = ChatColor.RED.toString(); // + Chatcolor.BOLD.toString() + "I";
              //  attachPotionEffect(le, 0xCC0033);
            }
        }
        if (et == EntityType.WITCH) {
            int do_i_elemental = 15;
            if (do_i_elemental >= elemental_chance) {
                // Give them elemental damage!
                elemental_type = "Poison";
                elemental_prefix = ChatColor.DARK_GREEN.toString(); // + Chatcolor.BOLD.toString() + "I";
              //  attachPotionEffect(le, 0x669900);
            }
        }
        if (et == EntityType.SILVERFISH) {
            int do_i_elemental = 15;
            if (do_i_elemental >= elemental_chance) {
                // Give them elemental damage!
                elemental_type = "Ice";
                elemental_prefix = ChatColor.BLUE.toString(); // + Chatcolor.BOLD.toString() + "I";
               // attachPotionEffect(le, 0x33FFFF);
            }
        }
        if (et == EntityType.ENDERMAN) {
            int do_i_elemental = 15;
            if (do_i_elemental >= elemental_chance) {
                // Give them elemental damage!
                elemental_type = "pure";
                elemental_prefix = ChatColor.GOLD.toString(); // + Chatcolor.BOLD.toString() + "I";
             //   attachPotionEffect(le, 0xFFFFFF);
            }
        }
        if (mob_name.equalsIgnoreCase("Acolyte")) {
            int do_i_elemental = 20;
            if (do_i_elemental >= elemental_chance) {
                // Give them elemental damage!
                elemental_type = "Fire";
                elemental_prefix = ChatColor.RED.toString(); // + Chatcolor.BOLD.toString() + "I";
            //    attachPotionEffect(le, 0xCC0033);
            }
        }
        if (mob_name.equalsIgnoreCase("Imp")) {
            int do_i_elemental = 15;
            if (do_i_elemental >= elemental_chance) {
                // Give them elemental damage!
                elemental_type = "Fire";
                elemental_prefix = ChatColor.RED.toString(); // + Chatcolor.BOLD.toString() + "I";
              //  attachPotionEffect(le, 0xCC0033);
            }
        }
        if (mob_name.equalsIgnoreCase("Daemon")) {
            int do_i_elemental = 10;
            if (do_i_elemental >= elemental_chance) {
                // Give them elemental damage!
                elemental_type = "pure";
                elemental_prefix = ChatColor.GOLD.toString(); // + Chatcolor.BOLD.toString() + "P";
              //  attachPotionEffect(le, 0xFFFFFF);
            }
        }
        if (mob_name.equalsIgnoreCase("zombie")) { // Fire
            int do_i_elemental = 10;
            if (do_i_elemental >= elemental_chance) {
                // Give them elemental damage!
                elemental_type = "Fire";
                elemental_prefix = ChatColor.RED.toString(); // + Chatcolor.BOLD.toString() + "F";
               // attachPotionEffect(le, 0xCC0033);
            }
        }
        if (mob_name.equalsIgnoreCase("skeleton")) { // PURE DMG
            int do_i_elemental = 5;
            if (do_i_elemental >= elemental_chance) {
                // Give them elemental damage!
                elemental_type = "pure";
                elemental_prefix = ChatColor.GOLD.toString(); // + Chatcolor.BOLD.toString() + "P";
                //attachPotionEffect(le, 0xFFFFFF);
            }
        }
        if (mob_name.equalsIgnoreCase("chaosskeleton")) { // PURE DMG
            int do_i_elemental = 5;
            if (do_i_elemental >= elemental_chance) {
                // Give them elemental damage!
                elemental_type = "pure";
                elemental_prefix = ChatColor.GOLD.toString(); // + Chatcolor.BOLD.toString() + "P";
               // attachPotionEffect(le, 0xFFFFFF);
            }
        }
        if (mob_name.equalsIgnoreCase("troll")) {
            int do_i_elemental = 20;
            if (do_i_elemental >= elemental_chance) {
                // Give them elemental damage!
                elemental_type = "Poison";
                elemental_prefix = ChatColor.DARK_GREEN.toString(); // + Chatcolor.BOLD.toString() + "P";
                //attachPotionEffect(le, 0x669900);
            }
        }
        if (mob_name.equalsIgnoreCase("goblin")) {
            int do_i_elemental = 20;
            if (do_i_elemental >= elemental_chance) {
                // Give them elemental damage!
                elemental_type = "Fire";
                elemental_prefix = ChatColor.RED.toString(); // + Chatcolor.BOLD.toString() + "F";
                //attachPotionEffect(le, 0xCC0033);
            }
        }
        if (mob_name.equalsIgnoreCase("naga")) {
            int do_i_elemental = 25;
            if (do_i_elemental >= elemental_chance) {
                // Give them elemental damage!
                elemental_type = "Ice";
                elemental_prefix = ChatColor.BLUE.toString(); // + Chatcolor.BOLD.toString() + "I";
                //attachPotionEffect(le, 0x33FFFF);
            }
        }
        if (mob_name.equalsIgnoreCase("tripolisoldier")) {
            int do_i_elemental = 3;
            if (do_i_elemental >= elemental_chance) {
                // Give them elemental damage!
                elemental_type = "pure";
                elemental_prefix = ChatColor.WHITE.toString(); // + Chatcolor.BOLD.toString() + "P";
               // attachPotionEffect(le, 0xFFFFFF);
            }
        }
        if (mob_name.equalsIgnoreCase("lizardman")) {
            int do_i_elemental = 10;
            if (do_i_elemental >= elemental_chance) {
                // Give them elemental damage!
                elemental_type = "Fire";
                elemental_prefix = ChatColor.RED.toString(); // + Chatcolor.BOLD.toString() + "F";
             //   attachPotionEffect(le, 0xCC0033);
            }
        }
        if (mob_name.equalsIgnoreCase("spider") || et == EntityType.CAVE_SPIDER || mob_name.equalsIgnoreCase("wolf")) {
            int do_i_elemental = 10;
            if (do_i_elemental >= elemental_chance) {
                // Give them elemental damage!
                elemental_type = "Ice";
                elemental_prefix = ChatColor.BLUE.toString(); // + Chatcolor.BOLD.toString() + "F";
             //   attachPotionEffect(le, 0x33FFFF);
            }
        }

        if (tier == 1) {
            if (mob_name.equalsIgnoreCase("goblin") || mob_name.equalsIgnoreCase("zombie") || mob_name.equalsIgnoreCase("skeleton")
                    || mob_name.equalsIgnoreCase("chaosskeleton") || mob_name.equalsIgnoreCase("daemon") || mob_name.equalsIgnoreCase("imp")) {
                // Demonic
                if (mob_name.equalsIgnoreCase("goblin") || mob_name.equalsIgnoreCase("imp")) {
                    mob_name = "Ugly " + mob_name;
                }
                if (mob_name.equalsIgnoreCase("skeleton") || mob_name.equalsIgnoreCase("chaosskeleton")) {
                    mob_name = "Broken " + mob_name;
                } else {
                    mob_name = "Rotting " + mob_name;
                }
            }

            if (mob_name.equalsIgnoreCase("witch")) {
                mob_name = "Old Hag";
            }

            if (mob_name.equalsIgnoreCase("enderman")) {
                mob_name = "Apparation";
            }

            if (mob_name.equalsIgnoreCase("magmacube")) {
                mob_name = "Magma Cube";
                mob_name = "Weak " + mob_name;
            }

            if (mob_name.equalsIgnoreCase("blaze")) {
                mob_name = "Firey Spirit";
            }

            if (et == EntityType.IRON_GOLEM) {
                // Construct
                mob_name = "Stone " + mob_name;
            }

            if (mob_name.equalsIgnoreCase("bandit") || mob_name.equalsIgnoreCase("tripolisoldier") || mob_name.equalsIgnoreCase("acolyte")) {
                // Humanoid
                int ran = new Random().nextInt(4); // 0, 1, 2
                if (ran == 0) {
                    mob_name = "Lazy " + mob_name;
                }
                if (ran == 1) {
                    mob_name = "Old " + mob_name;
                }
                if (ran == 2) {
                    mob_name = "Starving " + mob_name;
                }
                if (ran == 3) {
                    mob_name = "Clumsy " + mob_name;
                }
            }

            if (mob_name.equalsIgnoreCase("monk")) {
                mob_name = "Crimson Acolyte";
            }

            if (mob_name.equalsIgnoreCase("naga") || mob_name.equalsIgnoreCase("troll") || mob_name.equalsIgnoreCase("lizardman")) {
                // Misc.
                if (mob_name.equalsIgnoreCase("troll")) {
                    mob_name = mob_name + " Peon";
                } else {
                    mob_name = "Weak " + mob_name;
                }
            }

            if (mob_name.equalsIgnoreCase("spider") || et == EntityType.CAVE_SPIDER || mob_name.equalsIgnoreCase("wolf")
                    || mob_name.equalsIgnoreCase("silverfish")) {
                // Spiders.
                mob_name = "Harmless " + mob_name;
            }

        }

        if (tier == 2) {
            if (mob_name.equalsIgnoreCase("goblin") || mob_name.equalsIgnoreCase("zombie") || mob_name.equalsIgnoreCase("skeleton")
                    || mob_name.equalsIgnoreCase("chaosskeleton") || mob_name.equalsIgnoreCase("daemon") || mob_name.equalsIgnoreCase("imp")) {
                // Demonic
                if (mob_name.equalsIgnoreCase("goblin") || mob_name.equalsIgnoreCase("imp")) {
                    mob_name = "Angry " + mob_name;
                }
                if (mob_name.equalsIgnoreCase("skeleton") || mob_name.equalsIgnoreCase("chaosskeleton")) {
                    mob_name = "Cracking " + mob_name;
                }
                if (mob_name.equalsIgnoreCase("zombie")) {
                    mob_name = "Savaged " + mob_name;
                } else {
                    mob_name = "Wandering " + mob_name;
                }
            }

            if (mob_name.equalsIgnoreCase("witch")) {
                mob_name = "Witch";
            }

            if (mob_name.equalsIgnoreCase("enderman")) {
                mob_name = "Possessed Spirit";
            }

            if (et == EntityType.MAGMA_CUBE) {
                mob_name = "Magma Cube";
                mob_name = "Bubbling " + mob_name;
            }

            if (mob_name.equalsIgnoreCase("blaze")) {
                mob_name = "Burning Apparition";
            }

            if (et == EntityType.IRON_GOLEM) {
                // Construct
                mob_name = "Ancient Stone " + mob_name;
            }

            if (mob_name.equalsIgnoreCase("bandit") || mob_name.equalsIgnoreCase("tripolisoldier") || mob_name.equalsIgnoreCase("acolyte")) {
                // Humanoid
                int ran = new Random().nextInt(2); // 0, 1
                if (ran == 0) {
                    mob_name = "Horrible " + mob_name;
                }
                if (ran == 1) {
                    mob_name = "Vengeful " + mob_name;
                }
            }

            if (mob_name.equalsIgnoreCase("monk")) {
                mob_name = "Crimson Crusader";
            }

            if (mob_name.equalsIgnoreCase("naga") || mob_name.equalsIgnoreCase("troll") || mob_name.equalsIgnoreCase("lizardman")) {
                // Misc.
                if (mob_name.equalsIgnoreCase("troll")) {
                    if (weapon.getType().toString().toLowerCase().contains("bow")) {
                        mob_name = "Spear Thrower " + mob_name;
                    } else {
                        mob_name = mob_name + " Fighter";
                    }
                } else {
                    mob_name = "Tough " + mob_name;
                }
            }

            if (mob_name.equalsIgnoreCase("spider") || et == EntityType.CAVE_SPIDER || mob_name.equalsIgnoreCase("wolf")
                    || mob_name.equalsIgnoreCase("silverfish")) {
                // Spiders.
                mob_name = "Wild " + mob_name;
            }
        }

        if (tier == 3) {
            if (mob_name.equalsIgnoreCase("goblin") || mob_name.equalsIgnoreCase("zombie") || mob_name.equalsIgnoreCase("skeleton")
                    || mob_name.equalsIgnoreCase("chaosskeleton") || mob_name.equalsIgnoreCase("daemon") || mob_name.equalsIgnoreCase("imp")) {
                // Demonic
                if (mob_name.equalsIgnoreCase("goblin") || mob_name.equalsIgnoreCase("imp")) {
                    mob_name = "Warrior " + mob_name;
                }
                if (mob_name.equalsIgnoreCase("skeleton") || mob_name.equalsIgnoreCase("chaosskeleton")) {
                    mob_name = "Demonic " + mob_name;
                } else {
                    mob_name = "Greater " + mob_name;
                }
            }

            if (mob_name.equalsIgnoreCase("witch")) {
                mob_name = "Enchantress";
            }

            if (mob_name.equalsIgnoreCase("enderman")) {
                mob_name = "Specter";
            }

            if (et == EntityType.IRON_GOLEM) {
                // Construct
                mob_name = "Ironclad " + mob_name;
            }

            if (et == EntityType.MAGMA_CUBE) {
                mob_name = "Magma Cube";
                mob_name = "Unstable " + mob_name;
            }

            if (mob_name.equalsIgnoreCase("blaze")) {
                mob_name = "Incendiary Demon";
            }

            if (mob_name.equalsIgnoreCase("bandit") || mob_name.equalsIgnoreCase("tripolisoldier") || mob_name.equalsIgnoreCase("acolyte")) {
                // Humanoid
                int ran = new Random().nextInt(4); // 0, 1, 2, 3
                if (ran == 0) {
                    mob_name = "Cruel " + mob_name;
                }
                if (ran == 1) {
                    mob_name = mob_name + " Warrior";
                }
                if (ran == 2) {
                    mob_name = "Mountain " + mob_name;
                }
                if (ran == 3) {
                    mob_name = mob_name + " Champion";
                }
            }

            if (mob_name.equalsIgnoreCase("monk")) {
                mob_name = "Crimson Monk";
            }

            if (mob_name.equalsIgnoreCase("naga") || mob_name.equalsIgnoreCase("troll") || mob_name.equalsIgnoreCase("lizardman")) {
                // Misc.
                if (mob_name.equalsIgnoreCase("troll")) {
                    mob_name = mob_name + " Warrior";
                } else {
                    mob_name = "Giant " + mob_name;
                }
            }

            if (mob_name.equalsIgnoreCase("spider") || et == EntityType.CAVE_SPIDER || mob_name.equalsIgnoreCase("wolf")
                    || mob_name.equalsIgnoreCase("silverfish")) {
                // Spiders.
                mob_name = "Fierce " + mob_name;
            }
        }

        if (tier == 4) {
            if (mob_name.equalsIgnoreCase("goblin") || mob_name.equalsIgnoreCase("zombie") || mob_name.equalsIgnoreCase("skeleton")
                    || mob_name.equalsIgnoreCase("chaosskeleton") || mob_name.equalsIgnoreCase("daemon") || mob_name.equalsIgnoreCase("imp")) {
                // Demonic
                if (mob_name.equalsIgnoreCase("goblin") || mob_name.equalsIgnoreCase("imp")) {
                    mob_name = "Armoured " + mob_name;
                }
                if (mob_name.equalsIgnoreCase("zombie")) {
                    mob_name = "Demonic " + mob_name;
                } else {
                    mob_name = mob_name + " Guardian";
                }
            }

            if (et == EntityType.MAGMA_CUBE) {
                mob_name = "Magma Cube";
                mob_name = "Boiling " + mob_name;
            }

            if (mob_name.equalsIgnoreCase("blaze")) {
                mob_name = "Inferno Demon";
            }

            if (mob_name.equalsIgnoreCase("witch")) {
                mob_name = "Witch Doctor";
            }

            if (mob_name.equalsIgnoreCase("enderman")) {
                mob_name = "Demonic Entity";
            }

            if (et == EntityType.IRON_GOLEM) {
                // Construct
                mob_name = "Enchanted Ironclad " + mob_name;
            }

            if (mob_name.equalsIgnoreCase("bandit") || mob_name.equalsIgnoreCase("tripolisoldier") || mob_name.equalsIgnoreCase("acolyte")) {
                // Humanoid
                int ran = new Random().nextInt(3); // 0, 1, 2, 3
                if (ran == 0) {
                    mob_name = "Merciless " + mob_name;
                }
                if (ran == 1) {
                    mob_name = mob_name + " Legend";
                }
                if (ran == 2) {
                    mob_name = "Chief " + mob_name;
                }
            }

            if (mob_name.equalsIgnoreCase("monk")) {
                mob_name = "Crimson Paladin";
            }

            if (mob_name.equalsIgnoreCase("naga") || mob_name.equalsIgnoreCase("troll") || mob_name.equalsIgnoreCase("lizardman")) {
                // Misc.
                if (mob_name.equalsIgnoreCase("troll")) {
                    mob_name = mob_name + " Shaman";
                } else {
                    mob_name = "Gigantic " + mob_name;
                }
            }

            if (mob_name.equalsIgnoreCase("spider") || et == EntityType.CAVE_SPIDER || mob_name.equalsIgnoreCase("wolf")
                    || mob_name.equalsIgnoreCase("silverfish")) {
                // Spiders.
                mob_name = "Dangerous " + mob_name;
            }
        }

        if (tier == 5) {
            if (mob_name.equalsIgnoreCase("goblin") || mob_name.equalsIgnoreCase("zombie") || mob_name.equalsIgnoreCase("skeleton")
                    || mob_name.equalsIgnoreCase("chaosskeleton") || mob_name.equalsIgnoreCase("daemon") || mob_name.equalsIgnoreCase("imp")) {
                // Demonic
                mob_name = "Infernal " + mob_name;
            }

            if (et == EntityType.IRON_GOLEM) {
                // Construct
                mob_name = "Legendary Chaotic " + mob_name;
            }

            if (et == EntityType.MAGMA_CUBE) {
                mob_name = "Magma Cube";
                mob_name = "Unstoppable " + mob_name;
            }

            if (mob_name.equalsIgnoreCase("blaze")) {
                mob_name = "Source of Fire";
            }

            if (mob_name.equalsIgnoreCase("witch")) {
                mob_name = "Queen of Evil";
            }

            if (mob_name.equalsIgnoreCase("enderman")) {
                mob_name = "Phantom Revenant";
            }

            if (mob_name.equalsIgnoreCase("bandit") || mob_name.equalsIgnoreCase("tripolisoldier") || mob_name.equalsIgnoreCase("acolyte")) {
                // Humanoid
                if (mob_name.equalsIgnoreCase("bandit")) {
                    mob_name = "King " + mob_name;
                } else {
                    mob_name = "Captain " + mob_name;
                }
            }

            if (mob_name.equalsIgnoreCase("monk")) {
                mob_name = "Lord of the Crimson Order";
            }

            if (mob_name.equalsIgnoreCase("naga") || mob_name.equalsIgnoreCase("troll") || mob_name.equalsIgnoreCase("lizardman")) {
                // Misc.
                if (mob_name.equalsIgnoreCase("troll")) {
                    mob_name = mob_name + " Chieftain";
                } else {
                    mob_name = "Mythical " + mob_name;
                }
            }

            if (mob_name.equalsIgnoreCase("spider") || et == EntityType.CAVE_SPIDER || mob_name.equalsIgnoreCase("wolf")
                    || mob_name.equalsIgnoreCase("silverfish")) {
                // Spiders.
                mob_name = "Lethal " + mob_name;
            }
        }

        if (elemental_type.length() > 0) {
            // elemental_prefix = color
        	ItemStack wep = weapon;
            wep.addUnsafeEnchantment(Enchantment.LOOT_BONUS_MOBS, 1);
            le.getEquipment().setItemInHand(wep);

            mob_name = elemental_prefix + mob_name;
            String formal_elemental_type = elemental_type;
            if (formal_elemental_type.equalsIgnoreCase("pure")) {
                formal_elemental_type = "Holy";
                mob_name = elemental_prefix + formal_elemental_type + " " + mob_name;
            } else {
                try {
                    mob_name = mob_name.substring(0, mob_name.lastIndexOf(" ")) + " " + formal_elemental_type
                            + mob_name.substring(mob_name.lastIndexOf(" "), mob_name.length());
                } catch (StringIndexOutOfBoundsException err) {
                    //log.info("[MonsterMechanics] Error parsing mob name: " + mob_name);
                    mob_name = elemental_prefix + mob_name;
                    if (formal_elemental_type.equalsIgnoreCase("pure")) {
                        formal_elemental_type = "Holy";
                        mob_name = elemental_prefix + formal_elemental_type + " " + mob_name;
                    }
                }
            }
            le.setMetadata("etype", new FixedMetadataValue(Main.plugin, elemental_type));
        }
		return mob_name;

	}


	
	public EntityType getEntityType() {
		if (type.equalsIgnoreCase("naga")) return EntityType.ZOMBIE;
		if (type.equalsIgnoreCase("zombie")) return EntityType.ZOMBIE;
		if (type.equalsIgnoreCase("imp")) return EntityType.PIG_ZOMBIE;
		if (type.equalsIgnoreCase("skeleton")) return EntityType.SKELETON;
		if (type.equalsIgnoreCase("enderman")) return EntityType.ENDERMAN;
		if (type.equalsIgnoreCase("magmacube")) return EntityType.MAGMA_CUBE;
		if (type.equalsIgnoreCase("blaze")) return EntityType.BLAZE;
		if (type.equalsIgnoreCase("golem")) return EntityType.IRON_GOLEM;
		if (type.equalsIgnoreCase("goblin")) return EntityType.ZOMBIE;
		if (type.equalsIgnoreCase("troll")) return EntityType.ZOMBIE;
		if (type.equalsIgnoreCase("chaosskeleton")) return EntityType.SKELETON;
		if (type.equalsIgnoreCase("lizardman")) return EntityType.ZOMBIE;
		if (type.equalsIgnoreCase("bandit")) return EntityType.ZOMBIE;
		if (type.equalsIgnoreCase("blaze")) return EntityType.BLAZE;
		if (type.equalsIgnoreCase("monk")) return EntityType.ZOMBIE;
		if (type.equalsIgnoreCase("tripolisoldier")) return EntityType.SKELETON;
		
		return null;
	}//blaze
	//monk
	//tripolisoldier
	//wolf
	//spider
	//silverfish
	//daemon
	
	public int getTier() {
		return tier;
	}
	
	public boolean isElite() {
		return elite;
	}
	
	public boolean getNamedElite() {
		return namedElite;
	}
	
	public String getMobName() {
		return mobName;
	}
	
	public String getRawHP() {
		return rawhp;
	}

	public double getMaxHP() {
		return maxhp;
	}

	public void setMaxHP(double maxhp) {
		this.maxhp = maxhp;
	}

	public double getMinHP() {
		return minhp;
	}

	public void setMinHP(double minhp) {
		this.minhp = minhp;
	}

	 public static int getMobTier(Entity ent) {
	        if (ent instanceof Ghast) {
	            return 4;
	        }
	        if (ent.getPassenger() != null) {
	            Entity r_ent = ent.getPassenger();
	            ent = r_ent;
	        }
	        if (mob_tier.containsKey(ent)) {
	            return mob_tier.get(ent);
	        }
	        LivingEntity le = (LivingEntity) ent;
	        ItemStack i = le.getEquipment().getItemInHand();
	        if (i == null) {
	            return -1; // No tier.
	        }
	        int wep_tier = getItemTier(i);
	        // log.info("[MonsterMechanics] No mob tier stored for entity " + ent.toString() + ", saving new value to memory. (" + wep_tier + ")");
	        mob_tier.put(ent, wep_tier);
	        return wep_tier;
	    }
	 
	 public static int getItemTier(ItemStack i) {
	        try {
	            String name = CraftItemStack.asNMSCopy(i).getTag().getCompound("display").getString("Name");
	            if (name.contains(ChatColor.WHITE.toString())) {
	                return 1;
	            }
	            if (name.contains(ChatColor.GREEN.toString())) {
	                return 2;
	            }
	            if (name.contains(ChatColor.AQUA.toString())) {
	                return 3;
	            }
	            if (name.contains(ChatColor.LIGHT_PURPLE.toString())) {
	                return 4;
	            }
	            if (name.contains(ChatColor.YELLOW.toString())) {
	                return 5;
	            }
	        } catch (NullPointerException npe) {
	            return 1;
	        }
	        return 1;
	 }

	   public static String getMobType(Entity e, boolean system_name) {
	        if (!system_name) {
	            if (e.hasMetadata("mobname")) {
	                return e.getMetadata("mobname").get(0).asString();
	            }
	        }

	        String mob_type = e.getType().name().substring(0, 1).toUpperCase() + e.getType().name().substring(1, e.getType().name().length()).toLowerCase();

	        if (e instanceof LivingEntity) {
	            try {
	                LivingEntity le = (LivingEntity) e;
	                if (le.getEquipment().getHelmet() != null && le.getEquipment().getHelmet().getType() == Material.SKULL_ITEM) {
	                    ItemStack h = le.getEquipment().getHelmet();
	                    net.minecraft.server.v1_10_R1.ItemStack mItem = CraftItemStack.asNMSCopy(h);
	                    NBTTagCompound tag = mItem.getTag();
	                    String skin_name = tag.getString("SkullOwner");
	                    if (skin_name.equalsIgnoreCase("dEr_t0d") || skin_name.equalsIgnoreCase("niv330")) {
	                        mob_type = "Goblin";
	                        return mob_type;
	                    }
	                    if (/* skin_name.equalsIgnoreCase("hway234") || */skin_name.equalsIgnoreCase("Xmattpt") || skin_name.equalsIgnoreCase("TheNextPaladin")) {
	                        mob_type = "Bandit";
	                        return mob_type;
	                    }
	                    if (skin_name.equalsIgnoreCase("Yhmen")) {
	                        mob_type = "Monk";
	                        return mob_type;
	                    }
	                    // MagmaCube | Daemon | Imp | Acolyte
	                    if (skin_name.equalsIgnoreCase("InfinityWarrior_")) {
	                        mob_type = "Acolyte";
	                        return mob_type;
	                    }
	                    if (skin_name.equalsIgnoreCase("ArcadiaMovies") || skin_name.equalsIgnoreCase("Malware")) {
	                        mob_type = "Forest Troll";
	                        return mob_type;
	                    }
	                    if (skin_name.equalsIgnoreCase("Das_Doktor")) {
	                        mob_type = "Naga";
	                        return mob_type;
	                    }
	                    if (skin_name.equalsIgnoreCase("Xmattpt")) {
	                        mob_type = "Tripoli Soldier";
	                        return mob_type;
	                    }
	                    if (skin_name.equalsIgnoreCase("_Kashi_")) {
	                        mob_type = "Lizardman";
	                        return mob_type;
	                    }
	                } else if (le.getType() == EntityType.SKELETON) {
	                	Skeleton sk = (Skeleton) le;
	                    int skelly_type = sk.getType().getTypeId();
	                    if (skelly_type == 0) {
	                        mob_type = "Skeleton";
	                        return mob_type;
	                    }
	                    if (skelly_type == 1) {
	                        mob_type = "Wither Skeleton";
	                        return mob_type;
	                    }
	                } else if (le.getType() == EntityType.PIG_ZOMBIE) {
	                    PigZombie pz = (PigZombie) e;
	                    if (pz.isBaby()) {
	                        return "Imp";
	                    } else {
	                        return "Daemon";
	                    }
	                }
	            } catch (ClassCastException cce) {
	                cce.printStackTrace();
	            }
	        }

	        return mob_type;
	    }

	    public static String getMobType(Entity e) {
	        if (e.hasMetadata("mobname")) {
	            return ChatColor.stripColor(e.getMetadata("mobname").get(0).asString());
	        }

	        String mob_type = e.getType().name().substring(0, 1).toUpperCase() + e.getType().name().substring(1, e.getType().name().length()).toLowerCase();
	        try {
	            if (e instanceof LivingEntity) {
	                LivingEntity le = (LivingEntity) e;
	                if (le.getEquipment().getHelmet() != null && le.getEquipment().getHelmet().getType() == Material.SKULL_ITEM) {
	                    ItemStack h = le.getEquipment().getHelmet();
	                    net.minecraft.server.v1_10_R1.ItemStack mItem = CraftItemStack.asNMSCopy(h);
	                    NBTTagCompound tag = mItem.getTag();
	                    String skin_name = tag.getString("SkullOwner");
	                    if (skin_name.equalsIgnoreCase("dEr_t0d") || skin_name.equalsIgnoreCase("niv330")) {
	                        mob_type = "Goblin";
	                        return mob_type;
	                    }
	                    if (/* skin_name.equalsIgnoreCase("hway234") || */skin_name.equalsIgnoreCase("Xmattpt") || skin_name.equalsIgnoreCase("TheNextPaladin")) {
	                        mob_type = "Bandit";
	                        return mob_type;
	                    }
	                    if (skin_name.equalsIgnoreCase("Yhmen")) {
	                        mob_type = "Monk";
	                        return mob_type;
	                    }
	                    if (skin_name.equalsIgnoreCase("ArcadiaMovies") || skin_name.equalsIgnoreCase("Malware")) {
	                        mob_type = "Forest Troll";
	                        return mob_type;
	                    }
	                    if (skin_name.equalsIgnoreCase("Das_Doktor")) {
	                        mob_type = "Naga";
	                        return mob_type;
	                    }
	                    if (skin_name.equalsIgnoreCase("Xmattpt")) {
	                        mob_type = "Tripoli Soldier";
	                        return mob_type;
	                    }
	                    if (skin_name.equalsIgnoreCase("_Kashi_")) {
	                        mob_type = "Lizardman";
	                        return mob_type;
	                    }
	                } else if (le.getType() == EntityType.SKELETON) {
	                	Skeleton sk = (Skeleton) le;
	                    int skelly_type = sk.getType().getTypeId();
	                    if (skelly_type == 0) {
	                        mob_type = "Skeleton";
	                        return mob_type;
	                    }
	                    if (skelly_type == 1) {
	                        mob_type = "Wither Skeleton";
	                        return mob_type;
	                    }
	                } else if (le.getType() == EntityType.PIG_ZOMBIE) {
	                    PigZombie pz = (PigZombie) e;
	                    if (pz.isBaby()) {
	                        return "Imp";
	                    } else {
	                        return "Daemon";
	                    }
	                }
	            }
	        } catch (ClassCastException cce) {
	            cce.printStackTrace();
	        }

	        return mob_type;
	    }


}

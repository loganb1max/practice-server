package com.practiceserver.mobs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_10_R1.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.practiceserver.itemgenerator.Attributes;
import com.practiceserver.itemgenerator.enums.ItemTier;
import com.practiceserver.itemgenerator.enums.ItemType;
import com.practiceserver.mob.FileManager;
import com.practiceserver.utils.Utils;

import net.minecraft.server.v1_10_R1.NBTTagCompound;

public class ItemMechanics {
	
	//TODO RECODE 
	
	static FileManager c = FileManager.getInstance();
	
    public static ItemStack t1_arrow = ItemMechanics.signNewCustomItem(Material.ARROW, (short) 1, ChatColor.WHITE.toString() + "" + "Bent Arrow",
            ChatColor.GRAY.toString() + "Increase arrow damage by 5%");
    public static ItemStack t2_arrow = ItemMechanics.signNewCustomItem(Material.ARROW, (short) 1, ChatColor.GREEN.toString() + "" + "Precise Arrow",
            ChatColor.GRAY.toString() + "Increase arrow damage by 10%");
    public static ItemStack t3_arrow = ItemMechanics.signNewCustomItem(Material.ARROW, (short) 1, ChatColor.AQUA.toString() + "" + "Reinforced Arrow",
            ChatColor.GRAY.toString() + "Increase arrow damage by 20%");
    public static ItemStack t4_arrow = ItemMechanics.signNewCustomItem(Material.ARROW, (short) 1, ChatColor.LIGHT_PURPLE.toString() + "" + "Pointed Arrow",
            ChatColor.GRAY.toString() + "Increase arrow damage by 40%");
    public static ItemStack t5_arrow = ItemMechanics.signNewCustomItem(Material.ARROW, (short) 1, ChatColor.YELLOW.toString() + "" + "Piercing Arrow",
            ChatColor.GRAY.toString() + "Increase arrow damage by 80%");
    
    public static ItemStack glass = ItemMechanics.signNewCustomItem(Material.THIN_GLASS, (short) 0, " ", "");
    
    public static ItemStack glass1 = ItemMechanics.signNewCustomItem(Material.THIN_GLASS, (short) 0, "Nothing", "");
    
	public static ItemStack getItemFromFile(String file) {
		FileConfiguration f = c.loadFile(file + ".yml", "plugins/PracticeServer/Items/");
		ItemStack item = new ItemStack(getMaterialFromFile(file));
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(Utils.colorCodes(getNameFromFile(file)));
		meta.setLore(getLoreFromFile(file));
		item.setItemMeta(meta);
		return item;
		
	}
	
	public static Material getMaterialFromFile(String file) {
		return Material.getMaterial(c.loadFile(file + ".yml", "plugins/PracticeServer/Items/").getString("Item"));
		//
	}
	
	public static String getNameFromFile(String file) {
		return c.loadFile(file + ".yml", "plugins/PracticeServer/Items/").getString("Name").replaceAll("_", " ");
	}
	
	public static List<String> getLoreFromFile(String file) {
		String line = c.loadFile(file + ".yml", "plugins/PracticeServer/Items/").getString("Lore");
		line = ChatColor.translateAlternateColorCodes('&', line);
		line = line.replaceAll("_", " ");
		List<String> new_lore = new ArrayList();
		if (line.contains("(")) {
			String line_copy = line;
			for (String s : line_copy.split("\\(")) {
				if (s.contains("~")) {
					int lower = Integer.parseInt(s.substring(0, s.indexOf("~")));
		            int upper = Integer.parseInt(s.substring(s.indexOf("~") + 1, s.indexOf(")")));
		            int val = new Random().nextInt(upper - lower) + lower;
		            line = line.replace("(" + lower + "~" + upper + ")", String.valueOf(val));
				}
			}
		}
	
		if(line.contains(",")) {
			for (String s : line.split(",")) {
				new_lore.add(s);
			}
		} else {
			new_lore.add(line);
		}
		return new_lore;
	}
    
    public static ItemStack setToHealingPotion(net.minecraft.server.v1_10_R1.ItemStack i) {
        if (i == null) {
            //log.info("[ItemMechanics] NULL itemStack on setToHealingPotion()");
            return CraftItemStack.asBukkitCopy(i);
        }
        try {
            net.minecraft.server.v1_10_R1.NBTTagList cpe = new net.minecraft.server.v1_10_R1.NBTTagList();
            NBTTagCompound tag = new NBTTagCompound();
            tag.setByte("Id", (byte) 6);
            // ((NBTTagList)i.tag.getList("CustomPotionEffects", 0)).add(tag);
            cpe.add(tag);
            i.getTag().set("CustomPotionEffects", cpe);
        } catch (NullPointerException npe) {
            return CraftItemStack.asBukkitCopy(i);
        }
        return CraftItemStack.asBukkitCopy(i);
    }

    public static ItemStack signNewCustomItem(Material m, short meta_data, String name, String desc) {
        ItemStack is = new ItemStack(m, 1, meta_data);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(name);

        List<String> new_lore = new ArrayList<String>();
        if (desc.contains(",")) {
            for (String s : desc.split(",")) {
                new_lore.add(s);
            }
        } else {
            new_lore.add(desc);
        }

        im.setLore(new_lore);
        is.setItemMeta(im);

        /*
         * net.minecraft.server.v1_7_R2.ItemStack nms = CraftItemStack.asNMSCopy(iss);
         *
         * NBTTagCompound tag = nms.tag; tag = new NBTTagCompound(); tag.setCompound("display", new NBTTagCompound()); nms.tag = tag; tag =
         * nms.tag.getCompound("display");
         *
         * for(String s : desc.split(",")){ if(s.length() <= 1){continue;} NBTTagList lore = tag.getList("Lore"); if(lore == null){lore = new
         * NBTTagList("Lore");}
         *
         * lore.add(new NBTTagString("", s)); tag.set("Lore", lore); }
         *
         * tag.setString("Name", name); nms.tag.setCompound("display", tag); nms.tag.set("CustomPotionEffects", new NBTTagList());
         *
         * nms.setTag(nms.tag);
         */

        return setToHealingPotion(CraftItemStack.asNMSCopy(is));
    }
	
	public static int getHealthVal1(ItemStack armor) {
		String armor_data = ItemMechanics.getArmorData(armor);
		if(armor_data.equalsIgnoreCase("no")) { return 0; }

		int health_val = Integer.parseInt(armor_data.substring(armor_data.indexOf(":") + 1, armor_data.indexOf("@")));

		return health_val;
	}
	
	public static void addGlow(ItemStack is) {
		is.addUnsafeEnchantment(Enchantment.DURABILITY, 1);
		
	}
	
	 public static boolean hasProtection(ItemStack is) {
		    if (!is.hasItemMeta()) return false;
		    if (!is.getItemMeta().hasLore()) { return false;
		    }
		    for (String s : is.getItemMeta().getLore()) {
		      if (s.equalsIgnoreCase(ChatColor.GREEN.toString() + ChatColor.BOLD.toString() + "PROTECTED")) { return true;
		      }
		    }
		    return false;
		  }
	
	public static int getHealthVal(ItemStack armor) {
		String armor_data = ItemMechanics.getArmorData(armor);
		if(armor_data.equalsIgnoreCase("no")) { return 0; }

		int health_val = Integer.parseInt(armor_data.substring(armor_data.indexOf(":") + 1, armor_data.indexOf("@")));

		return health_val;
	}
	
	public static org.bukkit.inventory.ItemStack signCustomItem(Material m, short meta_data, String name, String desc) {
	    org.bukkit.inventory.ItemStack iss = new org.bukkit.inventory.ItemStack(m, 1, meta_data);
	    ItemMeta im = iss.getItemMeta();
	    List<String> new_lore = new ArrayList();
	    for (String s : desc.split(",")) {
	      if (s.length() > 1) {
	        if (s.contains("#")) {
	          s = s.substring(s.lastIndexOf("#") + 1, s.length());
	        }
	        new_lore.add(s);
	      }
	    }
	    im.setLore(new_lore);
	    im.setDisplayName(name);
	    
	    iss.setItemMeta(im);
	    removeAttributes(iss);
	    return iss;
	  }
	
	 public static org.bukkit.inventory.ItemStack removeAttributes(org.bukkit.inventory.ItemStack is) {
	    if (is == null) {
	      return null;
	    }
	    net.minecraft.server.v1_10_R1.ItemStack nms = CraftItemStack.asNMSCopy(is);
	    if ((nms != null) && (nms.hasTag()) && (is.getMaxStackSize() == 1)) {
	      Attributes attributes = new Attributes(is);
	      attributes.clear();
	      return attributes.getStack();
	    }
	    return is;
	  }
	
	 public static String getItemRarity(ItemStack is) {
	        if (is == null || !is.hasItemMeta() || !is.getItemMeta().hasLore()) {
	            return null;
	        }

	        List<String> lore = is.getItemMeta().getLore();
	        for (String s : lore) {
	            if (s.contains(ChatColor.ITALIC.toString())) {
	                if (s.equalsIgnoreCase(ChatColor.YELLOW.toString() + ChatColor.ITALIC.toString() + "Unique")) {
	                    return s;
	                }
	                if (s.equalsIgnoreCase(ChatColor.AQUA.toString() + ChatColor.ITALIC.toString() + "Rare")) {
	                    return s;
	                }
	                if (s.equalsIgnoreCase(ChatColor.GREEN.toString() + ChatColor.ITALIC.toString() + "Uncommon")) {
	                    return s;
	                }
	                if (s.equalsIgnoreCase(ChatColor.GRAY.toString() + ChatColor.ITALIC.toString() + "Common")) {
	                    return s;
	                }
	            }
	        }

	      
	        return ""; // 
	  }
	
	  public static int getEnchantCount(ItemStack is) {
			try {
				if(!(is.hasItemMeta())) { return 0; }
				if(!(is.getItemMeta().hasDisplayName())) { return 0; }
				String name = ChatColor.stripColor(is.getItemMeta().getDisplayName());
		          if(name.startsWith("[")) {
		                int enchant_count = Integer.parseInt(name.substring(name.indexOf("+") + 1, name.lastIndexOf("]")));
		                return enchant_count;
		          }
			} catch(Exception e) {
				return 0;
			}
			return 0; // No (+#), must be unenchanted.
		}
	
	 public static List<Integer> getDmgRangeOfWeapon(ItemStack is) {
	        List<Integer> dmg_range = new ArrayList<Integer>();
	        if (is != null && is.hasItemMeta() && is.getItemMeta().hasLore()) {
	            List<String> lore = is.getItemMeta().getLore();
	            for (String s : lore) {
	                if (s.startsWith(ChatColor.RED.toString() + "DMG:")) {
	                    s = s.replaceAll(ChatColor.RED.toString() + "DMG: ", "");
	                    s = s.replaceAll(" ", "");

	                    int min_dmg = Integer.parseInt(s.split("-")[0]);
	                    int max_dmg = Integer.parseInt(s.split("-")[1]);

	                    dmg_range.add(min_dmg);
	                    dmg_range.add(max_dmg);
	                }
	            }
	        }

	        return dmg_range;
	 }
	 
	 public static List<Integer> getDmgVal(ItemStack i, boolean with_dex) {
	        try {
	            if (getArmorData(i) == "no") {
	                return null;
	            }
	            String armor_data = getArmorData(i);
	            // log.info(armor_data);
	            if (!armor_data.contains("!")) {
	                return null; // Armor, not DMG %.
	            }
	            String armor_range = armor_data.substring(0, armor_data.indexOf(":"));
	            List<Integer> net_armor_vals = new ArrayList<Integer>();

	            armor_range = armor_range.replaceAll("!", "");

	            int min_armor = Integer.parseInt(armor_range.split("-")[0].replaceAll(" ", ""));
	            int max_armor = Integer.parseInt(armor_range.split("-")[1].replaceAll(" ", ""));
	            /*
	             * int armor_val = new Random().nextInt(max_armor); if(armor_val < min_armor){ armor_val = min_armor; } if(armor_val < 1){armor_val = 1;}
	             */

	            if (with_dex && armor_data.contains("dex")) {
	                String str_string = armor_data.split("dex=")[1];
	                int dex_atr = Integer.parseInt(str_string.substring(0, str_string.indexOf(":")));
	                double dex_armor_mod = 0.03D * dex_atr;

	                min_armor += dex_armor_mod;
	                max_armor += dex_armor_mod;
	            }

	            net_armor_vals.add(0, min_armor);
	            net_armor_vals.add(1, max_armor);
	            return net_armor_vals;
	        } catch (NullPointerException e) {
	            return null;
	        }
	    }
	 
	 
	   public static List<Integer> getArmorVal(ItemStack i, boolean with_str) {
	        try {
	            if (getArmorData(i) == "no") {
	                return null;
	            }
	            String armor_data = getArmorData(i);
	            if (armor_data.contains("!")) {
	                return null; // DMG %, not armor
	            }
	            String armor_range = armor_data.substring(0, armor_data.indexOf(":"));
	            List<Integer> net_armor_vals = new ArrayList<Integer>();

	            int min_armor = Integer.parseInt(armor_range.split("-")[0].replaceAll(" ", ""));
	            int max_armor = Integer.parseInt(armor_range.split("-")[1].replaceAll(" ", ""));

	            if (with_str && armor_data.contains("str")) {
	                String str_string = armor_data.split("str=")[1];
	                int str_atr = Integer.parseInt(str_string.substring(0, str_string.indexOf(":")));
	                double str_armor_mod = 0.03D * str_atr;

	                min_armor += str_armor_mod;
	                max_armor += str_armor_mod;
	            }
	            /*
	             * int armor_val = new Random().nextInt(max_armor); if(armor_val < min_armor){ armor_val = min_armor; } if(armor_val < 1){armor_val = 1;}
	             */
	            net_armor_vals.add(0, min_armor);
	            net_armor_vals.add(1, max_armor);
	            return net_armor_vals;
	        } catch (NullPointerException e) {
	            return null;
	        }
	    }
	   
	   public static String getArmorData(ItemStack i) {

	        try {
	            try {
	                try {
	                    List<String> lore = i.getItemMeta().getLore();
	                    // NBTTagList description = CraftItemStack.asNMSCopy(i).getTag().getCompound("display").getList("Lore", 0);

	                    // int x = 0;

	                    boolean hp_regen = false, /* hp_increase = false, */energy_regen = false, str = false, dex = false, vit = false, intel = false, block = false, dodge = false, thorns = false, reflection = false, gold_find = false, item_find = false, speed_boost = false;
	                    boolean fire_res = false, ice_res = false, poison_res = false;

	                    int hp_regen_amount = 0, energy_regen_amount = 0, val_fire_res = 0, val_ice_res = 0, val_poison_res = 0, dodge_chance = 0, str_atr = 0, dex_atr = 0, vit_atr = 0, int_atr = 0, block_chance = 0, thorns_amount = 0, reflection_chance = 0, item_find_amount = 0, gold_find_amount = 0;
	                    float speed_mod = 0.0F;

	                    List<String> all_attributes = new ArrayList<String>();

	                    for (String s : lore) {
	                        if (!(s).startsWith(ChatColor.RED.toString())) {
	                            // x++;
	                            continue;
	                        }

	                        all_attributes.add(s);
	                        if (s.contains("HP REGEN")) {
	                            hp_regen = true;
	                            String hp_regen_data = s;
	                            hp_regen_amount = Integer.parseInt(hp_regen_data.substring(hp_regen_data.indexOf(":") + 3, hp_regen_data.lastIndexOf(" "))
	                                    .replaceAll(" ", ""));
	                        }

	                        if (s.contains("ENERGY REGEN")) {
	                            energy_regen = true;
	                            String energy_regen_data = s;
	                            energy_regen_amount = Integer.parseInt(energy_regen_data.substring(energy_regen_data.indexOf(":") + 3,
	                                    energy_regen_data.indexOf("%")).replaceAll(" ", ""));
	                        }

	                        if (s.contains("FIRE RESISTANCE")) {
	                            fire_res = true;
	                            String fire_res_data = s;
	                            val_fire_res = Integer.parseInt(fire_res_data.substring(fire_res_data.indexOf(":") + 2, fire_res_data.indexOf("%")).replaceAll(" ",
	                                    ""));
	                        }

	                        if (s.contains("ICE RESISTANCE")) {
	                            ice_res = true;
	                            String ice_res_data = s;
	                            val_ice_res = Integer
	                                    .parseInt(ice_res_data.substring(ice_res_data.indexOf(":") + 2, ice_res_data.indexOf("%")).replaceAll(" ", ""));
	                        }

	                        if (s.contains("POISON RESISTANCE")) {
	                            poison_res = true;
	                            String poison_res_data = s;
	                            val_poison_res = Integer.parseInt(poison_res_data.substring(poison_res_data.indexOf(":") + 2, poison_res_data.indexOf("%"))
	                                    .replaceAll(" ", ""));
	                        }

	                        if (s.contains("STR")) {
	                            str = true;
	                            String stat_data = s;
	                            str_atr = Integer.parseInt(stat_data.substring(stat_data.indexOf(":") + 3, stat_data.length()).replaceAll(" ", ""));
	                        }

	                        if (s.contains("DEX")) {
	                            dex = true;
	                            String stat_data = s;
	                            dex_atr = Integer.parseInt(stat_data.substring(stat_data.indexOf(":") + 3, stat_data.length()).replaceAll(" ", ""));
	                        }

	                        if (s.contains("VIT")) {
	                            vit = true;
	                            String stat_data = s;
	                            vit_atr = Integer.parseInt(stat_data.substring(stat_data.indexOf(":") + 3, stat_data.length()).replaceAll(" ", ""));
	                        }

	                        if (s.contains("INT")) {
	                            intel = true;
	                            String int_string = s;
	                            int_atr = Integer.parseInt(int_string.substring(int_string.indexOf(":") + 3, int_string.length()));
	                        }

	                        if (s.contains("SPEED BOOST")) {
	                            speed_boost = true;
	                            String speed_boost_data = s;
	                            speed_mod = Float.parseFloat(speed_boost_data.substring(speed_boost_data.indexOf(":") + 3, speed_boost_data.indexOf("X"))
	                                    .replaceAll(" ", ""));
	                        }

	                        if (s.contains("DODGE")) {
	                            String dodge_data = s;

	                            dodge_chance = Integer.parseInt(dodge_data.substring(dodge_data.indexOf(":") + 1, dodge_data.indexOf("%")).replaceAll(" ", ""));
	                            dodge = true;
	                        }

	                        if (s.contains("BLOCK")) {
	                            String block_data = s;

	                            block_chance = Integer.parseInt(block_data.substring(block_data.indexOf(":") + 1, block_data.indexOf("%")).replaceAll(" ", ""));
	                            block = true;
	                        }

	                        if (s.contains("THORNS")) {
	                            thorns = true;
	                            String thorns_data = s;
	                            thorns_amount = Integer.parseInt(thorns_data.substring(thorns_data.indexOf(":") + 1, thorns_data.indexOf("%")).replaceAll(" ", ""));
	                        }

	                        if (s.contains("REFLECTION")) {
	                            String reflection_data = s;

	                            reflection_chance = Integer.parseInt(reflection_data.substring(reflection_data.indexOf(":") + 1, reflection_data.indexOf("%"))
	                                    .replaceAll(" ", ""));
	                            reflection = true;
	                        }

	                        if (s.contains("ITEM FIND")) {
	                            item_find = true;
	                            String item_find_data = s;
	                            item_find_amount = Integer.parseInt(item_find_data.substring(item_find_data.indexOf(":") + 1, item_find_data.indexOf("%"))
	                                    .replaceAll(" ", ""));
	                        }

	                        if (s.contains("GEM FIND") || s.contains("GOLD FIND")) {
	                            gold_find = true;
	                            String gold_find_data = s;
	                            gold_find_amount = Integer.parseInt(gold_find_data.substring(gold_find_data.indexOf(":") + 1, gold_find_data.indexOf("%"))
	                                    .replaceAll(" ", ""));
	                        }

	                        // x++;
	                    }

	                    String return_string = "";

	                    String larmor_data = all_attributes.get(0);
	                    if (larmor_data.contains("ARMOR")) {
	                        larmor_data = larmor_data.replaceAll(ChatColor.RED.toString() + "ARMOR: ", "");
	                        larmor_data = larmor_data.replaceAll(" ", "");

	                        int min_armor = Integer.parseInt(larmor_data.split("-")[0]);
	                        int max_armor = Integer.parseInt(larmor_data.split("-")[1].replaceAll("%", ""));

	                        return_string = min_armor + " - " + max_armor + ":";
	                        // int armor = new Random().nextInt(max_armor);
	                        // if(armor < min_armor){armor = min_armor;}

	                    } else if (larmor_data.contains("DPS")) {
	                        larmor_data = larmor_data.replaceAll(ChatColor.RED.toString() + "DPS: ", "");
	                        larmor_data = larmor_data.replaceAll(" ", "");

	                        int min_armor = Integer.parseInt(larmor_data.split("-")[0]);
	                        int max_armor = Integer.parseInt(larmor_data.split("-")[1].replaceAll("%", ""));

	                        return_string = "!" + min_armor + " - " + max_armor + ":";
	                    }

	                    if (return_string.length() <= 0) {
	                        return "no";
	                    }

	                    String health_data = all_attributes.get(1);
	                    health_data = health_data.replaceAll(ChatColor.RED.toString() + "HP: \\+", "");
	                    return_string += health_data + "@:";

	                    if (hp_regen == true) {
	                        return_string = return_string + "hp_regen=" + hp_regen_amount + "@hp_regen_split@:";
	                        // log.info("hp_regen = " + hp_regen_amount);
	                    }

	                    if (energy_regen == true) {
	                        return_string = return_string + "energy_regen=" + energy_regen_amount + "@energy_regen_split@:";
	                    }

	                    if (fire_res == true) {
	                        return_string = return_string + "fire_resistance=" + val_fire_res + ":";
	                    }

	                    if (ice_res == true) {
	                        return_string = return_string + "ice_resistance=" + val_ice_res + ":";
	                    }

	                    if (poison_res == true) {
	                        return_string = return_string + "poison_resistance=" + val_poison_res + ":";
	                    }

	                    if (str == true) {
	                        return_string = return_string + "str=" + str_atr + ":";
	                    }

	                    if (dex == true) {
	                        return_string = return_string + "dex=" + dex_atr + ":";
	                    }

	                    if (vit == true) {
	                        return_string = return_string + "vit=" + vit_atr + ":";
	                    }

	                    if (intel == true) {
	                        return_string = return_string + "int=" + int_atr + ":";
	                    }

	                    if (speed_boost == true) {
	                        return_string = return_string + "speed=" + speed_mod + ":";
	                    }

	                    if (block == true) {
	                        return_string = return_string + "block=" + block_chance + ":";
	                    }

	                    if (dodge == true) {
	                        return_string = return_string + "dodge=" + dodge_chance + ":";
	                    }

	                    if (thorns == true) {
	                        return_string = return_string + "thorns=" + thorns_amount + ":";
	                    }

	                    if (reflection == true) {
	                        return_string = return_string + "reflection=" + reflection_chance + ":";
	                    }

	                    if (gold_find == true) {
	                        return_string = return_string + "gold_find=" + gold_find_amount + ":";
	                    }

	                    if (item_find == true) {
	                        return_string = return_string + "item_find=" + item_find_amount + ":";
	                    }

	                    return return_string;
	                } catch (NumberFormatException e) {
	                    // e.printStackTrace();
	                    return "no";
	                }
	            } catch (IndexOutOfBoundsException e) {
	                // e.printStackTrace();
	                return "no";
	            }
	        } catch (NullPointerException e) {
	            // e.printStackTrace();
	            return "no";
	        }
	    }
	   
	   public static boolean isArmor(ItemStack i) {
	        if (getArmorData(i).equalsIgnoreCase("no")) {
	            return false;
	        } else if (getArmorData(i).contains("@:")) {
	            return true;
	        }
	        return false;
	    }

	    public static boolean isHelmet(ItemStack is) {
	        Material m = is.getType();
	        if (m == Material.LEATHER_HELMET || m == Material.GOLD_HELMET || m == Material.DIAMOND_HELMET || m == Material.CHAINMAIL_HELMET
	                || m == Material.IRON_HELMET) {
	            return true;
	        }
	        return false;
	    }

	    public static boolean isChestplate(ItemStack is) {
	        Material m = is.getType();
	        if (m == Material.LEATHER_CHESTPLATE || m == Material.GOLD_CHESTPLATE || m == Material.DIAMOND_CHESTPLATE || m == Material.CHAINMAIL_CHESTPLATE
	                || m == Material.IRON_CHESTPLATE) {
	            return true;
	        }
	        return false;
	    }

	    public static boolean isLeggings(ItemStack is) {
	        Material m = is.getType();
	        if (m == Material.LEATHER_LEGGINGS || m == Material.GOLD_LEGGINGS || m == Material.DIAMOND_LEGGINGS || m == Material.CHAINMAIL_LEGGINGS
	                || m == Material.IRON_LEGGINGS) {
	            return true;
	        }
	        return false;
	    }

	    public static boolean isBoots(ItemStack is) {
	        Material m = is.getType();
	        if (m == Material.LEATHER_BOOTS || m == Material.GOLD_BOOTS || m == Material.DIAMOND_BOOTS || m == Material.CHAINMAIL_BOOTS || m == Material.IRON_BOOTS) {
	            return true;
	        }
	        return false;
	    }

	    public static boolean isAxe(ItemStack is) {
	        Material m = is.getType();
	        if (m == Material.WOOD_AXE || m == Material.GOLD_AXE || m == Material.STONE_AXE || m == Material.IRON_AXE || m == Material.DIAMOND_AXE) {
	            return true;
	        }
	        return false;
	    }

	    public static boolean isSword(ItemStack is) {
	        Material m = is.getType();
	        if (m == Material.WOOD_SWORD || m == Material.GOLD_SWORD || m == Material.STONE_SWORD || m == Material.IRON_SWORD || m == Material.DIAMOND_SWORD) {
	            return true;
	        }
	        return false;
	    }

	    public static boolean isPolearm(ItemStack is) {
	        Material m = is.getType();
	        if (m == Material.WOOD_SPADE || m == Material.GOLD_SPADE || m == Material.STONE_SPADE || m == Material.IRON_SPADE || m == Material.DIAMOND_SPADE) {
	            return true;
	        }
	        return false;
	    }

	    public static boolean isStaff(ItemStack is) {
	        Material m = is.getType();
	        if (m == Material.WOOD_HOE || m == Material.GOLD_HOE || m == Material.STONE_HOE || m == Material.IRON_HOE || m == Material.DIAMOND_HOE) {
	            return true;
	        }
	        return false;
	    }

	    public static boolean isBow(ItemStack is) {
	        Material m = is.getType();
	        if (m == Material.BOW) {
	            return true;
	        }
	        return false;
	    }

	    public static boolean isWeapon(ItemStack i) {
	        if (getDamageData(i).equalsIgnoreCase("no")) {
	            return false;
	        } else if (getDamageData(i).contains(":")) {
	            return true;
	        }
	        return false;
	    }
	    
	    public static String getDamageData(ItemStack i) {
	        // CraftItemStack css = (CraftItemStack)i;
	        try {
	            try {
	                try {
	                    // NBTTagList description = CraftItemStack.asNMSCopy(i).getTag().getCompound("display").getList("Lore", 0);
	                    List<String> lore = i.getItemMeta().getLore();

	                    // int x = 0;

	                    boolean elemental_dmg = false;
	                    String elemental_data = "";
	                    int edmg = 0;
	                    String leech_percent = "";

	                    boolean crit_dmg = false;
	                    boolean leech = false;
	                    boolean knockback = false;
	                    boolean blind = false;
	                    boolean str = false;
	                    boolean dex = false;
	                    boolean vit = false;
	                    boolean intel = false;
	                    boolean pure_dmg = false;
	                    boolean armor_pen = false;
	                    boolean vs_players = false;
	                    boolean vs_monsters = false;
	                    boolean accuracy = false;

	                    int str_atr = 0;
	                    int dex_atr = 0;
	                    int vit_atr = 0;
	                    int int_atr = 0;

	                    double vs_modifier = 0;

	                    int pure_dmg_val = 0;
	                    int armor_pen_val = 0;
	                    int accuracy_val = 0;

	                    List<String> all_attributes = new ArrayList<String>();

	                    for (String s : lore) {
	                        if (!(s).startsWith(ChatColor.RED.toString())) {
	                            // x++;
	                            continue;
	                        }

	                        all_attributes.add(s);
	                        if (s.contains("DMG: +")) {
	                            elemental_dmg = true;
	                            elemental_data = s;
	                        }
	                        if (s.contains("STR:")) {
	                            str = true;
	                            String str_string = s;
	                            str_atr = Integer.parseInt(str_string.substring(str_string.indexOf(":") + 3, str_string.length()));
	                        }
	                        if (s.contains("DEX:")) {
	                            dex = true;
	                            String dex_string = s;
	                            dex_atr = Integer.parseInt(dex_string.substring(dex_string.indexOf(":") + 3, dex_string.length()));
	                        }
	                        if (s.contains("VIT:")) {
	                            vit = true;
	                            String vit_string = s;
	                            vit_atr = Integer.parseInt(vit_string.substring(vit_string.indexOf(":") + 3, vit_string.length()));
	                        }
	                        if (s.contains("INT:")) {
	                            intel = true;
	                            String int_string = s;
	                            int_atr = Integer.parseInt(int_string.substring(int_string.indexOf(":") + 3, int_string.length()));
	                        }
	                        if (s.contains("PURE DMG:")) {
	                            pure_dmg = true;
	                            String pure_dmg_string = s;
	                            pure_dmg_val = Integer.parseInt(pure_dmg_string.substring(pure_dmg_string.indexOf(":") + 3, pure_dmg_string.length()));
	                        }
	                        if (s.contains("ACCURACY:")) {
	                            accuracy = true;
	                            String accuracy_string = s;
	                            accuracy_val = Integer.parseInt(accuracy_string.substring(accuracy_string.indexOf(":") + 2, accuracy_string.indexOf("%")));
	                        }
	                        if (s.contains("ARMOR PENETRATION:")) {
	                            armor_pen = true;
	                            String armor_pen_string = s;
	                            armor_pen_val = Integer.parseInt(armor_pen_string.substring(armor_pen_string.indexOf(":") + 2, armor_pen_string.indexOf("%")));
	                        }
	                        if (s.contains("CRITICAL HIT")
	                                || (i.getType() == Material.WOOD_AXE || i.getType() == Material.STONE_AXE || i.getType() == Material.IRON_AXE
	                                        || i.getType() == Material.DIAMOND_AXE || i.getType() == Material.GOLD_AXE)) {

	                            int crit_chance = 0;

	                            if (s.contains("CRITICAL HIT")) {
	                                String crit_data = s;
	                                crit_chance = Integer.parseInt(crit_data.substring(crit_data.indexOf(":") + 1, crit_data.indexOf("%")).replaceAll(" ", ""));
	                            }

	                            if ((i.getType() == Material.WOOD_AXE || i.getType() == Material.STONE_AXE || i.getType() == Material.IRON_AXE
	                                    || i.getType() == Material.DIAMOND_AXE || i.getType() == Material.GOLD_AXE)) {
	                                crit_chance += 3;
	                            }

	                            if (intel == true) {
	                                crit_chance += (int_atr * 0.025);
	                            }

	                            if (crit_chance >= new Random().nextInt(100)) {
	                                crit_dmg = true;
	                            }
	                        }
	                        if (s.contains("LIFE STEAL")) {
	                            String leech_data = s;
	                            leech = true;
	                            leech_percent = leech_data.substring(leech_data.indexOf(":") + 1, leech_data.indexOf("%")).replaceAll(" ", "");
	                        }
	                        if (s.contains("KNOCKBACK")) {
	                            String kb_data = s;

	                            int kb_chance = Integer.parseInt(kb_data.substring(kb_data.indexOf(":") + 1, kb_data.indexOf("%")).replaceAll(" ", ""));
	                            if (kb_chance >= new Random().nextInt(100)) {
	                                knockback = true;
	                            }
	                        }
	                        if (s.contains("BLIND")) {
	                            String blind_data = s;

	                            int blind_chance = Integer.parseInt(blind_data.substring(blind_data.indexOf(":") + 1, blind_data.indexOf("%")).replaceAll(" ", ""));
	                            if (blind_chance >= new Random().nextInt(100)) {
	                                blind = true;
	                            }
	                        }
	                        if (s.contains("vs. PLAYERS")) {
	                            String vs_data = s;
	                            vs_modifier = Integer.parseInt(vs_data.substring(vs_data.indexOf("+") + 1, vs_data.indexOf("%")).replaceAll(" ", ""));
	                            vs_players = true;
	                        }
	                        if (s.contains("vs. MONSTERS")) {
	                            String vs_data = s;
	                            vs_modifier = Integer.parseInt(vs_data.substring(vs_data.indexOf("+") + 1, vs_data.indexOf("%")).replaceAll(" ", ""));
	                            vs_monsters = true;
	                        }
	                        // x++;
	                    }

	                    String dmg_data = all_attributes.get(0);
	                    dmg_data = dmg_data.replaceAll(ChatColor.RED.toString() + "DMG: ", "");

	                    dmg_data = dmg_data.replaceAll(" ", "");

	                    int min_dmg = Integer.parseInt(dmg_data.split("-")[0]);
	                    int max_dmg = Integer.parseInt(dmg_data.split("-")[1]);

	                    if ((max_dmg - min_dmg) <= 0) {
	                        max_dmg += 1;
	                    }

	                    if (min_dmg > max_dmg) {
	                        min_dmg = 1;
	                        max_dmg = 2;
	                    }

	                    int dmg = new Random().nextInt(max_dmg - min_dmg) + min_dmg;

	                    if (elemental_dmg == true) {
	                        edmg = Integer.parseInt(elemental_data.split("\\+")[1]);
	                        edmg += edmg * (1 + 0.05 * int_atr);
	                    }

	                    String return_string = String.valueOf(dmg + edmg) + ":";

	                    if (str == true
	                            && (i.getType() == Material.WOOD_AXE || i.getType() == Material.IRON_AXE || i.getType() == Material.STONE_AXE
	                                    || i.getType() == Material.DIAMOND_AXE || i.getType() == Material.GOLD_AXE)) {
	                        double str_mod = 0.015D * str_atr;
	                        return_string = return_string + "str=" + str_mod + ":";
	                    }

	                    if (dex == true && (i.getType() == Material.BOW)) {
	                        double dex_mod = 0.015D * dex_atr;
	                        return_string = return_string + "dex=" + dex_mod + ":";
	                    }

	                    if (vit == true
	                            && (i.getType() == Material.WOOD_SWORD || i.getType() == Material.IRON_SWORD || i.getType() == Material.STONE_SWORD
	                                    || i.getType() == Material.DIAMOND_SWORD || i.getType() == Material.GOLD_SWORD)) {
	                        double vit_mod = 0.015D * vit_atr;
	                        return_string = return_string + "vit=" + vit_mod + ":";
	                    }

	                    if (str == true) {
	                        return_string = return_string + "str_raw=" + str_atr + ":";
	                    }

	                    if (dex == true) {
	                        return_string = return_string + "dex_raw=" + dex_atr + ":";
	                    }

	                    if (vit == true) {
	                        return_string = return_string + "vit_raw=" + vit_atr + ":";
	                    }

	                    if (intel == true) {
	                        return_string = return_string + "int_raw=" + int_atr + ":";
	                    }

	                    if (accuracy == true) {
	                        return_string = return_string + "accuracy=" + accuracy_val + ":";
	                    }

	                    if (elemental_dmg == true) {
	                        String element_damage_type = elemental_data.substring(0, elemental_data.indexOf(" "));
	                        return_string = return_string + "edmg=" + element_damage_type + ":";
	                    }

	                    if (armor_pen == true) {
	                        return_string = return_string + "armor_pen=" + armor_pen_val + ":";
	                    }

	                    if (pure_dmg == true) {
	                        return_string = return_string + "pure_dmg=" + pure_dmg_val + ":";
	                    }

	                    if (crit_dmg == true) {
	                        return_string = return_string + "crit=true" + ":";
	                    }

	                    if (leech == true) {
	                        return_string = return_string + "leech=" + leech_percent + ":";
	                    }

	                    if (knockback == true) {
	                        return_string = return_string + "knockback=true" + ":";
	                    }

	                    if (vs_players == true) {
	                        return_string = return_string + "vs_players=" + vs_modifier + ":";
	                    }

	                    if (vs_monsters == true) {
	                        return_string = return_string + "vs_monsters=" + vs_modifier + ":";
	                    }

	                    if (blind == true) {
	                        return_string = return_string + "blind=true";
	                    }

	                    return return_string;
	                } catch (NumberFormatException e) {
	                    return "no";
	                }
	            } catch (IndexOutOfBoundsException e) {
	                return "no";
	            }
	        } catch (NullPointerException e) {
	            return "no";
	        }
	    }

		public static boolean isECNamed(ItemStack origItem) {
			// TODO Auto-generated method stub
			return false;
		}

	    public static boolean isCustomNamed(ItemStack is) {
	        if (is == null || !is.hasItemMeta() || !is.getItemMeta().hasDisplayName()) return false;
	        boolean found = false;
	        String iName = ChatColor.stripColor(is.getItemMeta().getDisplayName());
	        List<String> normalNames = new ArrayList<String>(Arrays.asList("Agile", "Reflective", "Mending",
	                "Protective", "Pure", "Accurate", "Brute", "Snaring", "Vampyric", "Deadly", "Penetrating", "of",
	                "Fortitude", "Fire", "Poison", "Ice", "and", "Pickpocketing", "Treasure", "Golden", "Spikes",
	                "Thorns", "Resist", "Blindness", "Slaying", "Slaughter"));
	        // strip enchantment + in name
	        if (iName.contains("[")) iName = iName.substring(iName.indexOf(" ") + 1);
	        for (ItemType type : ItemType.values()) {
	            for (ItemTier tier : ItemTier.values()) {
	                for (String word : type.getTierName(tier).split(" ")) {
	                    normalNames.add(word);
	                }
	            }
	        }
	        for (String word : iName.split(" ")) {
	            for (String normalWord : normalNames) {
	                if (word.contains(normalWord)) {
	                    found = true;
	                    break;
	                }
	            }
	            if (!found) return true;
	        }
	        return false;
	    }

	    public static ItemStack makeGems(int amount) {
			ItemStack i = new ItemStack(Material.EMERALD, amount);
			List<String> new_lore = new ArrayList<String>(Arrays.asList(ChatColor.GRAY.toString() + "The currency of Andalucia"));

			ItemMeta im = i.getItemMeta();
			im.setLore(new_lore);

			im.setDisplayName(ChatColor.WHITE.toString() + "Gem");
			i.setItemMeta(im);
			i.setAmount(amount);

			return i;
		}

}

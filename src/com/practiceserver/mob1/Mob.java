package com.practiceserver.mob1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.MagmaCube;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Skeleton.SkeletonType;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.practiceserver.Main;
import com.practiceserver.health.HealthUtils;
import com.practiceserver.itemgenerator.enums.ItemRarity;
import com.practiceserver.itemgenerator.enums.ItemTier;
import com.practiceserver.itemgenerator.enums.ItemType;
import com.practiceserver.mob.FileManager;
import com.practiceserver.mobs.ItemMechanics;
import com.practiceserver.utils.MyMetadata;
import com.practiceserver.utils.Utils;


/*
Copyright � matt11matthew 2016
*/

public class Mob implements Listener {

	static FileManager c = FileManager.getInstance();
	
	private static Mob instance = new Mob();
	public static HashMap<LivingEntity, Double> health = new HashMap<LivingEntity, Double>();
	public static HashMap<LivingEntity, Double> maxhealth = new HashMap<LivingEntity, Double>();
	public static HashMap<LivingEntity, Integer> staffmobs = new HashMap<LivingEntity, Integer>();
	public static Mob getInstance() {
		return instance;
	}
	
	public void saveMobs() {
		FileConfiguration f = c.loadFile("Mobs.yml", "plugins/PracticeServer/");
		File ff = new File("plugins/PracticeServer/", "Mobs.yml");
		try {
			f.save(ff);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	//
	public void setupMobs() {
		FileConfiguration f = c.loadFile("Mobs.yml", "plugins/PracticeServer/");
		File ff = new File("plugins/PracticeServer/", "Mobs.yml");
		List<String> moblist = new ArrayList();
		moblist.add("T1");
		f.set("MobList", moblist);
		f.set("T1.Name", "&fStaving Bandit");
		f.set("T1.MinHP", 10);
		f.set("T1.MaxHP", 40);
		f.set("T1.Shinny", false);
		f.set("T1.Head", "Lord_Kashi");
		f.set("T1.ArmorRand", "T1");
		f.set("T1.Time", 15);
		try {
			f.save(ff);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public FileConfiguration getMobs() {
		return c.loadFile("Mobs.yml", "plugins/PracticeServer/");
	}
	
	public double getHealth(String mob) {
		FileConfiguration f = c.loadFile("Mobs.yml", "plugins/PracticeServer/");
		return Utils.ir(f.getInt(mob + ".MinHP"), f.getInt(mob + ".MaxHP"));
		
	}
	
	public String getName(String mob) {
		FileConfiguration f = c.loadFile("Mobs.yml", "plugins/PracticeServer/");
		return f.getString(mob + ".Name");
	}
	
	public boolean shinny(String mob) {
		FileConfiguration f = c.loadFile("Mobs.yml", "plugins/PracticeServer/");
		return f.getBoolean(mob + ".Shinny");
	}
	
	public void clearMobs() {
		for (Entity e : Bukkit.getServer().getWorld("world").getEntities())e.remove();
	}
	
	public int getTier(String mob) {
		FileConfiguration f = c.loadFile("Mobs.yml", "plugins/PracticeServer/");
		return f.getInt(mob + ".Tier");
		
	}
	
	public int getMinDMG(String mob) {
		FileConfiguration f = c.loadFile("Mobs.yml", "plugins/PracticeServer/");
		int arm = f.getInt(mob + ".DMGMin");
		return arm;
	}
	
	public int getMaxDMG(String mob) {
		FileConfiguration f = c.loadFile("Mobs.yml", "plugins/PracticeServer/");
		int arm = f.getInt(mob + ".DMGMax");
		return arm;
	}
	
	public void setupArmor(LivingEntity l, String mob) {
		FileConfiguration f = c.loadFile("Mobs.yml", "plugins/PracticeServer/");
		String arm = f.getString(mob + ".ArmorRand");
		LivingEntity s = l;
		Random random = new Random();
		if(arm.equals("T1")) {
			int held = random.nextInt(7) + 1;
		      int chest = random.nextInt(2) + 1;
		      int pant = random.nextInt(2) + 1;
		      int boots = random.nextInt(2) + 1;
		      s.getEquipment().setHelmet(null);
		      s.getEquipment().setChestplate(null);
		      s.getEquipment().setLeggings(null);
		      s.getEquipment().setBoots(null);
		      s.getEquipment().setHelmetDropChance(0.0F);
		      s.getEquipment().setChestplateDropChance(0.0F);
		      s.getEquipment().setLeggingsDropChance(0.0F);
		      s.getEquipment().setBootsDropChance(0.0F);
		      s.getEquipment().setItemInMainHandDropChance(0.0F);
		      s.getEquipment().setItemInOffHandDropChance(0.0F);
		      
		      if ((held == 1) || (held == 2)) {
		        s.getEquipment().setItemInHand(
		          new ItemStack(Material.WOOD_SWORD));
		      }
		      if ((held == 3) || (held == 4)) {
		        s.getEquipment().setItemInHand(
		          new ItemStack(Material.WOOD_AXE));
		      }
		      if (held == 5) {
		        s.getEquipment().setItemInHand(
		          new ItemStack(Material.WOOD_SPADE));
		      }
		      if (held == 6) {
		        s.getEquipment().setItemInHand(new ItemStack(Material.BOW));
		      }
		      if (held == 7) {
			        s.getEquipment().setItemInHand(new ItemStack(Material.WOOD_HOE));
			        staffmobs.put(s, 1);
			   
		      }
		      if(customHelmet(mob)) {
			      ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
			      SkullMeta sm = (SkullMeta)skull.getItemMeta();
			      sm.setOwner(Mob.getInstance().getMobs().getString(mob + ".Head"));
			      skull.setItemMeta(sm);
			      s.getEquipment().setHelmet(skull);
		      } else {
		    	  s.getEquipment().setHelmet(new ItemStack(Material.LEATHER_HELMET));
		      }
		      if (chest == 1) {
		        s.getEquipment().setChestplate(
		          new ItemStack(Material.LEATHER_CHESTPLATE));
		      }
		      if (pant == 1) {
		        s.getEquipment().setLeggings(
		          new ItemStack(Material.LEATHER_LEGGINGS));
		      }
		      if (boots == 1) {
		        s.getEquipment().setBoots(
		          new ItemStack(Material.LEATHER_BOOTS));
		      }
		      if ((chest == 2) && (pant == 2) && (boots == 2)) {
		        s.getEquipment().setBoots(
		          new ItemStack(Material.LEATHER_BOOTS));
		      }
		      s.setCanPickupItems(false);
		      if(isElite(l)) {
		    	  s.getEquipment().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
		    	  s.getEquipment().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
		    	  s.getEquipment().setBoots(new ItemStack(Material.LEATHER_BOOTS));
		      }
		}
		if(arm.equals("T2")) {
			int held = random.nextInt(7) + 1;
		      int chest = random.nextInt(2) + 1;
		      int pant = random.nextInt(2) + 1;
		      int boots = random.nextInt(2) + 1;
		      s.getEquipment().setHelmet(null);
		      s.getEquipment().setChestplate(null);
		      s.getEquipment().setLeggings(null);
		      s.getEquipment().setBoots(null);
		      s.getEquipment().setHelmetDropChance(0.0F);
		      s.getEquipment().setChestplateDropChance(0.0F);
		      s.getEquipment().setLeggingsDropChance(0.0F);
		      s.getEquipment().setBootsDropChance(0.0F);
		      s.getEquipment().setItemInMainHandDropChance(0.0F);
		      s.getEquipment().setItemInOffHandDropChance(0.0F);
		      if ((held == 1) || (held == 2)) {
		        s.getEquipment().setItemInHand(
		          new ItemStack(Material.STONE_SWORD));
		      }
		      if ((held == 3) || (held == 4)) {
		        s.getEquipment().setItemInHand(
		          new ItemStack(Material.STONE_AXE));
		      }
		      if (held == 5) {
		        s.getEquipment().setItemInHand(
		          new ItemStack(Material.STONE_SPADE));
		      }
		      if (held == 7) {
			        s.getEquipment().setItemInHand(new ItemStack(Material.STONE_HOE));
			        staffmobs.put(s, 2);
			   
		      }
		      if (held == 6) {
		        s.getEquipment().setItemInHand(new ItemStack(Material.BOW));
		      }
		      if(customHelmet(mob)) {
			      ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
			      SkullMeta sm = (SkullMeta)skull.getItemMeta();
			      sm.setOwner(Mob.getInstance().getMobs().getString(mob + ".Head"));
			      skull.setItemMeta(sm);
			      s.getEquipment().setHelmet(skull);
		      } else {
		    	  s.getEquipment().setHelmet(new ItemStack(Material.CHAINMAIL_HELMET));
		      }
		      if (chest == 1) {
		        s.getEquipment().setChestplate(new ItemStack(Material.CHAINMAIL_CHESTPLATE));
		      }
		      if (pant == 1) {
		        s.getEquipment().setLeggings(new ItemStack(Material.CHAINMAIL_LEGGINGS));
		      }
		      if (boots == 1) {
		        s.getEquipment().setBoots(new ItemStack(Material.CHAINMAIL_BOOTS));
		      }
		      if ((chest == 2) && (pant == 2) && (boots == 2)) {
		        s.getEquipment().setBoots(new ItemStack(Material.CHAINMAIL_BOOTS));
		      }
		      s.setCanPickupItems(false);
		      if(isElite(l)) {
		    	  s.getEquipment().setChestplate(new ItemStack(Material.CHAINMAIL_CHESTPLATE));
		    	  s.getEquipment().setLeggings(new ItemStack(Material.CHAINMAIL_LEGGINGS));
		    	  s.getEquipment().setBoots(new ItemStack(Material.CHAINMAIL_BOOTS));
		      }
			
		}
		if(arm.equals("T3")) {
			int held = random.nextInt(7) + 1;
		      s.getEquipment().setHelmet(null);
		      s.getEquipment().setChestplate(null);
		      s.getEquipment().setLeggings(null);
		      s.getEquipment().setBoots(null);
		      s.getEquipment().setHelmetDropChance(0.0F);
		      s.getEquipment().setChestplateDropChance(0.0F);
		      s.getEquipment().setLeggingsDropChance(0.0F);
		      s.getEquipment().setBootsDropChance(0.0F);
		      s.getEquipment().setItemInMainHandDropChance(0.0F);
		      s.getEquipment().setItemInOffHandDropChance(0.0F);
		      if ((held == 1) || (held == 2)) {
		        s.getEquipment().setItemInHand(
		          new ItemStack(Material.IRON_SWORD));
		      }
		      if ((held == 3) || (held == 4)) {
		        s.getEquipment().setItemInHand(
		          new ItemStack(Material.IRON_AXE));
		      }
		      if (held == 5) {
		        s.getEquipment().setItemInHand(
		          new ItemStack(Material.IRON_SPADE));

		      }
		      if (held == 6) {
		        s.getEquipment().setItemInHand(new ItemStack(Material.BOW));
		      }
		      if (held == 7) {
			        s.getEquipment().setItemInHand(new ItemStack(Material.IRON_HOE));
			        staffmobs.put(s, 3);
			   
		      }
		      if(customHelmet(mob)) {
			      ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
			      SkullMeta sm = (SkullMeta)skull.getItemMeta();
			      sm.setOwner(Mob.getInstance().getMobs().getString(mob + ".Head"));
			      skull.setItemMeta(sm);
			      s.getEquipment().setHelmet(skull);
		      } else {
		    	  s.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
		      }
		      s.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
		      s.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
		      s.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
		      s.setCanPickupItems(false);
			
		}
		if(arm.equals("T4")) {
			int held = random.nextInt(7) + 1;
		      s.getEquipment().setHelmet(null);
		      s.getEquipment().setChestplate(null);
		      s.getEquipment().setLeggings(null);
		      s.getEquipment().setBoots(null);
		      s.getEquipment().setHelmetDropChance(0.0F);
		      s.getEquipment().setChestplateDropChance(0.0F);
		      s.getEquipment().setLeggingsDropChance(0.0F);
		      s.getEquipment().setBootsDropChance(0.0F);
		      s.getEquipment().setItemInMainHandDropChance(0.0F);
		      s.getEquipment().setItemInOffHandDropChance(0.0F);
		      if ((held == 1) || (held == 2)) {
		        s.getEquipment().setItemInHand(
		          new ItemStack(Material.DIAMOND_SWORD));
		      }
		      if ((held == 3) || (held == 4)) {
		        s.getEquipment().setItemInHand(
		          new ItemStack(Material.DIAMOND_AXE));
		      }
		      if (held == 5) {
		        s.getEquipment().setItemInHand(
		          new ItemStack(Material.DIAMOND_SPADE));
		    
		      }
		      if (held == 6) {
		        s.getEquipment().setItemInHand(new ItemStack(Material.BOW));
		      }
		      if (held == 7) {
			        s.getEquipment().setItemInHand(new ItemStack(Material.DIAMOND_HOE));
			        staffmobs.put(s, 4);
			   
		      }
		      if(customHelmet(mob)) {
			      ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
			      SkullMeta sm = (SkullMeta)skull.getItemMeta();
			      sm.setOwner(Mob.getInstance().getMobs().getString(mob + ".Head"));
			      skull.setItemMeta(sm);
			      s.getEquipment().setHelmet(skull);
		      } else {
			      s.getEquipment().setHelmet( new ItemStack(Material.DIAMOND_HELMET));
		      }
		      s.getEquipment().setChestplate(
		        new ItemStack(Material.DIAMOND_CHESTPLATE));
		      s.getEquipment().setLeggings(
		        new ItemStack(Material.DIAMOND_LEGGINGS));
		      s.getEquipment()
		        .setBoots(new ItemStack(Material.DIAMOND_BOOTS));
		      s.setCanPickupItems(false);
		}
		if(arm.equals("T5")) {
			 int held = random.nextInt(7) + 1;
		      s.getEquipment().setHelmet(null);
		      s.getEquipment().setChestplate(null);
		      s.getEquipment().setLeggings(null);
		      s.getEquipment().setBoots(null);
		      s.getEquipment().setHelmetDropChance(0.0F);
		      s.getEquipment().setChestplateDropChance(0.0F);
		      s.getEquipment().setLeggingsDropChance(0.0F);
		      s.getEquipment().setBootsDropChance(0.0F);
		      s.getEquipment().setItemInMainHandDropChance(0.0F);
		      s.getEquipment().setItemInOffHandDropChance(0.0F);
		      if ((held == 1) || (held == 2)) {
		        s.getEquipment().setItemInHand(
		          new ItemStack(Material.GOLD_SWORD));
		      }
		      if ((held == 3) || (held == 4)) {
		        s.getEquipment().setItemInHand(
		          new ItemStack(Material.GOLD_AXE));
		      }
		      if (held == 5) {
		        s.getEquipment().setItemInHand(new ItemStack(Material.GOLD_SPADE));
		  
		      }
		      if (held == 7) {
			        s.getEquipment().setItemInHand(new ItemStack(Material.GOLD_HOE));
			        staffmobs.put(s, 5);
		      }
		      if (held == 6) {
		        s.getEquipment().setItemInHand(new ItemStack(Material.BOW));
		      }
		      if(customHelmet(mob)) {
			      ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
			      SkullMeta sm = (SkullMeta)skull.getItemMeta();
			      sm.setOwner(Mob.getInstance().getMobs().getString(mob + ".Head"));
			      skull.setItemMeta(sm);
			      s.getEquipment().setHelmet(skull);
		      } else {
			      s.getEquipment().setHelmet(new ItemStack(Material.GOLD_HELMET));
		      }
		      s.getEquipment().setChestplate(
		        new ItemStack(Material.GOLD_CHESTPLATE));
		      s.getEquipment().setLeggings(
		        new ItemStack(Material.GOLD_LEGGINGS));
		      s.getEquipment().setBoots(new ItemStack(Material.GOLD_BOOTS));
		      s.setCanPickupItems(false);
		      s.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0));
		}
		s.setMaxHealth(Mob.getInstance().getHealth(mob));
		s.setHealth(s.getMaxHealth());
		s.setMetadata("Mob", (MetadataValue) new MyMetadata(Main.plugin, mob.trim()));
		s.setMetadata("ID", (MetadataValue) new MyMetadata(Main.plugin, mob.trim()));
		setupDisplayName(s, s.getCustomName());
		if(isElite(s)) {
			makeShinny(s);
		}
	}
	
	public boolean customHelmet(String s) {
		return getMobs().getBoolean(s + ".CustomHead");
	}

	public void makeShinny(LivingEntity l) {
		if(l.getEquipment().getBoots() != null) {
			ItemStack boots = l.getEquipment().getBoots();
			boots.addEnchantment(Enchantment.DURABILITY, 1);
			l.getEquipment().setBoots(boots);
		}
		if(l.getEquipment().getLeggings() != null) {
			ItemStack leg = l.getEquipment().getLeggings();
			leg.addEnchantment(Enchantment.DURABILITY, 1);
			l.getEquipment().setLeggings(leg);
		}
		if(l.getEquipment().getChestplate() != null) {
			ItemStack chest = l.getEquipment().getChestplate();
			chest.addEnchantment(Enchantment.DURABILITY, 1);
			l.getEquipment().setChestplate(chest);
		}
		if(l.getEquipment().getHelmet() != null) {
			ItemStack boots = l.getEquipment().getHelmet();
			if(boots.getType() != Material.SKULL_ITEM) {
				boots.addEnchantment(Enchantment.DURABILITY, 1);
				l.getEquipment().setHelmet(boots);
			}
		}


	}
	
	public boolean isElite(LivingEntity l) {
		boolean is = (boolean) l.getMetadata("Elite").get(0).value();
		return is;
	}
	
	public String getMobFromMob(LivingEntity l) {
		String s = (String) l.getMetadata("Mob").get(0).value();
		return s;
	}
	
	public String getName(LivingEntity l) {
		String s = (String) l.getMetadata("Name").get(0).value();
		return s;
	}
	
	public String getDisName(LivingEntity l) {
		String s = (String) l.getMetadata("DisName").get(0).value();
		return s;
	}
	
	public String getMobName(LivingEntity l) {
		String s = (String) l.getMetadata("MName").get(0).value();
		return s;
	}
	
	public String getMobNameID(LivingEntity l) {
		String s = (String) l.getMetadata("ID").get(0).value();
		return s;
	}
	
	public double getDMG(LivingEntity l) {
		return 0.0D;
	}
	 
	public List<String> getMobsThatAreCustom() {
		return c.loadFile("Mobs.yml", "plugins/PracticeServer/").getStringList("MobsWithCustomDrops");
	}
	
	public boolean hasCustomDrops(String mob) {
		if(getMobsThatAreCustom().contains(mob)) {
			return true;
		}
		return false;
	}
	
	public List<String> getDrops(String mob) {
		if(hasCustomDrops(mob))return getMobs().getStringList(mob + ".Drops");
		return null;
	}
	
	public String getDropString(String item) {
		return Mob.getInstance().getMobs().getString(item + ".item");
	}
	
	public ItemStack getCustomDrop(String item) {
		return ItemMechanics.getItemFromFile(item);
	}
	
	public boolean drop(String item) {
		int min = getMobs().getInt(item + ".minchance");
		int max = getMobs().getInt(item + ".maxchance");
		if(min == max)return true;
		int rand = Utils.ir(min, max);
		if((rand > 1) && (rand < 10))return true;
		return false;		
	}
	
	public boolean canCustomDrop(LivingEntity l, String mob) {
		String meta = getMobFromMob(l);
		if(meta.equalsIgnoreCase(mob)) {
			return true;
		}
		return false;
	}
	
	public static double getHealth(LivingEntity l) {
		if(health.containsKey(l))return health.get(l);
		return 0.0D;
	}
	
	public static void updateHealth(LivingEntity l) {
		if(!maxhealth.containsKey(l))return;
		if(!health.containsKey(l))return;
		if(getHealth(l) > getMaxHealth(l)) {
			l.damage(l.getHealth());
		}
	}
	
	public static void setupHealth(LivingEntity l, double max) {
		if(maxhealth.containsKey(l))return;
		if(health.containsKey(l))return;
		maxhealth.put(l, max);
		health.put(l, max);
	}
	
	public static double getMaxHealth(LivingEntity l) {
		if(maxhealth.containsKey(l))return maxhealth.get(l);
		return 0.0D;
	}
	
	@EventHandler
    public void onMobDeath(EntityDeathEvent e) {
		e.getDrops().clear();
		e.setDroppedExp(0);
	}
	
	public static boolean hasAxe(String s) {
		String ss = s.split("axe:")[1];
		String r = ss.split(",")[0];
		return Boolean.parseBoolean(r);
	}
	
	public static boolean hasSword(String s) {
		String ss = s.split("sword:")[1];
		String r = ss.split(",")[0];
		return Boolean.parseBoolean(r);
	}
	
	public static boolean hasPolearm(String s) {
		String ss = s.split("polearm:")[1];
		String r = ss.split(",")[0];
		return Boolean.parseBoolean(r);
	}
	
	public static boolean hasStaff(String s) {
		String ss = s.split("staff:")[1];
		String r = ss.split(",")[0];
		return Boolean.parseBoolean(r);
	}
	
	public static boolean hasBow(String s) {
		String ss = s.split("bow:")[1];
		String r = ss.split(",")[0];
		return Boolean.parseBoolean(r);
	}
	
	public static boolean hasBoots(String s) {
		String has = "|axe:false,sword:true,staff:false,polearm:false,bow:false,helmet:true,chestplate:true,legs:true,boots:true|";
		String ss = s.split("boots:")[1];
		String r = ss.split(",")[0];
		return Boolean.parseBoolean(r);
	}
	
	public static boolean hasChestplate(String s) {
		String has = "(axe:false,sword:true,staff:false,polearm:false,bow:false,helmet:true,chestplate:true,legs:true,boots:true)";
		String ss = s.split("chestplate:")[1];
		String r = ss.split(",")[0];
		return Boolean.parseBoolean(r);
	}
	
	public static boolean hasLeggings(String s) {
		String has = "(axe:false,sword:true,staff:false,polearm:false,bow:false,helmet:true,chestplate:true,legs:true,boots:true)";
		String ss = s.split("legs:")[1];
		String r = ss.split(",")[0];
		return Boolean.parseBoolean(r);
	}
	
	public static boolean hasHelmet(String s) {
		return true;
	}
	
				
	public void drop(int tier, LivingEntity l, Location loc, Player killer, String hasarmor) {
		double gem_drop_mod = 1 + (HealthUtils.getInstance().getGemFind(killer)) / 100;
		if(tier == 5) {
			Random r = new Random();
			int drop;//was 1 - 200
			drop = r.nextInt(100) + 1;
			if(isImp(l)) {
				drop = r.nextInt(80) + 1;
			}
	        if(isElite(l)) {
	        	drop = (drop / 2);
	        }
	        double item_drop_mod = 1 + (HealthUtils.getInstance().getItemFind(killer)) / 100;
	        drop = (int) (drop * item_drop_mod);
			int gems = r.nextInt(2) + 1;
	        int stacks = r.nextInt(4) + 1;
	        int amt = r.nextInt(17) + 16;
	        amt = (int) (amt * gem_drop_mod);
	        if (gems == 1) {      	
	        	for (int i = 0; i < stacks; i++) {
	        		ItemStack gem = new ItemStack(Material.EMERALD, 64);
	        		ItemMeta im = gem.getItemMeta();
	        		im.setDisplayName(ChatColor.WHITE + "Gem");
	            	im.setLore(Arrays.asList(new String[] {ChatColor.GRAY + "The currency of Andalucia" }));
	            	gem.setItemMeta(im);
	            	drop(loc, gem);
	        	}
	        	ItemStack gem = new ItemStack(Material.EMERALD, amt);
	            ItemMeta im = gem.getItemMeta();
	            im.setDisplayName(ChatColor.WHITE + "Gem");
	            im.setLore(Arrays.asList(new String[] {ChatColor.GRAY + "The currency of Andalucia" }));
	            gem.setItemMeta(im);
	            drop(loc, gem);
	        }
	        
	        if ((drop == 1) && (hasHelmet(hasarmor))) {
	        	drop(loc, ItemType.HELMET.generateArmor(ItemTier.T5, null, 4, ItemRarity.getRandomRarity()));    
	        	drop = 0;
	        	return;
	        }
	        if((drop == 2) && (hasChestplate(hasarmor))) {
	        	drop(loc, ItemType.CHESTPLATE.generateArmor(ItemTier.T5, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if((drop == 3) && (hasLeggings(hasarmor))) {
	        	drop(loc, ItemType.LEGGINGS.generateArmor(ItemTier.T5, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if((drop == 4) && (hasBoots(hasarmor))) {
	        	drop(loc, ItemType.BOOTS.generateArmor(ItemTier.T5, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if ((drop == 5) && (hasHelmet(hasarmor))) {
	        	drop(loc, ItemType.HELMET.generateArmor(ItemTier.T5, null, 4, ItemRarity.getRandomRarity()));    
	        	drop = 0;
	        	return;
	        }
	        if((drop == 6) && (hasChestplate(hasarmor))) {
	        	drop(loc, ItemType.CHESTPLATE.generateArmor(ItemTier.T5, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if((drop == 7) && (hasLeggings(hasarmor))) {
	        	drop(loc, ItemType.LEGGINGS.generateArmor(ItemTier.T5, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if((drop == 8) && (hasBoots(hasarmor))) {
	        	drop(loc, ItemType.BOOTS.generateArmor(ItemTier.T5, null, 4, ItemRarity.getRandomRarity()));  
	        	drop = 0;
	        	return;
	        }
	        if(drop == 11) {
	        	//drop(loc, GemSacks.t5_gem_pouch);  
	        }
	        
	        if((drop == 9) || (drop == 10)) {
	        	if(hasAxe(hasarmor)) {
	        		drop(loc, ItemType.AXE.generateWeapon(ItemTier.T5, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        	if(hasSword(hasarmor)) {
	        		drop(loc, ItemType.SWORD.generateWeapon(ItemTier.T5, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        	if(hasPolearm(hasarmor)) {
	        		drop(loc, ItemType.POLEARM.generateWeapon(ItemTier.T5, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        	if(hasStaff(hasarmor)) {
	        		drop(loc, ItemType.STAFF.generateWeapon(ItemTier.T5, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        	if(hasBow(hasarmor)) {
	        		drop(loc, ItemType.BOW.generateWeapon(ItemTier.T5, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        
	        }
		}
		if(tier == 4) {
			Random r = new Random();
			int drop;//was 140
			drop = r.nextInt(70) + 1;
			if(isImp(l)) {//was 120
				drop = r.nextInt(60) + 1;
			}
	        if(isElite(l)) {
	        	drop = (drop / 2);
	        }
	        double item_drop_mod = 1 + (HealthUtils.getInstance().getItemFind(killer)) / 100;
	        drop = (int) (drop * item_drop_mod);
			int gems = r.nextInt(2) + 1;
	        int amt = (int) (r.nextInt(33) + 32 * gem_drop_mod);
	        if (gems == 1) { 	        	
	        	ItemStack gem = new ItemStack(Material.EMERALD, amt);
	            ItemMeta im = gem.getItemMeta();
	            im.setDisplayName(ChatColor.WHITE + "Gem");
	            im.setLore(Arrays.asList(new String[] {ChatColor.GRAY + "The currency of Andalucia" }));
	            gem.setItemMeta(im);
	            drop(loc, gem);
	        }
	        /*if (drop == 1) {
	        	drop(loc, ItemType.HELMET.generateArmor(ItemTier.T4, null, 4, ItemRarity.getRandomRarity()));    
	        	return;
	        }
	        if(drop == 2) {
	        	drop(loc, ItemType.CHESTPLATE.generateArmor(ItemTier.T4, null, 4, ItemRarity.getRandomRarity())); 
	        	return;
	        }
	        if(drop == 3) {
	        	drop(loc, ItemType.LEGGINGS.generateArmor(ItemTier.T4, null, 4, ItemRarity.getRandomRarity()));   
	        	return;
	        }
	        if(drop == 4) {
	        	drop(loc, ItemType.BOOTS.generateArmor(ItemTier.T4, null, 4, ItemRarity.getRandomRarity()));   
	        	return;
	        }
	        if (drop == 5) {
	        	drop(loc, ItemType.HELMET.generateArmor(ItemTier.T4, null, 4, ItemRarity.getRandomRarity()));    
	        	return;
	        }
	        if(drop == 6) {
	        	drop(loc, ItemType.CHESTPLATE.generateArmor(ItemTier.T4, null, 4, ItemRarity.getRandomRarity()));   
	        	return;
	        }
	        if(drop == 7) {
	        	drop(loc, ItemType.LEGGINGS.generateArmor(ItemTier.T4, null, 4, ItemRarity.getRandomRarity()));   
	        	return;
	        }
	        if(drop == 8) {
	        	drop(loc, ItemType.BOOTS.generateArmor(ItemTier.T4, null, 4, ItemRarity.getRandomRarity()));   
	        	return;
	        }
	        if((drop == 9) || (drop == 10)) {
	        	drop(loc, ItemType.AXE.generateWeapon(ItemTier.T4, null, ItemRarity.getRandomRarity()));
	        	return;
	        }
	        if((drop == 11) || (drop == 12)) {
	        	drop(loc, ItemType.SWORD.generateWeapon(ItemTier.T4, null, ItemRarity.getRandomRarity()));
	        	return;
	        }*/
	        if ((drop == 1) && (hasHelmet(hasarmor))) {
	        	drop(loc, ItemType.HELMET.generateArmor(ItemTier.T4, null, 4, ItemRarity.getRandomRarity()));    
	        	drop = 0;
	        	return;
	        }
	        if((drop == 2) && (hasChestplate(hasarmor))) {
	        	drop(loc, ItemType.CHESTPLATE.generateArmor(ItemTier.T4, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if((drop == 3) && (hasLeggings(hasarmor))) {
	        	drop(loc, ItemType.LEGGINGS.generateArmor(ItemTier.T4, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if((drop == 4) && (hasBoots(hasarmor))) {
	        	drop(loc, ItemType.BOOTS.generateArmor(ItemTier.T4, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if ((drop == 5) && (hasHelmet(hasarmor))) {
	        	drop(loc, ItemType.HELMET.generateArmor(ItemTier.T4, null, 4, ItemRarity.getRandomRarity()));    
	        	drop = 0;
	        	return;
	        }
	        if((drop == 6) && (hasChestplate(hasarmor))) {
	        	drop(loc, ItemType.CHESTPLATE.generateArmor(ItemTier.T4, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if((drop == 7) && (hasLeggings(hasarmor))) {
	        	drop(loc, ItemType.LEGGINGS.generateArmor(ItemTier.T4, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if((drop == 8) && (hasBoots(hasarmor))) {
	        	drop(loc, ItemType.BOOTS.generateArmor(ItemTier.T4, null, 4, ItemRarity.getRandomRarity()));  
	        	drop = 0;
	        	return;
	        }
	        if(drop == 11) {
	        	//drop(loc, GemSacks.t4_gem_pouch);  
	        }
	        
	        if(drop == 12) {
	        	//drop(loc, new Teleportation("Spawn").getBookItem());
	        }
	        if(drop == 13) {
	        	//drop(loc, new Teleportation("Tripoli").getBookItem());
	        }
	        
	        if(drop == 14) {
	        	//drop(loc, new Teleportation("Avalon").getBookItem());
	        }
	        
	        if((drop == 9) || (drop == 10)) {
	        	if(hasAxe(hasarmor)) {
	        		drop(loc, ItemType.AXE.generateWeapon(ItemTier.T4, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        	if(hasSword(hasarmor)) {
	        		drop(loc, ItemType.SWORD.generateWeapon(ItemTier.T4, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        	if(hasPolearm(hasarmor)) {
	        		drop(loc, ItemType.POLEARM.generateWeapon(ItemTier.T4, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        	if(hasStaff(hasarmor)) {
	        		drop(loc, ItemType.STAFF.generateWeapon(ItemTier.T4, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        	if(hasBow(hasarmor)) {
	        		drop(loc, ItemType.BOW.generateWeapon(ItemTier.T4, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        }
		}
		if(tier == 3) {
			Random r = new Random();
			int drop;//was 100
			drop = r.nextInt(50) + 1;
			int gems = r.nextInt(2) + 1;
	        int amt = r.nextInt(17) + 16;
	        amt = (int) (amt * gem_drop_mod);
	        if(isElite(l)) {
	        	drop = (drop / 2);
	        }
	        double item_drop_mod = 1 + (HealthUtils.getInstance().getItemFind(killer)) / 100;
	        drop = (int) (drop * item_drop_mod);
	        if (gems == 1) { 	        	
	        	ItemStack gem = new ItemStack(Material.EMERALD, amt);
	            ItemMeta im = gem.getItemMeta();
	            im.setDisplayName(ChatColor.WHITE + "Gem");
	            im.setLore(Arrays.asList(new String[] {ChatColor.GRAY + "The currency of Andalucia" }));
	            gem.setItemMeta(im);
	            drop(loc, gem);
	        }
	        /*if (drop == 1) {
	        	drop(loc, ItemType.HELMET.generateArmor(ItemTier.T3, null, 4, ItemRarity.getRandomRarity()));  
	        	return;
	        }
	        if(drop == 2) {
	        	drop(loc, ItemType.CHESTPLATE.generateArmor(ItemTier.T3, null, 4, ItemRarity.getRandomRarity()));   
	        	return;
	        }
	        if(drop == 3) {
	        	drop(loc, ItemType.LEGGINGS.generateArmor(ItemTier.T3, null, 4, ItemRarity.getRandomRarity())); 
	        	return;
	        }
	        if(drop == 4) {
	        	drop(loc, ItemType.BOOTS.generateArmor(ItemTier.T3, null, 4, ItemRarity.getRandomRarity()));   
	        	return;
	        }
	        if (drop == 5) {
	        	drop(loc, ItemType.HELMET.generateArmor(ItemTier.T3, null, 4, ItemRarity.getRandomRarity()));   
	        	return;
	        }
	        if(drop == 6) {
	        	drop(loc, ItemType.CHESTPLATE.generateArmor(ItemTier.T3, null, 4, ItemRarity.getRandomRarity()));   
	        	return;
	        }
	        if(drop == 7) {
	        	drop(loc, ItemType.LEGGINGS.generateArmor(ItemTier.T3, null, 4, ItemRarity.getRandomRarity()));   
	        	return;
	        }
	        if(drop == 8) {
	        	drop(loc, ItemType.BOOTS.generateArmor(ItemTier.T3, null, 4, ItemRarity.getRandomRarity()));  
	        	return;
	        }
	        if((drop == 9) || (drop == 10)) {
	        	drop(loc, ItemType.AXE.generateWeapon(ItemTier.T3, null, ItemRarity.getRandomRarity()));
	        	return;
	        }
	        if((drop == 11) || (drop == 12)) {
	        	drop(loc, ItemType.SWORD.generateWeapon(ItemTier.T3, null, ItemRarity.getRandomRarity()));
	        	return;
	        }*/
	        if ((drop == 1) && (hasHelmet(hasarmor))) {
	        	drop(loc, ItemType.HELMET.generateArmor(ItemTier.T3, null, 4, ItemRarity.getRandomRarity()));    
	        	drop = 0;
	        	return;
	        }
	        if((drop == 2) && (hasChestplate(hasarmor))) {
	        	drop(loc, ItemType.CHESTPLATE.generateArmor(ItemTier.T3, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if((drop == 3) && (hasLeggings(hasarmor))) {
	        	drop(loc, ItemType.LEGGINGS.generateArmor(ItemTier.T3, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if((drop == 4) && (hasBoots(hasarmor))) {
	        	drop(loc, ItemType.BOOTS.generateArmor(ItemTier.T3, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if ((drop == 5) && (hasHelmet(hasarmor))) {
	        	drop(loc, ItemType.HELMET.generateArmor(ItemTier.T3, null, 4, ItemRarity.getRandomRarity()));    
	        	drop = 0;
	        	return;
	        }
	        if((drop == 6) && (hasChestplate(hasarmor))) {
	        	drop(loc, ItemType.CHESTPLATE.generateArmor(ItemTier.T3, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if((drop == 7) && (hasLeggings(hasarmor))) {
	        	drop(loc, ItemType.LEGGINGS.generateArmor(ItemTier.T3, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if((drop == 8) && (hasBoots(hasarmor))) {
	        	drop(loc, ItemType.BOOTS.generateArmor(ItemTier.T3, null, 4, ItemRarity.getRandomRarity()));  
	        	drop = 0;
	        	return;
	        }
	        if(drop == 11) {
	        //	drop(loc, GemSacks.t3_gem_pouch);  
	        }
	        
	        if(drop == 12) {
	       // 	drop(loc, new Teleportation("Spawn").getBookItem());
	        }
	        if(drop == 13) {
	      //  	drop(loc, new Teleportation("Tripoli").getBookItem());
	        }
	        
	        if(drop == 14) {
	      //  	drop(loc, new Teleportation("Avalon").getBookItem());
	        }
	        
	        if((drop == 9) || (drop == 10)) {
	        	if(hasAxe(hasarmor)) {
	        		drop(loc, ItemType.AXE.generateWeapon(ItemTier.T3, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        	if(hasSword(hasarmor)) {
	        		drop(loc, ItemType.SWORD.generateWeapon(ItemTier.T3, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        	if(hasPolearm(hasarmor)) {
	        		drop(loc, ItemType.POLEARM.generateWeapon(ItemTier.T3, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        	if(hasStaff(hasarmor)) {
	        		drop(loc, ItemType.STAFF.generateWeapon(ItemTier.T3, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        	if(hasBow(hasarmor)) {
	        		drop(loc, ItemType.BOW.generateWeapon(ItemTier.T3, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        }
		}
		if(tier == 2) {
			Random r = new Random();
			int drop;//was 60
			drop = r.nextInt(30) + 1;
			int gems = r.nextInt(2) + 1;
	        int amt = r.nextInt(7) + 10;
	        amt = (int) (amt * gem_drop_mod);
	        if(isElite(l)) {
	        	drop = (drop / 2);
	        }
	        double item_drop_mod = 1 + (HealthUtils.getInstance().getItemFind(killer)) / 100;
	        drop = (int) (drop * item_drop_mod);
	        if (gems == 1) { 	        	
	        	ItemStack gem = new ItemStack(Material.EMERALD, amt);
	            ItemMeta im = gem.getItemMeta();
	            im.setDisplayName(ChatColor.WHITE + "Gem");
	            im.setLore(Arrays.asList(new String[] {ChatColor.GRAY + "The currency of Andalucia" }));
	            gem.setItemMeta(im);
	            drop(loc, gem);
	        }
	        
	        /*if (drop == 1) {
	        	drop(loc, ItemType.HELMET.generateArmor(ItemTier.T2, null, 4, ItemRarity.getRandomRarity()));     	
	        	return;
	        }
	        if(drop == 2) {
	        	drop(loc, ItemType.CHESTPLATE.generateArmor(ItemTier.T2, null, 4, ItemRarity.getRandomRarity()));   
	        	return;
	        }
	        if(drop == 3) {
	        	drop(loc, ItemType.LEGGINGS.generateArmor(ItemTier.T2, null, 4, ItemRarity.getRandomRarity()));   
	        	return;
	        }
	        if(drop == 4) {
	        	drop(loc, ItemType.BOOTS.generateArmor(ItemTier.T2, null, 4, ItemRarity.getRandomRarity()));   
	        	return;
	        }
	        if (drop == 5) {
	        	drop(loc, ItemType.HELMET.generateArmor(ItemTier.T2, null, 4, ItemRarity.getRandomRarity()));    
	        	return;
	        }
	        if(drop == 6) {
	        	drop(loc, ItemType.CHESTPLATE.generateArmor(ItemTier.T2, null, 4, ItemRarity.getRandomRarity()));  
	        	return;
	        }
	        if(drop == 7) {
	        	drop(loc, ItemType.LEGGINGS.generateArmor(ItemTier.T2, null, 4, ItemRarity.getRandomRarity())); 
	        	return;
	        }
	        if(drop == 8) {
	        	drop(loc, ItemType.BOOTS.generateArmor(ItemTier.T2, null, 4, ItemRarity.getRandomRarity()));   
	        	return;
	        }
	        if((drop == 9) || (drop == 10)) {
	        	drop(loc, ItemType.AXE.generateWeapon(ItemTier.T2,null, ItemRarity.getRandomRarity()));
	        	return;
	        }
	        if((drop == 11) || (drop == 12)) {
	        	drop(loc, ItemType.SWORD.generateWeapon(ItemTier.T2, null, ItemRarity.getRandomRarity()));
	        	return;
	        }*/
	        if ((drop == 1) && (hasHelmet(hasarmor))) {
	        	drop(loc, ItemType.HELMET.generateArmor(ItemTier.T2, null, 4, ItemRarity.getRandomRarity()));    
	        	drop = 0;
	        	return;
	        }
	        if((drop == 2) && (hasChestplate(hasarmor))) {
	        	drop(loc, ItemType.CHESTPLATE.generateArmor(ItemTier.T2, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if((drop == 3) && (hasLeggings(hasarmor))) {
	        	drop(loc, ItemType.LEGGINGS.generateArmor(ItemTier.T2, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if((drop == 4) && (hasBoots(hasarmor))) {
	        	drop(loc, ItemType.BOOTS.generateArmor(ItemTier.T2, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if ((drop == 5) && (hasHelmet(hasarmor))) {
	        	drop(loc, ItemType.HELMET.generateArmor(ItemTier.T2, null, 4, ItemRarity.getRandomRarity()));    
	        	drop = 0;
	        	return;
	        }
	        if((drop == 6) && (hasChestplate(hasarmor))) {
	        	drop(loc, ItemType.CHESTPLATE.generateArmor(ItemTier.T2, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if((drop == 7) && (hasLeggings(hasarmor))) {
	        	drop(loc, ItemType.LEGGINGS.generateArmor(ItemTier.T2, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if((drop == 8) && (hasBoots(hasarmor))) {
	        	drop(loc, ItemType.BOOTS.generateArmor(ItemTier.T2, null, 4, ItemRarity.getRandomRarity()));  
	        	drop = 0;
	        	return;
	        }
	        if(drop == 11) {
	       // 	drop(loc, GemSacks.t2_gem_pouch);  
	        }
	        
	        if(drop == 12) {
	        	//drop(loc, new Teleportation("Spawn").getBookItem());
	        }
	        if(drop == 13) {
	        	//drop(loc, new Teleportation("Tripoli").getBookItem());
	        }
	        if((drop == 9) || (drop == 10)) {
	        	if(hasAxe(hasarmor)) {
	        		drop(loc, ItemType.AXE.generateWeapon(ItemTier.T2, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        	if(hasSword(hasarmor)) {
	        		drop(loc, ItemType.SWORD.generateWeapon(ItemTier.T2, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        	if(hasPolearm(hasarmor)) {
	        		drop(loc, ItemType.POLEARM.generateWeapon(ItemTier.T2, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        	if(hasStaff(hasarmor)) {
	        		drop(loc, ItemType.STAFF.generateWeapon(ItemTier.T2, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        	if(hasBow(hasarmor)) {
	        		drop(loc, ItemType.BOW.generateWeapon(ItemTier.T2, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        }
		}
		if(tier == 1) {
			Random r = new Random();
			int drop;//was 40
			drop = r.nextInt(20) + 1;
			int gems = r.nextInt(2) + 1;
	        int amt = r.nextInt(5) + 1;
	        if(isElite(l)) {
	        	drop = (drop / 2);
	        }
	        double item_drop_mod = 1 + (HealthUtils.getInstance().getItemFind(killer)) / 100;
	        drop = (int) (drop * item_drop_mod);
	        amt = (int) (amt * gem_drop_mod);
	        if (gems == 1) { 	        	
	        	ItemStack gem = new ItemStack(Material.EMERALD, amt);
	            ItemMeta im = gem.getItemMeta();
	            im.setDisplayName(ChatColor.WHITE + "Gem");
	            im.setLore(Arrays.asList(new String[] {ChatColor.GRAY + "The currency of Andalucia" }));
	            gem.setItemMeta(im);
	            drop(loc, gem);
	        }
	        /*if (drop == 1) {
	        	drop(loc, ItemType.HELMET.generateArmor(ItemTier.T1, null, 4, ItemRarity.getRandomRarity()));  
	        	drop = 0;
	        	return;
	        }
	        if(drop == 2) {
	        	drop(loc, ItemType.CHESTPLATE.generateArmor(ItemTier.T1, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if(drop == 3) {
	        	drop(loc, ItemType.LEGGINGS.generateArmor(ItemTier.T1, null, 4, ItemRarity.getRandomRarity()));  
	        	drop = 0;
	        	return;
	        }
	        if(drop == 4) {
	        	drop(loc, ItemType.BOOTS.generateArmor(ItemTier.T1, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if (drop == 5) {
	        	drop(loc, ItemType.HELMET.generateArmor(ItemTier.T1, null, 4, ItemRarity.getRandomRarity())); 
	        	drop = 0;
	        	return;
	        }
	        if(drop == 6) {
	        	drop(loc, ItemType.CHESTPLATE.generateArmor(ItemTier.T1, null, 4, ItemRarity.getRandomRarity())); 
	        	drop = 0;
	        	return;
	        }
	        if(drop == 7) {
	        	drop(loc, ItemType.LEGGINGS.generateArmor(ItemTier.T1, null, 4, ItemRarity.getRandomRarity()));  
	        	drop = 0;
	        	return;
	        }
	        if(drop == 8) {
	        	drop(loc, ItemType.BOOTS.generateArmor(ItemTier.T1, null, 4, ItemRarity.getRandomRarity()));
	        	drop = 0;
	        	return;
	        }
	        if((drop == 9) || (drop == 10)) {
	        	drop(loc, ItemType.AXE.generateWeapon(ItemTier.T1, null, ItemRarity.getRandomRarity()));
	        	drop = 0;
	        	return;
	        }
	        if((drop == 11) || (drop == 12)) {
	        	drop(loc, ItemType.SWORD.generateWeapon(ItemTier.T1, null, ItemRarity.getRandomRarity()));
	        	drop = 0;
	        	return;
	        }*/
	        if ((drop == 1) && (hasHelmet(hasarmor))) {
	        	drop(loc, ItemType.HELMET.generateArmor(ItemTier.T1, null, 4, ItemRarity.getRandomRarity()));    
	        	drop = 0;
	        	return;
	        }
	        if((drop == 2) && (hasChestplate(hasarmor))) {
	        	drop(loc, ItemType.CHESTPLATE.generateArmor(ItemTier.T1, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if((drop == 3) && (hasLeggings(hasarmor))) {
	        	drop(loc, ItemType.LEGGINGS.generateArmor(ItemTier.T1, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if((drop == 4) && (hasBoots(hasarmor))) {
	        	drop(loc, ItemType.BOOTS.generateArmor(ItemTier.T1, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if ((drop == 5) && (hasHelmet(hasarmor))) {
	        	drop(loc, ItemType.HELMET.generateArmor(ItemTier.T1, null, 4, ItemRarity.getRandomRarity()));    
	        	drop = 0;
	        	return;
	        }
	        if((drop == 6) && (hasChestplate(hasarmor))) {
	        	drop(loc, ItemType.CHESTPLATE.generateArmor(ItemTier.T1, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if((drop == 7) && (hasLeggings(hasarmor))) {
	        	drop(loc, ItemType.LEGGINGS.generateArmor(ItemTier.T1, null, 4, ItemRarity.getRandomRarity()));   
	        	drop = 0;
	        	return;
	        }
	        if((drop == 8) && (hasBoots(hasarmor))) {
	        	drop(loc, ItemType.BOOTS.generateArmor(ItemTier.T1, null, 4, ItemRarity.getRandomRarity()));  
	        	drop = 0;
	        	return;
	        }
	        if(drop == 11) {
	        	//drop(loc, GemSacks.t1_gem_pouch);  
	        }
	        if(drop == 12) {
	        //	drop(loc, new Teleportation("Spawn").getBookItem());
	        }
	        
	        if((drop == 9) || (drop == 10)) {
	        	if(hasAxe(hasarmor)) {
	        		drop(loc, ItemType.AXE.generateWeapon(ItemTier.T1, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        	if(hasSword(hasarmor)) {
	        		drop(loc, ItemType.SWORD.generateWeapon(ItemTier.T1, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        	if(hasPolearm(hasarmor)) {
	        		drop(loc, ItemType.POLEARM.generateWeapon(ItemTier.T1, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        	if(hasStaff(hasarmor)) {
	        		drop(loc, ItemType.STAFF.generateWeapon(ItemTier.T1, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        	if(hasBow(hasarmor)) {
	        		drop(loc, ItemType.BOW.generateWeapon(ItemTier.T1, null, ItemRarity.getRandomRarity()));
	        		return;
	        	}
	        }
		}
		Random rr = new Random();
        int rand = rr.nextInt(10);
        drop(loc, new ItemStack(Material.ARROW, rand));
        return;
	}
	
	
	private int itemFind(int drop, Player killer) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void drop(Location loc, ItemStack item) {
		loc.getWorld().dropItem(loc, item);
		//loc.getWorld().dropItemNaturally(loc, item);
		return;
	}

	public void setupDMG(LivingEntity s, String name) {
		
		
	}
	
	public void getI55D(LivingEntity l) {
		
	}
	
	public double getDamage(String mob, Player p) {
		double block = 0.0D;
		double dodge = 0.0D;
		block = HealthUtils.getInstance().getBlock(p);
		dodge = HealthUtils.getInstance().getDodge(p);
		Random ran = new Random();
	    int per = ran.nextInt(100) + 1;
	    if (per <= dodge) {
	    	return -5;
	    }
	    if (per <= block) {
	    	return -10;
	    }
		return Utils.ir(getMinDMG(mob), getMaxDMG(mob));
	}

	public String getIDName(LivingEntity l) {
		String s = (String) l.getMetadata("MobIDName").get(0).value();
		return s;
	}
	
	public void setupIDName(LivingEntity l, String id) {
		l.setMetadata("MobIDName", (MetadataValue) new MyMetadata(Main.plugin, id.trim()));
		
	}
	
	public boolean isImp(LivingEntity l) {
		String type = getType(Mob.getInstance().getMobNameID(l));
		return type.equalsIgnoreCase("imp");
	}
	
	public String getDisplayName(LivingEntity l) {
		String s = (String) l.getMetadata("DisplayName").get(0).value();
		return s;
	}
	
	public void setupDisplayName(LivingEntity l, String name) {
		l.setMetadata("DisplayName", (MetadataValue) new MyMetadata(Main.plugin, name.trim()));
	}
	
	public String getType(String mob) {
		return Mob.getInstance().getMobs().getString(mob + ".MobType");
	}
	
	public void spawnMob(String mob, Location loc) {
		String name = ChatColor.stripColor(mob);
		String type = getType(mob);
		if(type.equals("skeleton")) {
	
			Skeleton s = (Skeleton)loc.getWorld().spawn(loc, Skeleton.class);
			s.setMetadata("Name", (MetadataValue) new MyMetadata(Main.plugin, Mob.getInstance().getName(ChatColor.stripColor(mob))));
			s.setSkeletonType(SkeletonType.NORMAL);
			s.setCustomName(Utils.colorCodes(Mob.getInstance().getName(ChatColor.stripColor(mob))));
			s.setCustomNameVisible(true);
			//Entity e = Drops.spawnTierMo
			if(Mob.getInstance().shinny(name)) {
				s.setMetadata("Elite", (MetadataValue) new MyMetadata(Main.plugin, true));		
				//Drops.spawnTierMob(loc, EntityType.ZOMBIE, getTier(mob), -1, loc, true, "", Utils.colorCodes(Mob.getInstance().getName(ChatColor.stripColor(mob))), true);
			} else {
				s.setMetadata("Elite", (MetadataValue) new MyMetadata(Main.plugin, false));
				//Drops.spawnTierMob(loc, EntityType.ZOMBIE, getTier(mob), -1, loc, false, "", Utils.colorCodes(Mob.getInstance().getName(ChatColor.stripColor(mob))), true);
			}
			Mob.getInstance().setupArmor(s, name);
			Mob.getInstance().setupDMG(s, name);
			Mob.getInstance().setupIDName(s, name);
		}
		if(type.equals("zombie")) {
			Zombie s = (Zombie)loc.getWorld().spawn(loc, Zombie.class);
			s.setMetadata("Name", (MetadataValue) new MyMetadata(Main.plugin, Mob.getInstance().getName(ChatColor.stripColor(mob))));
			s.setCustomName(Utils.colorCodes(Mob.getInstance().getName(ChatColor.stripColor(mob))));
			s.setCustomNameVisible(true);
			//Entity e = Drops.spawnTierMob(name, loc, EntityType.ZOMBIE, getTier(mob), -1, loc, Mob.getInstance().shinny(name), "", "", true);
			//Zombie s = (Zombie) e;
			//s.setMetadata("Name", (MetadataValue) new MyMetadata(Main.plugin, Mob.getInstance().getName(ChatColor.stripColor(mob))));
			if(Mob.getInstance().shinny(name)) {
				s.setMetadata("Elite", (MetadataValue) new MyMetadata(Main.plugin, true));		
				//Drops.spawnTierMob(loc, EntityType.ZOMBIE, getTier(mob), -1, loc, true, "", Utils.colorCodes(Mob.getInstance().getName(ChatColor.stripColor(mob))), true);
			} else {
				s.setMetadata("Elite", (MetadataValue) new MyMetadata(Main.plugin, false));
				//Drops.spawnTierMob(loc, EntityType.ZOMBIE, getTier(mob), -1, loc, false, "", Utils.colorCodes(Mob.getInstance().getName(ChatColor.stripColor(mob))), true);
			}
			Mob.getInstance().setupArmor(s, name);
			Mob.getInstance().setupDMG(s, name);
			Mob.getInstance().setupIDName(s, name);
		}
		if(type.equals("naga")) {
			Zombie s = (Zombie)loc.getWorld().spawn(loc, Zombie.class);
			s.setMetadata("Name", (MetadataValue) new MyMetadata(Main.plugin, Mob.getInstance().getName(ChatColor.stripColor(mob))));
			s.setCustomName(Utils.colorCodes(Mob.getInstance().getName(ChatColor.stripColor(mob))));
			s.setCustomNameVisible(true);
			//Entity e = Drops.spawnTierMob(name, loc, EntityType.ZOMBIE, getTier(mob), -1, loc, Mob.getInstance().shinny(name), "naga", "", true);
			//Zombie s = (Zombie) e;
			//s.setMetadata("Name", (MetadataValue) new MyMetadata(Main.plugin, Mob.getInstance().getName(ChatColor.stripColor(mob))));
			if(Mob.getInstance().shinny(name)) {
				s.setMetadata("Elite", (MetadataValue) new MyMetadata(Main.plugin, true));		
				//Drops.spawnTierMob(loc, EntityType.ZOMBIE, getTier(mob), -1, loc, true, "", Utils.colorCodes(Mob.getInstance().getName(ChatColor.stripColor(mob))), true);
			} else {
				s.setMetadata("Elite", (MetadataValue) new MyMetadata(Main.plugin, false));
				//Drops.spawnTierMob(loc, EntityType.ZOMBIE, getTier(mob), -1, loc, false, "", Utils.colorCodes(Mob.getInstance().getName(ChatColor.stripColor(mob))), true);
			}
		    Mob.getInstance().setupArmor(s, name);
			Mob.getInstance().setupDMG(s, name);
			Mob.getInstance().setupIDName(s, name);
		}
		if(type.equals("imp")) {
			PigZombie s = (PigZombie)loc.getWorld().spawn(loc, PigZombie.class);
			s.setBaby(true);
			s.setMetadata("Name", (MetadataValue) new MyMetadata(Main.plugin, Mob.getInstance().getName(ChatColor.stripColor(mob))));
			s.setCustomName(Utils.colorCodes(Mob.getInstance().getName(ChatColor.stripColor(mob))));
			s.setCustomNameVisible(true);
			//Entity e = Drops.spawnTierMob(name, loc, EntityType.PIG_ZOMBIE, getTier(mob), -1, loc, Mob.getInstance().shinny(name), "imp", "", true);
			//LivingEntity s = (LivingEntity) e;
			//s.setMetadata("Name", (MetadataValue) new MyMetadata(Main.plugin, Mob.getInstance().getName(ChatColor.stripColor(mob))));
			if(Mob.getInstance().shinny(name)) {
				s.setMetadata("Elite", (MetadataValue) new MyMetadata(Main.plugin, true));	
				//Drops.spawnTierMob(loc, EntityType.PIG_ZOMBIE, getTier(mob), -1, loc, false, "imp", Utils.colorCodes(Mob.getInstance().getName(ChatColor.stripColor(mob))), true);
			} else {
				s.setMetadata("Elite", (MetadataValue) new MyMetadata(Main.plugin, false));
			}
			s.setAngry(true);
			Mob.getInstance().setupArmor(s, name);
			Mob.getInstance().setupDMG(s, name);
			Mob.getInstance().setupIDName(s, name);
		}
		if(type.equals("wskeleton")) {
			Skeleton s = (Skeleton)loc.getWorld().spawn(loc, Skeleton.class);
			s.setSkeletonType(SkeletonType.WITHER);
			s.setMetadata("Name", (MetadataValue) new MyMetadata(Main.plugin, Mob.getInstance().getName(ChatColor.stripColor(mob))));
			s.setCustomName(Utils.colorCodes(Mob.getInstance().getName(ChatColor.stripColor(mob))));
			s.setCustomNameVisible(true);
			//Entity e = Drops.spawnTierMob(name, loc, EntityType.SKELETON, getTier(mob), -1, loc, Mob.getInstance().shinny(name), "", "", true);
			//Skeleton s = (Skeleton) e;
			//s.setSkeletonType(SkeletonType.WITHER);
			//s.setMetadata("Name", (MetadataValue) new MyMetadata(Main.plugin, Mob.getInstance().getName(ChatColor.stripColor(mob))));
			if(Mob.getInstance().shinny(name)) {
				s.setMetadata("Elite", (MetadataValue) new MyMetadata(Main.plugin, true));		
			//	Drops.spawnTierMob(loc, EntityType.SKELETON, getTier(mob), -1, loc, true, "", Utils.colorCodes(Mob.getInstance().getName(ChatColor.stripColor(mob))), true);
				
			} else {
				s.setMetadata("Elite", (MetadataValue) new MyMetadata(Main.plugin, false));
				//Drops.spawnTierMob(loc, EntityType.SKELETON, getTier(mob), -1, loc, false, "", Utils.colorCodes(Mob.getInstance().getName(ChatColor.stripColor(mob))), true);
			}
			Mob.getInstance().setupArmor(s, name);
			Mob.getInstance().setupDMG(s, name);
			Mob.getInstance().setupIDName(s, name);
		}
		if(type.equals("cube")) {
			MagmaCube s = (MagmaCube)loc.getWorld().spawn(loc, MagmaCube.class);
			s.setCustomName(Utils.colorCodes(Mob.getInstance().getName(ChatColor.stripColor(mob))));
			s.setCustomNameVisible(true);
			s.setMetadata("Name", (MetadataValue) new MyMetadata(Main.plugin, Mob.getInstance().getName(ChatColor.stripColor(mob))));

			if(Mob.getInstance().shinny(name)) {
				s.setMetadata("Elite", (MetadataValue) new MyMetadata(Main.plugin, true));		
			} else {
				s.setMetadata("Elite", (MetadataValue) new MyMetadata(Main.plugin, false));
			}
			Mob.getInstance().setupArmor(s, name);
			Mob.getInstance().setupDMG(s, name);
			Mob.getInstance().setupIDName(s, name);
		}
	}
}

	
	
	

  	

	


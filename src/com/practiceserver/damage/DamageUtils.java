package com.practiceserver.damage;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import com.practiceserver.health.HealthUtils;
import com.practiceserver.utils.Utils;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

/*
Copyright � matt11matthew 2016
*/

public class DamageUtils {
	
	private static DamageUtils instance = new DamageUtils();

	private static WorldGuardPlugin wg = (WorldGuardPlugin)Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");
	
	public static WorldGuardPlugin getWorldGaurd() {
		return wg;
	}
	
	public static DamageUtils getInstance() {
		return instance;
	}
	

	
	public int getMinDura(ItemStack item)
	  {
		String value = "Durability";
	    int returnVal = -1;
	    ItemMeta meta = item.getItemMeta();//
	    try
	    {
	      List<?> lore = meta.getLore();
	      if (lore != null) {
	        for (int i = 0; i < lore.size(); i++) {
	          if (((String)lore.get(i)).contains(value))
	          {
	            String vals = ((String)lore.get(i)).split(": ")[1];
		        vals = ChatColor.stripColor(vals);
		        vals = vals.split(" / ")[0];
		        returnVal = Integer.parseInt(vals.trim());
	          }
	        }
	      }
	    }
	    catch (Exception localException) {}
	    return returnVal;
	  }

	public int getMaxDura(ItemStack item)
	  {
		String value = "Durability";
	    int returnVal = 0;
	    ItemMeta meta = item.getItemMeta();
	    try
	    {
	      List<?> lore = meta.getLore();
	      if (lore != null) {
	        for (int i = 0; i < lore.size(); i++) {
	          if (((String)lore.get(i)).contains(value))
	          {
	            String vals = ((String)lore.get(i)).split(": ")[1];
		        vals = ChatColor.stripColor(vals);
		        vals = vals.split(" / ")[1];
		        returnVal = Integer.parseInt(vals.trim());
	          }
	        }
	      }
	    }
	    catch (Exception localException) {}
	    return returnVal;
	  }
	
	public int getMinValueFromLore1(ItemStack item, String value, int i)
	  {
	    int returnVal = 1;
	    ItemMeta meta = item.getItemMeta();
	    try
	    {
	      List<?> lore = meta.getLore();
	      if ((lore != null) && (((String)lore.get(i)).contains(value)))
	      {
	        String vals = ((String)lore.get(i)).split(": ")[1];
	        vals = ChatColor.stripColor(vals);
	        vals = vals.split("/")[0];
	        returnVal = Integer.parseInt(vals.trim());
	      }
	    }
	    catch (Exception localException) {}
	    return returnVal;
	  }
	  
	public int getMaxValueFromLore2(ItemStack item, String value, int i)
	  {
	    int returnVal = 1;
	    ItemMeta meta = item.getItemMeta();
	    try
	    {
	      List<?> lore = meta.getLore();
	      if ((lore != null) && (((String)lore.get(i)).contains(value)))
	      {
	        String vals = ((String)lore.get(i)).split(": ")[1];
	        vals = ChatColor.stripColor(vals);
	        vals = vals.split("/")[1];
	        returnVal = Integer.parseInt(vals.trim());
	      }
	    }
	    catch (Exception localException) {}
	    return returnVal;
	  }
	
	public int getMinValueFromLore(ItemStack item, String value)
	  {
	    int returnVal = 1;
	    ItemMeta meta = item.getItemMeta();
	    try
	    {
	      List<?> lore = meta.getLore();
	      if ((lore != null) && (((String)lore.get(0)).contains(value)))
	      {
	        String vals = ((String)lore.get(0)).split(": ")[1];
	        vals = ChatColor.stripColor(vals);
	        vals = vals.split(" - ")[0];
	        returnVal = Integer.parseInt(vals.trim());
	      }
	    }
	    catch (Exception localException) {}
	    return returnVal;
	  }
	  
	public int getMaxValueFromLore(ItemStack item, String value)
	  {
	    int returnVal = 1;
	    ItemMeta meta = item.getItemMeta();
	    try
	    {
	      List<?> lore = meta.getLore();
	      if ((lore != null) && (((String)lore.get(0)).contains(value)))
	      {
	        String vals = ((String)lore.get(0)).split(": ")[1];
	        vals = ChatColor.stripColor(vals);
	        vals = vals.split(" - ")[1];
	        returnVal = Integer.parseInt(vals.trim());
	      }
	    }
	    catch (Exception localException) {}
	    return returnVal;
	  }

	public int getLifestealFromLore(ItemStack item, String value)
	  {
	    int returnVal = 0;
	    ItemMeta meta = item.getItemMeta();
	    try
	    {
	      List<?> lore = meta.getLore();
	      if (lore != null) {
	        for (int i = 0; i < lore.size(); i++) {
	          if (((String)lore.get(i)).contains(value))
	          {
	            String vals = ((String)lore.get(i)).split(": ")[1];
	            vals = ChatColor.stripColor(vals);
	            vals = vals.replace("%", "").trim().toString();
	            returnVal = Integer.parseInt(vals.trim());
	          }
	        }
	      }
	    }
	    catch (Exception localException) {}
	    return returnVal;
	  }
	  
	public static int getVitFromLore(ItemStack item, String value)

	  {
	    int returnVal = 0;
	    ItemMeta meta = item.getItemMeta();
	    try
	    {
	      List<?> lore = meta.getLore();
	      if ((lore != null) && (((String)lore.get(2)).contains(value)))
	      {
	        String vals = ((String)lore.get(2)).split(": +")[1];
	        vals = ChatColor.stripColor(vals);
	        returnVal = Integer.parseInt(vals.trim());
	      }
	    }
	    catch (Exception localException) {}
	  return returnVal;
	  }
	
	public int getElemFromLore(ItemStack item, String value)
	  {
	    int returnVal = 0;
	    ItemMeta meta = item.getItemMeta();
	    try
	    {
	      List<?> lore = meta.getLore();
	      if (lore != null) {
	        for (int i = 0; i < lore.size(); i++) {
	          if (((String)lore.get(i)).contains(value))
	          {
	            String vals = ((String)lore.get(i)).split(": +")[1];
	            vals = ChatColor.stripColor(vals);
	            returnVal = Integer.parseInt(vals.trim());
	          }
	        }
	      }
	    }
	    catch (Exception localException) {}
	    return returnVal;
	  }
	
	public double getDamage(ItemStack item, String value) {
		int min = getMinValueFromLore(item, value);
		int max = getMaxValueFromLore(item, value);
		return Utils.rand(min, max);
	}
	
	
	public double getIceDMG(ItemStack item) {
		if(item.getItemMeta().hasLore())return getElemFromLore(item, "ICE DMG");
		return 0;
	}
	
	public double getFireDMG(ItemStack item) {
		if(item.getItemMeta().hasLore())return getElemFromLore(item, "FIRE DMG");
		return 0;
	}
	
	public double getPoisonDMG(ItemStack item) {
		if(item.getItemMeta().hasLore())return getElemFromLore(item, "POISON DMG");
		return 0;
	}
	
	public double getPureDMG(ItemStack item) {
		if(item.getItemMeta().hasLore())return getElemFromLore(item, "PURE DMG");
		return 0;
	}
	
	public double getLifeSteal(ItemStack item) {
		if(item.getItemMeta().hasLore())return getLifestealFromLore(item, "LIFE STEAL");
		return 0;
	}
	
	public double getCriticalHit(ItemStack item) {
		if(item.getItemMeta().hasLore())return getLifestealFromLore(item, "CRITICAL HIT");
		return 0;
	}
	
	public boolean hasFireDMG(ItemStack item) {
		if(item.getItemMeta().getLore().contains("FIRE DMG"))return true;
		return false;
	}
	
	public boolean hasIceDMG(ItemStack item) {
		if(item.getItemMeta().getLore().contains("ICE DMG"))return true;
		return false;
	}
	
	public boolean hasPoisonDMG(ItemStack item) {
		if(item.getItemMeta().getLore().contains("POISON DMG"))return true;
		return false;
	}
	
	public boolean hasPureDMG(ItemStack item) {
		if(item.getItemMeta().getLore().contains("PURE DMG"))return true;
		return false;
	}
	
	public boolean hasLifeStealDMG(ItemStack item) {
		if(item.getItemMeta().getLore().contains("LIFE STEAL"))return true;
		return false;
	}
	
	public boolean hasCriticalHit(ItemStack item) {
		if(item.getItemMeta().getLore().contains("CRITICAL HIT"))return true;
		return false;
	}
	
	public double getDMGWithVit(Player p, double dmg) {
		PlayerInventory i = p.getInventory();
		if((p.getItemInHand() != null) && (p.getItemInHand().getType().name().contains("SWORD")) && (p.getItemInHand().getItemMeta().hasLore())) {
			double vital = 0.0D;
			if(i.getHelmet() != null) {
				vital += HealthUtils.getInstance().getVitFromItem(i.getHelmet());
			}
			if(i.getChestplate() != null) {
				vital += HealthUtils.getInstance().getVitFromItem(i.getChestplate());
			}
			if(i.getLeggings() != null) {
				vital += HealthUtils.getInstance().getVitFromItem(i.getLeggings());
			}
			if(i.getBoots() != null) {
				vital += HealthUtils.getInstance().getVitFromItem(i.getBoots());
			}
			if(i.getItemInHand() != null) {
				vital += HealthUtils.getInstance().getVitFromItem(i.getItemInHand());
			}
			if (vital > 0.0D) {
				double divide = vital / 7500.0D;
		        double pre = dmg * divide;
		        int cleaned = (int)(dmg + pre);
		        return cleaned;
			}
		}
		return dmg;
	}
	
	public double getDMGWithSTR(Player p, double dmg) {
		PlayerInventory i = p.getInventory();
		if((p.getItemInHand() != null) && (p.getItemInHand().getType().name().contains("AXE")) && (p.getItemInHand().getType().name().contains("SPADE") && (p.getItemInHand().getItemMeta().hasLore()))) {
			double vital = 0.0D;
			if(i.getHelmet() != null) {
				vital += HealthUtils.getInstance().getStrFromItem(i.getHelmet());
			}
			if(i.getChestplate() != null) {
				vital += HealthUtils.getInstance().getStrFromItem(i.getChestplate());
			}
			if(i.getLeggings() != null) {
				vital += HealthUtils.getInstance().getStrFromItem(i.getLeggings());
			}
			if(i.getBoots() != null) {
				vital += HealthUtils.getInstance().getStrFromItem(i.getBoots());
			}
			if(i.getItemInHand() != null) {
				vital += HealthUtils.getInstance().getStrFromItem(i.getItemInHand());
			}
			if (vital > 0.0D) {
				double divide = vital / 7500.0D;
		        double pre = dmg * divide;
		        int cleaned = (int)(dmg + pre);
		        return cleaned;
			}
		}
		return dmg;
	}
	
	public double getDMGWithDEX(Player p, double dmg) {
		PlayerInventory i = p.getInventory();
		if((p.getItemInHand() != null) && (p.getItemInHand().getType().name().contains("BOW")) && (p.getItemInHand().getItemMeta().hasLore())) {
			double vital = 0.0D;
			if(i.getHelmet() != null) {
				vital += HealthUtils.getInstance().getDexFromItem(i.getHelmet());
			}
			if(i.getChestplate() != null) {
				vital += HealthUtils.getInstance().getDexFromItem(i.getChestplate());
			}
			if(i.getLeggings() != null) {
				vital += HealthUtils.getInstance().getDexFromItem(i.getLeggings());
			}
			if(i.getBoots() != null) {
				vital += HealthUtils.getInstance().getDexFromItem(i.getBoots());
			}
			if(i.getItemInHand() != null) {
				vital += HealthUtils.getInstance().getDexFromItem(i.getItemInHand());
			}
			if (vital > 0.0D) {
				double divide = vital / 7500.0D;
		        double pre = dmg * divide;
		        int cleaned = (int)(dmg + pre);
		        return cleaned;
			}
		}
		return dmg;
	}
	
	public double getDMGWithINT(Player p, double dmg) {
		PlayerInventory i = p.getInventory();
		if((p.getItemInHand() != null) && (p.getItemInHand().getType().name().contains("HOE")) && (p.getItemInHand().getItemMeta().hasLore())) {
			double vital = 0.0D;
			if(i.getHelmet() != null) {
				vital += HealthUtils.getInstance().getIntFromItem(i.getHelmet());
			}
			if(i.getChestplate() != null) {
				vital += HealthUtils.getInstance().getIntFromItem(i.getChestplate());
			}
			if(i.getLeggings() != null) {
				vital += HealthUtils.getInstance().getIntFromItem(i.getLeggings());
			}
			if(i.getBoots() != null) {
				vital += HealthUtils.getInstance().getIntFromItem(i.getBoots());
			}
			if(i.getItemInHand() != null) {
				vital += HealthUtils.getInstance().getIntFromItem(i.getItemInHand());
			}
			if (vital > 0.0D) {
				double divide = vital / 7500.0D;
		        double pre = dmg * divide;
		        int cleaned = (int)(dmg + pre);
		        return cleaned;
			}
		}
		return dmg;
	}
	
	public boolean isAxe(Player p) {
		if(p.getItemInHand().getType().toString().contains("AXE"))return true;
		return false;
	}
	
	public static boolean isSword(ItemStack i) {
		return i.getType().toString().contains("SWORD");
	}
	
	public static boolean isAxe(ItemStack i) {
		return i.getType().toString().contains("AXE");
	}
	
	public static boolean isStaff(ItemStack i) {
		return i.getType().toString().contains("HOE");
	}
	
	public static boolean isPolearm(ItemStack i) {
		return i.getType().toString().contains("SPADE");
	}
	
	public static boolean isBow(ItemStack i) {
		return i.getType().toString().contains("BOW");
	}
	
	public static boolean isBoots(ItemStack i) {
		return i.getType().toString().contains("BOOTS");
	}
	
	public static boolean isHelmet(ItemStack i) {
		return i.getType().toString().contains("HELMET");
	}
	
	public static boolean isLeggings(ItemStack i) {
		return i.getType().toString().contains("LEGGINGS");
	}
	
	public static boolean isChestplate(ItemStack i) {
		return i.getType().toString().contains("CHESTPLATE");
	}
	
	public int decideDrop(double chance)
	  {
	    int rand = Utils.ir(0, 10000);
	    if (rand <= chance * 100.0D)
	    {
	      int decide = Utils.ir(1, 100);
	      if (decide >= 41) {
	        return Utils.ir(1, 4);
	      }
	      if (decide <= 40) {
	        return Utils.ir(10, 11);
	      }
	    }
	    return 0;
	  }

	public double getAccuracy(ItemStack item) {
		return HealthUtils.getInstance().getValueFromLore(item, "ACCURACY", "%", "");
		
	}
	
	public double getPVP(ItemStack item) {
		return HealthUtils.getInstance().getValueFromLore(item, "DMG vs. Players", "%", "");
	}
	
	public double getMVM(ItemStack item) {
		return HealthUtils.getInstance().getValueFromLore(item, "DMG vs. Monsters", "%", "");
	}
	
	public double getBlind(ItemStack item) {
		return HealthUtils.getInstance().getValueFromLore(item, "BLIND", "%", "");
	}
	
	public double getKnockBack(ItemStack item) {
		return HealthUtils.getInstance().getValueFromLore(item, "KNOCKBACK", "%", "");
	}
	
	public double getArmorPen(ItemStack item) {
		return HealthUtils.getInstance().getValueFromLore(item, "ARMOR PENETRATION", "%", "");
	}
	
	public double getArmorPenWithDex(ItemStack item, Player p) {
		return (HealthUtils.getInstance().getDEX(p) * 0.009D);
	}
	
	public double getFinalArmorPen(ItemStack item, Player p) {
		return (getArmorPenWithDex(item, p) * getArmorPen(item));
	}
	
	

	
	
	public boolean hasStat(ItemStack item, String stat, Player p) {
		List<?> lore = p.getItemInHand().getItemMeta().getLore();
        for (int i1 = 0; i1 < lore.size(); i1++)
        {
          if (((String)lore.get(i1)).contains(stat)) {
        	  return true;
          }
        }
		return false;
	}
	
	public boolean hasStat1(ItemStack item, String stat) {
		List<?> lore = item.getItemMeta().getLore();
        for (int i1 = 0; i1 < lore.size(); i1++)
        {
          if (((String)lore.get(i1)).contains(stat)) {
        	  return true;
          }
        }
		return false;
	}
	
}

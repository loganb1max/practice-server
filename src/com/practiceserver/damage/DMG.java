package com.practiceserver.damage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.WitherSkull;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.practiceserver.Main;
import com.practiceserver.mob1.Mob;
import com.practiceserver.player.PracticeServerPlayer;
import com.practiceserver.utils.RegionUtils;
import com.practiceserver.utils.Utils;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;

/*
Copyright � matt11matthew 2016
*/

public class DMG implements Listener {

	WorldGuardPlugin wg = (WorldGuardPlugin)Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");
	
	List<LivingEntity> nodamage = new ArrayList();
	List<String> combat = new ArrayList();
	//
	@EventHandler(priority=EventPriority.HIGH)
	public void onReachDMG(EntityDamageByEntityEvent e)
	  {
	    if (((e.getEntity() instanceof Player)) && ((e.getDamager() instanceof Player)) && (e.getDamager().getType().equals(WitherSkull.class))) {
	      if (e.getEntity().getLocation().distance(e.getDamager().getLocation()) > 4.0D)
	      {
	        e.setCancelled(true);
	       // e.setDamage(0.0D);
	      }
	    }
	  }

	@EventHandler(priority=EventPriority.LOWEST)
	public void onNo3Damager(EntityDamageByEntityEvent e)
	  {
	    if (((e.getEntity() instanceof LivingEntity)) && 
	      ((e.getDamager() instanceof LivingEntity)))
	    {
	      LivingEntity p = (LivingEntity)e.getDamager();
	      ApplicableRegionSet set = wg.getRegionManager(p.getWorld())
	        .getApplicableRegions(p.getLocation());
	      if (!set.allows(DefaultFlag.PVP))
	      {
	        e.setCancelled(true);
	    	  }
	      }
	    }
	  
	  
	@EventHandler(priority=EventPriority.LOWEST)
	public void onNoDamagerSt4affs(EntityDamageByEntityEvent e)
	  {
	    if (((e.getEntity() instanceof LivingEntity)) && 
	      ((e.getDamager() instanceof Projectile)))
	    {
	      Projectile p = (Projectile)e.getDamager();
	      ApplicableRegionSet set = wg.getRegionManager(p.getWorld())
	        .getApplicableRegions(p.getLocation());
	      if (!set.allows(DefaultFlag.PVP))
	      {
	        e.setCancelled(true);
	    	  }
	      
	    }
	  }




	@EventHandler
	  public void onCombatEnter(PlayerMoveEvent e) {
	    Player p = e.getPlayer();
	    if (combat.contains(p.getName())) {
	      int fromX = (int)e.getFrom().getX();
	      int fromY = (int)e.getFrom().getY();
	      int fromZ = (int)e.getFrom().getZ();
	      int toX = (int)e.getTo().getX();
	      int toY = (int)e.getTo().getY();
	      int toZ = (int)e.getTo().getZ();
	      if (((fromX != toX) || (fromZ != toZ) || (fromY != toY)) && 
	        (wg != null) && 
	        (wg.getRegionManager(p.getWorld()) != null) && 
	        (wg.getRegionManager(p.getWorld())
	        .getApplicableRegions(p.getLocation()) != null))
	      {
	        ApplicableRegionSet set = wg.getRegionManager(
	          e.getTo().getWorld()).getApplicableRegions(
	          e.getTo());
	        if (!set.allows(DefaultFlag.PVP))
	        {
	          Vector unitVector = p.getLocation().toVector()
	            .subtract(e.getTo().toVector()).normalize();
	          p.setVelocity(unitVector.multiply(0.5D).setY(0));
	        }
	      }
	      
	    }
	  }
	
	  @EventHandler(priority=EventPriority.LOWEST)
	  public void onNoAutoclick(EntityDamageByEntityEvent e)
	  {
	    if (((e.getEntity() instanceof LivingEntity)) && 
	      ((e.getDamager() instanceof Player)))
	    {
	      final LivingEntity p = (LivingEntity)e.getEntity();
	      Player pp = (Player) e.getDamager();
	      if(pp.getItemInHand().getType().toString().equalsIgnoreCase("SPADE")) {
	    	  return;
	      }
	      if (nodamage.contains(p)) {
	        e.setDamage(0.0D);
	      }
	      e.setCancelled(true);
	      nodamage.add(p);
	      new BukkitRunnable()
	      {
	        public void run()
	        {
	          nodamage.remove(p);
	        }
	      }.runTaskLater(Main.plugin, 2L);
	    }
	  }
	 
	 //@EventHandler(priority=EventPriority.LOWEST)
	  public void onNoEnergyDamage(EntityDamageByEntityEvent e)
	  {
	    if (((e.getEntity() instanceof LivingEntity)) && 
	      ((e.getDamager() instanceof Player)))
	    {
	      Player d = (Player)e.getDamager();
	      if (d.getExp() <= 0.0F)
	      {
	        e.setDamage(0.0D);
	        e.setCancelled(true);
	      }
	    }
	  }

	 @EventHandler(priority=EventPriority.HIGHEST)
	  public void onNoDamager(EntityDamageEvent e)
	  {
	    if (e.getEntity().hasMetadata("NPC"))
	    {
	      e.setDamage(0.0D);
	      e.setCancelled(true);
	    }
	  }

	 //@EventHandler
	  public void onMapOpen(PlayerInteractEvent e)
	  {
	    Player p = e.getPlayer();
	    if (((e.getAction() == Action.RIGHT_CLICK_AIR) || (e.getAction() == Action.RIGHT_CLICK_BLOCK)) && 
	      (p.getItemInHand().getType() == Material.EMPTY_MAP))
	    {
	      e.setCancelled(true);
	      p.updateInventory();
	    }
	  }
	  
	  @EventHandler
	  public void onGemPickup(PlayerPickupItemEvent e)
	  {
	    Player p = e.getPlayer();
	    if ((e.getItem().getItemStack().getType() == Material.EMERALD) && 
	      (e.getItem().getItemStack().getItemMeta().hasLore()))
	    {
	      p.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + 
	        "            +" + ChatColor.GREEN + 
	        e.getItem().getItemStack().getAmount() + ChatColor.GREEN + 
	        ChatColor.BOLD + "G");
	      p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);
	    }
	  }
	  
	  @EventHandler
	  public void onDamagePercent(EntityDamageEvent e)
	  {
	    if ((e.getEntity() instanceof Player))
	    {
	      if (e.getDamage() <= 0.0D) {
	        return;
	      }
	      Player p = (Player)e.getEntity();
	      if ((e.getCause().equals(EntityDamageEvent.DamageCause.POISON)) || 
	      
	        (e.getCause().equals(EntityDamageEvent.DamageCause.FIRE_TICK)) || 
	        (e.getCause().equals(EntityDamageEvent.DamageCause.FIRE)) || 
	        (e.getCause().equals(EntityDamageEvent.DamageCause.LAVA)))
	      {
	        if (p.getMaxHealth() / 80.0D < 1.0D) {
	          e.setDamage(1.0D);
	        } else {
	          e.setDamage(p.getMaxHealth() / 80.0D);
	        }
	      }
	      else if (e.getCause().equals(EntityDamageEvent.DamageCause.FALL)) {
	        if (e.getDamage() * p.getMaxHealth() / 80.0D >= p.getHealth()) {
	          e.setDamage(p.getHealth() - 1.0D);
	        } else if (e.getDamage() * p.getMaxHealth() / 80.0D < 1.0D) {
	          e.setDamage(1.0D);
	        } else {
	          e.setDamage(e.getDamage() * p.getMaxHealth() / 80.0D);
	        }
	      }
	    }
	  }
	  
	@EventHandler
	  public void onEntityDamageTick(EntityDamageByEntityEvent e)
	  {
	    if (((e.getEntity() instanceof LivingEntity)) && 
	      ((e.getDamager() instanceof LivingEntity)) && 
	      (!(e.getDamager() instanceof Player)))
	    {
	      final LivingEntity p = (LivingEntity)e.getEntity();
	      final LivingEntity d = (LivingEntity)e.getDamager();
	      if (e.getDamage() <= 0.0D) {
	        return;
	      }
	      if (p.getNoDamageTicks() < p.getMaximumNoDamageTicks() / 2)
	      {
	        Random random = new Random();
	        final int kb = random.nextInt(2) + 1;
	        new BukkitRunnable()
	        {
	          public void run()
	          {
	            if (kb == 1) {
	              p.setVelocity(new Vector(
	                d.getLocation().getDirection().getX() * 0.25D, 
	                0.175D, 
	                d.getLocation().getDirection().getZ() * 0.25D));
	            } else {
	              p.setVelocity(new Vector(d.getLocation()
	                .getDirection().getX() * 0.2D, 0.0D, d
	                .getLocation().getDirection().getZ() * 0.2D));
	            }
	          }
	        }.runTaskLater(Main.plugin, 1L);
	      }
	    }
	  }
	  
	  @EventHandler
	  public void onArrowKnock(EntityDamageByEntityEvent e)
	  {
	    if (((e.getEntity() instanceof LivingEntity)) && 
	      ((e.getDamager() instanceof Arrow)) && 
	      (!(e.getDamager() instanceof Player)))
	    {
	      LivingEntity p = (LivingEntity)e.getEntity();
	      Arrow d = (Arrow)e.getDamager();
	      if (e.getDamage() <= 0.0D) {
	        return;
	      }
	      d.remove();
	      if (p.getNoDamageTicks() < p.getMaximumNoDamageTicks() / 2)
	      {
	        Random random = new Random();
	        int kb = random.nextInt(2) + 1;
	        if (kb == 1) {
	          p.setVelocity(new Vector(d.getLocation().getDirection()
	            .getX() * 0.25D, 0.175D, d.getLocation()
	            .getDirection().getZ() * 0.25D));
	        } else {
	          p.setVelocity(new Vector(d.getLocation().getDirection()
	            .getX() * 0.2D, 0.0D, d.getLocation()
	            .getDirection().getZ() * 0.2D));
	        }
	      }
	    }
	  }
	  
	  @EventHandler
	  public void onEntityDamageKnock(EntityDamageByEntityEvent e) {
	    if (((e.getEntity() instanceof LivingEntity)) &&  ((e.getDamager() instanceof Player))) {
	      final Player d = (Player)e.getDamager();
	      final LivingEntity p = (LivingEntity)e.getEntity();
	      ApplicableRegionSet set = wg.getRegionManager(p.getWorld()).getApplicableRegions(p.getLocation());
	      if (RegionUtils.isSafeZone(p.getLocation())) {
	    	  e.setCancelled(true);
	    	  return;
	    	  
	    
	      }
	      if(p instanceof Player) {
	    	  if (RegionUtils.isWildernessZone(p.getLocation())) {
	    		  e.setCancelled(true);
		    	  return;
	    	  }
	      }
	      if(set.allows(DefaultFlag.PVP)) {
		      PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(d);
	    	  if(pl.isTogglePvP()) {
	    		  if(p instanceof Player) {
	    			  e.setCancelled(true);
		    		  return;
	    		  }
	    	  }
	      }

	      if (e.getDamage() <= 0.0D) {
	        return;
	      }

	      p.setNoDamageTicks(0);
	      Random random = new Random();
	      final int kb = random.nextInt(2) + 1;
	      new BukkitRunnable() {
	        public void run() {
	          if (kb == 1) {
	            p.setVelocity(new Vector(d.getLocation().getDirection().getX() * 0.25D, 0.175D, d.getLocation() .getDirection().getZ() * 0.25D));
	          } else {
	            p.setVelocity(new Vector(d.getLocation().getDirection().getX() * 0.2D, 0.0D, d.getLocation() .getDirection().getZ() * 0.2D));
	          }
	        }
	      }.runTaskLater(Main.plugin, 1L);
	    }
	  }
	  
	  @EventHandler
	  public void onEntityDamageKnock1(EntityDamageByEntityEvent e) {
	    if (((e.getEntity() instanceof LivingEntity)) &&  ((e.getDamager() instanceof Arrow))) {
	      final Arrow a = (Arrow) e.getDamager();
	      if(!(a.getShooter() instanceof Player))return;
	      final Player d = (Player) a.getShooter();
	      PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(d);
	      final LivingEntity p = (LivingEntity)e.getEntity();
	      ApplicableRegionSet set = wg.getRegionManager(p.getWorld()).getApplicableRegions(p.getLocation());
	      if (RegionUtils.isSafeZone(p.getLocation())) {
	    	  e.setCancelled(true);
	    	  return;
	    	  
	    
	      }
	      if(p instanceof Player) {
	    	  if (RegionUtils.isWildernessZone(p.getLocation())) {
	    		  e.setCancelled(true);
		    	  return;
	    	  }
	      }
	      if(set.allows(DefaultFlag.PVP)) {
	    	  if(pl.isTogglePvP()) {
	    		  if(p instanceof Player) {
	    			  e.setCancelled(true);
		    		  return;
	    		  }
	    	  }
	      }

	      if (e.getDamage() <= 0.0D) {
	        return;
	      }
	     p.setNoDamageTicks(0);
	      Random random = new Random();
	      final int kb = random.nextInt(2) + 1;
	      new BukkitRunnable() {
	        public void run() {
	          if (kb == 1) {
	            p.setVelocity(new Vector(d.getLocation().getDirection().getX() * 0.25D, 0.175D, d.getLocation() .getDirection().getZ() * 0.25D));
	          } else {
	            p.setVelocity(new Vector(d.getLocation().getDirection().getX() * 0.2D, 0.0D, d.getLocation() .getDirection().getZ() * 0.2D));
	          }
	        }
	      }.runTaskLater(Main.plugin, 1L);
	    }
	  }
	  

	  
	  public static int getMinValueFromLore(ItemStack item, String value)
	  {
	    int returnVal = 1;
	    ItemMeta meta = item.getItemMeta();
	    try
	    {
	      List<?> lore = meta.getLore();
	      if ((lore != null) && (((String)lore.get(0)).contains(value)))
	      {
	        String vals = ((String)lore.get(0)).split(": ")[1];
	        vals = ChatColor.stripColor(vals);
	        vals = vals.split(" - ")[0];
	        returnVal = Integer.parseInt(vals.trim());
	      }
	    }
	    catch (Exception localException) {}
	    return returnVal;
	  }
	  
	  public static int getMaxValueFromLore(ItemStack item, String value)
	  {
	    int returnVal = 1;
	    ItemMeta meta = item.getItemMeta();
	    try
	    {
	      List<?> lore = meta.getLore();
	      if ((lore != null) && (((String)lore.get(0)).contains(value)))
	      {
	        String vals = ((String)lore.get(0)).split(": ")[1];
	        vals = ChatColor.stripColor(vals);
	        vals = vals.split(" - ")[1];
	        returnVal = Integer.parseInt(vals.trim());
	      }
	    }
	    catch (Exception localException) {}
	    return returnVal;
	  }


	  public static int getLifestealFromLore(ItemStack item, String value)
	  {
	    int returnVal = 0;
	    ItemMeta meta = item.getItemMeta();
	    try
	    {
	      List<?> lore = meta.getLore();
	      if (lore != null) {
	        for (int i = 0; i < lore.size(); i++) {
	          if (((String)lore.get(i)).contains(value))
	          {
	            String vals = ((String)lore.get(i)).split(": ")[1];
	            vals = ChatColor.stripColor(vals);
	            vals = vals.replace("%", "").trim().toString();
	            returnVal = Integer.parseInt(vals.trim());
	          }
	        }
	      }
	    }
	    catch (Exception localException) {}
	    return returnVal;
	  }
	  
	  public static int getElemFromLore(ItemStack item, String value)
	  {
	    int returnVal = 0;
	    ItemMeta meta = item.getItemMeta();
	    try
	    {
	      List<?> lore = meta.getLore();
	      if (lore != null) {
	        for (int i = 0; i < lore.size(); i++) {
	          if (((String)lore.get(i)).contains(value))
	          {
	            String vals = ((String)lore.get(i)).split(": +")[1];
	            vals = ChatColor.stripColor(vals);
	            returnVal = Integer.parseInt(vals.trim());
	          }
	        }
	      }
	    }
	    catch (Exception localException) {}
	    return returnVal;
	  }

	  //@EventHandler(priority=EventPriority.LOWEST)
	  public void onPlayerDamage(EntityDamageByEntityEvent e)
	  {
	    if (((e.getDamager() instanceof Player)) && 
	      ((e.getEntity() instanceof LivingEntity)))
	    {
	      Player p = (Player)e.getDamager();
	      if (e.getDamage() <= 0.0D) {
	        return;
	      }
	      if ((p.getItemInHand() != null) && 
	        (p.getItemInHand().getType() != Material.AIR) && 
	        (p.getItemInHand().getItemMeta().hasLore()))
	      {
	        int damageMin = getMinValueFromLore(p.getItemInHand(), "DMG");
	        int damageMax = getMaxValueFromLore(p.getItemInHand(), "DMG");
	        Random random = new Random();
	        double dmg = random.nextInt(damageMax - damageMin + 1) + damageMin;
	        LivingEntity l = (LivingEntity) e.getEntity();
	        //dmg = getFinalDMG(p, dmg, l);
			if(dmg > l.getHealth()) {
				  p.sendMessage(ChatColor.RED + "            " + (int)dmg + ChatColor.RED + 
			    	        ChatColor.BOLD + " DMG " + ChatColor.RED + "-> " + 
			    	        ChatColor.RESET + l.getCustomName() + " [0HP]");
				  l.setHealth(0);
				  l.damage(1);
				  List<String> mobs = Mob.getInstance().getMobsThatAreCustom();
					for(String mob : mobs) {
						if(Mob.getInstance().hasCustomDrops(mob)){
							List<String> drops = Mob.getInstance().getDrops(mob);
							int rand = Utils.ir(0, drops.size());
							String drop = drops.get(rand);
						    if(Mob.getInstance().drop(drop)) {
						    	p.getWorld().dropItemNaturally(l.getLocation(), (Mob.getInstance().getCustomDrop(Mob.getInstance().getDropString(drop))));
						    }
						}
					}
					return;
			
			}
			 p.sendMessage(ChatColor.RED + "            " + (int)dmg + ChatColor.RED + 
		    	        ChatColor.BOLD + " DMG " + ChatColor.RED + "-> " + 
		    	        ChatColor.RESET + l.getCustomName() + " [" + (int)(l.getHealth() - dmg) + 
		    	        "HP]");
			 
			e.setDamage(dmg);
	        l.setNoDamageTicks(0);
	        return;
	      }
	
	    }
	  }
		
	  
	  //@EventHandler
	  public void onWeaponStats(EntityDamageByEntityEvent e)
	  {
	    if (((e.getDamager() instanceof Player)) && 
	      ((e.getEntity() instanceof LivingEntity)))
	    {
	      double dmg = e.getDamage();
	      Player p = (Player)e.getDamager();
	      LivingEntity li = (LivingEntity)e.getEntity();
	      if (e.getDamage() <= 0.0D) {
	        return;
	      }
	      if ((p.getItemInHand() != null) && 
	        (p.getItemInHand().getType() != Material.AIR) && 
	        (p.getItemInHand().getItemMeta().hasLore()))
	      {
	        List<?> lore = p.getItemInHand().getItemMeta().getLore();
	        for (int i = 0; i < lore.size(); i++)
	        {
	          if (((String)lore.get(i)).contains("ICE DMG"))
	          {
	            li.getWorld().playEffect(li.getEyeLocation(), 
	              Effect.POTION_BREAK, 8194);
	            li.addPotionEffect(new PotionEffect(
	              PotionEffectType.SLOW, 20, 0));
	            double eldmg = getElemFromLore(p.getItemInHand(), 
	              "ICE DMG");
	            dmg += eldmg;
	          }
	          if (((String)lore.get(i)).contains("POISON DMG"))
	          {
	            li.getWorld().playEffect(li.getEyeLocation(), 
	              Effect.POTION_BREAK, 8196);
	            li.addPotionEffect(new PotionEffect(
	              PotionEffectType.POISON, 10, 1));
	            double eldmg = getElemFromLore(p.getItemInHand(), 
	              "POISON DMG");
	            dmg += eldmg;
	          }
	          if (((String)lore.get(i)).contains("FIRE DMG"))
	          {
	            li.setFireTicks(40);
	            double eldmg = getElemFromLore(p.getItemInHand(), 
	              "FIRE DMG");
	            dmg += eldmg;
	          }
	          if (((String)lore.get(i)).contains("PURE DMG"))
	          {
	            double eldmg = getElemFromLore(p.getItemInHand(), 
	              "PURE DMG");
	            dmg += eldmg;
	          }
	          if (((String)lore.get(i)).contains("CRITICAL HIT"))
	          {
	            int crit = getLifestealFromLore(p.getItemInHand(), 
	              "CRITICAL HIT");
	            Random random = new Random();
	            int drop = random.nextInt(25) + 1;
	            if (drop <= crit) {
	              dmg *= 2.0D;
	            }
	          }
	          if (((String)lore.get(i)).contains("LIFE STEAL"))
	          {
	            li.getWorld().playEffect(li.getEyeLocation(), 
	              Effect.STEP_SOUND, Material.REDSTONE_BLOCK);
	            double base = getLifestealFromLore(p.getItemInHand(), 
	              "LIFE STEAL");
	            double pcnt = base / 100.0D;
	            int life = 0;
	            if ((int)(pcnt * dmg) > 0) {
	              life = (int)(pcnt * dmg);
	            } else {
	              life = 1;
	            }
	            p.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + 
	              "            +" + ChatColor.GREEN + life + 
	              ChatColor.GREEN + ChatColor.BOLD + " HP " + 
	              ChatColor.GRAY + "[" + (int)p.getHealth() + 
	              "/" + (int)p.getMaxHealth() + "HP]");
	            if (p.getHealth() < p.getMaxHealth() - life) {
	              p.setHealth(p.getHealth() + life);
	            } else if (p.getHealth() >= p.getMaxHealth() - life) {
	              p.setHealth(p.getMaxHealth());
	            }
	          }
	        }
	      }
	      e.setDamage(dmg);
	    }
	  }
	  
	  
	  
	//  @EventHandler(priority=EventPriority.HIGH)
	  public void onDeathMessageElem(EntityDamageEvent e)
	  {
	    if ((e.getEntity() instanceof Player))
	    {
	      Player p = (Player)e.getEntity();
	      if ((e.getDamage() >= p.getHealth()) && 
	        (p.getNoDamageTicks() <= p.getMaximumNoDamageTicks() / 2) && 
	        (p.getHealth() > 0.0D)) {
	        if ((e.getCause().equals(EntityDamageEvent.DamageCause.LAVA)) || 
	        
	          (e.getCause().equals(EntityDamageEvent.DamageCause.FIRE)) || 
	          
	          (e.getCause().equals(EntityDamageEvent.DamageCause.FIRE_TICK))) {
	          Bukkit.getServer().broadcastMessage(
	            p.getDisplayName() + ChatColor.WHITE + 
	            " burned to death");
	        }
	      }
	    }
	  }
	  
	  @EventHandler
	  public void onDeath(PlayerDeathEvent e) {
		  e.setDeathMessage(null);
	  }
	  
	// @EventHandler(priority=EventPriority.HIGH)
	  public void onDeathMessage(EntityDamageByEntityEvent e)
	  {
	    if (((e.getEntity() instanceof Player)) && 
	      ((e.getDamager() instanceof LivingEntity)))
	    {
	      Player p = (Player)e.getEntity();
	      if ((e.getDamage() >= p.getHealth()) && 
	        (p.getNoDamageTicks() <= p.getMaximumNoDamageTicks() / 2) && 
	        (p.getHealth() > 0.0D)) {
	        if ((e.getDamager() instanceof Player))
	        {
	          Player d = (Player)e.getDamager();
	          if ((d.getItemInHand() != null) && 
	            (d.getItemInHand().getType() != Material.AIR)) {
	            Bukkit.getServer().broadcastMessage(
	              p.getDisplayName() + 
	              ChatColor.WHITE + 
	              " was killed by " + 
	              ChatColor.RESET + 
	              d.getDisplayName() + 
	              ChatColor.WHITE + 
	              " with a(n) " + 
	              d.getItemInHand().getItemMeta()
	              .getDisplayName());
	          } else {
	            Bukkit.getServer().broadcastMessage(
	              p.getDisplayName() + ChatColor.WHITE + 
	              " was killed by " + ChatColor.RESET + 
	              d.getDisplayName() + ChatColor.WHITE + 
	              " with a(n) Air");
	          }
	        }
	        else if ((e.getDamager() instanceof LivingEntity))
	        {
	          LivingEntity l = (LivingEntity)e.getDamager();
	          if (l.getCustomName() != null) {
	            Bukkit.getServer().broadcastMessage(
	              p.getDisplayName() + ChatColor.WHITE + 
	              " was killed by a(n) " + 
	              ChatColor.RESET + l.getCustomName());
	          }
	        }
	      }
	    }
	  }
	  
	  @EventHandler
	  public void onDamageSound(EntityDamageByEntityEvent e)
	  {
	    if (((e.getDamager() instanceof Player)) && 
	      ((e.getEntity() instanceof LivingEntity)))
	    {
	      Player d = (Player)e.getDamager();
	      if (e.getDamage() <= 0.0D) {
	        return;
	      }
	      d.playSound(d.getLocation(), Sound.ENTITY_GENERIC_HURT, 1.0F, 1.0F);
	    }
	  }
	  
	  //@EventHandler
	  public void onArmorDura(EntityDamageEvent e)
	  {
	    if ((e.getEntity() instanceof Player))
	    {
	      Player p = (Player)e.getEntity();
	      final PlayerInventory i = p.getInventory();
	      if (i.getHelmet() != null) {
	        i.getHelmet().setDurability((short)0);
	      }
	      if (i.getChestplate() != null) {
	        i.getChestplate().setDurability((short)0);
	      }
	      if (i.getLeggings() != null) {
	        i.getLeggings().setDurability((short)0);
	      }
	      if (i.getBoots() != null) {
	        i.getBoots().setDurability((short)0);
	      }
	      new BukkitRunnable()
	      {
	        public void run()
	        {
	          if (i.getHelmet() != null) {
	            i.getHelmet().setDurability((short)0);
	          }
	          if (i.getChestplate() != null) {
	            i.getChestplate().setDurability((short)0);
	          }
	          if (i.getLeggings() != null) {
	            i.getLeggings().setDurability((short)0);
	          }
	          if (i.getBoots() != null) {
	            i.getBoots().setDurability((short)0);
	          }
	        }
	      }.runTaskLater(Main.plugin, 1L);
	    }
	  }
	  
	  //@EventHandler
	  public void onEntityDamage(EntityDamageByEntityEvent e)
	  {
	    if (((e.getDamager() instanceof Player)) && 
	      ((e.getEntity() instanceof LivingEntity)))
	    {
	      Player p = (Player)e.getDamager();
	      if ((p.getItemInHand().getType() == Material.WOOD_SWORD) || 
	        (p.getItemInHand().getType() == Material.WOOD_AXE) || 
	        (p.getItemInHand().getType() == Material.STONE_SWORD) || 
	        (p.getItemInHand().getType() == Material.STONE_AXE) || 
	        (p.getItemInHand().getType() == Material.IRON_SWORD) || 
	        (p.getItemInHand().getType() == Material.IRON_AXE) || 
	        (p.getItemInHand().getType() == Material.DIAMOND_SWORD) || 
	        (p.getItemInHand().getType() == Material.DIAMOND_AXE) || 
	        (p.getItemInHand().getType() == Material.GOLD_SWORD) || 
	        (p.getItemInHand().getType() == Material.GOLD_AXE) || 
	        (p.getItemInHand().getType() == Material.GOLD_PICKAXE))
	      {
	        p.getInventory().getItemInHand().setDurability((short)0);
	        p.updateInventory();
	      }
	    }
	  }
	  
	  public static int getValueFromLore(ItemStack item, String value)
	  {
	    int returnVal = 0;
	    ItemMeta meta = item.getItemMeta();
	    try
	    {
	      List<?> lore = meta.getLore();
	      if ((lore != null) && (((String)lore.get(1)).contains(value)))
	      {
	        String vals = ((String)lore.get(1)).split(": +")[1];
	        vals = ChatColor.stripColor(vals);
	        returnVal = Integer.parseInt(vals.trim());
	      }
	    }
	    catch (Exception localException) {}
	    return returnVal;
	  }
	  
	  public static int getVitFromLore(ItemStack item, String value)
	  {
	    int returnVal = 0;
	    ItemMeta meta = item.getItemMeta();
	    try
	    {
	      List<?> lore = meta.getLore();
	      if ((lore != null) && (((String)lore.get(2)).contains(value)))
	      {
	        String vals = ((String)lore.get(2)).split(": +")[1];
	        vals = ChatColor.stripColor(vals);
	        returnVal = Integer.parseInt(vals.trim());
	      }
	    }
	    catch (Exception localException) {}
	    return returnVal;
	  }


	  //@EventHandler(priority=EventPriority.NORMAL)
	  public void onVitSword(EntityDamageByEntityEvent e)
	  {
	    if (((e.getDamager() instanceof Player)) && 
	      ((e.getEntity() instanceof LivingEntity)))
	    {
	      if (e.getDamage() <= 0.0D) {
	        return;
	      }
	      Player p = (Player)e.getDamager();
	      PlayerInventory i = p.getInventory();
	      if ((p.getItemInHand() != null) && 
	        (p.getItemInHand().getType().name().contains("SWORD")) && 
	        (p.getItemInHand().getItemMeta().hasLore()))
	      {
	        double dmg = e.getDamage();
	        double vital = 0.0D;
	        if ((i.getHelmet() != null) && 
	          (i.getHelmet().getItemMeta().hasLore()))
	        {
	          int vit = getVitFromLore(i.getHelmet(), "VIT");
	          vital += vit;
	        }
	        if ((i.getChestplate() != null) && 
	          (i.getChestplate().getItemMeta().hasLore()))
	        {
	          int vit = getVitFromLore(i.getChestplate(), "VIT");
	          vital += vit;
	        }
	        if ((i.getLeggings() != null) && 
	          (i.getLeggings().getItemMeta().hasLore()))
	        {
	          int vit = getVitFromLore(i.getLeggings(), "VIT");
	          vital += vit;
	        }
	        if ((i.getBoots() != null) && 
	          (i.getBoots().getItemMeta().hasLore()))
	        {
	          int vit = getVitFromLore(i.getBoots(), "VIT");
	          vital += vit;
	        }
	        if (vital > 0.0D)
	        {
	          double divide = vital / 7500.0D;
	          double pre = dmg * divide;
	          int cleaned = (int)(dmg + pre);
	          e.setDamage(cleaned);
	        }
	        else
	        {
	          e.setDamage(dmg);
	        }
	      }
	    }
	  }
	  
	 // @EventHandler(priority=EventPriority.NORMAL)
	  public void onDpsDamage(EntityDamageByEntityEvent e)
	  {
	    if (((e.getDamager() instanceof Player)) && 
	      ((e.getEntity() instanceof LivingEntity)))
	    {
	      if (e.getDamage() <= 0.0D) {
	        return;
	      }
	      Player p = (Player)e.getDamager();
	      PlayerInventory i = p.getInventory();
	      if ((p.getItemInHand() != null) && 
	        (p.getItemInHand().getType() != Material.AIR) && 
	        (p.getItemInHand().getItemMeta().hasLore()))
	      {
	        double dmg = e.getDamage();
	        double dps = 0.0D;
	        if ((i.getHelmet() != null) && 
	          (i.getHelmet().getItemMeta().hasLore()))
	        {
	          int adddps = getMinValueFromLore(i.getHelmet(), "DPS");
	          dps += adddps;
	        }
	        if ((i.getChestplate() != null) && 
	          (i.getChestplate().getItemMeta().hasLore()))
	        {
	          int adddps = getMinValueFromLore(i.getChestplate(), "DPS");
	          dps += adddps;
	        }
	        if ((i.getLeggings() != null) && 
	          (i.getLeggings().getItemMeta().hasLore()))
	        {
	          int adddps = getMinValueFromLore(i.getLeggings(), "DPS");
	          dps += adddps;
	        }
	        if ((i.getBoots() != null) && 
	          (i.getBoots().getItemMeta().hasLore()))
	        {
	          int adddps = getMinValueFromLore(i.getBoots(), "DPS");
	          dps += adddps;
	        }
	        if (dps > 0.0D)
	        {
	          double divide = dps / 100.0D;
	          double pre = dmg * divide;
	          int cleaned = (int)(dmg + pre);
	          e.setDamage(cleaned);
	        }
	        else
	        {
	          e.setDamage(dmg);
	        }
	      }
	    }
	  }
	  
	  public static void kit(Player p)
	  {
	    PlayerInventory i = p.getInventory();
	    Random random = new Random();
	    int min = random.nextInt(2) + 2;
	    int max = random.nextInt(2) + 4;
	    int wep = random.nextInt(2) + 1;
	    if (wep == 1)
	    {
	      ItemStack S = new ItemStack(Material.WOOD_SWORD);
	      ItemMeta smeta = S.getItemMeta();
	      smeta.setDisplayName(ChatColor.WHITE + "Training Sword");
	      List<String> slore = new ArrayList();
	      slore.add(ChatColor.RED + "DMG: " + min + " - " + max);
	      slore.add(ChatColor.GRAY + "Untradeable");
	      smeta.setLore(slore);
	      S.setItemMeta(smeta);
	      i.addItem(new ItemStack[] { S });
	    }
	    if (wep == 2)
	    {
	      ItemStack S = new ItemStack(Material.WOOD_AXE);
	      ItemMeta smeta = S.getItemMeta();
	      smeta.setDisplayName(ChatColor.WHITE + "Training Hatchet");
	      List<String> slore = new ArrayList();
	      slore.add(ChatColor.RED + "DMG: " + min + " - " + max);
	      slore.add(ChatColor.GRAY + "Untradeable");
	      smeta.setLore(slore);
	      S.setItemMeta(smeta);
	      i.addItem(new ItemStack[] { S });
	    }
	    ItemStack bread = new ItemStack(Material.BREAD, 1);
	    ItemMeta smeta = bread.getItemMeta();
	    List<String> slore = new ArrayList();
	    slore.add(ChatColor.GRAY + "Untradeable");
	      smeta.setLore(slore);
	      bread.setItemMeta(smeta);
	      i.setItem(i.firstEmpty(), bread);
	      i.setItem(i.firstEmpty(), bread);
	    p.setMaxHealth(50.0D);
	    p.setHealth(50.0D);
	    p.setHealthScale(20.0D);
	    p.setHealthScaled(true);
	  }

	 /* public void damageMob(LivingEntity li, Player p, double dmg) {
		  PlayerInventory i = p.getInventory();
		  if ((p.getItemInHand() != null) && 
			        (p.getItemInHand().getType().name().contains("SWORD")) && 
			        (p.getItemInHand().getItemMeta().hasLore()))
			      {
			        double vital = 0.0D;
			        if ((i.getHelmet() != null) && 
			          (i.getHelmet().getItemMeta().hasLore()))
			        {
			          int vit = getVitFromLore(i.getHelmet(), "VIT");
			          vital += vit;
			        }
			        if ((i.getChestplate() != null) && 
			          (i.getChestplate().getItemMeta().hasLore()))
			        {
			          int vit = getVitFromLore(i.getChestplate(), "VIT");
			          vital += vit;
			        }
			        if ((i.getLeggings() != null) && 
			          (i.getLeggings().getItemMeta().hasLore()))
			        {
			          int vit = getVitFromLore(i.getLeggings(), "VIT");
			          vital += vit;
			        }
			        if ((i.getBoots() != null) && 
			          (i.getBoots().getItemMeta().hasLore()))
			        {
			          int vit = getVitFromLore(i.getBoots(), "VIT");
			          vital += vit;
			        }
			        if (vital > 0.0D)
			        {
			          double divide = vital / 7500.0D;
			          double pre = dmg * divide;
			          int cleaned = (int)(dmg + pre);
			          dmg += cleaned;
			        }
			        else
			        {
			          dmg = dmg;
			        }
			      }
	  
	      if ((p.getItemInHand() != null) && 
	        (p.getItemInHand().getType() != Material.AIR) && 
	        (p.getItemInHand().getItemMeta().hasLore()))
	      {
	        double dps = 0.0D;
	        if ((i.getHelmet() != null) && 
	          (i.getHelmet().getItemMeta().hasLore()))
	        {
	          int adddps = getMinValueFromLore(i.getHelmet(), "DPS");
	          dps += adddps;
	        }
	        if ((i.getChestplate() != null) && 
	          (i.getChestplate().getItemMeta().hasLore()))
	        {
	          int adddps = getMinValueFromLore(i.getChestplate(), "DPS");
	          dps += adddps;
	        }
	        if ((i.getLeggings() != null) && 
	          (i.getLeggings().getItemMeta().hasLore()))
	        {
	          int adddps = getMinValueFromLore(i.getLeggings(), "DPS");
	          dps += adddps;
	        }
	        if ((i.getBoots() != null) && 
	          (i.getBoots().getItemMeta().hasLore()))
	        {
	          int adddps = getMinValueFromLore(i.getBoots(), "DPS");
	          dps += adddps;
	        }
	        if (dps > 0.0D)
	        {
	          double divide = dps / 100.0D;
	          double pre = dmg * divide;
	          int cleaned = (int)(dmg + pre);
	          dmg += cleaned;
	        }else {
	        	dmg = dmg;
	        }
		  
		  if ((p.getItemInHand() != null) && 
			        (p.getItemInHand().getType() != Material.AIR) && 
			        (p.getItemInHand().getItemMeta().hasLore()))
			      {
			        List<?> lore = p.getItemInHand().getItemMeta().getLore();
			        for (int i1 = 0; i1 < lore.size(); i1++)
			        {
			          if (((String)lore.get(i1)).contains("ICE DMG"))
			          {
			            li.getWorld().playEffect(li.getEyeLocation(), 
			              Effect.POTION_BREAK, 8194);
			            li.addPotionEffect(new PotionEffect(
			              PotionEffectType.SLOW, 20, 0));
			            double eldmg = getElemFromLore(p.getItemInHand(), 
			              "ICE DMG");
			            dmg += eldmg;
			          }
			          if (((String)lore.get(i1)).contains("POISON DMG"))
			          {
			            li.getWorld().playEffect(li.getEyeLocation(), 
			              Effect.POTION_BREAK, 8196);
			            li.addPotionEffect(new PotionEffect(
			              PotionEffectType.POISON, 10, 1));
			            double eldmg = getElemFromLore(p.getItemInHand(), 
			              "POISON DMG");
			            dmg += eldmg;
			          }
			          if (((String)lore.get(i1)).contains("FIRE DMG"))
			          {
			            li.setFireTicks(40);
			            double eldmg = getElemFromLore(p.getItemInHand(), 
			              "FIRE DMG");
			            dmg += eldmg;
			          }
			          if (((String)lore.get(i1)).contains("PURE DMG"))
			          {
			            double eldmg = getElemFromLore(p.getItemInHand(), 
			              "PURE DMG");
			            dmg += eldmg;
			          }
			          if (((String)lore.get(i1)).contains("CRITICAL HIT"))
			          {
			            int crit = getLifestealFromLore(p.getItemInHand(), 
			              "CRITICAL HIT");
			            Random random = new Random();
			            int drop = random.nextInt(25) + 1;
			            if (drop <= crit) {
			              dmg *= 2.0D;
			            }
			          }
			          if (((String)lore.get(i1)).contains("LIFE STEAL"))
			          {
			            li.getWorld().playEffect(li.getEyeLocation(), 
			              Effect.STEP_SOUND, Material.REDSTONE_BLOCK);
			            double base = getLifestealFromLore(p.getItemInHand(), 
			              "LIFE STEAL");
			            double pcnt = base / 100.0D;
			            int life = 0;
			            if ((int)(pcnt * dmg) > 0) {
			              life = (int)(pcnt * dmg);
			            } else {
			              life = 1;
			            }
			            p.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + 
			              "            +" + ChatColor.GREEN + life + 
			              ChatColor.GREEN + ChatColor.BOLD + " HP " + 
			              ChatColor.GRAY + "[" + (int)p.getHealth() + 
			              "/" + (int)p.getMaxHealth() + "HP]");
			            if (p.getHealth() < p.getMaxHealth() - life) {
			              p.setHealth(p.getHealth() + life);
			            } else if (p.getHealth() >= p.getMaxHealth() - life) {
			              p.setHealth(p.getMaxHealth());
			            }
			          }
			        }
			      }
		  dmg = dmg / 2;
		  if(dmg > li.getHealth()) {
			  li.damage(10000000000000000000000.0D);
			  p.sendMessage(ChatColor.RED + "            " + (int)dmg + ChatColor.RED + 
		    	        ChatColor.BOLD + " DMG " + ChatColor.RED + "-> " + 
		    	        ChatColor.RESET + li.getCustomName() + " [0HP]");
		  } else {
			  

		  li.damage(dmg);
		  p.sendMessage(ChatColor.RED + "            " + (int)dmg + ChatColor.RED + 
	    	        ChatColor.BOLD + " DMG " + ChatColor.RED + "-> " + 
	    	        ChatColor.RESET + li.getCustomName() + " [" + (int)li.getHealth() + 
	    	        "HP]");
		  }
	      }
	  
		  
	  }*/
	 /* 
	  public double getFinalDMG(Player p, double dmg, LivingEntity li) {
		  PlayerInventory i = p.getInventory();
		  if ((p.getItemInHand() != null) && 
			        (p.getItemInHand().getType().name().contains("SWORD")) && 
			        (p.getItemInHand().getItemMeta().hasLore()))
			      {
			        double vital = 0.0D;
			        if ((i.getHelmet() != null) && 
			          (i.getHelmet().getItemMeta().hasLore()))
			        {
			          int vit = getVitFromLore(i.getHelmet(), "VIT");
			          vital += vit;
			        }
			        if ((i.getChestplate() != null) && 
			          (i.getChestplate().getItemMeta().hasLore()))
			        {
			          int vit = getVitFromLore(i.getChestplate(), "VIT");
			          vital += vit;
			        }
			        if ((i.getLeggings() != null) && 
			          (i.getLeggings().getItemMeta().hasLore()))
			        {
			          int vit = getVitFromLore(i.getLeggings(), "VIT");
			          vital += vit;
			        }
			        if ((i.getBoots() != null) && 
			          (i.getBoots().getItemMeta().hasLore()))
			        {
			          int vit = getVitFromLore(i.getBoots(), "VIT");
			          vital += vit;
			        }
			        if (vital > 0.0D)
			        {
			          double divide = vital / 7500.0D;
			          double pre = dmg * divide;
			          int cleaned = (int)(dmg + pre);
			          dmg += cleaned;
			        }
			        else
			        {
			          dmg = dmg;
			        }
			      }
	  
	      if ((p.getItemInHand() != null) && 
	        (p.getItemInHand().getType() != Material.AIR) && 
	        (p.getItemInHand().getItemMeta().hasLore()))
	      {
	        double dps = 0.0D;
	        if ((i.getHelmet() != null) && 
	          (i.getHelmet().getItemMeta().hasLore()))
	        {
	          int adddps = getMinValueFromLore(i.getHelmet(), "DPS");
	          dps += adddps;
	        }
	        if ((i.getChestplate() != null) && 
	          (i.getChestplate().getItemMeta().hasLore()))
	        {
	          int adddps = getMinValueFromLore(i.getChestplate(), "DPS");
	          dps += adddps;
	        }
	        if ((i.getLeggings() != null) && 
	          (i.getLeggings().getItemMeta().hasLore()))
	        {
	          int adddps = getMinValueFromLore(i.getLeggings(), "DPS");
	          dps += adddps;
	        }
	        if ((i.getBoots() != null) && 
	          (i.getBoots().getItemMeta().hasLore()))
	        {
	          int adddps = getMinValueFromLore(i.getBoots(), "DPS");
	          dps += adddps;
	        }
	        if (dps > 0.0D)
	        {
	          double divide = dps / 100.0D;
	          double pre = dmg * divide;
	          int cleaned = (int)(dmg + pre);
	          dmg += cleaned;
	        }else {
	        	dmg = dmg;
	        }
		  
		  if ((p.getItemInHand() != null) && 
			        (p.getItemInHand().getType() != Material.AIR) && 
			        (p.getItemInHand().getItemMeta().hasLore()))
			      {
			        List<?> lore = p.getItemInHand().getItemMeta().getLore();
			        for (int i1 = 0; i1 < lore.size(); i1++)
			        {
			          if (((String)lore.get(i1)).contains("ICE DMG"))
			          {
		
			        	  li.getWorld().playEffect(li.getEyeLocation(), 
					              Effect.POTION_BREAK, 8194);
					            li.addPotionEffect(new PotionEffect(
					              PotionEffectType.SLOW, 20, 0));

			            double eldmg = getElemFromLore(p.getItemInHand(), 
			              "ICE DMG");
			            dmg += eldmg;
			          }
			          if (((String)lore.get(i1)).contains("POISON DMG"))
			          {
			        	  li.getWorld().playEffect(li.getEyeLocation(), 
					              Effect.POTION_BREAK, 8196);
					            li.addPotionEffect(new PotionEffect(
					              PotionEffectType.POISON, 10, 1));

			            double eldmg = getElemFromLore(p.getItemInHand(), 
			              "POISON DMG");
			            dmg += eldmg;
			          }
			          if (((String)lore.get(i1)).contains("FIRE DMG"))
			          {
			 
			            double eldmg = getElemFromLore(p.getItemInHand(), 
			              "FIRE DMG");
			            li.setFireTicks(40);
			            dmg += eldmg;
			          }
			          if (((String)lore.get(i1)).contains("PURE DMG"))
			          {
			            double eldmg = getElemFromLore(p.getItemInHand(), 
			              "PURE DMG");
			            dmg += eldmg;
			          }
			          if (((String)lore.get(i1)).contains("CRITICAL HIT"))
			          {
			            int crit = getLifestealFromLore(p.getItemInHand(), 
			              "CRITICAL HIT");
			            Random random = new Random();
			            int drop = random.nextInt(25) + 1;
			            if (drop <= crit) {
			              dmg *= 2.0D;
			            }
			          }
			          if (((String)lore.get(i1)).contains("LIFE STEAL"))
			          {
			            li.getWorld().playEffect(li.getEyeLocation(), 
			              Effect.STEP_SOUND, Material.REDSTONE_BLOCK);
			            double base = getLifestealFromLore(p.getItemInHand(), 
			              "LIFE STEAL");
			            double pcnt = base / 100.0D;
			            int life = 0;
			            if ((int)(pcnt * dmg) > 0) {
			              life = (int)(pcnt * dmg);
			            } else {
			              life = 1;
			            }
			            p.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + 
			              "            +" + ChatColor.GREEN + life + 
			              ChatColor.GREEN + ChatColor.BOLD + " HP " + 
			              ChatColor.GRAY + "[" + (int)p.getHealth() + 
			              "/" + (int)p.getMaxHealth() + "HP]");
			            if (p.getHealth() < p.getMaxHealth() - life) {
			              p.setHealth(p.getHealth() + life);
			            } else if (p.getHealth() >= p.getMaxHealth() - life) {
			              p.setHealth(p.getMaxHealth());
			            }
			          }
			        }
			      }
	      }
		return dmg / 2;
	  
	  }
*/
	  
}

	  



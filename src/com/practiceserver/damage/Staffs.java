package com.practiceserver.damage;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.SmallFireball;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.WitherSkull;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

/*
Copyright � matt11matthew 2016
*/

public class Staffs implements Listener {
	
    public static HashMap<Integer, Player> staffPlayerList = new HashMap();
    
	private static Staffs instance = new Staffs();
	
	public static Staffs getInstance() {
		return instance;
	}
	//
	@EventHandler
	public void onStaffShoot(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if(e.getAction() == Action.RIGHT_CLICK_AIR) {
			if(p.getItemInHand().getType() == Material.WOOD_HOE) {
				Projectile snowball = p.launchProjectile(Snowball.class);
		        snowball.setShooter(p);
		        staffPlayerList.put(Integer.valueOf(snowball.getEntityId()), p);
		        p.playSound(p.getLocation(), Sound.ENTITY_ARROW_SHOOT, 1.0F, 1.0F);
			}
			if(p.getItemInHand().getType() == Material.STONE_HOE) {
				Projectile fireball = p.launchProjectile(SmallFireball.class);
		        fireball.setShooter(p);
		        staffPlayerList.put(Integer.valueOf(fireball.getEntityId()), p);
		        p.playSound(p.getLocation(), Sound.ENTITY_ARROW_SHOOT, 1.0F, 1.0F);
			}
			if(p.getItemInHand().getType() == Material.IRON_HOE) {
				Projectile enderperal = p.launchProjectile(EnderPearl.class);
		        enderperal.setShooter(p);
		        staffPlayerList.put(Integer.valueOf(enderperal.getEntityId()), p);
		        p.playSound(p.getLocation(), Sound.ENTITY_ARROW_SHOOT, 1.0F, 1.0F);
			}
			if(p.getItemInHand().getType() == Material.DIAMOND_HOE) {
				Projectile witherskull = p.launchProjectile(WitherSkull.class);
		        witherskull.setShooter(p);
		        staffPlayerList.put(Integer.valueOf(witherskull.getEntityId()), p);
		        p.playSound(p.getLocation(), Sound.ENTITY_ARROW_SHOOT, 1.0F, 1.0F);
			}
			if(p.getItemInHand().getType() == Material.GOLD_HOE) {
				Projectile bigfireball = p.launchProjectile(SmallFireball.class);
		        bigfireball.setShooter(p);
		        staffPlayerList.put(Integer.valueOf(bigfireball.getEntityId()), p);
		        p.playSound(p.getLocation(), Sound.ENTITY_ARROW_SHOOT, 1.0F, 1.0F);
			}
		}
	}
	
    @EventHandler
    public void onProjectileLand(ProjectileHitEvent e) {
    	if(staffPlayerList.containsKey(Integer.valueOf(e.getEntity().getEntityId()))) {
    		staffPlayerList.remove(Integer.valueOf(e.getEntity().getEntityId()));
    	}
    }
    
    @EventHandler(priority=EventPriority.HIGH)
    public void onStaffTeleport(PlayerTeleportEvent e)
    {
      if (e.getCause().equals(PlayerTeleportEvent.TeleportCause.ENDER_PEARL)) {
        e.setCancelled(true);
      }
    }
    
    @EventHandler(priority=EventPriority.HIGHEST)
    public void onEntityExplodeEvent(EntityExplodeEvent e)
    {
      if (((e.getEntity() instanceof SmallFireball)) || ((e.getEntity() instanceof WitherSkull)))
      {
        e.setCancelled(false);
        e.setYield(0.0F);
      }
    }
    
    @EventHandler(priority=EventPriority.HIGHEST)
    public void onEntityExplodePrimeEvent(ExplosionPrimeEvent e)
    {
      if ((e.getEntity() instanceof WitherSkull))
      {
        Projectile p = (Projectile)e.getEntity();
        if ((p.getShooter() instanceof Player))
        {
          e.setFire(false);
          e.setRadius(0.0F);
        }
      }
    }
    
    @EventHandler
    public void onFireCreation(BlockIgniteEvent e)
    {
      if (e.getCause().equals(BlockIgniteEvent.IgniteCause.FIREBALL)) {
        e.setCancelled(true);
      }
    }
}
 

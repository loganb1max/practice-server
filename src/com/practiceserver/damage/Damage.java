package com.practiceserver.damage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.WitherSkull;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.practiceserver.Main;
import com.practiceserver.chat.ChatUtils;
import com.practiceserver.enums.Alignment;
import com.practiceserver.enums.Zone;
import com.practiceserver.health.HealthUtils;
import com.practiceserver.mob1.Mob;
import com.practiceserver.player.PlayerAlignment;
import com.practiceserver.player.PracticeServerPlayer;
import com.practiceserver.utils.RegionUtils;
import com.practiceserver.utils.Utils;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;



/*
Copyright � matt11matthew 2016
*/

public class Damage implements Listener {
	
	private static Damage instance = new Damage();
	protected static WorldGuardPlugin wg = (WorldGuardPlugin)org.bukkit.Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");
	public static List<Player> tagged = new ArrayList<Player>();
	
	
	public static Damage getInstance() {
		return instance;
	}
	
	@EventHandler
	public void pReSpawn(PlayerRespawnEvent e) {
		Player p = e.getPlayer();
		PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
		if(pl.isCombat()) {
			pl.setCombat(false);
		}
	}
	

	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		if(e.getEntity() instanceof Player) {
			Player p = e.getEntity();
			e.setDeathMessage(null);
			//p.spigot().respawn();//
			
		}
	}
	

	 @EventHandler
	    public void onPlayerItemHeld(PlayerItemHeldEvent e) {
	        final Player p = e.getPlayer(); // volumn, pitch
	        if (p.getInventory().getItem(e.getNewSlot()) == null) {
	            return;
	        }

	        ItemStack i = p.getInventory().getItem(e.getNewSlot());
	        if (i.getType() == Material.BOW) {
	            p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0F, 1.4F);
	        }
	        if (i.getType() == Material.WOOD_SWORD || i.getType() == Material.STONE_SWORD || i.getType() == Material.IRON_SWORD
	                || i.getType() == Material.DIAMOND_SWORD || i.getType() == Material.GOLD_SWORD) {
	            p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0F, 1.4F);
	        }
	        if (i.getType() == Material.WOOD_AXE || i.getType() == Material.STONE_AXE || i.getType() == Material.IRON_AXE || i.getType() == Material.DIAMOND_AXE
	                || i.getType() == Material.GOLD_AXE) {
	            p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0F, 1.4F);
	        }
	        if (i.getType().name().toLowerCase().contains("spade")) {
	            p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0F, 1.4F);
	        }
	        if (i.getType().name().toLowerCase().contains("hoe")) {
	            p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0F, 1.4F);
	        }
	 }
	
	public void killEntity(LivingEntity mob) {
		mob.setHealth(0);
		mob.damage(1);
	}
	
	public void arrowDamageMob(Entity player, LivingEntity l) {
		Arrow a = (Arrow)player;
		if(!(a.getShooter() instanceof Player))return;
		Player p = (Player)a.getShooter();
		//if(PartyHandler.hasParty(p)) {
			//PartyHandler.getParty(p).updateScoreBoard();
		//}
		PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
		int dmg = Integer.valueOf((String)((MetadataValue)a.getMetadata("DMG").get(0)).value()).intValue();
	    ItemStack bow = (ItemStack)((MetadataValue)a.getMetadata("Bow").get(0)).value();
	    double raw_finaldmg = getFinalDamage(p, dmg, l);
		if(raw_finaldmg == -1) {
			return;
		}
	    if(raw_finaldmg == -10) {
			block(p, l);
			return;
		} else if (raw_finaldmg == -5) {
			dodge(p, l);
			return;
		} else {
			//RepairUtils.getInstance().useWeapon(p);
				if(raw_finaldmg > l.getHealth()) {
					killEntity(l);
					String name = Utils.colorCodes(Mob.getInstance().getName(l));
					if(pl.isToggleDebug()) {
						p.sendMessage(ChatColor.RED + "            " + (int)raw_finaldmg + ChatColor.RED + "" + ChatColor.BOLD + " DMG " + ChatColor.RED + "-> " + ChatColor.RESET + name + " [" + 0 + "HP]");
					}
				
					drops(p, l);
					Utils.updateHealthbar(l);
					
					return;
					

				} else {
				//e.setDamage(Double.parseDouble(getFinalDamage(p, rawdmg, l)));
					
					String name = Utils.colorCodes(Mob.getInstance().getName(l));
					if(pl.isToggleDebug()) {
					p.sendMessage(ChatColor.RED + "            " + (int)raw_finaldmg + ChatColor.RED + 
			    	        ChatColor.BOLD + " DMG " + ChatColor.RED + "-> " + 
			    	        ChatColor.RESET + name
			    	        + " [" + (int)(l.getHealth() - raw_finaldmg) + 
			    	        "HP]"); 
					}
					//e.setDamage(raw_finaldmg);
					l.damage(raw_finaldmg);
					Utils.updateHealthbar(l);
					return;
				}
		}
	}

	public void staffDamagePlayer(Entity player, LivingEntity l) {
		if((player instanceof Projectile) && ((l instanceof Player))) {
			Player p = (Player)Staffs.staffPlayerList.get(Integer.valueOf(player.getEntityId()));
			Player pp = (Player) l;
			if(p.getLevel() < 1) {
				return;
			}
			PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
		//	if(PartyHandler.hasParty(pp)) {
			//	PartyHandler.getParty(p).updateScoreBoard();
			//}
			//if(PartyHandler.hasParty(p)) {
			//	PartyHandler.getParty(pp).updateScoreBoard();
			//}
			double rawdmg = DamageUtils.getInstance().getDamage(p.getItemInHand(), "DMG");
			//if(PartyHandler.isInSameParty(p, pp))return;
			double raw_finaldmg = getFinalDamage(p, rawdmg, l);
			if(raw_finaldmg == -1) {
				return;
			}
			if(raw_finaldmg == -10) {
				return;
			} else if (raw_finaldmg == -5) {
				return;
			} else {
				damageplayer((int)raw_finaldmg, (Player) l, p, 0);
			}
		}
	}
	
	public void bowDamageMob(Entity player, LivingEntity l) {
		
	}
	
	public void staffDamageMob(Entity player, LivingEntity l) {
		if((player instanceof Projectile) && (!(l instanceof Player))) {
			Player p = (Player)Staffs.staffPlayerList.get(Integer.valueOf(player.getEntityId()));
			if(p.getLevel() < 1) {
				return;
			}
			p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_HURT, 1.0F, 1.0F);
			pushAwayPlayer(p, l, 0.5);
			double rawdmg = DamageUtils.getInstance().getDamage(p.getItemInHand(), "DMG");
			double raw_finaldmg = getFinalDamage(p, rawdmg, l);
			PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
			if(raw_finaldmg == -1) {
				return;
			}
			if(raw_finaldmg == -10) {
				block(p, l);
				return;
			} else if (raw_finaldmg == -5) {
				dodge(p, l);
				return;
			} else {
				//RepairUtils.getInstance().useWeapon1(p);
					if(raw_finaldmg > l.getHealth()) {
						killEntity(l);
						String name = Utils.colorCodes(Mob.getInstance().getName(l));
						if(pl.isToggleDebug()) {
						p.sendMessage(ChatColor.RED + "            " + (int)raw_finaldmg + ChatColor.RED + "" + ChatColor.BOLD + " DMG " + ChatColor.RED + "-> " + ChatColor.RESET + name + " [" + 0 + "HP]");
						}
						drops(p, l);
						Utils.updateHealthbar(l);
						return;
						

					} else {
						l.setHealth((l.getHealth() - raw_finaldmg));
						
					//e.setDamage(Double.parseDouble(getFinalDamage(p, rawdmg, l)));
						
						String name = Utils.colorCodes(Mob.getInstance().getName(l));
						if(pl.isToggleDebug()){
							p.sendMessage
								(ChatColor.RED + "            " + (int)raw_finaldmg + ChatColor.RED + 
				    	        ChatColor.BOLD + " DMG " + ChatColor.RED + "-> " + 
				    	        ChatColor.RESET + name
				    	        + " [" + (int)(l.getHealth()) + 
				    	        "HP]"); 
						}
						//e.setDamage(raw_finaldmg);
					
						Utils.updateHealthbar(l);
						return;
					}
			}
		}
	}

	
	  @EventHandler
	  public void onTag(EntityDamageEvent e) {
	    if ((e.getEntity() instanceof Player)) {
	      if (e.getDamage() <= 0.0D) {
	        return;
	      }
	      final Player p = (Player)e.getEntity();
	      if(e.getCause() == DamageCause.FALL)return;
	      tagged.add(p);
	      new BukkitRunnable() {
	        public void run() {
	          tagged.remove(p);
	        }
	        
	      }.runTaskLater(Main.plugin, 200L);
	    }
	  }
	  @EventHandler(priority=EventPriority.LOWEST)
	  public void onKickLog(PlayerKickEvent e)
	  {
	    Player p = e.getPlayer();
	    if (e.getReason().equals("Illegal characters in chat")) {
	      e.setCancelled(true);
	    } else
	      for (int i = 0; i < tagged.size(); i++)
	        tagged.remove(p);
	  }
	  
	  @EventHandler
	  public void onHitTag(EntityDamageByEntityEvent e) {
	    if (((e.getDamager() instanceof Player)) && 
	      ((e.getEntity() instanceof LivingEntity))) {
	      if (e.getDamage() <= 0.0D) {
	        return;
	      }
	      final Player p = (Player)e.getDamager();
	      tagged.add(p);
	      new BukkitRunnable() {
	        public void run() {
	          tagged.remove(p);
	        }
	        
	      }.runTaskLater(Main.plugin, 200L);
	    }
	  }


	public static double getDMGReduce(Player p, int dmg) { 
		double armor = HealthUtils.getInstance().getFinalArmor(p);
		if(armor > 0.0D) {
			double divide = armor / 100.0D;
			double pre = dmg * divide;
			int cleaned = (int)(dmg - pre);
			dmg = cleaned;
			return dmg;
		}
		return dmg;
	}
	
	public double getFinalDamage(Player p, double dmg, LivingEntity mob) {
		PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
		 ApplicableRegionSet set = wg.getRegionManager(mob.getLocation().getWorld()).getApplicableRegions(mob.getLocation());
		 if (pl.getZone() == Zone.SAFE) return -1;
		 if (RegionUtils.isSafeZone(mob.getLocation())) return -1;
		 if (mob instanceof Player) {
			 if(RegionUtils.isWildernessZone(mob.getLocation())) return -1;
			 if(RegionUtils.isWildernessZone(p.getLocation())) return -1;
		 }

			 
		 
		 if(p.getLevel() < 1) {
				return -1;
			}
		ItemStack item = p.getItemInHand();
		if(!item.hasItemMeta())return 1.0D;
		dmg = DamageUtils.getInstance().getDMGWithDEX(p, dmg);
		dmg = DamageUtils.getInstance().getDMGWithSTR(p, dmg);
		dmg = DamageUtils.getInstance().getDMGWithVit(p, dmg);
		dmg = DamageUtils.getInstance().getDMGWithINT(p, dmg);
		boolean hasCrit = DamageUtils.getInstance().hasStat(item, "CRITICAL HIT", p);
		boolean hasIceDMG = DamageUtils.getInstance().hasStat(item, "ICE DMG", p);
		boolean hasFireDMG = DamageUtils.getInstance().hasStat(item, "FIRE DMG", p);
		boolean hasPoisonDMG = DamageUtils.getInstance().hasStat(item, "POISON DMG", p);
		boolean hasPureDMG = DamageUtils.getInstance().hasStat(item, "PURE DMG", p);
		boolean hasLifeStealDMG = DamageUtils.getInstance().hasStat(item, "LIFE STEAL", p);
		boolean hasMVM = DamageUtils.getInstance().hasStat(item, "DMG vs. Monsters", p);
		boolean hasPVP = DamageUtils.getInstance().hasStat(item, "DMG vs. Players", p);
		boolean hasBlind = DamageUtils.getInstance().hasStat(item, "BLIND", p);
		boolean kb = false;
		boolean hasKb = DamageUtils.getInstance().hasStat(item, "KNOCKBACK", p);
		double crit = 0.0D;
		double block = 0.0D;
		double dodge = 0.0D;
		double acc = 0.0D;
		double dps = 0.0D;
		double armor = 0.0D;
		boolean hasArmorpen = DamageUtils.getInstance().hasStat(item, "ARMOR PENETRATION", p);
		dps = HealthUtils.getInstance().getFinalDPS(p);
		armor = HealthUtils.getInstance().getFinalArmor(p);
		if(dps > 0.0D) {
			double divide = dps / 100.0D;
			double pre = dmg * divide;
			int cleaned = (int)(dmg + pre);
			dmg = cleaned;
		}
		if(mob instanceof Player) {
			double armor1 = HealthUtils.getInstance().getFinalArmor(p);
			if(hasArmorpen) {
				double armorpen = DamageUtils.getInstance().getFinalArmorPen(item, p);
				armor1 = (armor1 - armorpen);
				if(armor1 > 0.0D) {
					double divide = armor1 / 100.0D;
					double pre = dmg * divide;
					int cleaned = (int)(dmg - pre);
					dmg = cleaned;
				}
			}
		}
		if(mob instanceof Player) {
			block = HealthUtils.getInstance().getBlock((Player) mob);
			dodge = HealthUtils.getInstance().getDodge((Player) mob);
			if(DamageUtils.getInstance().hasStat(item, "ACCURACY", p)) {
				acc = DamageUtils.getInstance().getAccuracy(item);
				block = (block - acc);
				dodge = (dodge - acc);
			}
			Random ran = new Random();
		    int per = ran.nextInt(100) + 1;
		    if ((DamageUtils.getInstance().decideDrop(dodge) != 0) || (dodge >= 100)) {
		    	return -5;
		    }
		    if ((DamageUtils.getInstance().decideDrop(block) != 0) || (block >= 100)) {
		    	return -10;
		    }

		}   		
		if(hasFireDMG) {
			if(mob instanceof Player) {
				if(!pl.isTogglePvP())
					dmg += DamageUtils.getInstance().getFireDMG(item);
					mob.setFireTicks(40);
			} else {
				dmg += DamageUtils.getInstance().getFireDMG(item);
				mob.setFireTicks(40);
			}
			
		}
		if(hasIceDMG) {
			dmg += DamageUtils.getInstance().getIceDMG(item);
			mob.getWorld().playEffect(mob.getEyeLocation(), Effect.POTION_BREAK, 8194);
		    mob.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20, 0));
		}
		if(hasPureDMG) {
			dmg += DamageUtils.getInstance().getPureDMG(item);
		}
		if(hasPoisonDMG) {
			dmg += DamageUtils.getInstance().getPoisonDMG(item);
			 mob.getWorld().playEffect(mob.getEyeLocation(), Effect.POTION_BREAK, 8196);
	         mob.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 10, 1));
		}
		if(hasKb) {
			if (DamageUtils.getInstance().getKnockBack(item) >= new Random().nextInt(100)) {
				kb = true;
			}
		}
		if(kb) {
			pushAwayPlayer(p, mob, 0.7);
		}
		
		if(hasPVP) {
			if(mob instanceof Player) {
				double base = DamageUtils.getInstance().getPVP(item);
				double pcnt = base / 100.0D;
	            dmg += dmg * pcnt;
			}
		}
		if(hasMVM) {
			if(!(mob instanceof Player)) {
				double base = DamageUtils.getInstance().getMVM(item);
				double pcnt = base / 100.0D;
	            dmg += dmg * pcnt;
			}
		}
		if(hasBlind) {
			if(mob instanceof Player) {
				int tier = com.practiceserver.mobs.Mob.getItemTier(item);
				double blind = DamageUtils.getInstance().getBlind(item);
				if ((DamageUtils.getInstance().decideDrop(blind) != 0) || (blind >= 100)) {
					if(tier == 1) {
						mob.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 30, 1));
					}
					if(tier == 2) {
						mob.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 40, 1));
					}
					if(tier == 3) {
						mob.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 50, 1));
					}
					if(tier == 4) {
						mob.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 60, 1));
					}
					if(tier == 5) {
						mob.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 60, 1));
					}
				}
			}
		}
		if(hasLifeStealDMG) {
			if((mob instanceof Player)) {
				if(!pl.isTogglePvP()) {
					mob.getWorld().playEffect(mob.getEyeLocation(), 
				              Effect.STEP_SOUND, Material.REDSTONE_BLOCK);
				            double base = DamageUtils.getInstance().getLifeSteal(p.getItemInHand());
				            double pcnt = base / 100.0D;
				            int life = 0;
				            if ((int)(pcnt * dmg) > 0) {
				              life = (int)(pcnt * dmg);
				            } else {
				              life = 1;
				            }
				            p.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + 
				              "            +" + ChatColor.GREEN + life + 
				              ChatColor.GREEN + ChatColor.BOLD + " HP " + 
				              ChatColor.GRAY + "[" + (int)p.getHealth() + 
				              "/" + (int)p.getMaxHealth() + "HP]");
				            if (p.getHealth() < p.getMaxHealth() - life) {
				              p.setHealth(p.getHealth() + life);
				            } else if (p.getHealth() >= p.getMaxHealth() - life) {
				              p.setHealth(p.getMaxHealth());
				            }
				}
			} else {
				mob.getWorld().playEffect(mob.getEyeLocation(), 
			              Effect.STEP_SOUND, Material.REDSTONE_BLOCK);
			            double base = DamageUtils.getInstance().getLifeSteal(p.getItemInHand());
			            double pcnt = base / 100.0D;
			            int life = 0;
			            if ((int)(pcnt * dmg) > 0) {
			              life = (int)(pcnt * dmg);
			            } else {
			              life = 1;
			            }
			            p.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + 
			              "            +" + ChatColor.GREEN + life + 
			              ChatColor.GREEN + ChatColor.BOLD + " HP " + 
			              ChatColor.GRAY + "[" + (int)p.getHealth() + 
			              "/" + (int)p.getMaxHealth() + "HP]");
			            if (p.getHealth() < p.getMaxHealth() - life) {
			              p.setHealth(p.getHealth() + life);
			            } else if (p.getHealth() >= p.getMaxHealth() - life) {
			              p.setHealth(p.getMaxHealth());
			            }
			
			}
		}
		if(hasCrit) {
			crit = DamageUtils.getInstance().getCriticalHit(item);
		}
		if(DamageUtils.getInstance().isAxe(p)) {
			crit += 20.0D;
		}
		if ((DamageUtils.getInstance().decideDrop(crit) != 0) || (crit >= 100)) {
			dmg *= 2.0D;
			p.playSound(mob.getLocation(), Sound.ENTITY_PLAYER_ATTACK_CRIT, 1.0F, 0.65F);
		}
		//RepairUtils.getInstance().useWeapon1(p);
		return dmg;
	}
	
	public static void pushAwayMob(Player p, Entity entity, double speed)
	  {
	    Vector unitVector = p.getLocation().toVector().subtract(entity.getLocation().toVector()).normalize();
	    
	    double e_y = entity.getLocation().getY();
	    double p_y = p.getLocation().getY();
	    
	    Material m = p.getLocation().subtract(0.0D, 1.0D, 0.0D).getBlock().getType();
	    
	    if ((p_y - 1.0D <= e_y) || (m == Material.AIR)) {
	      p.setVelocity(unitVector.multiply(speed));
	    }
	  }
	
	public static void pushAwayPlayer(LivingEntity entity, LivingEntity p, double speed)
	  {
	    Vector unitVector = p.getLocation().toVector().subtract(entity.getLocation().toVector()).normalize();
	    
	    double e_y = entity.getLocation().getY();
	    double p_y = p.getLocation().getY();
	    
	    Material m = p.getLocation().subtract(0.0D, 1.0D, 0.0D).getBlock().getType();
	    
	    if ((p_y - 1.0D <= e_y) || (m == Material.AIR)) {
	      p.setVelocity(unitVector.multiply(speed));
	    }
	  }
	
	public void mobDamagePlayerBow(Entity e, Player p) {
		if(p instanceof Player) {
			Arrow a = (Arrow)e;
			//if(PartyHandler.hasParty(p)) {
			//	PartyHandler.getParty(p).updateScoreBoard();
			//}
			PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
			
			LivingEntity li = (LivingEntity) a.getShooter();
			if(!(li instanceof Player)) {
				double dmg = Mob.getInstance().getDamage(Mob.getInstance().getIDName(li), p);
				dmg = (dmg * 0.80D);
				if(Utils.chargedMobs.contains(li)) {
					li.getWorld().playSound(li.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 1.0F, 0.3F);
					//ParticleEffect.EXPLODE.display(p.getLocation().add(0.0D, 1.0D, 0.0D), new Random().nextFloat(), new Random().nextFloat(), new Random().nextFloat(), 0.4F, 40);
					dmg *= 3;
					pushAwayPlayer(li, p, 1.0);
					Utils.chargedMobs.remove(li);
				}
				if(dmg == -1) {
					tagged.remove(p);
					return;
				}
				else if(dmg == -10) {
					p.sendMessage(Utils.colorCodes("           " + "&2&l*BLOCK* (&r" + Mob.getInstance().getDisplayName(li) + "&2&l)"));
					p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 2.0F, 1.0F);
					dmg = -2;
					
				} else if (dmg == -5) {
					p.sendMessage(Utils.colorCodes("           " + "&a&l*DODGED* (&r" + Mob.getInstance().getDisplayName(li) + "&a&l)"));
					p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_INFECT, 2.0F, 1.5F);
					dmg = -2;
				}
				else if (dmg == -2) {
					return;
					
				} else {
					 if ((p.getNoDamageTicks() <= p.getMaximumNoDamageTicks() / 2) && (p.getHealth() > 0.0D)) {
						 if(dmg <= 0) {
							 return;
						 }
						 int health = 0;
						 dmg = getDMGReduce(p, (int) dmg);
					     if (p.getHealth() - dmg > 0.0D) {
					        health = (int)(p.getHealth() - dmg);
					        p.damage(dmg);
					        if(pl.isToggleDebug()) {
					        p.sendMessage(ChatColor.RED + "            -" + (int)dmg + ChatColor.RED + "" + ChatColor.BOLD + "HP " + ChatColor.GRAY + "[-" + (int)HealthUtils.getInstance().getArmor(p) + "%A -> -" + (int)getDMGReduce(p, (int) dmg) + "" + ChatColor.BOLD +  "DMG" + ChatColor.GRAY + "] " + ChatColor.GREEN + "[" + (int)health + ChatColor.BOLD + "HP" + ChatColor.GREEN + "]");
					        }
					        li.setCustomName(Mob.getInstance().getDisplayName(li));
					        li.setCustomNameVisible(true);
					       // RepairUtils.getInstance().useArmor(p);
					        
					        return;
					     } else {
							 p.damage(p.getHealth());
							 if(pl.isToggleDebug()) {
							 p.sendMessage(ChatColor.RED + "            -" + (int)dmg + ChatColor.RED + "" + ChatColor.BOLD + "HP " + ChatColor.GRAY + "[-" + (int)HealthUtils.getInstance().getArmor(p) + "%A -> -" + (int)getDMGReduce(p, (int) dmg) + "" + ChatColor.BOLD +  "DMG" + ChatColor.GRAY + "] " + ChatColor.GREEN + "[" + (int)0 + ChatColor.BOLD + "HP" + ChatColor.GREEN + "]");
							 
							 }
							 
							 li.setCustomName(Mob.getInstance().getDisplayName(li));
							 li.setCustomNameVisible(true); 
							 String name = "&7" + p.getName();
							 String msg = name + "&f was killed a(n) &r" + Mob.getInstance().getDisplayName(li);
							 ChatUtils.sendMessage(Utils.colorCodes(msg), 20, p);
							 //ChatHandler.sendMessageToLocalPlayers1(msg, p);
							 //RepairUtils.getInstance().useArmor(p);
							 
							 p.spigot().respawn();
							 return;
					     }
					 }
				}
			}
		}
	}
	
	public void mobDamagePlayer(LivingEntity l, Player p) {
		if(p instanceof Player) {
			//if(PartyHandler.hasParty(p)) {
			//	PartyHandler.getParty(p).updateScoreBoard();
			//}
			LivingEntity li = (LivingEntity) l;
			double dmg = Mob.getInstance().getDamage(Mob.getInstance().getIDName(l), p);
			if(Utils.chargedMobs.contains(li)) {
				li.getWorld().playSound(li.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 1.0F, 0.3F);
				//ParticleEffect.EXPLODE.display(p.getLocation().add(0.0D, 1.0D, 0.0D), new Random().nextFloat(), new Random().nextFloat(), new Random().nextFloat(), 0.4F, 40);
				dmg *= 3;
				pushAwayPlayer(li, p, 1.0);
				Utils.chargedMobs.remove(li);
				
			}
			if(dmg == -1) {
				tagged.remove(p);
				return;
			}
			else if(dmg == -10) {
				p.sendMessage(Utils.colorCodes("           " + "&2&l*BLOCK* (&r" + Mob.getInstance().getDisplayName(li) + "&2&l)"));
				p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 2.0F, 1.0F);
				dmg = -2;
				
			} else if (dmg == -5) {
				p.sendMessage(Utils.colorCodes("           " + "&a&l*DODGED* (&r" + Mob.getInstance().getDisplayName(li) + "&a&l)"));
				p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_INFECT, 2.0F, 1.5F);
				dmg = -2;
			} else if (dmg == -2) {
				return;
			} else {
				String type = Mob.getInstance().getType(Mob.getInstance().getMobNameID(l));
				dmg = getDMGReduce(p, (int) dmg);
				if(type.equalsIgnoreCase("imp")) {
					int health = 0;
	
					
				     if (p.getHealth() - dmg > 0.0D) {
				        health = (int)(p.getHealth() - dmg);
				        
				        p.damage(dmg);
						PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
				        if(pl.isToggleDebug()) {
				        p.sendMessage(ChatColor.RED + "            -" + (int)dmg + ChatColor.RED + "" + ChatColor.BOLD + "HP " + ChatColor.GRAY + "[-" + (int)HealthUtils.getInstance().getArmor(p) + "%A -> -" + (int)getDMGReduce(p, (int) dmg) + "" + ChatColor.BOLD +  "DMG" + ChatColor.GRAY + "] " + ChatColor.GREEN + "[" + (int)health + ChatColor.BOLD + "HP" + ChatColor.GREEN + "]");
				        }
				        li.setCustomName(Mob.getInstance().getDisplayName(li));
				        li.setCustomNameVisible(true);
				        //RepairUtils.getInstance().useArmor(p);
				        return;
				        
				     } else {
				    	 p.damage(p.getHealth());
							PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
						 if(pl.isToggleDebug()) {
						 p.sendMessage(ChatColor.RED + "            -" + (int)dmg + ChatColor.RED + "" + ChatColor.BOLD + "HP " + ChatColor.GRAY + "[-" + (int)HealthUtils.getInstance().getArmor(p) + "%A -> -" + (int)getDMGReduce(p, (int) dmg) + "" + ChatColor.BOLD +  "DMG" + ChatColor.GRAY + "] " + ChatColor.GREEN + "[" + (int)0 + ChatColor.BOLD + "HP" + ChatColor.GREEN + "]");
						 }
						 li.setCustomName(Mob.getInstance().getDisplayName(li));
						 li.setCustomNameVisible(true); 
						 String name = "&7" + p.getName();
						 String msg = name + "&f was killed a(n) &r" + Mob.getInstance().getDisplayName(li);
						 ChatUtils.sendMessage(Utils.colorCodes(msg), 20, p);
						 //RepairUtils.getInstance().useArmor(p);
						 
						 p.spigot().respawn();
						 return;
				     }
					
				}
				if ((p.getNoDamageTicks() <= p.getMaximumNoDamageTicks() / 2) && (p.getHealth() > 0.0D)) {
					if(dmg <= 0) {
						 return;
					 }
					 int health = 0;
				     if (p.getHealth() - dmg > 0.0D) {
				        health = (int)(p.getHealth() - dmg);
				        p.damage(dmg);
						PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
				        if(pl.isToggleDebug()) {
				        p.sendMessage(ChatColor.RED + "            -" + (int)dmg + ChatColor.RED + "" + ChatColor.BOLD + "HP " + ChatColor.GRAY + "[-" + (int)HealthUtils.getInstance().getArmor(p) + "%A -> -" + (int)getDMGReduce(p, (int) dmg) + "" + ChatColor.BOLD +  "DMG" + ChatColor.GRAY + "] " + ChatColor.GREEN + "[" + (int)health + ChatColor.BOLD + "HP" + ChatColor.GREEN + "]");
				        }
				        li.setCustomName(Mob.getInstance().getDisplayName(li));
				        li.setCustomNameVisible(true);
				        
				       // RepairUtils.getInstance().useArmor(p);
				        return;
				        
				     } else {
				    	 p.damage(p.getHealth());
							PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
						 if(pl.isToggleDebug()) {
						 p.sendMessage(ChatColor.RED + "            -" + (int)dmg + ChatColor.RED + "" + ChatColor.BOLD + "HP " + ChatColor.GRAY + "[-" + (int)HealthUtils.getInstance().getArmor(p) + "%A -> -" + (int)getDMGReduce(p, (int) dmg) + "" + ChatColor.BOLD +  "DMG" + ChatColor.GRAY + "] " + ChatColor.GREEN + "[" + (int)0 + ChatColor.BOLD + "HP" + ChatColor.GREEN + "]");
						 }
						 li.setCustomName(Mob.getInstance().getDisplayName(li));
						 li.setCustomNameVisible(true); 
						 String name = "&7" + p.getName();
						 String msg = name + "&f was killed a(n) &r" + Mob.getInstance().getDisplayName(li);
						 ChatUtils.sendMessage(Utils.colorCodes(msg), 20, p);
						// ChatHandler.sendMessageToLocalPlayers1(msg, p);
						// RepairUtils.getInstance().useArmor(p);
						 
						 p.spigot().respawn();
						 return;
				     }
				}
			}
			}
		}
	//}
		
	
	@EventHandler
	public void onFall(EntityDamageEvent e) {
		if(e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
			if(e.getCause() == DamageCause.FALL) {
				if(pl.isToggleDebug()) {
					 p.sendMessage(ChatColor.RED + "            -" + (int)e.getDamage() + ChatColor.RED + "" + ChatColor.BOLD + "HP " + ChatColor.GRAY + "[-" + (int)HealthUtils.getInstance().getArmor(p) + "%A -> -" + (int)getDMGReduce(p, (int) e.getDamage()) + "" + ChatColor.BOLD +  "DMG" + ChatColor.GRAY + "] " + ChatColor.GREEN + "[" + (int)0 + ChatColor.BOLD + "HP" + ChatColor.GREEN + "]");
				}
			}
			
		}
	}

	@EventHandler
	public void onCombatDMG(EntityDamageByEntityEvent e) {
		if(!(e.getDamager() instanceof Player) && (!(e.getDamager() instanceof Arrow) && (!(e.getDamager() instanceof Projectile)))) {
			mobDamagePlayer((LivingEntity)e.getDamager(), (Player)e.getEntity());
		}
		if((e.getDamager() instanceof Arrow) && ((e.getEntity() instanceof Player))) {
			PlayerDamagePlayerBow(e.getDamager(), (Player)e.getEntity());
		}
		if((e.getDamager() instanceof Arrow) && (!(e.getEntity() instanceof Player))) {
			e.setCancelled(true);
			arrowDamageMob(e.getDamager(), (LivingEntity) e.getEntity());
		}
		if(!(e.getDamager() instanceof Player) && (e.getDamager() instanceof Arrow) && ((e.getEntity() instanceof Player))) {
			mobDamagePlayerBow(e.getDamager(), (Player)e.getEntity());
		}
		if((!(e.getEntity() instanceof Player) && e.getDamager() instanceof Projectile) || (e.getDamager() instanceof Snowball) || (e.getDamager() instanceof WitherSkull) || (e.getDamager() instanceof Fireball) || (e.getDamager() instanceof EnderPearl)) {
			e.setCancelled(true);
			staffDamageMob(e.getDamager(), (LivingEntity) e.getEntity());	
		}
		if(((e.getEntity() instanceof Player) && e.getDamager() instanceof Projectile) || (e.getDamager() instanceof Snowball) || (e.getDamager() instanceof WitherSkull) || (e.getDamager() instanceof Fireball) || (e.getDamager() instanceof EnderPearl)) {
			e.setCancelled(true);
			staffDamagePlayer(e.getDamager(), (LivingEntity) e.getEntity());
		}
		if((e.getDamager() instanceof Player) && (!(e.getEntity() instanceof Player))) {
			playerDamageMob((LivingEntity)e.getEntity(), (Player)e.getDamager());
		}
		if((e.getDamager() instanceof Player) && (e.getEntity() instanceof Player)) {
			playerDamagePlayer((Player)e.getDamager(), (Player)e.getEntity());
		}
	}
	
	public void PlayerDamagePlayerBow(Entity damager, Player l) {
		if(damager instanceof Arrow) {
			Arrow a = (Arrow) damager;
			if(a.getShooter() instanceof Player) {
				Player p = (Player) a.getShooter();
				PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
				//if(PartyHandler.hasParty(l)) {
				//	PartyHandler.getParty(l).updateScoreBoard();
				//}
				int dmg = Integer.valueOf((String)((MetadataValue)a.getMetadata("DMG").get(0)).value()).intValue();
			    ItemStack bow = (ItemStack)((MetadataValue)a.getMetadata("Bow").get(0)).value();
			   // if(PartyHandler.isInSameParty(p, l))return;
			    double raw_finaldmg = getFinalDamage(p, dmg, l);
			 
				if(raw_finaldmg == -1) {
					return;
				}
			    if(raw_finaldmg == -10) {
					return;
				} else if (raw_finaldmg == -5) {
					return;
				} else {
					if(pl.isTogglePvP())return;
					damageplayer((int) raw_finaldmg, l, p, 0);
				}
			}
		}
	}

	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if(e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			if((e.getCause() == DamageCause.LAVA) || (e.getCause() == DamageCause.FIRE) || (e.getCause() == DamageCause.FIRE_TICK)) {
		
				double dmg = e.getDamage();
				boolean dead = false;
				if(p.getHealth() == 2) {
					dead = true;
				     if(dead) {
						// String name = Utils.getRankPrefForCommand(p.getName());
				    	 String name = "&7" + p.getName();
						 String msg = name + "&f burned to death";
						 if(dead) {
							 p.setHealth(0);
							 ChatUtils.sendMessage(msg, 20, p);
							// ChatHandler.sendMessageToLocalPlayers1(msg, p);
							 p.spigot().respawn();
							 dead = false;
							 return;
						 }
						 ChatUtils.sendMessage(msg, 20, p);
						 return;
				     } else {
				    	 e.setDamage(dmg);
				    	 return;
				     }
				}
			}
		}
	}
	
	public void playerDamagePlayer(Player p, Player l) {
		if(p.getLevel() < 1) {
			return;
		
		}
		PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
		if(l.hasMetadata("NPC"))return;
		//if(PartyHandler.isInSameParty(p, l))return;
		if(p.getItemInHand().getType().toString().contains("BOW")) { 
			p.playSound(p.getLocation(), Sound.BLOCK_PISTON_EXTEND, 1.0F, 1.5F);
			//EnergyHandler.removeEnergy(p, 0.20F);
			return;
		}
		ItemStack item = p.getItemInHand();
		double rawdmg = DamageUtils.getInstance().getDamage(item, "DMG");
		double raw_finaldmg = getFinalDamage(p, rawdmg, l);
		if(raw_finaldmg == -1) {
			tagged.remove(p);
			return;
		}
		else if(raw_finaldmg == -10) {
			raw_finaldmg = -2;
			
		} else if (raw_finaldmg == -5) {
			raw_finaldmg = -2;
		}
		damageplayer((int) getFinalDamage(p, rawdmg, l), l, p, 0);
	}
	
	
	public void playerDamageMobpole(LivingEntity ll, Player p) {
		PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
		List<Entity> aoe = p.getNearbyEntities(5.0D, 5.0D, 5.0D);
		for (Entity aoe_ent : aoe) {
			ll = (LivingEntity) aoe_ent;		
			double rawdmg = DamageUtils.getInstance().getDamage(p.getItemInHand(), "DMG");
			double raw_finaldmg = getFinalDamage(p, rawdmg, ll);
			if(raw_finaldmg == -1) {
				return;
			}
			if(raw_finaldmg > ll.getHealth()) {
				ll.setHealth(0);
				ll.damage(1);
				String name = Utils.colorCodes(Mob.getInstance().getName(ll));
				if(pl.isToggleDebug()) {
					p.sendMessage(ChatColor.RED + "            " + (int)raw_finaldmg + ChatColor.RED + "" + ChatColor.BOLD + " DMG " + ChatColor.RED + "-> " + ChatColor.RESET + name + " [" + 0 + "HP]");
				}
				drops(p, ll);
				Utils.updateHealthbar(ll);
				return;
			} else {
				String name = Utils.colorCodes(Mob.getInstance().getName(ll));
				if(pl.isToggleDebug()) {
					p.sendMessage(ChatColor.RED + "            " + (int)raw_finaldmg + ChatColor.RED + ChatColor.BOLD + " DMG " + ChatColor.RED + "-> " + ChatColor.RESET + name+ " [" + (int)(ll.getHealth() - raw_finaldmg) + "HP]"); 
				}
				ll.damage(raw_finaldmg);
				Utils.updateHealthbar(ll);
				return;
			}
		}
	}
	public void playerDamageMob(LivingEntity l, Player p) {
		if(p.getLevel() < 1) {
			return;
		}
		PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
		List<LivingEntity> processing_ent_dmg_event = new ArrayList<LivingEntity>();
		processing_ent_dmg_event.add(l);
		ItemStack item = p.getItemInHand();
		if(DamageUtils.isBow(item)) {
			p.playSound(p.getLocation(), Sound.BLOCK_PISTON_EXTEND, 1.0F, 1.5F);
			pushAwayPlayer(p, l, 1);
			//EnergyHandler.removeEnergy(p, 0.235F * 1.1F);
			return;
		}
		if(DamageUtils.isPolearm(item)) {
			playerDamageMobpole(l, p);
			return;
		
		
		}
	
		LivingEntity ll = (LivingEntity) l;
		double rawdmg = DamageUtils.getInstance().getDamage(item, "DMG");
		double raw_finaldmg = getFinalDamage(p, rawdmg, ll);
		if(raw_finaldmg == -1) {
			return;
		}
		if(raw_finaldmg > ll.getHealth()) {
			ll.setHealth(0);
			ll.damage(1);
			String name = Utils.colorCodes(Mob.getInstance().getName(ll));
			if(pl.isToggleDebug()) {
				p.sendMessage(ChatColor.RED + "            " + (int)raw_finaldmg + ChatColor.RED + "" + ChatColor.BOLD + " DMG " + ChatColor.RED + "-> " + ChatColor.RESET + name + " [" + 0 + "HP]");
			}
			drops(p, ll);
			Utils.updateHealthbar(ll);
			return;
		} else {
			String name = Utils.colorCodes(Mob.getInstance().getName(ll));
			if(pl.isToggleDebug()) {
				p.sendMessage(ChatColor.RED + "            " + (int)raw_finaldmg + ChatColor.RED + ChatColor.BOLD + " DMG " + ChatColor.RED + "-> " + ChatColor.RESET + name+ " [" + (int)(ll.getHealth() - raw_finaldmg) + "HP]"); 
			}
			ll.damage(raw_finaldmg);
			Utils.updateHealthbar(ll);
			return;
		}
	}

	

	

	public void block(Player p, LivingEntity l) {
		p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "OPPONENT BLOCK");
		
	}
	
	public void dodge(Player p, LivingEntity l) {
		p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "OPPONENT DODGE");
	
		
	}

	public static void debug(Player p, double dmg, LivingEntity l) {
		double health = 0.0D;
		PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
		PracticeServerPlayer pl1 = null;
		if(l instanceof Player) {
			Player pp = (Player) l;
			pl1 = PracticeServerPlayer.getPracticeServerPlayer(pp);
			if(dmg > pp.getHealth()) {
				health = 0.0D;
			} else {
				health = (pp.getHealth());
			}
			if(dmg == -10) {
				if(pl.isToggleDebug()) {
					p.sendMessage(Utils.colorCodes("           " + "&c&l*OPPONENT BLOCKED* (" + pp.getName() + ")"));
				}
				if(pl1.isToggleDebug()) {
					pp.sendMessage(Utils.colorCodes("           " + "&2&l*BLOCK* (" + p.getName() + ")"));
					pp.playSound(pp.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 2.0F, 1.0F);
					
				}
				dmg = -2;
			}
			if(dmg == -5) {
				if(pl.isToggleDebug()) {
					p.sendMessage(Utils.colorCodes("           " + "&c&l*OPPONENT DODGED* (" + pp.getName() + ")"));
				}
				if(pl1.isToggleDebug()) {
					pp.sendMessage(Utils.colorCodes("           " + "&a&l*DODGE* (" + p.getName() + ")"));
					pp.playSound(pp.getLocation(), Sound.ENTITY_ZOMBIE_INFECT, 2.0F, 1.5F);
				}
				dmg = -2;
			}
			if(dmg == -2) {
				return;
			}
			if(pl.isToggleDebug()) {
				p.sendMessage(ChatColor.RED + "            " + (int)dmg + ChatColor.RED + "" + ChatColor.BOLD + " DMG " + ChatColor.RED + "-> " + pp.getName());
			}
			if(pl1.isToggleDebug()) {
				pp.sendMessage(ChatColor.RED + "            -" + (int)dmg + ChatColor.RED + "" + ChatColor.BOLD + "HP " + ChatColor.GRAY + "[-" + (int)HealthUtils.getInstance().getArmor(p) + "%A -> -" + (int)getDMGReduce(p, (int) dmg) + "" + ChatColor.BOLD +  "DMG" + ChatColor.GRAY + "] " + ChatColor.GREEN + "[" + (int)health + ChatColor.BOLD + "HP" + ChatColor.GREEN + "]");
			}

			
		} else {
			if(dmg > l.getHealth()) {
				health = 0.0D;
			} else {
				health = (l.getHealth());
			}
			if(pl.isToggleDebug()) {
			p.sendMessage(ChatColor.RED + "            " + (int)dmg + ChatColor.RED + "" + ChatColor.BOLD + " DMG " + ChatColor.RED + "-> " + ChatColor.RESET + Mob.getInstance().getName(l) + " [" + (int)health + "HP]");
			}
		}
	}
	
	public void drops(Player p, LivingEntity l) {
		String mobname = Mob.getInstance().getMobFromMob(l);
		List<String> mobs = Mob.getInstance().getMobsThatAreCustom();
		for(String mob : mobs) {
			if(Mob.getInstance().hasCustomDrops(mobname)){
				List<String> drops = Mob.getInstance().getDrops(mobname);
				int rand = Utils.ir(0, drops.size());
				String drop = drops.get(rand);
			    if(Mob.getInstance().drop(drop)) {
			    	p.getWorld().dropItemNaturally(l.getLocation(), (Mob.getInstance().getCustomDrop(Mob.getInstance().getDropString(drop))));
			    	return;
			    }
			} else {
				String has = ",axe:%axe%,sword:%sword%,staff:%staff%,polearm:%polearm%,bow:%bow%,helmet:%helm%,chestplate:%chest%,legs:%leg%,boots:%boot%,";
				if(l.getEquipment().getHelmet() != null) {
					has = has.replaceAll("%helm%", "true");
				} else {
					has = has.replaceAll("%helm%", "false");
				}
				if(l.getEquipment().getChestplate() != null) {
					has = has.replaceAll("%chest%", "true");
				} else {
					has = has.replaceAll("%chest%", "false");
				}
				if(l.getEquipment().getLeggings() != null) {
					has = has.replaceAll("%leg%", "true");
				} else {
					has = has.replaceAll("%leg%", "false");
				}
				if(l.getEquipment().getBoots() != null) {
					has = has.replaceAll("%boot%", "true");
				} else {
					has = has.replaceAll("%boot%", "false");
				}
				ItemStack item = l.getEquipment().getItemInHand();
				if(DamageUtils.isAxe(item)) {
					has = has.replaceAll("%axe%", "true");
				} else {
					has = has.replaceAll("%axe%", "false");
				}
				if(DamageUtils.isSword(item)) {
					has = has.replaceAll("%sword%", "true");
				} else {
					has = has.replaceAll("%sword%", "false");
				}
				if(DamageUtils.isStaff(item)) {
					has = has.replaceAll("%staff%", "true");
				} else {
					has = has.replaceAll("%staff%", "false");
				}
				if(DamageUtils.isBow(item)) {
					has = has.replaceAll("%bow%", "true");
				} else {
					has = has.replaceAll("%bow%", "false");
				}
				if(DamageUtils.isPolearm(item)) {
					has = has.replaceAll("%polearm%", "true");
				} else {
					has = has.replaceAll("%polearm%", "false");
				}
				Mob.getInstance().drop(Mob.getInstance().getTier(mobname), l, l.getLocation(), p, has);
				return;
				

			}
		}
	}
	
	
	public static void noKnockback(LivingEntity le, final Location epicenter)
	  {
	    Main.plugin.getServer().getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable()
	    {
	      public void run()
	      {
	        le.setVelocity(new Vector());
	        
	        Vector unitVector = le.getLocation().toVector().subtract(le.getLocation().toVector()).normalize();
	        if (epicenter != null)
	        {
	          unitVector = le.getLocation().toVector().subtract(epicenter.toVector()).normalize();
	          
	          le.setVelocity(unitVector.multiply(0.1D));
	        }
	      }
	    }, 1L);
	  }
	
	  @EventHandler
	  public void addMeMaybe(EntityDamageByEntityEvent e) {
		  Random rand = new Random();
	    if (((e.getDamager() instanceof Player)) && 
	      (e.getEntity().getType() != EntityType.PLAYER) && 
	      ((e.getEntity() instanceof LivingEntity))) {
	      LivingEntity d = (LivingEntity)e.getEntity();
	      int randed = rand.nextInt(10) + 1;
	      if (randed == 2) {
	        if (Utils.chargedMobs.contains(d))
	          return;
	        Utils.chargedMobs.add(d);
	      }
	    }
	  }
	  
	  @EventHandler
	  public void onDeath(EntityDeathEvent e) {
		  if(!(e.getEntity() instanceof Player)) {
			  e.getDrops().clear();
			
			  
		  }
	  }

		public static void damageplayer(int dmg, Player l, Player p, int Block) {
			PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
			PracticeServerPlayer pl1 = PracticeServerPlayer.getPracticeServerPlayer(l);
			ApplicableRegionSet set = wg.getRegionManager(p.getWorld()).getApplicableRegions(p.getLocation());
			if (!set.allows(DefaultFlag.PVP)) {
				//Bukkit.getServer().broadcastMessage("pvp off");
				return;
			} else {
		
				if (l instanceof Player) {
					
					//if(PartyHandler.hasParty(l)) {
					//	PartyHandler.getParty(l).updateScoreBoard();
					//}
					if(p.getItemInHand().getType().toString().contains("BOW")) { 
						p.playSound(p.getLocation(), Sound.BLOCK_PISTON_EXTEND, 1.0F, 1.5F);
						pushAwayPlayer(p, l, 1);
						//EnergyHandler.removeEnergy(p, 0.235F * 1.1F);
						return;
					}
					boolean togglepvp = pl.isTogglePvP();
					if (togglepvp) {
						//Bukkit.getServer().broadcastMessage("pvp off");
						return;
					} else {
						Player pp = l;
						if(dmg == -10) {
							if(pl.isToggleDebug()) {
								p.sendMessage(Utils.colorCodes("           " + "&c&l*OPPONENT BLOCKED* (" + pp.getName() + ")"));
							}
							if(pl1.isToggleDebug()) {
								pp.sendMessage(Utils.colorCodes("           " + "&2&l*BLOCK* (" + p.getName() + ")"));
								pp.playSound(pp.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR , 2.0F, 1.0F);
								
							}
							dmg = -2;
						}
						if(dmg == -5) {
							if(pl.isToggleDebug()) {
								p.sendMessage(Utils.colorCodes("           " + "&c&l*OPPONENT DODGED* (" + pp.getName() + ")"));
							}
							if(pl1.isToggleDebug()) {
								pp.sendMessage(Utils.colorCodes("           " + "&a&l*DODGE* (" + p.getName() + ")"));
								pp.playSound(pp.getLocation(), Sound.ENTITY_ZOMBIE_INFECT, 2.0F, 1.5F);
							}
							dmg = -2;
						}
						if(dmg == -2) {
							return;
						}
						addCombatPvP(p);
						//addCombatPvP(l);
						dmg = (int) getDMGReduce(p, (int) dmg);
						if (dmg > l.getHealth()) {
							l.damage(dmg);
							//TODO add combat
							//	p.getItemInHand().setDurability((short) (p.getItemInHand().getDurability() + 1));
							debug(p, dmg, l);
							//p.sendMessage(ChatColor.RED + "            " + dmg + ChatColor.RED + ChatColor.BOLD + " DMG " + ChatColor.RED + "-> " + ChatColor.RESET + p.getCustomName() + " [" + "0" + "HP]");
							al(p, l, true);
							//Main.AddCombat(p);
							//RepairUtils.getInstance().useArmor(l);
							//RepairUtils.getInstance().useWeapon1(p);
							//add combat
							String name = "&7" + l.getName();
							String pname = "&7" + p.getName();
							String msg = name + " &fwas killed by &r" + pname + "&f with a(n) " + p.getItemInHand().getItemMeta().getDisplayName(); 
							ChatUtils.sendMessage(Utils.colorCodes(msg), 20, p);
							p.playSound(p.getLocation(), Sound.ENTITY_GENERIC_HURT, 1.0F, 1.0F);
							l.spigot().respawn();
							//ScoreBoardManager.setOverheadHP(l, (int) l.getHealth());
							return;
						} else {
							//RepairUtils.getInstance().useArmor(l);
							l.damage(dmg);
							//p.getItemInHand().setDurability((short) (p.getItemInHand().getDurability() + 1));
							debug(p, dmg, l);
							//p.sendMessage(ChatColor.RED + "            " +(int) dmg + ChatColor.RED + ChatColor.BOLD + " DMG " + ChatColor.RED + "-> " + ChatColor.RESET + p.getCustomName() + " [" + (int) l.getHealth() + "HP]");
							al(p, l, false);
							//RepairUtils.getInstance().useWeapon1(p);
							p.playSound(p.getLocation(), Sound.ENTITY_GENERIC_HURT, 1.0F, 1.0F);
							//ScoreBoardManager.setOverheadHP(l, (int) l.getHealth());
							return;
						}
						
					}
				}
					}
				
			}
		
		

	
		//		CombatHandler.setAlignment(p, Alignments.CHAOTIC);
		//setInt(p, "ChaoticTime", 300);
		//setInt(p, "NeutralTime", 0);
		public static void al(Player p, LivingEntity l, boolean dead) {
			PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
			PracticeServerPlayer pl1 = PracticeServerPlayer.getPracticeServerPlayer((Player) l);
			boolean TogglePvP = pl.isTogglePvP();
			boolean ToggleChao = pl.isToggleChaos();
			boolean lTogglePvP =  pl1.isTogglePvP();
			boolean lToggleChao = pl1.isToggleChaos();
			boolean ischao = pl.getAlignment().equals(Alignment.CHAOTIC);
			boolean isneut = pl.getAlignment().equals(Alignment.NEUTRAL);
			boolean islawful = pl.getAlignment().equals(Alignment.LAWFUL);
			boolean lischao = pl1.getAlignment().equals(Alignment.CHAOTIC);
			boolean lisneut = pl1.getAlignment().equals(Alignment.NEUTRAL);
			boolean lislawful = pl1.getAlignment().equals(Alignment.LAWFUL);
			int neuttimer = pl.getAlignmentTime();
			int chaotimer = pl.getAlignmentTime();
			if (!(l instanceof Player)) {
				return;
			} else if((islawful) && (!lislawful)) {
				if(ToggleChao) {
					return;
				}
			} else if (ischao == false && isneut == false && islawful == true && lisneut == false && lischao == false && lislawful == true) {
				if (ToggleChao == true && TogglePvP == true) {
					return;
				} else {
					PlayerAlignment.getPlayerAlignment(p).setAlignment(Alignment.NEUTRAL);
					PlayerAlignment.getPlayerAlignment(p).setAlignmentTime(60);
					pl.getPlayer().setPlayerListName(pl.getAlignment().getTabNameColor() + p.getName());
					return;
				}
			} else if (ischao == false && isneut == false && islawful == true && lisneut == true && lischao == false && lislawful == false) {
				if(TogglePvP == true) {
					return;
				} else {
					PlayerAlignment.getPlayerAlignment(p).setAlignment(Alignment.NEUTRAL);
					PlayerAlignment.getPlayerAlignment(p).setAlignmentTime(60);
					pl.getPlayer().setPlayerListName(pl.getAlignment().getTabNameColor() + p.getName());
					return;
				}
			} else if (ischao == false && isneut == true && islawful == false && lisneut == false && lischao == false && lislawful == true) {
				if (ToggleChao == true && TogglePvP == true) {
					return;
				} else {
					if (dead) {
						PlayerAlignment.getPlayerAlignment(p).setAlignment(Alignment.CHAOTIC);
						PlayerAlignment.getPlayerAlignment(p).setAlignmentTime(300);
						pl.getPlayer().setPlayerListName(pl.getAlignment().getTabNameColor() + p.getName());
						return;
					} else {
						PlayerAlignment.getPlayerAlignment(p).setAlignment(Alignment.NEUTRAL);
						PlayerAlignment.getPlayerAlignment(p).setAlignmentTime(60);
						pl.getPlayer().setPlayerListName(pl.getAlignment().getTabNameColor() + p.getName());
						return;
					}

				}
			} else if (ischao == false && isneut == true && islawful == false && lisneut == true && lischao == false && lislawful == false) {
				if (TogglePvP == true) {
					return;
				} else {
					PlayerAlignment.getPlayerAlignment(p).setAlignment(Alignment.NEUTRAL);
					PlayerAlignment.getPlayerAlignment(p).setAlignmentTime(60);
					pl.getPlayer().setPlayerListName(pl.getAlignment().getTabNameColor() + p.getName());
					return;
				}
			} else if (ischao == false && isneut == true && islawful == false && lisneut == false && lischao == true && lislawful == false) {
				if (TogglePvP == true) {
					return;
				} else {
					PlayerAlignment.getPlayerAlignment(p).setAlignment(Alignment.NEUTRAL);
					PlayerAlignment.getPlayerAlignment(p).setAlignmentTime(60);
					pl.getPlayer().setPlayerListName(pl.getAlignment().getTabNameColor() + p.getName());
					return;
				}
			} else if (ischao == true && isneut == false && islawful == false && lisneut == false && lischao == false && lislawful == true) {
				if (ToggleChao == true && TogglePvP == true) {
					return;
				} else {
					if(dead) {
						//p.sendMessage(ChatColor.RED + "+300 seconds added to chaotic time due to killing a lawful player.");
						p.sendMessage("�cLAWFUL player slain, �l+300s �cadded to Chaotic timer");
						pl.setAlignmentTime((pl.getAlignmentTime() + 300));
						pl.setAlignment(Alignment.CHAOTIC);
						pl.getPlayer().setPlayerListName(pl.getAlignment().getTabNameColor() + p.getName());
						return;
						
					}
				}
				
			} else if (ischao == true && isneut == false && islawful == false && lisneut == false && lischao == true && lislawful == false) {
				if (TogglePvP == true) {
					return;
				} else {
					if (dead) {
						pl.setAlignmentTime((pl.getAlignmentTime() - 160));
						p.sendMessage("�cCHAOTIC player slain, �l-160s �cremoved from Chaotic timer");
						return;
					}

				}

			} else if (ischao == false && isneut == false && islawful == true && lisneut == false && lischao == true && lislawful == false) {
				if (TogglePvP == true) {
					return;
				} else {
					PlayerAlignment.getPlayerAlignment(p).setAlignment(Alignment.NEUTRAL);
					PlayerAlignment.getPlayerAlignment(p).setAlignmentTime(60);
					pl.getPlayer().setPlayerListName(pl.getAlignment().getTabNameColor() + p.getName());
					return;
				}


			/*} else if (ischao == false && isneut == false && islawful == true && lisneut == false && lischao == true && lislawful == false) {
				if (TogglePvP == true && ToggleChao == true) {
					return;
				} else {
					if (l.isDead()) {
						CombatHandler.setInt(p, "NeutralTime", 0);
						CombatHandler.setInt(p, "ChaoticTime", chaotimer + 300);
						CombatHandler.setAlignment(p, Alignments.CHAOTIC);
						p.sendMessage(ChatColor.RED + "+300 seconds added to chaotic time due to killing a lawful player.");
					}
				}
			}*/
			}
		}

	  
		public static void addCombatPvP(Player p) {
			PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
			 if(RegionUtils.isSafeZone(p.getLocation())) return;
			 pl.setCombatTime(10);
			 pl.setCombat(true);
			
		}
		

		@EventHandler
		public void onLava(EntityDamageEvent e) {
			if(e.getCause() == DamageCause.LAVA)e.setCancelled(true);
		}
	
		
}
	


package com.practiceserver.damage;

import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.practiceserver.Main;
import com.practiceserver.utils.MyMetadata;

/*
Copyright � matt11matthew 2016
*/

public class Bows implements Listener {
	
	private static Bows instance = new Bows();
	
	public static Bows getInstance() {
		return instance;
	}
	//
	@EventHandler
	public void onShoot(ProjectileLaunchEvent e) {
		if(e.getEntity() instanceof Arrow) {
			if(e.getEntity().getShooter() instanceof Player) {
				Player p = (Player)e.getEntity().getShooter();
				if(p.getItemInHand() == null) {
					return;
				}
				if(p.getItemInHand().getItemMeta().getLore() == null) {
					return;
				}
				double dmg = DamageUtils.getInstance().getDamage(p.getItemInHand(), "DMG");
				e.getEntity().setMetadata("DMG", new MyMetadata(Main.plugin, String.valueOf((int)dmg)));
				e.getEntity().setMetadata("Bow", new MyMetadata(Main.plugin, p.getItemInHand()));
			}
		}
	}

	public boolean isVanillaArrow(ItemStack item) {
		if(!item.getItemMeta().hasDisplayName()) {
			if(item.getType() == Material.ARROW) {
				return true;
			}
		}
		return false;
	}
	
	public void removeVanillaArrows(Player p) {
		for(ItemStack item : p.getInventory().getContents()) {
			if(item != null) {
				if(isVanillaArrow(item)) {
					//p.getInventory().remove(item);
				}
			}
		}
	}
	
	  @EventHandler
	  public void onTag(EntityDamageEvent e) {
	    if ((e.getEntity() instanceof Player)) {
	      if (e.getDamage() <= 0.0D) {
	        return;
	      }
	      final Player p = (Player)e.getEntity();
	      if(e.getCause() == DamageCause.FALL)return;
	      Damage.tagged.add(p);
	      new BukkitRunnable() {
	        public void run() {
	        	Damage.tagged.remove(p);
	        }
	        
	      }.runTaskLater(Main.plugin, 200L);
	    }
	  }
	  @EventHandler(priority=EventPriority.LOWEST)
	  public void onKickLog(PlayerKickEvent e)
	  {
	    Player p = e.getPlayer();
	    if (e.getReason().equals("Illegal characters in chat")) {
	      e.setCancelled(true);
	    } else
	      for (int i = 0; i < Damage.tagged.size(); i++)
	    	  Damage.tagged.remove(p);
	  }
	  
	  @EventHandler
	  public void onHitTag(EntityDamageByEntityEvent e) {
	    if (((e.getDamager() instanceof Player)) && 
	      ((e.getEntity() instanceof LivingEntity))) {
	      if (e.getDamage() <= 0.0D) {
	        return;
	      }
	      final Player p = (Player)e.getDamager();
	      Damage.tagged.add(p);
	      new BukkitRunnable() {
	        public void run() {
	        	Damage.tagged.remove(p);
	        }
	        
	      }.runTaskLater(Main.plugin, 200L);
	    }
	  }
}

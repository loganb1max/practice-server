package com.practiceserver;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.practiceserver.banks.BankHandler;
import com.practiceserver.banks.commands.CommandBank;
import com.practiceserver.chat.ChatHandler;
import com.practiceserver.damage.Bows;
import com.practiceserver.damage.DMG;
import com.practiceserver.damage.Damage;
import com.practiceserver.damage.Staffs;
import com.practiceserver.database.Config;
import com.practiceserver.database.DataHandler;
import com.practiceserver.events.PlayerCustomEventListener;
import com.practiceserver.health.HealthMechanics;
import com.practiceserver.itemgenerator.ItemGenerator;
import com.practiceserver.lootchest.LootChestManager;
import com.practiceserver.mob1.Mob;
import com.practiceserver.mounts.MountHandler;
import com.practiceserver.mounts.MountUtils;
import com.practiceserver.network.NetworkHandler;
import com.practiceserver.player.InventoryHandler;
import com.practiceserver.player.PlayerAlignment;
import com.practiceserver.player.PlayerListener;
import com.practiceserver.player.PracticeServerPlayer;
import com.practiceserver.player.commands.CommandSC;
import com.practiceserver.player.commands.CommandSayall;
import com.practiceserver.player.commands.CommandSetAlignment;
import com.practiceserver.player.commands.CommandTest;
import com.practiceserver.player.commands.CommandZone;
import com.practiceserver.player.commands.toggles.CommandDebug;
import com.practiceserver.player.commands.toggles.CommandToggleChaos;
import com.practiceserver.player.commands.toggles.CommandToggleFilter;
import com.practiceserver.player.commands.toggles.CommandTogglePvP;
import com.practiceserver.shards.Server;
import com.practiceserver.shards.ShardHandler;
import com.practiceserver.shards.commands.CommandShard;
import com.practiceserver.spawners.SpawnerManager;
import com.practiceserver.spawners.commands.CommandHideMS;
import com.practiceserver.spawners.commands.CommandShowMS;
import com.practiceserver.staff.StaffHandler;
import com.practiceserver.utils.Utils;

public class Main extends JavaPlugin {
	
	public static Main plugin;
	
	public void onEnable() {
		plugin = this;
		registerListeners();
		registerCommands();
		NetworkHandler.setup();
		Config.setup();
		runTasks();
		SpawnerManager sm = new SpawnerManager();
		sm.loadAllSpawners();
		sm.setup();
		ItemGenerator.loadModifiers();
		ShardHandler.setup();
		InventoryHandler.setSystemPath();
		Mob mob = new Mob();
		mob.setupMobs();
		LootChestManager lootchest = new LootChestManager();
		lootchest.setup();
		
	}
	
	public void onDisable() {
		ShardHandler.close();
		BankHandler.close();
	}
	
	public void registerListeners() {
		PluginManager p = Bukkit.getPluginManager();
		p.registerEvents(new PlayerListener(), this);
		p.registerEvents(new PlayerCustomEventListener(), this);
		//p.registerEvents(new MobHandler(), this);//
		p.registerEvents(new SpawnerManager(), this);
		p.registerEvents(new ShardHandler(), this);
		p.registerEvents(new InventoryHandler(), this);
		p.registerEvents(new Bows(), this);
		p.registerEvents(new Mob(), this);
		p.registerEvents(new Staffs(), this);
		p.registerEvents(new Damage(), this);
		p.registerEvents(new HealthMechanics(), this);
		p.registerEvents(new DMG(), this);
		p.registerEvents(new MountHandler(), this);
		p.registerEvents(new MountUtils(), this);
		p.registerEvents(new BankHandler(), this);
		p.registerEvents(new StaffHandler(), this);
		p.registerEvents(new ChatHandler(), this);
		p.registerEvents(new LootChestManager(), this);
	}
	
	public void registerCommands() {
		getCommand("zone").setExecutor(new CommandZone());
		getCommand("setalignment").setExecutor(new CommandSetAlignment());
		getCommand("sayall").setExecutor(new CommandSayall());
		getCommand("sc").setExecutor(new CommandSC());
		getCommand("test").setExecutor(new CommandTest());
		getCommand("shard").setExecutor(new CommandShard());
		getCommand("showms").setExecutor(new CommandShowMS());
		getCommand("hidems").setExecutor(new CommandHideMS());
		getCommand("bank").setExecutor(new CommandBank());
		getCommand("debug").setExecutor(new CommandDebug());
		getCommand("togglepvp").setExecutor(new CommandTogglePvP());
		getCommand("togglefilter").setExecutor(new CommandToggleFilter());
		getCommand("togglechaos").setExecutor(new CommandToggleChaos());
	}
	
	public void runTasks() {
		combatTimeTask();
		regenTask();
		bossBarTask();
		updatePlayerShard();
		updateShardNumber();
		MountUtils.mountHorseTask();
		BankHandler.bankTask();
	}
	
	public void updateShardNumber() {
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			public void run() {
				for(String shard : ShardHandler.getShards()) {
					Server server = new Server(shard);
					DataHandler.getInstance().setServerOnlinePlayersCount(server.getName(), server.getOnlinePlayers());
				}
				
			}
		}, 5L, 5L);
		
		
	}

	public void combatTimeTask() {
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			public void run() {
				for (Player p : Bukkit.getServer().getOnlinePlayers()) {
					PlayerAlignment.getPlayerAlignment(p).tick();
				}
			}
		}, 20L, 20L);
	}
	
	public void bossBarTask() {
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			public void run() {
				for(Player p : Bukkit.getServer().getOnlinePlayers()) {
					PlayerListener.updateBossBar(p);
				}
				
			}
		}, 20L, 20L);
	}
	
	public void updatePlayerShard() {
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			public void run() {
				for(Player p : Bukkit.getServer().getOnlinePlayers()) {
					PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
					String shard = Utils.getShard1();
					Server server = new Server(shard);
					pl.setShard(server.getName());
					
					
					
					
					
				}
				
			}
		}, 20L, 20L);
	}
	
	public void regenTask() {
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			public void run() {
				for(Player p : Bukkit.getServer().getOnlinePlayers()) {
					HealthMechanics.getInstance().regen(p);
				}
				
			}
		}, 1L, 20L);
	}
	
}

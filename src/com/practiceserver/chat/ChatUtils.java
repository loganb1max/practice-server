package com.practiceserver.chat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.practiceserver.utils.Utils;

public class ChatUtils {
	

	public static void sendMessage(String msg, double d, LivingEntity l) {
		List<Entity> entitys = l.getNearbyEntities(d, d, d);
		List<Player> players = new ArrayList<Player>();
		for (Entity e : entitys) {
			if (e instanceof Player) {
				if (!players.contains((Player) e)) {
					players.add((Player) e);
				}
				for (Player pl : players) {
					pl.sendMessage(Utils.colorCodes(msg));	
				}
				if (players.size() == 1) {
					l.sendMessage(Utils.colorCodes("&7&oNo one heard you."));
					return;
				}
				return;
			}
		}
	}
	
	public static String fixCaps(String s) {
		String return_string = "";
		if (s.contains(" ")) {
			for (String ss : s.split(" ")) {
				String s1 = wordToLowerCase(ss);
				return_string = return_string + " " + s1;
			}
		} else {
			String s1 = wordToLowerCase(s);
			return_string = return_string + " " + s1;
		}
		return return_string.trim();
	}
	
	public static String filter(String s) {
		String return_string = "";
		if (s.contains(" ")) {
			for (String ss : s.split(" ")) {
				String s1 = replaceBannedWords(ss);
				return_string = return_string + " " + s1;
			}
		} else {
			String s1 = replaceBannedWords(s);
			return_string = return_string + " " + s1;
		}
		return return_string.trim();
	}
	
	public static String generateDots(String bannedword) {
		String return_string = "";
		for (int i = 0; i < bannedword.length(); i++) {
			return_string = return_string + "*";
		}
		return return_string.trim();
	}
	
	public static String replaceBannedWords(String s) {
		for (String ss : bannedwords) {
			if (s.toLowerCase().contains(ss.toLowerCase())) {
				s = s.toLowerCase();	
				s = s.replaceAll(ss, generateDots(ss));
				return s.trim();
			}
		}
		return s;
	
	}
	
	public static void sendMessageToLocalPlayers(String msg, Player sender) {
		List<Player> to_send = new ArrayList<Player>();
        for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
            if (pl.getName().equalsIgnoreCase(sender.getName())) {
                continue;
            }
            if (!pl.getWorld().getName().equalsIgnoreCase(pl.getWorld().getName())) {
                continue;
            }
            if (pl.getLocation().distanceSquared(sender.getLocation()) > 16384) {
                continue;
            }
            to_send.add(pl);
        }
        if (to_send.size() <= 0) {
            sender.sendMessage(Utils.colorCodes(msg));
            sender.sendMessage(Utils.colorCodes("&7&oNo one heard you."));
            return;
        }
        for (Player pl : to_send) {
            pl.sendMessage(Utils.colorCodes(msg));
        }
        sender.sendMessage(Utils.colorCodes(msg));
        return;
	}
 	
	public static String wordToLowerCase(String s) {
		if (s.substring(0, 1).startsWith(s.substring(0, 1).toUpperCase())) {
			s = s.toUpperCase();
			String l = s.substring(1, s.length());
			l = l.toLowerCase().trim();
			String ll = s.substring(0, 1);
			return ll + l;
		}
		return s;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static List<String> bannedwords = new ArrayList<String>(Arrays.asList("shit", "fuck", "cunt", "bitch", "whore", "slut", "wank", "asshole", "cock",
            "dick", "clit", "homo", "fag", "queer", "nigger", "dike", "dyke", "retard", "motherfucker", "vagina", "boob", "pussy", "rape", "gay", "penis",
            "cunt", "titty", "anus", "faggot", "fk", "fuk"));
}
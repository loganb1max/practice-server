package com.practiceserver.chat;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

import com.practiceserver.events.PlayerChatEvent;
import com.practiceserver.player.PlayerListener;
import com.practiceserver.player.PracticeServerPlayer;
import com.practiceserver.utils.Utils;

public class ChatHandler implements Listener {
	
	@EventHandler
	public void onDMG(EntityDamageEvent e) {
		if (e.getEntity() instanceof Player) {
			PlayerListener.updateBossBar((Player) e.getEntity());
		}
	}
	
	@EventHandler
	public void onChat(org.bukkit.event.player.PlayerChatEvent e) {
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onChat(PlayerChatEvent e) {
		Player p = e.getPlayer();
		PracticeServerPlayer pl = e.getPracticeServerPlayer();
		String msg = e.getMessage();
		msg = ChatUtils.fixCaps(msg);
		msg = ChatUtils.filter(msg);
		msg = ChatColor.stripColor(msg);
		String name = "";
		if (p.isOp()) {
			name = "&b&lGM &7" + p.getName();
		} else {
			name = "&7" + p.getName();
		}
		String f_msg = Utils.colorCodes(name + "&7: &f" + msg);
		if (!e.isCancelled()) {
			ChatUtils.sendMessageToLocalPlayers(f_msg, p);
		}
		return;
	}

}

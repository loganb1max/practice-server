package com.practiceserver.mounts;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Horse.Color;
import org.bukkit.entity.Horse.Variant;
import org.bukkit.entity.Player;
import org.bukkit.entity.Spider;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerAnimationEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.bukkit.inventory.ItemStack;

import com.practiceserver.Main;
import com.practiceserver.damage.Damage;
import com.practiceserver.mobs.Mob;
import com.practiceserver.player.PracticeServerPlayer;
import com.practiceserver.shards.Server;
import com.practiceserver.utils.Utils;

public class MountUtils implements Listener {
	
	public static HashMap<String, Horse> mount_map = new HashMap<String, Horse>();
	public static HashMap<String, ItemStack> saddle_map = new HashMap<String, ItemStack>();
	public static HashMap<String, Location> mounting_location = new HashMap<String, Location>();
    public static ConcurrentHashMap<String, Integer> summon_mount = new ConcurrentHashMap<String, Integer>();
	
	public static void mountHorseTask() {
		MountHandler m = new MountHandler();
		Main.plugin.getServer().getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
			public void run() {
				   for (Entry<String, Integer> data : summon_mount.entrySet()) {
	                    final String p_name = data.getKey();
	                    int seconds = data.getValue();

	                    if (Bukkit.getPlayer(p_name) == null) {
	                        summon_mount.remove(p_name);
	                        mounting_location.remove(p_name);
	                        saddle_map.remove(p_name);
	                        continue;
	                    }

	                    final Player pl = Bukkit.getPlayer(p_name);
	                    final PracticeServerPlayer p = PracticeServerPlayer.getPracticeServerPlayer(pl);
	                    seconds--;

	                    if (seconds <= 0) {
	                       ItemStack is = saddle_map.get(p_name);
	                       if (m.isSaddle(is)) {	               
	                    	   mountHorse(pl, is);
	                    	   saddle_map.remove(p_name);
	                    	   mounting_location.remove(p_name);
	                    	   summon_mount.remove(p_name);
	                    	   return;
	                       }
	                    }
	                    p.msg("&f&lSUMMONING &f... " + seconds + "&ls");

	                    ItemStack is = saddle_map.get(p_name);
	                    if (m.isSaddle(is)) {
	                        try {
	                        	pl.spawnParticle(Particle.SPELL, pl.getLocation().add(0, 0.15, 0), (int) new Random().nextFloat(), new Random().nextFloat(), new Random().nextFloat(), 0.5F, 80);
	                        } catch (Exception err) {
	                            err.printStackTrace();
	                        }
	                    }

	                    summon_mount.put(p_name, seconds);
	                }
	            }
	        }, 5 * 20L, 20L);
	}
	
    @EventHandler
    public void onPlayerMoveEvent(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
        if (mounting_location.containsKey(p.getName())) {
            Location loc = mounting_location.get(p.getName());
            if (!p.getWorld().getName().equalsIgnoreCase(loc.getWorld().getName()) || e.getTo().distanceSquared(loc) >= 2) {
                mounting_location.remove(p.getName());
                summon_mount.remove(p.getName());
                saddle_map.remove(p.getName());
                pl.msg("&cMount Summon - " + ChatColor.BOLD + "CANCELLED");
                return;
            }
        }
    }
	
    @EventHandler
    public void onPlayerAnimation(PlayerAnimationEvent e) {
        Player p = e.getPlayer();
        PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
        if (summon_mount.containsKey(p.getName())) {
            mounting_location.remove(p.getName());
            summon_mount.remove(p.getName());
            saddle_map.remove(p.getName());
            pl.msg("&cMount Summon - " + ChatColor.BOLD + "CANCELLED");
            e.setCancelled(true);
            return;
        }
    }
    
    @EventHandler
    public void leaveMount(VehicleExitEvent e) {
    	if (e.getExited() instanceof Player) {
    		Player p = (Player) e.getExited();
            Entity horse = e.getVehicle();
            if ((horse instanceof Horse || horse instanceof Spider) && mount_map.containsKey(p.getName())) {
                Entity ent = mount_map.get(p.getName());
                // ent.eject();
                ent.remove();
                mount_map.remove(p.getName());
            }
    	}
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamage(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player && e.getDamage() > 0 && !(e.isCancelled())) {
            Player p = (Player) e.getEntity();
            PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
            if (summon_mount.containsKey(p.getName())) {
                summon_mount.remove(p.getName());
                mounting_location.remove(p.getName());
                saddle_map.remove(p.getName());
                pl.msg("&cMount Summon - " + ChatColor.BOLD + "CANCELLED");
                return;
            }
            if (mount_map.containsKey(p.getName())) {
            	if (e.getDamager() instanceof Player) {
            		PracticeServerPlayer pl1 = PracticeServerPlayer.getPracticeServerPlayer((Player) e.getDamager());
            		pl1.setCombatTime(10);
            		pl1.setCombat(true);
            		Damage.tagged.add(pl1.getPlayer());
            	}
            	Damage.tagged.add(pl.getPlayer());
            	
                Entity mount = mount_map.get(p.getName());
                mount_map.remove(p.getName());
                mount.eject();
                mount.remove();
                if (mount instanceof Horse) {
                    e.setCancelled(true);
                    e.setDamage(0.0D);
                }
                return;
            }
            
        }

    }
    
	public static void mountHorse(Player player, ItemStack saddle) {
		MountHandler m = new MountHandler();
		if (m.isSaddle(saddle)) {
			Horse horse = (Horse) Bukkit.getWorld(new Server(Utils.getShard1()).getWorldName()).spawnEntity(player.getLocation(), EntityType.HORSE);
			horse.setVariant(Variant.HORSE);
			horse.setAdult();
			horse.setTamed(true);
			horse.setStyle(Horse.Style.NONE);
			horse.setOwner(player);
			if (player.getName().equalsIgnoreCase("matt11matthew")) {
				horse.setColor(Color.WHITE);
			} else {
				horse.setColor(Color.BROWN);
			}
			horse.setDomestication(100);
			horse.getInventory().setItem(0, new ItemStack(Material.SADDLE));
			int tier = Mob.getItemTier(saddle);
			if (!mount_map.containsKey(player.getName())) {
				mount_map.put(player.getName(), horse);
			}
			if (tier == 3) {
				horse.getInventory().setItem(1, new ItemStack(Material.IRON_BARDING));
				horse.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).setBaseValue(0.25D);
				horse.setJumpStrength(0.75D);
			}
			if (tier == 4) {
				horse.getInventory().setItem(1, new ItemStack(Material.DIAMOND_BARDING));
				horse.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).setBaseValue(0.30D);
				horse.setJumpStrength(0.80D);
			}
			if (tier == 5) {
				horse.getInventory().setItem(1, new ItemStack(Material.GOLD_BARDING));
				horse.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).setBaseValue(0.40D);
				horse.setJumpStrength(0.90D);
			}
			horse.setPassenger(player);
			return;
		}
	}

}

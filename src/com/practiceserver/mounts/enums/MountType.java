package com.practiceserver.mounts.enums;

import org.bukkit.inventory.ItemStack;

import com.practiceserver.mounts.MountHandler;

public enum MountType {
	
	T2_MOUNT(MountHandler.t2_horse_mount, 3000),
	T3_HORSE(MountHandler.t3_horse_armor, 7000),
	T4_HORSE(MountHandler.t4_horse_armor, 15000),
	T5_HORSE(MountHandler.t5_horse_armor, 30000);
	
	ItemStack item;
	int cost;
	
	MountType(ItemStack item, int cost) {
		this.item = item;
		this.cost = cost;
		
	}
	
	public ItemStack getItem() {
		return this.item;
	}
	
	public int getCost() {
		return this.cost;
	}

	public ItemStack getItemPriced() {
		return new MountHandler().setPrice(this.item, this.cost);
	}
	
}

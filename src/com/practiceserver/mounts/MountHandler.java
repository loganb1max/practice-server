package com.practiceserver.mounts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.practiceserver.banks.BankHandler;
import com.practiceserver.database.DataHandler;
import com.practiceserver.events.NpcRightClickEvent;
import com.practiceserver.events.PlayerChatEvent;
import com.practiceserver.health.HealthUtils;
import com.practiceserver.mobs.ItemMechanics;
import com.practiceserver.mobs.Mob;
import com.practiceserver.mounts.enums.MountType;
import com.practiceserver.player.PracticeServerPlayer;
import com.practiceserver.utils.Utils;

public class MountHandler implements Listener {
	
	public static List<Player> inMenu = new ArrayList<Player>();
	public static HashMap<Player, MountType> buyingMount = new HashMap<Player, MountType>();
	
    public static ItemStack t2_horse_mount = ItemMechanics.signCustomItem(Material.SADDLE, (short) 0, ChatColor.GREEN.toString() + "Old Horse Mount",
            ChatColor.RED.toString() + "Speed: 120%" + "," + ChatColor.GRAY.toString() + ChatColor.ITALIC.toString() + "An old brown starter horse." + ","
                    + ChatColor.GRAY.toString() + "Permanent Untradeable");
    public static ItemStack t3_horse_mount = ItemMechanics.signCustomItem(
            Material.SADDLE,
            (short) 0,
            ChatColor.AQUA.toString() + "Traveler's Horse Mount",
            ChatColor.RED.toString() + "Speed: 140%" + "," + ChatColor.RED.toString() + "Jump: 105%" + "," + ChatColor.GRAY.toString()
                    + ChatColor.ITALIC.toString() + "A standard healthy horse." + "," + ChatColor.GRAY.toString() + "Permanent Untradeable");
    public static ItemStack t4_horse_mount = ItemMechanics.signCustomItem(
            Material.SADDLE,
            (short) 0,
            ChatColor.LIGHT_PURPLE.toString() + "Knight's Horse Mount",
            ChatColor.RED.toString() + "Speed: 160%" + "," + ChatColor.RED.toString() + "Jump: 110%" + "," + ChatColor.GRAY.toString()
                    + ChatColor.ITALIC.toString() + "A fast well-bred horse." + "," + ChatColor.GRAY.toString() + "Permanent Untradeable");
    public static ItemStack t5_horse_mount = ItemMechanics.signCustomItem(Material.SADDLE,
    		(short) 0,
            ChatColor.YELLOW.toString() + "War Stallion Mount",
            ChatColor.RED.toString() + "Speed: 200%" + "," + ChatColor.RED.toString() + "Jump: 120%" + "," + ChatColor.GRAY.toString()
                    + ChatColor.ITALIC.toString() + "A trusty powerful steed." + "," + ChatColor.GRAY.toString() + "Permanent Untradeable");
    
    public static ItemStack t3_horse_armor = ItemMechanics.signCustomItem(Material.IRON_BARDING, (short) 0, ChatColor.AQUA.toString() + "Iron Horse Armor",
            ChatColor.RED.toString() + "Speed: 140%" + "," + ChatColor.RED.toString() + "Jump: 105%" + "," + ChatColor.RED.toString() + ChatColor.BOLD + "REQ:"
                    + ChatColor.GREEN.toString() + " Old Horse Mount" + "," + ChatColor.GRAY.toString() + ChatColor.ITALIC.toString()
                    + "A well made Iron-plated armor piece for your mount." + "," + ChatColor.GRAY.toString() + "Permanent Untradeable");
    public static ItemStack t4_horse_armor = ItemMechanics.signCustomItem(Material.DIAMOND_BARDING, (short) 0, ChatColor.LIGHT_PURPLE.toString()
            + "Diamond Horse Armor", ChatColor.RED.toString() + "Speed: 160%" + "," + ChatColor.RED.toString() + "Jump: +110%" + "," + ChatColor.RED.toString()
            + ChatColor.BOLD.toString() + "REQ:" + ChatColor.AQUA + " Traveler's Horse Mount" + "," + ChatColor.GRAY.toString() + ChatColor.ITALIC.toString()
            + "Powerful brilliant diamond horse armor." + "," + ChatColor.GRAY.toString() + "Permanent Untradeable");
    public static ItemStack t5_horse_armor = ItemMechanics.signCustomItem(Material.GOLD_BARDING, (short) 0, ChatColor.YELLOW.toString()
            + "Gold Horse Armor", ChatColor.RED.toString() + "Speed: +200%" + "," + ChatColor.RED.toString() + "Jump: +120%" + "," + ChatColor.RED.toString()
            + ChatColor.BOLD.toString() + "REQ:" + ChatColor.LIGHT_PURPLE.toString() + " Knight's Horse Mount" + "," + ChatColor.GRAY.toString()
            + ChatColor.ITALIC.toString() + "A legendary forged intricate golden armour piece." + "," + ChatColor.GRAY.toString() + "Permanent Untradeable");

    
    @EventHandler
    public void onNpcRightClick(NpcRightClickEvent e) {
    	if (e.getNPCName().equalsIgnoreCase("Animal Tamer")) {
    		setupInventory(e.getPlayer());
    	
    		
    	}
    }
    
    public boolean hasPrice(ItemStack is) {
    	if (HealthUtils.getInstance().getPriceFromItem(is) > 0) {
    		return true;
    	}
		return false;
    }
    
    public ItemStack removePrice(ItemStack is) {
    	if (hasPrice(is)) {
    		ItemMeta m = is.getItemMeta();
    		List<String> lore = m.getLore();
    		lore.remove(Utils.colorCodes("&aPrice: &f" + HealthUtils.getInstance().getPriceFromItem(is) + "g"));
    		m.setLore(lore);
    		is.setItemMeta(m);
    		return is;
    	}
		return is;
    }
    
    public ItemStack setPrice(ItemStack is, int price) {
    	removePrice(is);
    	ItemMeta m = is.getItemMeta();
		List<String> lore = m.getLore();
		lore.add(Utils.colorCodes("&aPrice: &f" + price + "g"));
		m.setLore(lore);
		is.setItemMeta(m);
		return is;
    }

    
    @EventHandler
    public void onClose(InventoryCloseEvent e) {
    	Player p = (Player) e.getPlayer();
    	if (inMenu.contains(p)) {
    		inMenu.remove(p);	
    	}
    }
    
    @EventHandler
    public void onPracticeServerChatEvent(PlayerChatEvent e) {
    	Player p = e.getPlayer();
    	PracticeServerPlayer pl = e.getPracticeServerPlayer();
    	if (buyingMount.containsKey(p)) {
    		e.setCancelled(true);
    		MountType mount = buyingMount.get(p);
    		int cost = mount.getCost();
    		ItemStack mountItem = mount.getItem();
    		if (e.getMessage().equalsIgnoreCase("y")) {
    			if (!hasGems(p, cost)) {
    				pl.msg("&cYou do not have enough gems to purchase this mount.");
    				pl.msg("&c&lCOST: &c" + cost + "&lG");
    				buyingMount.remove(p);
    				return;
    			} 
    			if (p.getInventory().firstEmpty() == -1) {
    				pl.msg("&cNo space available in inventory. Type 'cancel' or clear some room.");
    				buyingMount.remove(p);
    				return;
    			}
    			takeGems(p, cost);
    			pl.msg("&c&l-&c" + cost + "&lG");
    			pl.msg("&aTransaction successful.");
    			pl.playSound(Sound.ENTITY_EXPERIENCE_ORB_TOUCH, 1.0F, 1.0F);
    			p.getInventory().setItem(p.getInventory().firstEmpty(), removePrice(mountItem));
    			if (isSaddle(mountItem)) {
    				pl.msg("&7You are now the proud owner of a mount -- &nto summon your new mount, simply right click with the saddle in your player's hand.");
    				DataHandler.getInstance().setMountTier(p, 2);
    				buyingMount.remove(p);
    				return;
    			}
    			DataHandler.getInstance().setMountTier(p, Mob.getItemTier(removePrice(mountItem)));
    			buyingMount.remove(p);
    		} else {  		
    			buyingMount.remove(p);
    			return;
    		}
    	}
    }
    
    private void takeGems(Player p, int cost) {
    	BankHandler.subtractMoney(p, cost);
	}
    
    public boolean canBuyMount(Player player, int tier) {
    	int t = DataHandler.getInstance().getMountTier(player);
    	if (t < 2) return true;
    	if ((t == (tier - 1))) return true;
		return false;
    }
    
	private boolean hasGems(Player p, int cost) {
		return BankHandler.doTheyHaveEnoughMoney(p, cost);
	}
	
	public boolean isHorseArmor(ItemStack is) {
		return (is.getType().toString().contains("BARDING"));
	}
	
	public boolean isSaddle(ItemStack is) {
		return (is.getType() == Material.SADDLE);
	}
	
	public ItemStack getMountFromTier(int tier) {
		if (tier == 2) {
			return MountHandler.t2_horse_mount;
		}
		if (tier == 3) {
			return MountHandler.t3_horse_mount;
		}
		if (tier == 4) {
			return MountHandler.t4_horse_mount;
		}
		if (tier == 5) {
			return MountHandler.t5_horse_mount;
		}
		return null;
	}
	
	
	@EventHandler
	public void onRightClick(PlayerInteractEvent e) {
		if ((e.getAction() == Action.RIGHT_CLICK_AIR) || (e.getAction() == Action.RIGHT_CLICK_BLOCK)) {
			Player p = e.getPlayer();
			if (isSaddle(p.getItemInHand())) {
				e.setCancelled(true);
				if (MountUtils.summon_mount.containsKey(p.getName())) return;
				MountUtils.mounting_location.put(p.getName(), p.getLocation());
				MountUtils.saddle_map.put(p.getName(), p.getItemInHand());
				MountUtils.summon_mount.put(p.getName(), 6);
				return;
				
			}
		}
	}
	
	
	@EventHandler
	public void onHorseArmorClickOnMount(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
		if (e.getSlotType().equals(SlotType.OUTSIDE)) {
			return;
		}
		if ((e.getClickedInventory().getName().contains("Bank"))) {
			return;
		}
		if (e.getClickedInventory().getHolder().equals(p.getInventory().getHolder())) {
			ItemStack armor = e.getCursor();
			ItemStack saddle = e.getCurrentItem();
			if ((isHorseArmor(armor)) && (isSaddle(saddle))) {
				int armor_tier = Mob.getItemTier(armor);
				int saddle_tier = Mob.getItemTier(saddle);
				if ((saddle_tier == 2) && (armor_tier == 3)) {
					e.setCancelled(true);
					e.setCurrentItem(getMountFromTier(armor_tier));
					e.setCursor(new ItemStack(Material.AIR));
					pl.playSound(Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 1.0F);
					p.updateInventory();
					return;
				} else if ((saddle_tier == 3) && (armor_tier == 4)) {
					e.setCancelled(true);
					e.setCurrentItem(getMountFromTier(armor_tier));
					e.setCursor(new ItemStack(Material.AIR));
					pl.playSound(Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 1.0F);
					p.updateInventory();
					return;
				} else if ((saddle_tier == 4) && (armor_tier == 5)) {
					e.setCancelled(true);
					e.setCurrentItem(getMountFromTier(armor_tier));
					e.setCursor(new ItemStack(Material.AIR));
					pl.playSound(Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 1.0F);
					p.updateInventory();
					return;
				} else {
					pl.msg("&cYou &ncannot&c apply this tier of armor to this tier horse.");
					e.setCancelled(true);
					return;
				}
			}
		}
	}


	@EventHandler
    public void onClick(InventoryClickEvent e) {
    	Player p = (Player) e.getWhoClicked();
    	PracticeServerPlayer pl = PracticeServerPlayer.getPracticeServerPlayer(p);
    	if (e.getCurrentItem() == ItemMechanics.glass) e.setCancelled(true);
    	if (e.getInventory().getTitle().equalsIgnoreCase("Animal Tamer")) {
    		e.setCancelled(true);
    		if (!inMenu.contains(p)) {
    			p.closeInventory();
    			return;
    		}
    		if (e.getCurrentItem().equals(MountType.T2_MOUNT.getItemPriced())) {
    			if (!canBuyMount(p, Mob.getItemTier(e.getCurrentItem()))) {
    				p.closeInventory();
    				return;
    			}
    			if (p.getInventory().contains(MountType.T2_MOUNT.getItem())) {
    				p.closeInventory();
    				return;
    			}
    			if (p.getInventory().contains(MountHandler.t3_horse_armor)) {
    				p.closeInventory();
    				return;
    			}
    			buyingMount.put(p, MountType.T2_MOUNT);
    			pl.msg("&7The '" + MountType.T2_MOUNT.getItem().getItemMeta().getDisplayName() + "' &7costs &a&l" + MountType.T2_MOUNT.getCost() + " GEM(s)&7.");
    			pl.msg("&7This item is non-refundable. Type &a&lY&7 to confirm.");
    			p.closeInventory();	
    			return;
    		}
    		if (e.getCurrentItem().equals(MountType.T3_HORSE.getItemPriced())) {
    			if (!canBuyMount(p, Mob.getItemTier(e.getCurrentItem()))) {
    				p.closeInventory();
    				return;
    			}
    			if (p.getInventory().contains(MountType.T3_HORSE.getItem())) {
    				p.closeInventory();
    				return;
    			}
    			if (p.getInventory().contains(MountHandler.t3_horse_armor)) {
    				p.closeInventory();
    				return;
    			}
    			buyingMount.put(p, MountType.T3_HORSE);
    			pl.msg("&7The '" + MountType.T3_HORSE.getItem().getItemMeta().getDisplayName() + "' &7costs &a&l" + MountType.T3_HORSE.getCost() + " GEM(s)&7.");
    			pl.msg("&7This item is non-refundable. Type &a&lY&7 to confirm.");
    			p.closeInventory();	
    			return;
    		}
    		if (e.getCurrentItem().equals(MountType.T4_HORSE.getItemPriced())) {
    			if (!canBuyMount(p, Mob.getItemTier(e.getCurrentItem()))) {
    				p.closeInventory();
    				return;
    			}
    			if (p.getInventory().contains(MountType.T4_HORSE.getItem())) {
    				p.closeInventory();
    				return;
    			}
    			if (p.getInventory().contains(MountHandler.t4_horse_armor)) {
    				p.closeInventory();
    				return;
    			}
    			buyingMount.put(p, MountType.T4_HORSE);
    			pl.msg("&7The '" + MountType.T4_HORSE.getItem().getItemMeta().getDisplayName() + "' &7costs &a&l" + MountType.T4_HORSE.getCost() + " GEM(s)&7.");
    			pl.msg("&7This item is non-refundable. Type &a&lY&7 to confirm.");
    			p.closeInventory();	
    			return;
    		}
    		if (e.getCurrentItem().equals(MountType.T5_HORSE.getItemPriced())) {
    			if (!canBuyMount(p, Mob.getItemTier(e.getCurrentItem()))) {
    				p.closeInventory();
    				return;
    			}
    			if (p.getInventory().contains(MountType.T5_HORSE.getItem())) {
    				p.closeInventory();
    				return;
    			}
    			if (p.getInventory().contains(MountHandler.t5_horse_armor)) {
    				p.closeInventory();
    				return;
    			}
    			buyingMount.put(p, MountType.T5_HORSE);
    			pl.msg("&7The '" + MountType.T5_HORSE.getItem().getItemMeta().getDisplayName() + "' &7costs &a&l" + MountType.T5_HORSE.getCost() + " GEM(s)&7.");
    			pl.msg("&7This item is non-refundable. Type &a&lY&7 to confirm.");
    			p.closeInventory();	
    			return;
    		}
    	}
    }

	public void setupInventory(Player player) {
		Inventory inv = Bukkit.createInventory(null, 9, "Animal Tamer");
		inv.setItem(0, MountType.T2_MOUNT.getItemPriced());
		inv.setItem(1, MountType.T3_HORSE.getItemPriced());
		inv.setItem(2, MountType.T4_HORSE.getItemPriced());
		inv.setItem(3, MountType.T5_HORSE.getItemPriced());
		inv.setItem(4, ItemMechanics.glass);
		inv.setItem(5, ItemMechanics.glass);
		inv.setItem(6, ItemMechanics.glass);
		inv.setItem(7, ItemMechanics.glass);
		inv.setItem(8, ItemMechanics.glass);
		inMenu.add(player);
		player.openInventory(inv);
	}
}

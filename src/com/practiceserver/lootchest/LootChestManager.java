package com.practiceserver.lootchest;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.practiceserver.Main;
import com.practiceserver.database.Config;
import com.practiceserver.database.ConnectionPool;
import com.practiceserver.events.PlayerChatEvent;

public class LootChestManager implements Listener{

	public static HashMap<Location, LootChest> loot_chests = new HashMap<Location, LootChest>();
	private static HashMap<String, Double> item_chances = new HashMap<String, Double>();
	private HashMap<Player, Integer> lootchest_create_step = new HashMap<Player, Integer>();
	private HashMap<Player, Location> lootchest_create_loc = new HashMap<Player, Location>();
	private HashMap<Player, String> lootchest_create_loot = new HashMap<Player, String>();
	private HashMap<Player, Location> opened_chest = new HashMap<Player, Location>();


	private List<String> stringToLootList(String lootString) {
		lootString.replace(".", ",");
		String[] lootArray = lootString.split(",");
		List<String> output = new ArrayList<String>();
		for (String s : lootArray) {
			output.add(s);
		}
		return output;
	}

	private void loadAllLootChests() {
		PreparedStatement pst = null;

		try {
			pst = ConnectionPool
					.getConnection()
					.prepareStatement(
							"SELECT loc, loot, delay FROM lootchest order by loc");

			pst.execute();
			ResultSet rs = pst.getResultSet();

			while (rs.next()){			
				String s = rs.getString("loc").replace(".", ",");
				String[] sSplit = s.split(",");
				Location l = Bukkit.getServer().getWorld(Config.getWorld()).getBlockAt(Integer.parseInt(sSplit[0]), Integer.parseInt(sSplit[1]), Integer.parseInt(sSplit[2])).getLocation();
				String loot = rs.getString("loot");
				int delay = rs.getInt("delay");
				loot_chests.put(l, new LootChest(l, this.stringToLootList(loot), delay));
			}

		}catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (pst != null) {
					try {
						pst.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			} finally {

			}
		}
		Bukkit.getLogger().log(Level.INFO, "LootChest Loaded!");
	}
	
	private void loadItemChances() {
		PreparedStatement pst = null;

		try {
			pst = ConnectionPool
					.getConnection()
					.prepareStatement(
							"SELECT name, chance FROM item_chances order by name");

			pst.execute();
			ResultSet rs = pst.getResultSet();

			while (rs.next()){			
				String name = rs.getString("name");
				double chance = rs.getDouble("chance");
				item_chances.put(name, chance);
			}

		}catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (pst != null) {
					try {
						pst.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			} finally {

			}
		}
		Bukkit.getLogger().log(Level.INFO, "Item Chances Loaded!");
	}
	
	public static HashMap<String, Double> getItemChances() {
		return item_chances;
	}

	public void setup() {	
		this.loadAllLootChests();
		this.loadItemChances();
		Random rand = new Random();
		for (LootChest s : loot_chests.values()) {
			Main.plugin.getServer().getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {	
				public void run() {					
					if (rand.nextDouble() < 0.50) {
						s.spawn();
					}
				}
			}, rand.nextInt(500), (s.getDelay()*20));
		}
	}

	@EventHandler
	public void onLootChestPlace(BlockPlaceEvent e) {
		if (e.getPlayer().isOp()) {
			if (e.getBlock().getType().equals(Material.BEACON)) {
				e.setCancelled(true);
				e.getPlayer().sendMessage(ChatColor.GREEN.toString() + ChatColor.BOLD.toString() + "Loot Chest Creation Started!");
				e.getPlayer().sendMessage(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD.toString() + "Please Enter the Types of Loot!");
				e.getPlayer().sendMessage(ChatColor.GRAY.toString() + "Loot Format: \"orb, gemnote:100-150, gems:10-14\"");
				e.getPlayer().sendMessage(ChatColor.GRAY.toString() + "To Cancel Type \"cancel\"");
				lootchest_create_step.put(e.getPlayer(), 1);
				lootchest_create_loc.put(e.getPlayer(), e.getBlock().getLocation());
			}
		} else {
			e.getPlayer().getInventory().remove(e.getItemInHand());
			e.getPlayer().sendMessage(ChatColor.RED.toString() + "You Are Not Permited to Have That Item!");
		}
	}

	@EventHandler
	public void onLootChestBreak(BlockBreakEvent e) {
		if (e.getPlayer().isOp()) {
			if (e.getBlock().getType().equals(Material.BEACON)) {
				if (loot_chests.containsKey(e.getBlock().getLocation())) {
					loot_chests.get(e.getBlock().getLocation()).delete();
					loot_chests.remove(e.getBlock().getLocation());
					e.getPlayer().sendMessage(ChatColor.RED.toString() + ChatColor.BOLD.toString() + "LootChest Removed!");
				}
			}
		}
	}

	@EventHandler
	public void onSpawnerInfo(PlayerInteractEvent e) {
		if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			if (e.getClickedBlock().getType().equals(Material.BEACON)) {
				if (loot_chests.containsKey(e.getClickedBlock().getLocation())) {
					e.getPlayer().sendMessage(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD.toString() + ChatColor.UNDERLINE.toString() + "Spawner Info");
					e.getPlayer().sendMessage(ChatColor.DARK_GRAY.toString() + "Type: " + ChatColor.GRAY.toString() + loot_chests.get(e.getClickedBlock().getLocation()).getLoot().toString());
					e.getPlayer().sendMessage(ChatColor.DARK_GRAY.toString() + "Delay: " + ChatColor.GRAY.toString() + loot_chests.get(e.getClickedBlock().getLocation()).getDelay());
				}
			}
		}
	}

	@EventHandler
	public void onChatEvent(PlayerChatEvent e) {
		if (lootchest_create_step.containsKey(e.getPlayer())) {
			e.setCancelled(true);
			if (lootchest_create_step.get(e.getPlayer()) == 1) {
				String msg = e.getMessage();
				if (msg.equalsIgnoreCase("cancel")) {
					lootchest_create_step.remove(e.getPlayer());
					lootchest_create_loc.remove(e.getPlayer());
					e.getPlayer().sendMessage(ChatColor.RED.toString() + ChatColor.BOLD.toString() + "LootChest Creation CANCELLED!");
					return;
				}
				lootchest_create_loot.put(e.getPlayer(), msg);
				e.getPlayer().sendMessage(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD.toString() +  "Please Enter the Spawn Delay!");
				e.getPlayer().sendMessage(ChatColor.GRAY.toString() + "Do NOT use Decimals! EX: (30) IS IN SECONDS!");
				e.getPlayer().sendMessage(ChatColor.GRAY.toString() + "To Cancel Type \"cancel\"");
				lootchest_create_step.put(e.getPlayer(), 2);
			}
			else if (lootchest_create_step.get(e.getPlayer()) == 2) {
				String msg = e.getMessage();
				if (msg.equalsIgnoreCase("cancel")) {
					lootchest_create_step.remove(e.getPlayer());
					lootchest_create_loc.remove(e.getPlayer());
					lootchest_create_loot.remove(e.getPlayer());
					e.getPlayer().sendMessage(ChatColor.RED.toString() + ChatColor.BOLD.toString() + "LootChest Creation CANCELLED!");
					return;
				}
				int delay = Integer.parseInt(msg);
				e.getPlayer().sendMessage(ChatColor.GREEN.toString() + ChatColor.BOLD.toString() + "LootChest Created!");
				loot_chests.put(lootchest_create_loc.get(e.getPlayer()), new LootChest(lootchest_create_loc.get(e.getPlayer()), this.stringToLootList(lootchest_create_loot.get(e.getPlayer())), delay));
				loot_chests.get(lootchest_create_loc.get(e.getPlayer())).save();
				lootchest_create_step.remove(e.getPlayer());
				lootchest_create_loc.remove(e.getPlayer());
				lootchest_create_loot.remove(e.getPlayer());
			}
		}
	}

	@EventHandler
	public void breakchest(PlayerInteractEvent e) {
		if (e.getAction() == Action.LEFT_CLICK_BLOCK){
			if(e.getClickedBlock().getType() == Material.CHEST){
				if (loot_chests.containsKey(e.getClickedBlock().getLocation())){
					e.getClickedBlock().breakNaturally();
					e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_ZOMBIE_BREAK_DOOR_WOOD, 0.5F, 1.2F);

				}
			}
		}
	}
	
	@EventHandler
	public void onChestOpen(PlayerInteractEvent e) {
		if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			if (e.getClickedBlock().getType().equals(Material.CHEST)) {
				if (loot_chests.containsKey(e.getClickedBlock().getLocation())) {
					opened_chest.put(e.getPlayer(), e.getClickedBlock().getLocation());
				}
			}
		}
	}
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		Player p = (Player) e.getPlayer();
		if (opened_chest.containsKey(p)) {
			if (e.getInventory().getHolder() instanceof Chest) {
				int items_left = 0;
				for(ItemStack i : e.getInventory().getContents()) {
					if(i != null && i.getType() != Material.AIR) {
						items_left++;
					}
					if(items_left <= 0) {
						Location loc = opened_chest.get(p);
						loc.getBlock().breakNaturally();
						p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_BREAK_DOOR_WOOD, 0.5F, 1.2F);
					}
				}
			}
			opened_chest.remove(e.getPlayer());
		}
	}

	
	
}

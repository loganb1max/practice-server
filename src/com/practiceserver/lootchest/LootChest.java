package com.practiceserver.lootchest;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.practiceserver.banks.MoneyUtils;
import com.practiceserver.database.ConnectionPool;

public class LootChest {

	private int tier;
	private Location loc;
	private List<String> loot_table;
	private int delay;
	private MoneyUtils mu = new MoneyUtils();
	
	
	
	public LootChest(Location loc, List<String> loot, int delay) {
		this.loc = loc;
		this.loot_table = loot;
		this.delay = delay;
	}
	
	public int getTier() {
		return tier;
	}
	public void setTier(int tier) {
		this.tier = tier;
	}
	public Location getLoc() {
		return loc;
	}
	public void setLoc(Location loc) {
		this.loc = loc;
	}
	public List<String> getLoot() {
		return loot_table;
	}
	public int getDelay() {
		return delay;
	}
	public void setDelay(int delay) {
		this.delay = delay;
	}
	private String locToString() {
		return this.getLoc().getBlockX() + "." + this.getLoc().getBlockY() + "." + this.getLoc().getBlockZ();
	}
	private String lootTableToString() {
		String output = this.loot_table.get(0);
		for (int i = 1; i < this.loot_table.size(); i++) {
			output+= "." + this.loot_table.get(i);
		}
		return output;
	}
	
	public void spawn() {
		Random rand = new Random();
		this.getLoc().getBlock().setType(Material.CHEST);
		Chest chest = (Chest) this.getLoc().getBlock();
		List<ItemStack> items = new ArrayList<ItemStack>();
		for (String s : loot_table) {
			if (rand.nextDouble() <= LootChestManager.getItemChances().get(s.toLowerCase())) {
				if (s.equalsIgnoreCase("orb")) {
					
				}
				if (s.contains("gem:")) {
					String amt = s.split(":")[1];
					int min = Integer.parseInt(amt.split("-")[0]);
					int max = Integer.parseInt(amt.split("-")[1]);
					int famt = rand.nextInt(max-min)+min;
					int numofstacks = famt/64;
					while (famt < 0) {
						if (numofstacks > 0) {
							ItemStack gem = new ItemStack(Material.EMERALD, 64);
				            ItemMeta im = gem.getItemMeta();
				            im.setDisplayName(ChatColor.WHITE + "Gem");
				            im.setLore(Arrays.asList(new String[] {ChatColor.GRAY + "The currency of Andalucia" }));
				            gem.setItemMeta(im);
				            items.add(gem);
						} else {
							ItemStack gem = new ItemStack(Material.EMERALD, famt);
				            ItemMeta im = gem.getItemMeta();
				            im.setDisplayName(ChatColor.WHITE + "Gem");
				            im.setLore(Arrays.asList(new String[] {ChatColor.GRAY + "The currency of Andalucia" }));
				            gem.setItemMeta(im);
				            items.add(gem);
						}
					}
				}
				if (s.contains("gemnote:")) {
					String amt = s.split(":")[1];
					int min = Integer.parseInt(amt.split("-")[0]);
					int max = Integer.parseInt(amt.split("-")[1]);
					mu.createBankNote(rand.nextInt(max-min)+min);
				}
			}
		}
		for (ItemStack i : items) {
			chest.getInventory().addItem(i);
		}
	}
	
	public void save() {
		String loc = locToString();
		String loot = this.lootTableToString();
		int delay = this.getDelay();

		PreparedStatement pst = null;
		try{
			pst = ConnectionPool.getConnection().prepareStatement("INSERT INTO lootchest "
					+ "(loc, loot, delay) "
					+ "VALUES('" + loc + "', '" + loot + "', '" + delay + "') "
					+ "ON DUPLICATE KEY UPDATE loc='" + loc + "', loot='" + loot + "', delay='" + delay + "'");
			pst.executeUpdate();	
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
		}
		Bukkit.getLogger().log(Level.INFO, "LootChest @ " + loc + " Saved!");
	}
	public void delete() {
		PreparedStatement pst = null;
		try{
			pst = ConnectionPool.getConnection().prepareStatement("DELETE FROM `prserver`.`lootchest` WHERE `lootchest`.`loc` = \'" + locToString() + "\'");
			pst.executeUpdate();	
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
		}
		Bukkit.getLogger().log(Level.INFO, "LootChest @ " + locToString() + " Removed!");
	}
	
}
//
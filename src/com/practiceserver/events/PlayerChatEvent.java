package com.practiceserver.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.practiceserver.player.PracticeServerPlayer;

public class PlayerChatEvent extends Event implements Cancellable {

	private static final HandlerList handlers = new HandlerList();
	private Player player;
    private boolean cancelled;
    private PracticeServerPlayer practiceServerPlayer;
    private String msg;
    
    public PlayerChatEvent(Player player, String msg) {
    	this.player = player;
    	this.msg = msg;
    	this.practiceServerPlayer = PracticeServerPlayer.getPracticeServerPlayer(player);
    }
    
    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }

    public HandlerList getHandlers() {
        return handlers;
    }
    
    public String getMessage() {
    	return msg;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
    
    public Player getPlayer() {
    	return player;
    }
     
	public PracticeServerPlayer getPracticeServerPlayer() {
		return practiceServerPlayer;
	}
}



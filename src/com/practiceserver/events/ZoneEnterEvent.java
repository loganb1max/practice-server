package com.practiceserver.events;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.practiceserver.enums.Zone;
import com.practiceserver.player.PracticeServerPlayer;

public class ZoneEnterEvent extends Event implements Cancellable {
	
	private static final HandlerList handlers = new HandlerList();
	private Location newLocation;
	private Location oldLocation;
	private Player player;
	private Zone newZone;
	private Zone oldZone;
    private boolean cancelled;
    private PracticeServerPlayer practiceServerPlayer;
    
    public ZoneEnterEvent(Player player, Location newLocation, Location oldLocation, Zone newZone, Zone oldZone) {
    	this.player = player;
    	this.newLocation = newLocation;
    	this.oldLocation = oldLocation;
    	this.newZone = newZone;
    	this.oldZone = oldZone;
    	this.practiceServerPlayer = PracticeServerPlayer.getPracticeServerPlayer(player);
    }
    
    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }
//
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
    
    public Player getPlayer() {
    	return player;
    }
    
    public Location getNewLocation() {
    	return newLocation;
    }
    
    public Location getOldLocation() {
    	return oldLocation;
    }
    
    public Zone getNewZone() {
    	return newZone;
    }
    
    public Zone getOldZone() {
    	return oldZone;
    }
    
	public PracticeServerPlayer getPracticeServerPlayer() {
		return practiceServerPlayer;
	}
	

}

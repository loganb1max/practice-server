package com.practiceserver.events;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.practiceserver.player.PracticeServerPlayer;

public class PlayerKilledByMobEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	private PracticeServerPlayer practiceServerPlayer;
	private Player player;
	private LivingEntity killer;
	private String message;
	private Entity entityKiller;
	
	public PlayerKilledByMobEvent(Player player, Entity killer) {
		this.player = player;
		this.entityKiller = killer;
		this.practiceServerPlayer = PracticeServerPlayer.getPracticeServerPlayer(player);
		this.message = player.getName() + " has died";
		if (killer instanceof Arrow) {
			Arrow a = (Arrow) killer;
			this.killer = (LivingEntity) a.getShooter();
		}
		if (killer instanceof Projectile) {
			this.killer = (LivingEntity) entityKiller;
		}
	}
	
    public Entity getEntityKiller() {
		return entityKiller;
	}

	public HandlerList getHandlers() {
        return handlers;
    }
//
    public static HandlerList getHandlerList() {
        return handlers;
    }
    
    public PracticeServerPlayer getPracticeServerPlayer() {
		return practiceServerPlayer;
	}
    
    public LivingEntity getKiller() {
    	return killer;
    }
    
    public Player getPlayer() {
    	return player;
    }
    
    public String getMessage() {
    	return message;
    }
    
    public void setMessage(String message) {
    	this.message = message;
    }
    

}

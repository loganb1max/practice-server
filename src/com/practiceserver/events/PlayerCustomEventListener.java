package com.practiceserver.events;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import com.practiceserver.enums.Zone;
import com.practiceserver.utils.RegionUtils;

public class PlayerCustomEventListener implements Listener {
	
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		Location from = e.getFrom();
		Location to = e.getTo();
		if ((to.getX() != from.getX()) || (to.getY() != from.getY()) || (to.getZ() != from.getZ())) {
			Zone zoneFrom = RegionUtils.getZone(from);
            Zone zoneTo = RegionUtils.getZone(to);
            if (!(zoneTo.equals(zoneFrom))) {
            	ZoneEnterEvent zoneEnterEvent = new ZoneEnterEvent(p, to, from, zoneTo, zoneFrom);
            	Bukkit.getPluginManager().callEvent(zoneEnterEvent);
            	if(zoneEnterEvent.isCancelled()) {
            		p.teleport(from);
            		e.setCancelled(true);
            		return;
            	}
            }
		}
	}
	
	@EventHandler
	public void onChat(org.bukkit.event.player.PlayerChatEvent e) {
		Player p = e.getPlayer();
		PlayerChatEvent ee = new PlayerChatEvent(p, e.getMessage());
		Bukkit.getServer().getPluginManager().callEvent(ee);
		if (ee.isCancelled()) {
			e.setCancelled(true);
			return;
		}
	}
	
	@EventHandler
	public void BankerNPC(PlayerInteractEntityEvent e) {
		Player p = e.getPlayer();
		if ((e.getRightClicked() instanceof HumanEntity)) {
			HumanEntity npc = (HumanEntity) e.getRightClicked();
			if(npc.hasMetadata("NPC")) {
				NpcRightClickEvent ee = new NpcRightClickEvent(p, npc.getName());
				if(!ee.isCancelled()) {
					Bukkit.getPluginManager().callEvent(ee);
				} else {
					e.setCancelled(true);
					return;
				}
			}
		}
	}
}
			